//
//  AlertView.m
//  CustomPopUps
//
//  Created by EvgenyMikhaylov on 8/4/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "AlertView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

static CGFloat const kDefaultActionHeight = 50.0f;

@class AlertView;

@interface AlertAction ()

@property (nonatomic) NSString *title;
@property (nonatomic) UIImage *image;
@property (nonatomic, copy) void(^handler)(AlertAction*);

@end

@protocol AlertContentViewDatasource <NSObject>

- (NSArray*)alertContentViewActions;
- (BOOL)alertContentViewShouldAlignActionsHorizontally;

@end

@protocol AlertContentViewDelegate <NSObject>

- (BOOL)alertViewActionSelected:(AlertAction*)action;

@end

@class AlertContentView;

@interface AlertView () <AlertContentViewDatasource, AlertContentViewDelegate, UIViewControllerTransitioningDelegate>

@property (nonatomic) AlertViewStyle style;
@property (nonatomic) AlertContentView *alertContentView;
@property (nonatomic) UIView *backgroundView;
@property (nonatomic) NSMutableArray *mutableActions;
@property (nonatomic) NSLayoutConstraint *alertContentViewBottomContraint;

@end

@class AlertLabel;

@interface AlertContentView : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *messageLabel;
@property (nonatomic) UIView *labelsContainerView;
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) UIFont *actionFont;
@property (nonatomic) UIColor *actionTextColor;
@property (nonatomic) CGFloat actionHeight;
@property (nonatomic) UIEdgeInsets labelsInsets;
@property (nonatomic) UIEdgeInsets buttonsInsets;
@property (nonatomic, weak) id<AlertContentViewDelegate> delegate;
@property (nonatomic, weak) id<AlertContentViewDatasource> datasource;

- (void)reloadData;

@end

@interface AlertCell : UICollectionViewCell

@property (nonatomic) AlertActionViewsPosition viewsPosition;
@property (nonatomic) AlertLabel *label;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIView *containerView;

@end

@implementation AlertAction

+ (instancetype)actionWithTitle:(NSString *)title image:(UIImage*)image handler:(void (^)(AlertAction *action))handler{
    return [[AlertAction alloc] initWithTitle:title image:image handler:handler];
}

- (instancetype)initWithTitle:(NSString *)title image:(UIImage*)image handler:(void (^)(AlertAction *action))handler{
    self = [super init];
    if (self) {
        self.title = title;
        self.image = image;
        self.handler = handler;
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.enabled = YES;
        self.backgroundColor = [UIColor clearColor];
        self.viewsPosition = AlertActionViewsPositionImageViewLabel;
    }
    return self;
}

@end

@implementation AlertView

+ (instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(AlertViewStyle)preferredStyle{
    AlertView *alertView = [[AlertView alloc] initWithTitle:title message:message preferredStyle:preferredStyle];
    return alertView;
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(AlertViewStyle)preferredStyle{
    self = [super init];
    if (self) {
        self.mutableActions = [[NSMutableArray alloc] init];
        self.alertContentViewBottomContraint = nil;
        self.style = preferredStyle;
        
        _title = title;
        _message = message;
        _actionHeight = kDefaultActionHeight;
        _overlayColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        _alertColor = [UIColor whiteColor];

        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.backgroundView];
        [self addConstraints:self.backgroundViewConstraints];
        
        self.alertContentView = [[AlertContentView alloc] init];
        self.alertContentView.datasource = self;
        self.alertContentView.delegate = self;
        self.alertContentView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.alertContentView];
        [self addConstraints:self.alertViewConstraints];
        
        [self updateAlertContentView];
        [self.alertContentView reloadData];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.alertContentView invalidateIntrinsicContentSize];
}

#pragma mark Constraints

- (NSArray*)backgroundViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    NSDictionary *views = @{@"view":self.backgroundView};
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
    return constraints;
}

- (NSArray*)alertViewConstraints{
    UIEdgeInsets insets = self.alertInsets;
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.alertContentView
                                                        attribute:NSLayoutAttributeCenterX
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeCenterX
                                                       multiplier:1.0f
                                                         constant:0.0f]];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.alertContentView
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationLessThanOrEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeHeight
                                                       multiplier:1.0f
                                                         constant:0.0f]];
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.alertContentView
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeWidth
                                                                      multiplier:1.0f
                                                                        constant:-(insets.left + insets.right)];
    widthConstraint.priority = 950;
    [constraints addObject:widthConstraint];
    NSLayoutConstraint *widthLandscapeConstraint = [NSLayoutConstraint constraintWithItem:self.alertContentView
                                                                                attribute:NSLayoutAttributeWidth
                                                                                relatedBy:NSLayoutRelationLessThanOrEqual
                                                                                   toItem:self
                                                                                attribute:NSLayoutAttributeHeight
                                                                               multiplier:1.0f
                                                                                 constant:-(insets.left + insets.right)];
    widthLandscapeConstraint.priority = 960;
    [constraints addObject:widthLandscapeConstraint];

    switch (self.style) {
        case AlertViewStyleAlert:
            [constraints addObject:[NSLayoutConstraint constraintWithItem:self.alertContentView
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self
                                                                attribute:NSLayoutAttributeCenterY
                                                               multiplier:1.0f
                                                                 constant:0.0f]];
            break;
        case AlertViewStyleActionSheet:
            self.alertContentViewBottomContraint = [NSLayoutConstraint constraintWithItem:self.alertContentView
                                                                                attribute:NSLayoutAttributeBottom
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self
                                                                                attribute:NSLayoutAttributeBottom
                                                                               multiplier:1.0f
                                                                                 constant:0.0f];
            [constraints addObject:self.alertContentViewBottomContraint];
            break;
        default:
            break;
    }
    return constraints;
}

- (void)updateAlertContentViewConstraints{
    [self.alertContentView removeFromSuperview];
    [self addSubview:self.alertContentView];
    [self addConstraints:[self alertViewConstraints]];
}

- (void)updateAlertContentView{
    self.alertContentView.titleLabel.text = _title;
    self.alertContentView.messageLabel.text = _message;
    self.alertContentView.titleLabel.font = _titleFont;
    self.alertContentView.titleLabel.textColor = _titleColor;
    self.alertContentView.messageLabel.font = _messageFont;
    self.alertContentView.messageLabel.textColor = _messageColor;
    self.alertContentView.actionFont = _actionsFont;
    self.alertContentView.actionTextColor = _actionsColor;
    self.alertContentView.actionHeight = _actionHeight;
    self.backgroundView.backgroundColor = _overlayColor;
    self.alertContentView.backgroundColor = _alertColor;
    self.alertContentView.labelsInsets = _alertLabelsInsets;
    self.alertContentView.buttonsInsets = _alertButtonsInsets;
}

- (void)show{
    if(!self.superview){
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview:self];
        self.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *views = @{@"view":self};
        [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
        [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
    }
    else{
        [self.superview bringSubviewToFront:self];
    }
    [self.alertContentView setNeedsLayout];
    [self.alertContentView layoutIfNeeded];
    switch (self.style) {
        case AlertViewStyleAlert:{
            self.backgroundView.alpha = 0.0f;
            self.alertContentView.alpha = 0.0f;
            [UIView animateWithDuration:0.4f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.backgroundView.alpha = 1.0f;
                self.alertContentView.alpha = 1.0f;
            } completion:nil];
        }
            break;
        case AlertViewStyleActionSheet:{
            NSLayoutConstraint *animationConstraint = [NSLayoutConstraint constraintWithItem:self.alertContentView
                                                                                   attribute:NSLayoutAttributeTop
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:self
                                                                                   attribute:NSLayoutAttributeBottom
                                                                                  multiplier:1.0f
                                                                                    constant:0.0f];
            [self removeConstraint:self.alertContentViewBottomContraint];
            [self addConstraint:animationConstraint];
            [self setNeedsLayout];
            [self layoutIfNeeded];
            [self removeConstraint:animationConstraint];
            [self addConstraint:self.alertContentViewBottomContraint];
            [self setNeedsLayout];
            self.backgroundView.alpha = 0.0f;
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                 usingSpringWithDamping:0.7f
                  initialSpringVelocity:5.0f
                                options:0
                             animations:^{
                                 self.backgroundView.alpha = 1.0f;
                                 [self layoutIfNeeded];
                             } completion:nil];
        }
            break;
        default:
            break;
    }
}

- (void)dismiss{
    switch (self.style) {
        case AlertViewStyleAlert:{
            [UIView animateWithDuration:0.4f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.backgroundView.alpha = 0.0f;
                self.alertContentView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                self.backgroundView.alpha = 1.0f;
                self.alertContentView.alpha = 1.0f;
                [self removeFromSuperview];
            }];
        }
            break;
        case AlertViewStyleActionSheet:{
            NSLayoutConstraint *animationConstraint = [NSLayoutConstraint constraintWithItem:self.alertContentView
                                                                                   attribute:NSLayoutAttributeTop
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:self
                                                                                   attribute:NSLayoutAttributeBottom
                                                                                  multiplier:1.0f
                                                                                    constant:0.0f];
            CGRect initialAlertContentViewFrame = self.alertContentView.frame;
            [self removeConstraint:self.alertContentViewBottomContraint];
            [self addConstraint:animationConstraint];
            [self setNeedsLayout];
            [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.backgroundView.alpha = 0.0f;
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
                self.backgroundView.alpha = 1.0f;
                self.alertContentView.frame = initialAlertContentViewFrame;
                [self removeFromSuperview];
            }];
        }
            break;
        default:
            break;
    }
}

#pragma mark Setters

- (void)setTitle:(NSString *)title{
    _title = title;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setMessage:(NSString *)message{
    _message = message;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setTitleFont:(UIFont *)titleFont{
    _titleFont = titleFont;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setTitleFont:(UIFont *)titleFont forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.titleFont = titleFont;
}

- (void)setTitleColor:(UIColor *)titleColor{
    _titleColor = titleColor;
    [self updateAlertContentView];
}

- (void)setTitleColor:(UIColor *)titleColor forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.titleColor = titleColor;
}

- (void)setMessageFont:(UIFont *)messageFont{
    _messageFont = messageFont;
    [self updateAlertContentView];
}

- (void)setMessageFont:(UIFont *)messageFont forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.messageFont = messageFont;
}

- (void)setMessageColor:(UIColor *)messageColor{
    _messageColor = messageColor;
    [self updateAlertContentView];
}

- (void)setMessageColor:(UIColor *)messageColor forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.messageColor = messageColor;
}

- (void)setActionsFont:(UIFont *)actionsFont{
    _actionsFont = actionsFont;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setActionsFont:(UIFont *)actionsFont forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.actionsFont = actionsFont;
}

- (void)setActionsColor:(UIColor *)actionsColor{
    _actionsColor = actionsColor;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setActionsColor:(UIColor *)actionsColor forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.actionsColor = actionsColor;
}

- (void)setActionHeight:(CGFloat)actionHeight{
    _actionHeight = actionHeight;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setActionHeight:(CGFloat)actionHeight forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.actionHeight = actionHeight;
}

- (void)setOverlayColor:(UIColor *)overlayColor{
    _overlayColor = overlayColor;
    [self updateAlertContentView];
}

- (void)setOverlayColor:(UIColor *)overlayColor forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.overlayColor = overlayColor;
}

- (void)setAlertColor:(UIColor *)alertColor{
    _alertColor = alertColor;
    [self updateAlertContentView];
}

- (void)setAlertColor:(UIColor *)alertColor forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.alertColor = alertColor;
}

- (void)setAlertInsets:(UIEdgeInsets)alertInsets{
    _alertInsets = alertInsets;
    [self updateAlertContentViewConstraints];
}

- (void)setAlertInsets:(UIEdgeInsets)alertInsets forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.alertInsets = alertInsets;
}

- (void)setAlertLabelsInsets:(UIEdgeInsets)alertLabelsInsets{
    _alertLabelsInsets = alertLabelsInsets;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setAlertLabelsInsets:(UIEdgeInsets)alertLabelsInsets forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.alertLabelsInsets = alertLabelsInsets;
}

- (void)setAlertButtonsInsets:(UIEdgeInsets)alertButtonsInsets{
    _alertButtonsInsets = alertButtonsInsets;
    [self updateAlertContentView];
    [self.alertContentView reloadData];
}

- (void)setAlertButtonsInsets:(UIEdgeInsets)alertButtonsInsets forStyle:(AlertViewStyle)style{
    if (self.style != style)
        return;
    self.alertButtonsInsets = alertButtonsInsets;
}

#pragma mark Getters

- (NSArray *)actions{
    return self.mutableActions.copy;
}

- (RACSignal *)rac_signal
{
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self setHandler:^(AlertAction *action) {
            [subscriber sendNext:action];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

#pragma mark Actions

- (void)addAction:(AlertAction *)action{
    [self.mutableActions addObject:action];
    [self.alertContentView reloadData];
}

- (void)addActions:(NSArray *)actions{
    [actions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self addAction:obj];
    }];
}

#pragma mark AlertContentViewDatasource

- (NSArray *)alertContentViewActions{
    return self.mutableActions;
}

- (BOOL)alertContentViewShouldAlignActionsHorizontally{
    return (self.actions.count == 2) && (self.style == AlertViewStyleAlert);
}

#pragma mark AlertContentViewDelegate

- (BOOL)alertViewActionSelected:(AlertAction *)action{
    [self dismiss];
    if (self.handler){
        self.handler(action);
        return YES;
    }
    else{
        return NO;
    }
}

@end

@implementation AlertContentView

- (instancetype)init{
    self = [super init];
    if (self) {
        
        self.labelsContainerView = [[UIView alloc] init];
        self.labelsContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.labelsContainerView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.labelsContainerView addSubview:self.titleLabel];

        self.messageLabel = [[UILabel alloc] init];
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.labelsContainerView addSubview:self.messageLabel];

        [self updateLabelsConstraints];
        
        UICollectionViewFlowLayout *collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        collectionViewFlowLayout.minimumInteritemSpacing = 0.0f;
        collectionViewFlowLayout.minimumLineSpacing = 0.0f;
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:collectionViewFlowLayout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView registerClass:[AlertCell class] forCellWithReuseIdentifier:NSStringFromClass([AlertCell class])];
        self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.collectionView];
        
        NSDictionary *containers = NSDictionaryOfVariableBindings(_labelsContainerView, _collectionView);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_labelsContainerView]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:containers]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:containers]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_labelsContainerView][_collectionView]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:containers]];
    }
    return self;
}

- (void)updateLabelsConstraints{
    
    [self.titleLabel removeFromSuperview];
    [self.messageLabel removeFromSuperview];
    [self.labelsContainerView addSubview:self.titleLabel];
    [self.labelsContainerView addSubview:self.messageLabel];
    
    UIEdgeInsets titleInsets = UIEdgeInsetsZero;
    UIEdgeInsets messageInsets = UIEdgeInsetsZero;
    if (self.titleLabel.text.length && self.messageLabel.text.length){
        UIEdgeInsets titleInsets = self.labelsInsets;
        titleInsets.bottom = 0.0f;
        messageInsets = self.labelsInsets;
    }
    else if (self.titleLabel.text.length){
        titleInsets = self.labelsInsets;
    }
    else if (self.messageLabel.text.length){
        messageInsets = self.labelsInsets;
    }
    
    NSDictionary *labels = NSDictionaryOfVariableBindings(_titleLabel, _messageLabel);
    NSDictionary *horizontalMetrics = @{
                                        @"left" : @(self.labelsInsets.left),
                                        @"right" : @(self.labelsInsets.right),
                                        };
    NSDictionary *verticalMetrics = @{
                                        @"top" : @(titleInsets.top),
                                        @"middle" : @(titleInsets.bottom + messageInsets.top),
                                        @"bottom" : @(messageInsets.bottom),
                                        };
    [self.labelsContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[_titleLabel]-right-|"
                                                                                     options:0
                                                                                     metrics:horizontalMetrics
                                                                                       views:labels]];
    [self.labelsContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[_messageLabel]-right-|"
                                                                                     options:0
                                                                                     metrics:horizontalMetrics
                                                                                       views:labels]];
    [self.labelsContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-top@1000-[_titleLabel]-middle-[_messageLabel]-bottom@950-|"
                                                                                     options:0
                                                                                     metrics:verticalMetrics
                                                                                       views:labels]];

}

- (void)reloadData{
    [self updateLabelsConstraints];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self.collectionView reloadData];
    [self invalidateIntrinsicContentSize];
}

#pragma mark UICollectionViewDatasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section{
    return [[self.datasource alertContentViewActions] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AlertAction *action = [self.datasource alertContentViewActions][indexPath.item];
    NSString *cellIdentifier = NSStringFromClass([AlertCell class]);
    AlertCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = action.backgroundColor;
    cell.viewsPosition = action.viewsPosition;
    cell.label.text = action.title;
    cell.label.textAlignment = NSTextAlignmentCenter;
    cell.label.font = self.actionFont;
    cell.label.textColor = self.actionTextColor;
    if (action.labelConfigurationBlock){
        action.labelConfigurationBlock(cell.label);
    }
    cell.imageView.image = action.image;
    if (action.imageViewConfigurationBlock){
        action.imageViewConfigurationBlock(cell.imageView);
    }
    return cell;
}

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    AlertAction *action = [self.datasource alertContentViewActions][indexPath.item];
    if (![self.delegate alertViewActionSelected:action]){
        if (action.handler){
            action.handler(action);
        }
    }
}

#pragma mark UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.datasource alertContentViewShouldAlignActionsHorizontally]){
        return CGSizeMake(CGRectGetWidth(collectionView.frame)/[self.datasource alertContentViewActions].count, self.actionHeight);
    } else {
        CGFloat width = CGRectGetWidth(collectionView.frame) - self.buttonsInsets.left - self.buttonsInsets.right;
        return CGSizeMake(width, self.actionHeight);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section{
    return self.buttonsInsets;
}

#pragma mark IntrinsicContentSize

-(CGSize)intrinsicContentSize{
    CGSize size = CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.labelsContainerView.frame) + self.collectionView.contentSize.height);
    return size;
}

@end

@implementation AlertCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.containerView = [[UIView alloc] init];
        self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.containerView];
        
        self.imageView = [[UIImageView alloc] init];
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.containerView addSubview:self.imageView];
        
        self.label = [[AlertLabel alloc] init];
        self.label.translatesAutoresizingMaskIntoConstraints = NO;
        [self.containerView addSubview:self.label];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.containerView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.containerView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        
        self.viewsPosition = AlertActionViewsPositionImageViewLabel;
    }
    return self;
}

- (void)setViewsPosition:(AlertActionViewsPosition)viewsPosition{
    _viewsPosition = viewsPosition;
    [self.label removeFromSuperview];
    [self.imageView removeFromSuperview];
    [self.containerView addSubview:self.label];
    [self.containerView addSubview:self.imageView];
    NSDictionary *views = NSDictionaryOfVariableBindings(_label, _imageView);
    NSString *horizontalVisualFormatString = nil;
    switch (self.viewsPosition) {
        case AlertActionViewsPositionImageViewLabel:
            horizontalVisualFormatString = @"H:|[_imageView][_label]|";
            break;
        case AlertActionViewsPositionLabelImageView:
            horizontalVisualFormatString = @"H:|[_label][_imageView]|";
            break;
    }
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalVisualFormatString
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:views]];
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageView]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:views]];
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_label]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:views]];
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    self.alpha = selected ? 0.5f : 1.0f;
}

- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    self.alpha = highlighted ? 0.5f : 1.0f;
}

@end

@implementation AlertLabel

- (void)setTextInsets:(UIEdgeInsets)textInsets{
    _textInsets = textInsets;
    [self invalidateIntrinsicContentSize];
}

-(void)drawTextInRect:(CGRect)rect{
    CGRect rectWithInsets = UIEdgeInsetsInsetRect(rect, self.textInsets);
    [super drawTextInRect:rectWithInsets];
}

-(CGSize)intrinsicContentSize{
    CGSize size = [super intrinsicContentSize] ;
    size.height += self.textInsets.top + self.textInsets.bottom;
    size.width += self.textInsets.left + self.textInsets.right;
    return size;
}

@end