//
//  AlertView.h
//  CustomPopUps
//
//  Created by EvgenyMikhaylov on 8/4/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AlertViewStyle) {
    AlertViewStyleActionSheet = 0,
    AlertViewStyleAlert
};

typedef NS_ENUM(NSInteger, AlertActionViewsPosition) {
    AlertActionViewsPositionImageViewLabel = 0,
    AlertActionViewsPositionLabelImageView
};

@interface AlertLabel : UILabel

@property (nonatomic) UIEdgeInsets textInsets;

@end

@interface AlertAction : NSObject

+ (instancetype)actionWithTitle:(NSString *)title image:(UIImage*)image handler:(void (^)(AlertAction *action))handler;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic) BOOL enabled;
@property (nonatomic) AlertActionViewsPosition viewsPosition;
@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic, copy) void(^labelConfigurationBlock)(AlertLabel *label);
@property (nonatomic, copy) void(^imageViewConfigurationBlock)(UIImageView *imageView);

@end

@class RACSignal;

@interface AlertView : UIView

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *message;
@property (nonatomic, readonly) NSArray *actions;
@property (nonatomic, copy) void(^handler)(AlertAction*);
@property (nonatomic, readonly) RACSignal *rac_signal;

@property (nonatomic) UIFont *titleFont UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *titleColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIFont *messageFont UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *messageColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIFont *actionsFont UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *actionsColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat actionHeight UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *overlayColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *alertColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIEdgeInsets alertInsets UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIEdgeInsets alertLabelsInsets UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIEdgeInsets alertButtonsInsets UI_APPEARANCE_SELECTOR;

+ (instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(AlertViewStyle)preferredStyle;
- (void)addAction:(AlertAction *)action;
- (void)addActions:(NSArray *)actions;
- (void)show;
- (void)dismiss;

- (void)setTitleFont:(UIFont *)titleFont forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setTitleColor:(UIColor *)titleColor forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setMessageFont:(UIFont *)messageFont forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setMessageColor:(UIColor *)messageColor forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setActionsFont:(UIFont *)messageFont forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setActionsColor:(UIColor *)messageColor forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setActionHeight:(CGFloat)actionHeight forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setOverlayColor:(UIColor *)overlayColor forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setAlertColor:(UIColor *)alertColor forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setAlertInsets:(UIEdgeInsets)alertInsets forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setAlertLabelsInsets:(UIEdgeInsets)alertLabelsInsets forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;
- (void)setAlertButtonsInsets:(UIEdgeInsets)alertButtonsInsets forStyle:(AlertViewStyle)style UI_APPEARANCE_SELECTOR;

@end
