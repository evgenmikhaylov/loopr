//
//  ActivityIndicator.m
//
//  Created by EvgenyMikhaylov on 9/1/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "ActivityIndicator.h"
#import "ArrayView.h"
#import "CircleActivityView.h"
#import "CircleProgressView.h"

static const CGSize kIndicatorDefaultSize = {100.0f,100.0f};
static const UIOffset kIndicatorDefaultOffset = {0.f, 0.f};
static const CGFloat kIndicatorMaxWidth = 250.0f;
static const CGFloat kIndicatorCornerRadius = 5.0f;
static const CGFloat kIndicatorCircleRadius = 24.0f;
static const CGFloat kIndicatorCircleThickness = 2.0f;
static const CGFloat kIndicatorDefaultMargin = 10.0f;
static const NSTimeInterval kDefaultDelay = 2.0f;

@interface ActivityIndicator ()

@property (nonatomic) BOOL ignoreInteractionEvents UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *backroundViewColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIFont *font UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIImage *successImage UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIImage *errorImage UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *indicatorColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGSize indicatorSize UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIOffset indicatorOffset UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat indicatorCornerRadius UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat indicatorCircleRadius UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat indicatorCircleThickness UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *indicatorCircleForegroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *text;
@property (nonatomic) NSTimer *timer;

@property (nonatomic) UIView *contentView;
@property (nonatomic) UIView *backgroundView;
@property (nonatomic) UIView *activityView;
@property (nonatomic) UILabel *label;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) CircleActivityView *circleActivityView;
@property (nonatomic) CircleProgressView *circleProgressView;
@property (nonatomic) UIView *statusView;
@property (nonatomic) NSLayoutConstraint *contentViewBottomConstraint;

@end

@implementation ActivityIndicator

@synthesize successImage = _successImage;
@synthesize errorImage = _errorImage;

+ (instancetype)indicatorForView:(UIView*)view{
    ActivityIndicator *activityIndicator = [[self activityIndicatorsInView:view] firstObject];
    if (activityIndicator)
        return activityIndicator;
    return [[self alloc] init];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _backroundViewColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
        _indicatorColor = [UIColor whiteColor];
        _indicatorSize = kIndicatorDefaultSize;
        _indicatorOffset = kIndicatorDefaultOffset;
        _indicatorCornerRadius = kIndicatorCornerRadius;
        _indicatorCircleRadius = kIndicatorCircleRadius;
        _indicatorCircleThickness = kIndicatorCircleThickness;
        _indicatorCircleForegroundColor = [UIColor blackColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

#pragma mark - Getters

- (UIImage *)successImage{
    if (!_successImage){
        NSURL *url = [[NSBundle bundleForClass:self.class] URLForResource:@"ActivityIndicator" withExtension:@"bundle"];
        NSString *path = [[NSBundle bundleWithURL:url] pathForResource:@"success" ofType:@"png"];
        _successImage = [UIImage imageWithContentsOfFile:path];
    }
    return _successImage;
}

- (UIImage *)errorImage{
    if (!_errorImage){
        NSURL *url = [[NSBundle bundleForClass:self.class] URLForResource:@"ActivityIndicator" withExtension:@"bundle"];
        NSString *path = [[NSBundle bundleWithURL:url] pathForResource:@"error" ofType:@"png"];
        _errorImage = [UIImage imageWithContentsOfFile:path];
    }
    return _errorImage;
}

- (CircleActivityView *)circleActivityView{
    if (!_circleActivityView){
        _circleActivityView = [[CircleActivityView alloc] init];
        _circleActivityView.radius = self.indicatorCircleRadius;
        [_circleActivityView startAnimating];
        [_circleActivityView addConstraints:self.circleActivityViewConstraints];
    }
    _circleActivityView.strokeColor = self.indicatorCircleForegroundColor;
    _circleActivityView.strokeThickness = self.indicatorCircleThickness;
    return _circleActivityView;
}

- (CircleProgressView *)circleProgressView{
    if (!_circleProgressView){
        _circleProgressView = [[CircleProgressView alloc] init];
        _circleProgressView.radius = kIndicatorCircleRadius;
        [_circleProgressView addConstraints:self.circleProgressViewConstraints];
    }
    _circleProgressView.foregroundStrokeColor = self.indicatorCircleForegroundColor;
    _circleProgressView.backgroundStrokeColor = [self.indicatorCircleForegroundColor colorWithAlphaComponent:0.2f];
    _circleProgressView.strokeThickness = self.indicatorCircleThickness;
    return _circleProgressView;
}

- (UIImageView *)imageView{
    if (!_imageView){
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeCenter;
        [_imageView addConstraints:self.imageViewConstraints];
    }
    return _imageView;
}

- (UILabel *)label{
    if (!_label){
        _label = [[UILabel alloc] init];
        _label.numberOfLines = 0;
        _label.textAlignment = NSTextAlignmentCenter;
    }
    if (self.font){
        _label.font = self.font;
    }
    return _label;
}

#pragma mark - Views

- (void)createCommonViewsWithContentViews:(NSArray*)contentViews{
    
    if (!self.contentView){
        self.contentView = [[UIView alloc] init];
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.contentView];
        [self addConstraints:self.contentViewConstraints];
    }
    
    if (!self.backgroundView){
        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.backgroundView];
        [self addConstraints:self.backgroundViewConstraints];
    }
    self.backgroundView.backgroundColor = self.backroundViewColor;
    
    if (self.activityView){
        [self.activityView removeFromSuperview];
        self.activityView = nil;
    }
    self.activityView = [ArrayView arrayViewWithViews:contentViews
                                              spacing:0
                                                 axis:UILayoutConstraintAxisVertical
                                            alignment:ArrayViewAlignmentCenter];
    self.activityView.layoutMargins = UIEdgeInsetsMake(kIndicatorDefaultMargin, kIndicatorDefaultMargin, kIndicatorDefaultMargin, kIndicatorDefaultMargin);
    self.activityView.backgroundColor = self.indicatorColor;
    self.activityView.layer.cornerRadius = self.indicatorCornerRadius;
    self.activityView.clipsToBounds = YES;
    self.activityView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.activityView];
    [self.contentView addConstraints:self.activityViewConstraints];
}

- (void)createStatusView{
    self.statusView = [[UIView alloc] init];
    [self.activityView addSubview:self.statusView];
    [self.activityView addConstraints:self.statusViewConstraints];
}

- (void)removeViews{
    [self.backgroundView removeFromSuperview];
    self.backgroundView = nil;
    [self.activityView removeFromSuperview];
    self.activityView = nil;
    [self.circleActivityView removeFromSuperview];
    self.circleActivityView = nil;
}

+ (UIWindow*)keyWindow{
    return [[UIApplication sharedApplication] keyWindow];
}

+ (NSArray*)activityIndicatorsInView:(UIView*)view{
    NSArray *activityIndicators = [view.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class = %@", self]];
    return activityIndicators;
}

+ (BOOL)viewContainsActivityIndicator:(UIView*)view{
    NSArray *activityIndicators = [self activityIndicatorsInView:view];
    return activityIndicators.count > 0;
}


#pragma mark - Constraints

- (NSArray*)contentViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0f
                                                         constant:0.0f]];
    self.contentViewBottomConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                    attribute:NSLayoutAttributeBottom
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self
                                                                    attribute:NSLayoutAttributeBottom
                                                                   multiplier:1.0f
                                                                     constant:-self.visibleKeyboardHeight];
    [constraints addObject:self.contentViewBottomConstraint];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                        attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeft
                                                       multiplier:1.0f
                                                         constant:0.0f]];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                        attribute:NSLayoutAttributeRight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeRight
                                                       multiplier:1.0f
                                                         constant:0.0f]];
    return constraints;
}

- (NSArray*)backgroundViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    NSDictionary *views = @{@"view":self.backgroundView};
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
    return constraints;
}

- (NSArray*)activityViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.activityView
                                                        attribute:NSLayoutAttributeCenterX
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.contentView
                                                        attribute:NSLayoutAttributeCenterX
                                                       multiplier:1.0f
                                                         constant:self.indicatorOffset.horizontal]];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.activityView
                                                        attribute:NSLayoutAttributeCenterY
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.contentView
                                                        attribute:NSLayoutAttributeCenterY
                                                       multiplier:1.0f
                                                         constant:self.indicatorOffset.vertical]];
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self.activityView
                                                        attribute:NSLayoutAttributeWidth
                                                        relatedBy:NSLayoutRelationLessThanOrEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0f
                                                         constant:kIndicatorMaxWidth]];
    return constraints;
}

- (NSArray*)circleActivityViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    NSDictionary *views = @{@"view":self.circleActivityView};
    NSDictionary *metrics = @{@"width":@(self.indicatorSize.width - 2*kIndicatorDefaultMargin),@"height":@(self.indicatorSize.height - 2*kIndicatorDefaultMargin)};
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(width)]" options:0 metrics:metrics views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]" options:0 metrics:metrics views:views]];
    return constraints;
}

- (NSArray*)circleProgressViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    NSDictionary *views = @{@"view":self.circleProgressView};
    NSDictionary *metrics = @{@"width":@(self.indicatorSize.width - 2*kIndicatorDefaultMargin),@"height":@(self.indicatorSize.height - 2*kIndicatorDefaultMargin)};
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(width)]" options:0 metrics:metrics views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]" options:0 metrics:metrics views:views]];
    return constraints;
}

- (NSArray*)imageViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    NSDictionary *views = @{@"view":self.imageView};
    NSDictionary *metrics = @{@"width":@(self.indicatorSize.width - 2*kIndicatorDefaultMargin),@"height":@(self.indicatorSize.height - 2*kIndicatorDefaultMargin)};
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(width)]" options:0 metrics:metrics views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]" options:0 metrics:metrics views:views]];
    return constraints;
}

- (NSArray*)statusViewConstraints{
    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    NSDictionary *views = @{@"view":self.statusView};
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
    return constraints;
}

#pragma mark - Interface Methods

+ (void)ignoreInteractionEvents:(BOOL)ignore{
    [[ActivityIndicator appearance] setIgnoreInteractionEvents:ignore];
}

+ (void)setBackgroundColor:(UIColor*)color{
    [[ActivityIndicator appearance] setBackroundViewColor:color];
}

+ (void)setFont:(UIFont*)font{
    [[ActivityIndicator appearance] setFont:font];
}

+ (void)setSuccessImage:(UIImage*)image{
    [[ActivityIndicator appearance] setSuccessImage:image];
}

+ (void)setErrorImage:(UIImage*)image{
    [[ActivityIndicator appearance] setErrorImage:image];
}

+ (void)setIndicatorColor:(UIColor *)color{
    [[ActivityIndicator appearance] setIndicatorColor:color];
}

+ (void)setIndicatorSize:(CGSize)size{
    [[ActivityIndicator appearance] setIndicatorSize:size];
}

+ (void)setIndicatorOffset:(UIOffset)offset{
    [[ActivityIndicator appearance] setIndicatorOffset:offset];
}

+ (void)resetIndicatorOffset {
    [[ActivityIndicator appearance] setIndicatorOffset:kIndicatorDefaultOffset];
}

+ (void)setIndicatorCornerRadius:(CGFloat)radius{
    [[ActivityIndicator appearance] setIndicatorCornerRadius:radius];
}

+ (void)setIndicatorCircleRadius:(CGFloat)radius{
    [[ActivityIndicator appearance] setIndicatorCircleRadius:radius];
}

+ (void)setIndicatorCircleThickness:(CGFloat)thickness{
    [[ActivityIndicator appearance] setIndicatorCircleThickness:thickness];
}

+ (void)setIndicatorCircleForegroundColor:(UIColor*)color{
    [[ActivityIndicator appearance] setIndicatorCircleForegroundColor:color];
}

+ (void)show{
    [self showInView:[ActivityIndicator keyWindow]];
}

+ (void)showInView:(UIView*)view{
    [[self indicatorForView:view] showInView:view];
}

+ (void)showWithProgress:(CGFloat)progress{
    [self showWithProgress:progress inView:[ActivityIndicator keyWindow]];
}

+ (void)showWithProgress:(CGFloat)progress inView:(UIView*)view{
    [[self indicatorForView:view] showWithProgress:progress inView:view];
}

+ (void)showWithProgress:(CGFloat)progress text:(NSString*)text{
    [self showWithProgress:progress text:text inView:[ActivityIndicator keyWindow]];
}

+ (void)showWithProgress:(CGFloat)progress text:(NSString*)text inView:(UIView*)view{
    [[self indicatorForView:view] showWithProgress:progress text:text inView:view];
}

+ (void)showWithText:(NSString*)text{
    [self showWithText:text inView:[self keyWindow]];
}

+ (void)showWithText:(NSString*)text inView:(UIView*)view{
    [[self indicatorForView:view] showWithText:text inView:view];
}

+ (void)showSuccessWithText:(NSString*)text{
    [self showSuccessWithText:text inView:[self keyWindow]];
}

+ (void)showSuccessWithText:(NSString*)text inView:(UIView*)view{
    [[self indicatorForView:view] showSuccessWithText:text inView:view];
}

+ (void)showErrorWithText:(NSString *)text{
    [self showErrorWithText:text inView:[self keyWindow]];
}

+ (void)showErrorWithText:(NSString *)text inView:(UIView*)view{
    [[self indicatorForView:view] showErrorWithText:text inView:view];
}

+ (void)dismiss{
    [self dismissInView:[self keyWindow]];
}

+ (void)dismissInView:(UIView*)view{
    NSArray *activityIndicators = [self activityIndicatorsInView:view];
    [activityIndicators enumerateObjectsUsingBlock:^(ActivityIndicator *obj, NSUInteger idx, BOOL *stop) {
        [obj dismiss];
    }];
}

+ (void)dismissAfterDelay:(NSTimeInterval)delay{
    [self dismissAfterDelay:delay inView:[self keyWindow]];
}

+ (void)dismissAfterDelay:(NSTimeInterval)delay inView:(UIView*)view{
    NSArray *activityIndicators = [self activityIndicatorsInView:view];
    [activityIndicators enumerateObjectsUsingBlock:^(ActivityIndicator *obj, NSUInteger idx, BOOL *stop) {
        [obj dismissAfterDelay:delay];
    }];
}

#pragma mark - Methods

- (void)showInView:(UIView*)view{
    [self createInView:view];
    [self createCommonViewsWithContentViews:@[self.circleActivityView]];
}

- (void)showWithText:(NSString*)text inView:(UIView*)view{
    [self createInView:view];
    self.label.text = text;
    [self createCommonViewsWithContentViews:@[self.circleActivityView, self.label]];
}

- (void)showWithProgress:(CGFloat)progress inView:(UIView*)view{
    [self createInView:view];
    [self.circleProgressView setProgress:progress animated:NO];
    [self createCommonViewsWithContentViews:@[self.circleProgressView]];
}

- (void)showWithProgress:(CGFloat)progress text:(NSString*)text inView:(UIView*)view{
    [self createInView:view];
    [self.circleProgressView setProgress:progress animated:NO];
    self.label.text = text;
    [self createCommonViewsWithContentViews:@[self.circleProgressView, self.label]];
}

- (void)showSuccessWithText:(NSString*)text inView:(UIView*)view{
    [self createInView:view];
    self.imageView.image = self.successImage;
    self.label.text = text;
    [self createCommonViewsWithContentViews:@[self.imageView, self.label]];
    [self dismissAfterDelay:kDefaultDelay];
}

- (void)showErrorWithText:(NSString*)text inView:(UIView*)view{
    [self createInView:view];
    self.imageView.image = self.errorImage;
    self.label.text = text;
    [self createCommonViewsWithContentViews:@[self.imageView, self.label]];
    [self dismissAfterDelay:kDefaultDelay];
}

- (void)createInView:(UIView*)view{
    [self removeTimer];
    
    if (self.superview){
        [self.superview bringSubviewToFront:self];
    }
    else{
        [view addSubview:self];
        self.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *views = @{@"view":self};
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
        self.alpha = 0.0f;
        self.activityView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        if (self.ignoreInteractionEvents){
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        }
        [UIView animateWithDuration:0.15f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.alpha = 1.0f;
            self.activityView.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
}

- (void)dismiss{
    [self removeTimer];
    if (self.ignoreInteractionEvents){
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    [UIView animateWithDuration:0.15f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeViews];
        [self removeFromSuperview];
        self.alpha = 1.0f;
    }];
}

- (void)dismissAfterDelay:(NSTimeInterval)delay{
    [self removeTimer];
    [self createTimerWithInterval:delay];
}

#pragma mark - Timer

- (void)timerAction:(NSTimer*)timer{
    [self dismiss];
}

- (void)createTimerWithInterval:(NSTimeInterval)interval{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(timerAction:) userInfo:nil repeats:NO];
}

- (void)removeTimer{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - Keyboard

- (CGFloat)visibleKeyboardHeight{
    UIWindow *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]){
        if(![[testWindow class] isEqual:[UIWindow class]]){
            keyboardWindow = testWindow;
            break;
        }
    }
    for (__strong UIView *possibleKeyboard in [keyboardWindow subviews]){
        if([possibleKeyboard isKindOfClass:NSClassFromString(@"UIPeripheralHostView")] || [possibleKeyboard isKindOfClass:NSClassFromString(@"UIKeyboard")]){
            return CGRectGetHeight(possibleKeyboard.bounds);
        } else if([possibleKeyboard isKindOfClass:NSClassFromString(@"UIInputSetContainerView")]){
            for (__strong UIView *possibleKeyboardSubview in [possibleKeyboard subviews]){
                if([possibleKeyboardSubview isKindOfClass:NSClassFromString(@"UIInputSetHostView")]){
                    return CGRectGetHeight(possibleKeyboardSubview.bounds);
                }
            }
        }
    }
    return 0;
}

#pragma mark - Notifications

- (void)keyboardWillShowNotification:(NSNotification*)notification{
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve = [[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    self.contentViewBottomConstraint.constant = -CGRectGetHeight(keyboardFrame);
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        [self layoutIfNeeded];
    } completion:nil];
}

- (void)keyboardWillHideNotification:(NSNotification*)notification{
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval animationDuration = [[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve = [[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    self.contentViewBottomConstraint.constant = 0.0f;
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        [self layoutIfNeeded];
    } completion:nil];
}

@end