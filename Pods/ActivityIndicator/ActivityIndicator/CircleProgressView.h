//
//  CircleProgressView.h
//
//  Created by EvgenyMikhaylov on 9/21/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleProgressView : UIView

@property (nonatomic, readonly) CGFloat progress;
@property (nonatomic) CGFloat strokeThickness;
@property (nonatomic) CGFloat radius;
@property (nonatomic) UIColor *foregroundStrokeColor;
@property (nonatomic) UIColor *backgroundStrokeColor;

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated;

@end

