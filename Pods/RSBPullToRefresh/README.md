# RSBPullToRefresh

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RSBPullToRefresh is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RSBPullToRefresh"
```

## Author

Anton Kormakov, anton.kormakov@rosberry.com

## License

RSBPullToRefresh is available under the MIT license. See the LICENSE file for more info.
