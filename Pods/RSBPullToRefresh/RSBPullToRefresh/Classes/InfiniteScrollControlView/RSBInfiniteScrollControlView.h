//
//  KAInfiniteScrollControlView.h
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSBLoaderView.h"

@interface RSBInfiniteScrollControlView : UIView

@property (nonatomic, strong) RSBLoaderView *loaderView;

@end
