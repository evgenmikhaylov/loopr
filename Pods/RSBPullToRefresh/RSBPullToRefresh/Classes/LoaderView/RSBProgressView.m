//
//  CircleProgressView.m
//  Collaborate
//
//  Created by Anton on 27.05.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBProgressView.h"

@interface RSBProgressView ()

@property (nonatomic, assign) double destinationProgress;
@property (nonatomic, assign) double progressUpdateValue;
@property (nonatomic, assign) NSInteger updateTimes;
@property (nonatomic, assign) BOOL isAnimating;
@property (nonatomic, strong) NSTimer *timer;

@end

static double kTimerUpdateTimeInterval = 1.f / 60.f;

@implementation RSBProgressView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _circleColor = [UIColor clearColor];
        _fillColor = [UIColor blackColor];
        _lineWidth = 1.f;
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, _lineWidth);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    double side = MIN(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    double radius = (side / 2.0f);
    double lineHalfWidth = _lineWidth/2.f;
    
    CGMutablePathRef circle = CGPathCreateMutable();
    CGPathAddEllipseInRect(circle, NULL, CGRectMake(CGRectGetWidth(self.frame)/2.f - radius + lineHalfWidth,
                                                    CGRectGetHeight(self.frame)/2.f - radius + lineHalfWidth,
                                                    side - _lineWidth,
                                                    side - _lineWidth));
    CGContextAddPath(context, circle);
    CGContextSetStrokeColorWithColor(context, _circleColor.CGColor);
    CGContextDrawPath(context, kCGPathStroke);
    CGPathRelease(circle);
    
    double startAngle = - M_PI_2;
    double endAngle = (_progress * 2.f * (double)M_PI) + startAngle;
    
    CGMutablePathRef fill = CGPathCreateMutable();
    CGPathAddArc(fill, NULL, CGRectGetMidX(rect), CGRectGetMidY(rect), radius - lineHalfWidth, startAngle, endAngle, NO);
    CGContextAddPath(context, fill);
    CGContextSetStrokeColorWithColor(context, _fillColor.CGColor);
    CGContextDrawPath(context, kCGPathStroke);
    CGPathRelease(fill);
}

- (void)setProgress:(double)progress {
    [self setProgress:progress animated:NO];
}

- (void)setProgress:(double)progress animated:(BOOL)animated {
    if (_isAnimating) {
        [self stopTimer];
        _progress = _destinationProgress;
        [self setNeedsDisplay];
    }
    progress = progress > 1.f ? 1.f : progress;
    progress = progress < 0.f ? 0.f : progress;
    if (animated) {
        double animationDuration = .3f;
        _updateTimes = animationDuration/kTimerUpdateTimeInterval;
        _destinationProgress = progress;
        _progressUpdateValue = (_destinationProgress - _progress)/(double)_updateTimes;
        [self startTimer];
    } else {
        _progress = progress;
        [self setNeedsDisplay];
    }
}

- (void)setCircleColor:(UIColor *)circleColor {
    _circleColor = circleColor;
    [self setNeedsDisplay];
}

- (void)setFillColor:(UIColor *)fillColor {
    _fillColor = fillColor;
    [self setNeedsDisplay];
}

- (void)setLineWidth:(double)lineWidth {
    _lineWidth = lineWidth;
    [self setNeedsDisplay];
}

- (void)startTimer {
    if (self.timer)
        [self stopTimer];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kTimerUpdateTimeInterval target:self selector:@selector(updateProgressToDestination) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    [self updateProgressToDestination];
    _isAnimating = YES;
}

- (void)stopTimer {
    [self.timer invalidate];
    [self setTimer:nil];
    _isAnimating = NO;
}

- (void)updateProgressToDestination {
    _progress += _progressUpdateValue;
    --_updateTimes;
    if (_updateTimes == 0) {
        [self stopTimer];
        _progress = _destinationProgress;
    }
    [self setNeedsDisplay];
}

@end
