//
//  KAInfiniteScrollingRefreshProtocol.m
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBInfiniteScrollRefreshControl.h"
#import "RSBInfiniteScrollControlView.h"

@implementation RSBInfiniteScrollRefreshControl

@synthesize view, refreshControlHeight, refreshControlInset, refreshControlActivationInset, isLoading, actionHandlerBlock, enabled, delegate;

- (instancetype)init {
    self = [super init];
    if (self) {
        RSBInfiniteScrollControlView *refreshView = [[RSBInfiniteScrollControlView alloc] init];

        self.view = refreshView;
        self.refreshControlHeight = 60.0;
        self.refreshControlActivationInset = 60.0;
        self.enabled = YES;
    }
    return self;
}

- (void)refreshControlShowWithProgress:(double)progress {
    //
}

- (void)startLoading {
    self.isLoading = YES;

    RSBInfiniteScrollControlView *refreshView = (id)self.view;
    refreshView.hidden = NO;
    if(![refreshView.loaderView isAnimating])
        [refreshView.loaderView startAnimating];
}

- (void)endLoading {
    [self.delegate refreshControlDidStopAnimating:self animated:YES];
    self.isLoading = NO;
}

- (void)endLoadingNow {
    [self.delegate refreshControlDidStopAnimating:self animated:NO];
    self.isLoading = NO;
}

@end
