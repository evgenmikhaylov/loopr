//
//  RefreshControlProtocol.h
//  Collaborate
//
//  Created by Anton on 23.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#ifndef Collaborate_KARefreshControlProtocol_h
#define Collaborate_KARefreshControlProtocol_h

#import <UIKit/UIKit.h>
@protocol RSBRefreshControlDelegate;

typedef void(^RSBActionHandlerBlock)();

@protocol RSBRefreshControlProtocol <NSObject>

@property (nonatomic, copy) RSBActionHandlerBlock actionHandlerBlock;
@property (nonatomic, strong) UIView *view;

@property (nonatomic, assign) double refreshControlHeight;
@property (nonatomic, assign) double refreshControlInset;
@property (nonatomic, assign) double refreshControlActivationInset;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, weak) id<RSBRefreshControlDelegate> delegate;

- (void)refreshControlShowWithProgress:(double)progress;

- (void)startLoading;

- (void)endLoading;
- (void)endLoadingNow;

@end


@protocol RSBRefreshControlDelegate <NSObject>

- (void)refreshControlDidStopAnimating:(id<RSBRefreshControlProtocol>)control animated:(BOOL)animated;

@end

#endif
