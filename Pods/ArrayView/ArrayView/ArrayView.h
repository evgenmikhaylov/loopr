//
//  ArrayView.h
//
//  Created by EvgenyMikhaylov on 9/1/15.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ArrayViewAlignment) {
    ArrayViewAlignmentLeft,
    ArrayViewAlignmentRight,
    ArrayViewAlignmentTop = ArrayViewAlignmentLeft,
    ArrayViewAlignmentBottom = ArrayViewAlignmentRight,
    ArrayViewAlignmentCenter,
};

@interface ArrayView : UIView

+ (instancetype)arrayViewWithViews:(NSArray *)views
                           spacing:(CGFloat)spacing
                              axis:(UILayoutConstraintAxis)axis
                         alignment:(ArrayViewAlignment)alignment;
- (instancetype)initWithViews:(NSArray *)views
                      spacing:(CGFloat)spacing
                         axis:(UILayoutConstraintAxis)axis
                    alignment:(ArrayViewAlignment)alignment;

@property (nonatomic, readonly) NSArray *views;
@property (nonatomic, readonly) CGFloat spacing;
@property (nonatomic, readonly) UILayoutConstraintAxis axis;
@property (nonatomic, readonly) ArrayViewAlignment alignment;

@end
