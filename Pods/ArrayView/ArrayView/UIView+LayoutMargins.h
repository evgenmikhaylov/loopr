//
//  UIView+LayoutMargins.h
//
//  Created by EvgenyMikhaylov on 9/2/15.
//
//

#import <UIKit/UIKit.h>

@interface UIView (LayoutMargins)

@property (nonatomic) BOOL flexibleLayoutMargins;

@end
