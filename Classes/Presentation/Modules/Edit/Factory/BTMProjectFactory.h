//
//  BTMEditFactory.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMProjectViewController;

@interface BTMProjectFactory : NSObject

+ (BTMProjectViewController *)viewController;

@end
