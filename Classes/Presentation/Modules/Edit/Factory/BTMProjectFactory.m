//
//  BTMEditFactory.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectFactory.h"

#import "BTMProjectViewController.h"
#import "BTMProjectInteractor.h"
#import "BTMProjectViewModel.h"

#import "BTMServicesFactory.h"

@implementation BTMProjectFactory

+ (BTMProjectViewController *)viewController {
    BTMProjectViewController *viewController = [[BTMProjectViewController alloc] init];
    viewController.viewModel = [self viewModel];
    return viewController;
}

+ (BTMProjectViewModel *)viewModel {
    BTMProjectViewModel *viewModel = [[BTMProjectViewModel alloc] initWithInteractor:[self interactor]];
    return viewModel;
}

+ (BTMProjectInteractor *)interactor {
    BTMProjectInteractor *interactor = [[BTMProjectInteractor alloc] init];
    interactor.videoStorage = [BTMServicesFactory videoStorage];
    interactor.audioStorage = [BTMServicesFactory audioStorage];
    interactor.beatDetectionService = [BTMServicesFactory beatDetectionService];
    interactor.cacheService = [BTMServicesFactory cacheService];
    interactor.projectRenderService = [BTMServicesFactory projectRenderService];
    return interactor;
}

@end
