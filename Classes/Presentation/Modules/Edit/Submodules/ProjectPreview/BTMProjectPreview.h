//
//  BTMProjectPreview.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/27/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTMProjectPreview;

@protocol BTMProjectPreviewDelegate <NSObject>

@optional
- (void)projectPreview:(BTMProjectPreview *)view currentTimeChanged:(NSTimeInterval)time;
- (void)projectPreviewWillBeginTouching:(BTMProjectPreview *)view;
- (void)projectPreviewDidEndTouching:(BTMProjectPreview *)view;

@end

@class BTMProjectPreviewModel;

@interface BTMProjectPreview : UIView

@property (weak, nonatomic) id<BTMProjectPreviewDelegate> delegate;
@property (nonatomic) BTMProjectPreviewModel *viewModel;

@end
