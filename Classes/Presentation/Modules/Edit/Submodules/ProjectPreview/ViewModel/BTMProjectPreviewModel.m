    //
//  BTMProjectPreviewModel.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectPreviewModel.h"
#import "BTMVideoFilter.h"
#import "BTMProject.h"
#import "BTMVideo.h"
#import "BTMAudio.h"

#import <AVFoundation/AVFoundation.h>

@interface BTMProjectPreviewModel ()

@property (nonatomic) BTMProject *project;

@end

@implementation BTMProjectPreviewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.drawingEnabled = YES;
        self.pause = NO;
    }
    return self;
}

- (void)setCropRect:(CGRect)cropRect {
    self.project.video.cropRect = cropRect;
}

- (CGRect)cropRect {
    return self.project.video.cropRect;
}

- (NSURL *)videoURL {
    return [NSURL fileURLWithPath:self.project.videoPath];
}

- (NSURL *)audioURL {
    return [NSURL fileURLWithPath:self.project.audioPath];
}

- (CMTime)videoDuration {
    return self.project.video.duration;
}

- (CMTimeRange)audioTimeRange {
    return self.project.audio.timeRange;
}

- (CGFloat)videoSpeedFactor {
    return 1.0 / self.project.selectedVideoFilter.timeStretch;
}

- (CGFloat)minVideoScale {
    return self.project.selectedVideoFilter.minScale;
}

- (CGFloat)maxVideoScale {
    return self.project.selectedVideoFilter.maxScale;
}

- (NSUInteger)crossFadeFramesCount {
    return self.project.videoСrossFadeFramesCount;
}

- (NSUInteger)framesPerSecond {
    return self.project.videoFramesPerSecond;
}

- (BOOL)drawOnlyFirstVideoFrame {
    return self.project.selectedVideoFilter.videoMode == BTMVideoFilterVideoModeStillFrame;
}

- (BOOL)hasVideo {
    return (self.project.video != nil);
}

- (BOOL)hasAudio {
    return (self.project.audio != nil);
}

- (BOOL)mute {
    return self.project.mute;
}

- (void (^)())reload {
    if (!_reload) {
        _reload = ^{
        };
    }
    return _reload;
}

- (NSValue *)beatTimeValueAtIndex:(NSUInteger)index {
    return [self.project beatTimeValueAtIndex:index];
}

- (BOOL)isLastBeatIndex:(NSUInteger)index {
    return [self.project isLastBeatIndex:index];
}


@end
