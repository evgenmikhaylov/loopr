//
//  BTMProjectPreviewModel.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZViewModel.h"
#import <CoreMedia/CoreMedia.h>

@class BTMProject;

NS_ASSUME_NONNULL_BEGIN

@interface BTMProjectPreviewModel : MVZViewModel

@property (nonatomic) BOOL drawingEnabled;
@property (nonatomic) BOOL pause;
@property (nonatomic) CGRect cropRect;

@property (nonatomic, readonly) NSURL *videoURL;
@property (nonatomic, readonly) NSURL *audioURL;
@property (nonatomic, readonly) CMTime videoDuration;
@property (nonatomic, readonly) CMTimeRange audioTimeRange;
@property (nonatomic, readonly) CGFloat videoSpeedFactor;
@property (nonatomic, readonly) CGFloat minVideoScale;
@property (nonatomic, readonly) CGFloat maxVideoScale;
@property (nonatomic, readonly) NSUInteger crossFadeFramesCount;
@property (nonatomic, readonly) NSUInteger framesPerSecond;
@property (nonatomic, readonly) BOOL drawOnlyFirstVideoFrame;
@property (nonatomic, readonly) BOOL hasVideo;
@property (nonatomic, readonly) BOOL hasAudio;
@property (nonatomic, readonly) BOOL mute;

@property (nonatomic, copy) void(^reload)();

- (NSValue *)beatTimeValueAtIndex:(NSUInteger)index;
- (BOOL)isLastBeatIndex:(NSUInteger)index;

- (void)setProject:(BTMProject *)project;

@end

NS_ASSUME_NONNULL_END
