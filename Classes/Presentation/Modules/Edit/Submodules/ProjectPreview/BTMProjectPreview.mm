//
//  BTMProjectPreview.mm
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/27/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "BTMProjectPreview.h"
#import "BTMProjectPreviewModel.h"

#import "ZoomView.h"
#import "GLView.h"

#import "CPVideoColorMatrix601Program.h"
#import "CPVideoColorMatrix709Program.h"
#import "CoreGraphicsExtensions.h"
#import "CPGLVideoTexture.h"
#import "CPImageProgram.h"
#import "GLProgramCache.h"
#import "GLMatrix4x4.hpp"
#import "GLStateCache.h"

#import "AVAudioPlayer+Additions.h"
#import "UIImage+Crop.h"

@interface BTMProjectPreview () <GLViewDelegate, ZoomViewDelegate>

@property (nonatomic) AVAudioPlayer *audioPlayer;

@property (nonatomic) dispatch_source_t backgroundTimer;
@property (nonatomic) CADisplayLink *timer;
@property (nonatomic) CMTime time;
@property (nonatomic) BOOL drawingStarted;
@property (nonatomic, readonly) BOOL needsDrawNextFrame;

@property (nonatomic) NSUInteger indexOfNextBeat;
@property (nonatomic) CMTime nextBeatTime;
@property (nonatomic) CMTime videoTimeOffset;
@property (nonatomic) NSInteger currentCrossFadeFramesCount;
@property (nonatomic, readonly) CGFloat videoScale;

@property (nonatomic) CPVideoColorMatrix601Program *backgroundVideo601Program;
@property (nonatomic) CPVideoColorMatrix709Program *backgroundVideo709Program;

@property (nonatomic) CPGLVideoTexture *firstBackgroundTexture;
@property (nonatomic) CPGLVideoTexture *secondBackgroundTexture;

@property (nonatomic) GLView *glView;
@property (nonatomic) ZoomView *backgroundScaleView;

@end

@implementation BTMProjectPreview

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.backgroundVideo601Program = [GLProgramCache programWithClass:[CPVideoColorMatrix601Program class]];
    self.backgroundVideo709Program = [GLProgramCache programWithClass:[CPVideoColorMatrix709Program class]];
}

- (void)dealloc {
    [self.audioPlayer stop];
    self.audioPlayer = nil;
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    self.glView.frame = self.bounds;
    self.backgroundScaleView.frame = self.bounds;
}

#pragma mark - Setters/Getters

- (void)setViewModel:(BTMProjectPreviewModel *)viewModel {
    _viewModel = viewModel;
    [self bindViewModel];
}

- (NSTimeInterval)currentTime {
    NSTimeInterval currentTime = CMTimeGetSeconds(self.viewModel.audioTimeRange.start) + CMTimeGetSeconds(self.time);
    if (isnan(currentTime)){
        currentTime = 0;
    }
    return currentTime;
}

- (CGFloat)videoScale {
    CGFloat minScale = self.viewModel.minVideoScale;
    CGFloat maxScale = self.viewModel.maxVideoScale;
    CMTime startTime = [[self.viewModel beatTimeValueAtIndex:(self.indexOfNextBeat - 1)] CMTimeValue];
    CMTime endTime = self.nextBeatTime;
    CMTime duration = CMTimeMake(endTime.value - startTime.value, endTime.timescale);
    CMTime transformedTime = CMTimeConvertScale(self.time, startTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
    CGFloat timeScale = (double)(transformedTime.value - startTime.value) / (double)duration.value;
    CGFloat videoScale = (maxScale - minScale) * timeScale + minScale;
    videoScale = MAX(videoScale, minScale);
    videoScale = MIN(videoScale, maxScale);
    return videoScale;
}

- (BOOL)needsDrawNextFrame {
    return self.time.value % (int)(1.0 / self.viewModel.videoSpeedFactor) == 0;
}

#pragma mark - ViewModel

- (void)bindViewModel {
    __weak typeof (self) weakSelf = self;
    self.viewModel.update = ^{
        [weakSelf bindViewModel];
    };
    self.viewModel.reload = ^{
        [weakSelf reload];
    };
    if (self.viewModel.pause) {
        [self.audioPlayer pause];
    }
    else {
        [self.audioPlayer play];
    }
    self.audioPlayer.mute = self.viewModel.mute;
}

#pragma mark - GL

- (void)reset {
    [self stopTimer];
    [self.audioPlayer stop];
    
    self.firstBackgroundTexture = nil;
    self.secondBackgroundTexture = nil;
}

- (void)reload {
    [self reset];
    
    if (!self.viewModel.hasVideo) {
        return;
    }
    
    [self resetTime];
    self.currentCrossFadeFramesCount = self.viewModel.crossFadeFramesCount;
    
    self.audioPlayer = nil;
    if (self.viewModel.hasAudio){
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.viewModel.audioURL error:nil];
        self.audioPlayer.currentTime = CMTimeGetSeconds(self.viewModel.audioTimeRange.start);
        self.audioPlayer.mute = self.viewModel.mute;
    }
    else {
        self.audioPlayer = nil;
    }
    
    if (!self.glView){
        self.glView = [[GLView alloc] init];
        self.glView.options = GLViewOptionsDepthBuffer;
        self.glView.delegate = self;
        [self addSubview:self.glView];
    }
    if (!self.backgroundScaleView){
        self.backgroundScaleView = [[ZoomView alloc] init];
        self.backgroundScaleView.delegate = self;
        [self addSubview:self.backgroundScaleView];
    }
    
    [self createSecondBackgroundTextureWithOffset:CMTimeGetSeconds(self.time)];
    
    [self startTimer];
}

- (void)createFirstBackgroundTextureWithOffset:(NSTimeInterval)offset {
    __weak typeof (self) weakSelf = self;
    
    CPGLVideoTexture *videoTexture = [[CPGLVideoTexture alloc] init];
    videoTexture.currentURL = self.viewModel.videoURL;
    videoTexture.shouldRepeat = YES;
    self.firstBackgroundTexture = videoTexture;
    
    [videoTexture startReadingWithOffset:offset completionBlock:^{
        CGSize videoSize = videoTexture.assetVideoTrack.naturalSize;
        weakSelf.backgroundScaleView.contentSize = videoSize;
        if (CGRectIsEmpty(weakSelf.viewModel.cropRect) || CGRectIsInfinite(weakSelf.viewModel.cropRect)){
            weakSelf.viewModel.cropRect = [UIImage cropRectForImageWithSize:videoSize inContainerWithSize:weakSelf.frame.size];
        }
    }];
}

- (void)createSecondBackgroundTextureWithOffset:(NSTimeInterval)offset {
    __weak typeof (self) weakSelf = self;

    CPGLVideoTexture *videoTexture = [[CPGLVideoTexture alloc] init];
    videoTexture.currentURL = self.viewModel.videoURL;
    videoTexture.shouldRepeat = YES;
    self.secondBackgroundTexture = videoTexture;
    
    [videoTexture startReadingWithOffset:offset completionBlock:^{
        CGSize videoSize = videoTexture.assetVideoTrack.naturalSize;
        weakSelf.backgroundScaleView.contentSize = videoSize;
        if (CGRectIsEmpty(weakSelf.viewModel.cropRect) || CGRectIsInfinite(weakSelf.viewModel.cropRect)){
            weakSelf.viewModel.cropRect = [UIImage cropRectForImageWithSize:videoSize inContainerWithSize:weakSelf.frame.size];
        }
    }];
}

#pragma mark - Draw

- (void)drawBackground {
    CGFloat videoScale = self.videoScale;
    if (self.currentCrossFadeFramesCount > 0) {
        NSTimeInterval offset = 2.0 * (CMTimeGetSeconds(self.nextBeatTime) - CMTimeGetSeconds(self.videoTimeOffset)) * self.viewModel.videoSpeedFactor;
        CGFloat topTextureAlpha = (float)self.currentCrossFadeFramesCount / (float)self.viewModel.crossFadeFramesCount;
        if (self.indexOfNextBeat % 2 == 1) {
            self.secondBackgroundTexture.pause = self.viewModel.pause;
            [self drawVideoTexture:self.secondBackgroundTexture withScale:videoScale alpha:1.0];
            [self drawVideoTexture:self.firstBackgroundTexture withScale:self.viewModel.maxVideoScale alpha:topTextureAlpha];
            if (self.currentCrossFadeFramesCount == 1) {
                [self createFirstBackgroundTextureWithOffset:offset];
            }
        }
        else {
            self.firstBackgroundTexture.pause = self.viewModel.pause;
            [self drawVideoTexture:self.firstBackgroundTexture withScale:videoScale alpha:1.0];
            [self drawVideoTexture:self.secondBackgroundTexture withScale:self.viewModel.maxVideoScale alpha:topTextureAlpha];
            if (self.currentCrossFadeFramesCount == 1) {
                [self createSecondBackgroundTextureWithOffset:offset];
            }
        }
        self.currentCrossFadeFramesCount--;
    }
    else {
        CPGLVideoTexture *backgroundTexture = (self.indexOfNextBeat % 2 == 1) ? self.secondBackgroundTexture : self.firstBackgroundTexture;
        backgroundTexture.pause = self.viewModel.pause;
        [self drawVideoTexture:backgroundTexture withScale:videoScale alpha:1.0];
    }
}

- (void)drawVideoTexture:(CPGLVideoTexture *)videoTexture withScale:(CGFloat)scale alpha:(CGFloat)alpha {
    if (self.viewModel.drawOnlyFirstVideoFrame || !self.needsDrawNextFrame) {
        [self drawVideoTextureWithCurrentFrame:videoTexture scale:scale alpha:alpha];
    } else {
        [self drawVideoTextureWithNextFrame:videoTexture scale:scale alpha:alpha];
    }
}

- (void)drawVideoTextureWithNextFrame:(CPGLVideoTexture *)videoTexture scale:(CGFloat)scale alpha:(CGFloat)alpha {
    __weak typeof (self) weakSelf = self;
    [videoTexture readNextFrameWithCompletionBlock:^(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle) {
        [weakSelf drawBackgroundVideoWithTexture:texture
                                           scale:scale
                                           alpha:alpha
                                     colorMatrix:colorMatrix
                             chromaTextureHandle:chromaTextureHandle
                              lumiaTextureHandle:lumiaTextureHandle];
    }];
}

- (void)drawVideoTextureWithCurrentFrame:(CPGLVideoTexture *)videoTexture scale:(CGFloat)scale alpha:(CGFloat)alpha {
    __weak typeof (self) weakSelf = self;
    [videoTexture readCurrentFrameWithCompletionBlock:^(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle) {
        [weakSelf drawBackgroundVideoWithTexture:texture
                                           scale:scale
                                           alpha:alpha
                                     colorMatrix:colorMatrix
                             chromaTextureHandle:chromaTextureHandle
                              lumiaTextureHandle:lumiaTextureHandle];
    }];
}

- (void)drawBackgroundVideoWithTexture:(CPGLVideoTexture*)texture
                                 scale:(CGFloat)scale
                                 alpha:(CGFloat)alpha
                           colorMatrix:(GLVideoTextureColorMatrix)colorMatrix
                   chromaTextureHandle:(GLenum)chromaTextureHandle
                    lumiaTextureHandle:(GLenum)lumiaTextureHandle {
    
    CGRect cropRect = self.viewModel.cropRect;
    
    CGRect scaledCropRect = rectApplyingScale(cropRect, 1.0 / scale);
    float texCoordinateMinX = CGRectGetMinX(scaledCropRect);
    float texCoordinateMaxX = CGRectGetMaxX(scaledCropRect);
    float texCoordinateMinY = CGRectGetMinY(scaledCropRect);
    float texCoordinateMaxY = CGRectGetMaxY(scaledCropRect);
        
    GLProgram<CPVideoProgramProtocol> *program = (colorMatrix == GLVideoTextureColorMatrix601) ? self.backgroundVideo601Program : self.backgroundVideo709Program;
    [GLStateCache useGLProgram:program];
    [program setUniform1i:program.uniformSamplerY value:chromaTextureHandle];
    [program setUniform1i:program.uniformSamplerUV value:lumiaTextureHandle];
    [program setUniform1f:program.uniformAlpha value:alpha];
    [program setUniformMatrix4x4:program.uniformProjectionMatrix matrix:GLMatrix4x4::GLMatrix4x4()];
    [program setUniformMatrix4x4:program.uniformModelViewMatrix matrix:GLMatrix4x4::GLMatrix4x4()];
    
    float vertices[] =
    {
        -1.0f, -1.0f, texCoordinateMinX, texCoordinateMaxY,
        1.0f, -1.0f, texCoordinateMaxX, texCoordinateMaxY,
        -1.0f, 1.0f, texCoordinateMinX, texCoordinateMinY,
        1.0f, 1.0f, texCoordinateMaxX, texCoordinateMinY,
    };
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glVertexAttribPointer(program.attribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4, &vertices[0]);
    glVertexAttribPointer(program.attribTexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4, &vertices[2]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisable(GL_BLEND);
}

#pragma mark - Timer

- (dispatch_source_t)createDispatchTimerWithInterval:(uint64_t)interval
                                              leeway:(uint64_t)leeway
                                               queue:(dispatch_queue_t)queue
                                               block:(dispatch_block_t)block {
    
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), interval, leeway);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

- (void)startTimer {
    [self stopTimer];

    self.drawingStarted = NO;
    
    uint64_t interval = (int64_t)((1.0 / self.viewModel.framesPerSecond) * NSEC_PER_SEC);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    __weak typeof (self) weakSelf = self;
    self.backgroundTimer = [self createDispatchTimerWithInterval:interval leeway:interval/10 queue:queue block:^{
        if (weakSelf.drawingStarted && weakSelf.viewModel.drawingEnabled){
            [weakSelf updateTime];
        }
    }];
    
    self.timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(timerAction:)];
    [self.timer setFrameInterval:(60 / self.viewModel.framesPerSecond)];
    [self.timer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
    if (self.backgroundTimer){
        dispatch_source_cancel(self.backgroundTimer);
        self.backgroundTimer = nil;
    }
}

- (void)timerAction:(id)sender {
    if (!self.viewModel.drawingEnabled) {
        return;
    }
    self.drawingStarted = YES;
    [self.glView display];
}

- (void)updateTime {
    NSTimeInterval displayLinkTime = CMTimeGetSeconds(self.time);
    NSTimeInterval playerTime = self.audioPlayer.currentTime;
    if (fabs(displayLinkTime - playerTime) > 0.1){
        if (displayLinkTime <= self.audioPlayer.duration){
            self.audioPlayer.currentTime = displayLinkTime;
        }
    }
    NSValue *beatTimeValue = [self.viewModel beatTimeValueAtIndex:self.indexOfNextBeat];
    if (beatTimeValue) {
        CMTime beatTime = [beatTimeValue CMTimeValue];
        CMTime tBeatTime = CMTimeConvertScale(beatTime, self.time.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (tBeatTime.value == self.time.value){
            if ([self.viewModel isLastBeatIndex:self.indexOfNextBeat]) {
                [self resetTime];
            }
            else {
                NSValue *nextTimeValue = [self.viewModel beatTimeValueAtIndex:(self.indexOfNextBeat + 1)];
                if (nextTimeValue) {
                    CMTime nextTime = [nextTimeValue CMTimeValue];
                    self.nextBeatTime = nextTime;
                    self.indexOfNextBeat++;
                    NSTimeInterval videoInterval = CMTimeGetSeconds(self.viewModel.videoDuration) / (2.0 * self.viewModel.videoSpeedFactor)
                    - (CMTimeGetSeconds(self.nextBeatTime) - CMTimeGetSeconds(self.videoTimeOffset));
                    NSTimeInterval beatInterval = CMTimeGetSeconds(nextTime) - CMTimeGetSeconds(self.nextBeatTime);
                    if (videoInterval < beatInterval) {
                        self.videoTimeOffset = self.nextBeatTime;
                    }
                }
            }
            self.currentCrossFadeFramesCount = self.viewModel.crossFadeFramesCount;
        }
    }
    if (!self.viewModel.pause) {
        self.time = CMTimeMake(self.time.value + 1, (int32_t)self.viewModel.framesPerSecond);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(projectPreview:currentTimeChanged:)]) {
            [self.delegate projectPreview:self currentTimeChanged:CMTimeGetSeconds(self.time)];
        }
    });
}

- (void)resetTime {
    self.time = [[self.viewModel beatTimeValueAtIndex:0] CMTimeValue];
    [self resetBeatTime];
}

- (void)resetBeatTime {
    self.indexOfNextBeat = 0;
    self.videoTimeOffset = [[self.viewModel beatTimeValueAtIndex:self.indexOfNextBeat] CMTimeValue];
    self.indexOfNextBeat++;
    self.nextBeatTime = [[self.viewModel beatTimeValueAtIndex:self.indexOfNextBeat] CMTimeValue];
}

#pragma mark - GLViewDelegate

- (void)glView:(GLView *)view drawInRect:(CGRect)rect {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    [GLStateCache setViewportSize:view.sizeInPixels];
    [self drawBackground];
}

#pragma mark - ZoomViewDelegate

- (void)zoomViewWillBeginZooming:(ZoomView *)view {
    self.viewModel.pause = YES;
    if ([self.delegate respondsToSelector:@selector(projectPreviewWillBeginTouching:)]){
        [self.delegate projectPreviewWillBeginTouching:self];
    }
}

- (void)zoomViewDidEndZooming:(ZoomView *)view {
    self.viewModel.pause = NO;
    if ([self.delegate respondsToSelector:@selector(projectPreviewDidEndTouching:)]){
        [self.delegate projectPreviewDidEndTouching:self];
    }
}

- (void)zoomView:(ZoomView *)view didChangeVisibleRect:(CGRect)rect {
    self.viewModel.cropRect = rect;
}

@end
