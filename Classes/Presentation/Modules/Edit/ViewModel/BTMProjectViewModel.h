//
//  BTMEditViewModel.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZViewModel.h"
#import <Foundation/Foundation.h>

@class BTMProjectAudioFilterButtonModel;
@class BTMProjectVideoFilterButtonModel;
@class BTMProjectPreviewModel;
@class BTMProjectInteractor;
@class CPITunesTrack;
@class PHAsset;

NS_ASSUME_NONNULL_BEGIN

@interface BTMProjectViewModel : MVZViewModel

- (instancetype)initWithInteractor:(BTMProjectInteractor *)interactor;
- (void)viewDidLoad;
- (void)viewDidAppear;
- (void)viewWillDisappear;
- (void)willResignActive;
- (void)didBecomeActive;

- (void)setVideoAsset:(PHAsset *)videoAsset;
- (void)setVideoURL:(NSURL *)videoURL;

- (void)setAudioTrack:(CPITunesTrack *)audioTrack;
- (CPITunesTrack *)audioTrack;

@property (nonatomic, readonly) BOOL videoFilterButtonsHidden;
@property (nonatomic, readonly) NSArray<BTMProjectVideoFilterButtonModel *> *videoFilterButtonModels;
- (void)videoFilterButtonPressedAtIndex:(NSUInteger)index;

@property (nonatomic, readonly) BOOL audioFilterButtonsHidden;
@property (nonatomic, readonly) NSArray<BTMProjectAudioFilterButtonModel *> *audioFilterButtonModels;
- (void)audioFilterButtonPressedAtIndex:(NSUInteger)index;

- (void)beatDetectorTypeSelectedAtIndex:(NSUInteger)index;

@property (nonatomic, readonly) BOOL topToolbarInfoLabelHidden;
@property (nonatomic, readonly) NSString *topToolbarInfoLabelText;

@property (nonatomic, readonly) BOOL filtersUpdateButtonHidden;
@property (nonatomic, readonly) NSString *filtersUpdateButtonTitle;
- (void)filtersUpdateButtonPressed;

@property (nonatomic, readonly) BOOL renderButtonHidden;
@property (nonatomic, readonly) NSString *renderButtonTitle;
- (void)renderButtonPressed;

@property (nonatomic, readonly) NSString *videoButtonTitle;
@property (nonatomic, readonly) NSString *audioButtonTitle;
@property (nonatomic, readonly) BTMProjectPreviewModel *projectPreviewModel;
#warning audio
@property (nonatomic, readonly) id audio;
@property (nonatomic, readonly) NSTimeInterval audioDuration;

@end

NS_ASSUME_NONNULL_END
