//
//  BTMEditViewModel.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectViewModel.h"

#import "BTMProjectVideoFilterButtonModel.h"
#import "BTMProjectAudioFilterButtonModel.h"
#import "MVZActivityIndicatorModel.h"
#import "MVZAlertControllerModel.h"
#import "BTMProjectPreviewModel.h"
#import "BTMProjectInteractor.h"

#import "BTMAudioFilter.h"
#import "BTMVideoFilter.h"
#import "CPITunesTrack.h"
#import "BTMProject.h"
#import "BTMVideo.h"
#import "BTMAudio.h"

#import <ReactiveCocoa.h>

@interface BTMProjectViewModel ()

@property (nonatomic) BTMProjectInteractor *interactor;

@property (nonatomic) PHAsset *videoAsset;
@property (nonatomic) NSURL *videoURL;
@property (nonatomic) CPITunesTrack *audioTrack;

@property (nonatomic) BOOL topToolbarInfoLabelHidden;
@property (nonatomic) NSString *topToolbarInfoLabelText;

@property (nonatomic) BOOL filtersUpdateButtonHidden;
@property (nonatomic) BOOL renderButtonHidden;

@property (nonatomic) BOOL videoFilterButtonsHidden;
@property (nonatomic) NSArray<BTMProjectVideoFilterButtonModel *> *videoFilterButtonModels;

@property (nonatomic) BOOL audioFilterButtonsHidden;
@property (nonatomic) NSArray<BTMProjectAudioFilterButtonModel *> *audioFilterButtonModels;

@property (nonatomic) BTMProjectPreviewModel *projectPreviewModel;

@property (nonatomic) BTMProject *project;


@end

@implementation BTMProjectViewModel

- (instancetype)initWithInteractor:(BTMProjectInteractor *)interactor {
    self = [super init];
    if (self) {
        self.topToolbarInfoLabelHidden = YES;
        self.interactor = interactor;
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [self updateProject];
}

- (void)viewDidAppear {
    [self playProjectPreview];
}

- (void)viewWillDisappear {
    [self pauseProjectPreview];
}

- (void)willResignActive {
    [self pauseProjectPreview];
    [MVZActivityIndicatorModel dismiss];
    [[self.interactor rac_cancelRenderingSignal] subscribeCompleted:^{}];
}

- (void)didBecomeActive {
    [self playProjectPreview];
}

#pragma mark - Actions

- (void)videoFilterButtonPressedAtIndex:(NSUInteger)selectedIndex {
    [self.videoFilterButtonModels enumerateObjectsUsingBlock:^(BTMProjectVideoFilterButtonModel * _Nonnull viewModel, NSUInteger idx, BOOL * _Nonnull stop) {
        viewModel.selected = NO;
    }];
    [self.videoFilterButtonModels enumerateObjectsUsingBlock:^(BTMProjectVideoFilterButtonModel * _Nonnull viewModel, NSUInteger idx, BOOL * _Nonnull stop) {
        viewModel.selected = (idx == selectedIndex);
    }];
    self.projectPreviewModel.update();
}

- (void)audioFilterButtonPressedAtIndex:(NSUInteger)selectedIndex {
    [self.audioFilterButtonModels enumerateObjectsUsingBlock:^(BTMProjectAudioFilterButtonModel * _Nonnull viewModel, NSUInteger idx, BOOL * _Nonnull stop) {
        viewModel.selected = NO;
    }];
    [self.audioFilterButtonModels enumerateObjectsUsingBlock:^(BTMProjectAudioFilterButtonModel * _Nonnull viewModel, NSUInteger idx, BOOL * _Nonnull stop) {
        viewModel.selected = (idx == selectedIndex);
    }];
    
    self.projectPreviewModel.update();
}

- (void)beatDetectorTypeSelectedAtIndex:(NSUInteger)index {
    
    [[NSUserDefaults standardUserDefaults] setInteger:index forKey:@"beatDetectorType"]; // I'm so, sooo sorry
    
    CPITunesTrack *audioTrack = self.audioTrack;
    if(!audioTrack) {
        audioTrack = [[CPITunesTrack alloc] init];
        audioTrack.localPreviewURL = self.projectPreviewModel.audioURL;
    }
 
    if (audioTrack.localPreviewURL) {
        self.audioTrack = audioTrack; // woohoo side-effects!
    }
}

- (void)filtersUpdateButtonPressed {
    [self updateProject];
}

- (void)renderButtonPressed {
    @weakify(self)
    [[[[self.interactor rac_renderProjectSignal:self.project] initially:^{
        @strongify(self)
        [self pauseProjectPreview];
    }] finally:^{
        @strongify(self)
        [self playProjectPreview];
    }] subscribeError:^(NSError *error) {
        [MVZActivityIndicatorModel showErrorWithText:error.localizedDescription];
    } completed:^{
        [MVZActivityIndicatorModel showSuccessWithText:@"Saved to Camera Roll"];
    }];
    [[self.interactor rac_renderingProgressSignal] subscribeNext:^(id x) {
        [MVZActivityIndicatorModel showWithProgress:[x floatValue] text:@"Rendering..."];
    }];
}

#pragma mark - Setters/Getters

- (NSString *)filtersUpdateButtonTitle {
    return @"Update filters";
}

- (NSString *)renderButtonTitle {
    return @"Render";
}

- (NSString *)videoButtonTitle {
    return @"Video";
}

- (NSString *)audioButtonTitle {
    return @"Audio";
}

- (BTMProjectPreviewModel *)projectPreviewModel {
    if (!_projectPreviewModel) {
        _projectPreviewModel = [[BTMProjectPreviewModel alloc] init];
    }
    return _projectPreviewModel;
}

- (void)setProject:(BTMProject *)project {
    _project = project;
    [self updateVideoFilterButtonModels];
    [self updateAudioFilterButtonModels];
}

- (BTMAudio *)audio {
    return self.project.audio;
}

- (NSTimeInterval)audioDuration {
    return CMTimeGetSeconds(self.project.audio.duration);
}

- (void)setVideoAsset:(PHAsset *)videoAsset {
    _videoAsset = videoAsset;
    [self configureProjectWithSignal:[self.interactor rac_updateProjectVideoSignal:self.project withPHAsset:self.videoAsset]];
}

- (void)setVideoURL:(NSURL *)videoURL {
    _videoURL = videoURL;
    [[self.interactor rac_saveVideoWithURLToCameraRollSignal:self.videoURL] subscribeCompleted:^{}];
    [self configureProjectWithSignal:[self.interactor rac_updateProjectVideoSignal:self.project withURL:self.videoURL]];
}

- (void)setAudioTrack:(CPITunesTrack *)audioTrack {
    _audioTrack = audioTrack;
    [self configureProjectWithSignal:[self.interactor rac_updateProjectAudioSignal:self.project withURL:audioTrack.localPreviewURL]];
}

#pragma mark - Business Logic

- (void)playProjectPreview {
    self.projectPreviewModel.pause = NO;
    self.projectPreviewModel.drawingEnabled = YES;
    self.projectPreviewModel.update();
}

- (void)pauseProjectPreview {
    self.projectPreviewModel.pause = YES;
    self.projectPreviewModel.drawingEnabled = NO;
    self.projectPreviewModel.update();
}

- (void)updateVideoFilterButtonModels {
    @weakify(self)
    NSMutableArray<BTMProjectVideoFilterButtonModel *> *videoFilterButtonModels = [[NSMutableArray alloc] init];
    [self.project.videoFilters enumerateObjectsUsingBlock:^(BTMVideoFilter * _Nonnull filter, NSUInteger idx, BOOL * _Nonnull stop) {
        BTMProjectVideoFilterButtonModel *filterButtonModel = [[BTMProjectVideoFilterButtonModel alloc] initWithTitle:filter.name didSelectBlock:^{
            @strongify(self)
            self.project.selectedVideoFilter = filter;
            self.projectPreviewModel.reload();
            [[self.interactor rac_cacheProjectSignal:self.project] subscribeCompleted:^{}];
        }];
        if ([filter.name isEqual:self.project.selectedVideoFilter.name]) {
            filterButtonModel.selected = YES;
        }
        [videoFilterButtonModels addObject:filterButtonModel];
    }];
    if (videoFilterButtonModels.count > 0) {
        self.videoFilterButtonModels = videoFilterButtonModels.copy;
    }
}

- (void)updateAudioFilterButtonModels {
    @weakify(self)
    NSMutableArray<BTMProjectAudioFilterButtonModel *> *audioFilterButtonModels = [[NSMutableArray alloc] init];
    [self.project.audioFilters enumerateObjectsUsingBlock:^(BTMAudioFilter * _Nonnull filter, NSUInteger idx, BOOL * _Nonnull stop) {
        BTMProjectAudioFilterButtonModel *filterButtonModel = [[BTMProjectAudioFilterButtonModel alloc] initWithTitle:filter.name didSelectBlock:^{
            @strongify(self)
            self.project.selectedAudioFilter = filter;
            self.projectPreviewModel.reload();
            self.update();
            [[self.interactor rac_cacheProjectSignal:self.project] subscribeCompleted:^{}];
        }];
        if ([filter.name isEqual:self.project.selectedAudioFilter.name]) {
            filterButtonModel.selected = YES;
        }
        [audioFilterButtonModels addObject:filterButtonModel];
    }];
    if (audioFilterButtonModels.count > 0) {
        self.audioFilterButtonModels = audioFilterButtonModels.copy;
    }
}

- (void)configureProjectWithSignal:(RACSignal *)signal {
    @weakify(self)
    [[[signal initially:^{
        @strongify(self)
        [self pauseProjectPreview];
        [MVZActivityIndicatorModel show];
    }] finally:^{
        @strongify(self)
        [self playProjectPreview];
        [MVZActivityIndicatorModel dismiss];
    }] subscribeError:^(NSError *error) {
        [MVZAlertControllerModel showError:error];
    } completed:^{
        @strongify(self)
        [self.projectPreviewModel setProject:self.project];
        self.projectPreviewModel.reload();
        self.update();
    }];
}

- (void)loadProject {
    @weakify(self)
    [[[[self.interactor rac_updatedProjectSignal] initially:^{
        @strongify(self)
        [self pauseProjectPreview];
    }] finally:^{
        @strongify(self)
        [self playProjectPreview];
    }] subscribeNext:^(BTMProject *project) {
        @strongify(self)
        [self.projectPreviewModel setProject:self.project];
        self.projectPreviewModel.reload();
        self.update();
    } error:^(NSError *error) {
        [MVZAlertControllerModel showError:error];
    }];
}

- (void)updateProject {
    @weakify(self)
    [[[[self.interactor rac_updatedProjectSignal] initially:^{
        @strongify(self)
        self.topToolbarInfoLabelText = @"Fetching video filters...";
        self.topToolbarInfoLabelHidden = NO;
        self.audioFilterButtonsHidden = YES;
        self.videoFilterButtonsHidden = YES;
        self.filtersUpdateButtonHidden = YES;
        self.renderButtonHidden = YES;
        self.update();
    }] finally:^{
        @strongify(self)
        self.topToolbarInfoLabelHidden = YES;
        self.audioFilterButtonsHidden = NO;
        self.videoFilterButtonsHidden = NO;
        self.filtersUpdateButtonHidden = NO;
        self.renderButtonHidden = NO;
        self.update();
    }] subscribeNext:^(BTMProject *project) {
        @strongify(self)
        self.project = project;
        [self.projectPreviewModel setProject:self.project];
        self.projectPreviewModel.reload();
        self.update();
    } error:^(NSError *error) {
        [MVZAlertControllerModel showError:error];
        self.topToolbarInfoLabelHidden = NO;
        self.topToolbarInfoLabelText = @"Connection error. Try again";
        self.update();
    }];
}

@end
