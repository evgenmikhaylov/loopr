//
//  BTMProjectFilterButtonModel.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 23/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectVideoFilterButtonModel.h"

@interface BTMProjectVideoFilterButtonModel ()

@property (nonatomic) NSString *title;
@property (nonatomic, copy) void(^didSelectBlock)();

@end

@implementation BTMProjectVideoFilterButtonModel

- (instancetype)initWithTitle:(NSString *)title didSelectBlock:(void(^)())didSelectBlock {
    self = [super init];
    if (self) {
        self.title = title;
        self.didSelectBlock = didSelectBlock;
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    if (_selected) {
        if (self.didSelectBlock) {
            self.didSelectBlock();
        }
    }
}

@end
