//
//  BTMProjectAudioFilterButtonModel.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 24/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZViewModel.h"

@interface BTMProjectAudioFilterButtonModel : MVZViewModel

- (instancetype)initWithTitle:(NSString *)title didSelectBlock:(void(^)())didSelectBlock;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) void(^didSelectBlock)();

@property (nonatomic) BOOL selected;

@end
