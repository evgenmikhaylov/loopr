//
//  BTMEditViewController.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectViewController.h"

#import "BTMProjectAudioFilterButtonModel.h"
#import "BTMProjectVideoFilterButtonModel.h"
#import "BTMProjectPreviewModel.h"
#import "BTMProjectViewModel.h"

#import "CPITunesSearchViewController.h"
#import "CPImagePickerController.h"
#import "BTMProjectPreview.h"
#import "PlayerView.h"

#import "UIView+Frame.h"
#import "UIColor+CP.h"

@interface BTMProjectViewController ()
<
CPITunesSearchViewControllerDelegate,
CPImagePickerControllerDelegate,
BTMProjectPreviewDelegate
>

@property (nonatomic, weak) UIView *topToolbarView;
@property (nonatomic, weak) UILabel *topToolbarViewInfoLabel;
@property (nonatomic, weak) UIView *buttonsContainerView;
@property (nonatomic, weak) UIButton *filtersUpdateButton;
@property (nonatomic, weak) UIButton *renderButton;
@property (nonatomic, weak) UISegmentedControl *videoFiltersSegmentedControl;
@property (nonatomic, weak) UISegmentedControl *audioFiltersSegmentedControl;
@property (nonatomic, weak) UISegmentedControl *beatDetectorTypeSegmentedControl;
@property (nonatomic, weak) UIView *bottomToolbarView;
@property (nonatomic, weak) UIButton *videoButton;
@property (nonatomic, weak) UIButton *audioButton;
@property (nonatomic, weak) BTMProjectPreview *projectPreview;
@property (nonatomic, weak) PlayerView *playerView;

@end

@implementation BTMProjectViewController

- (void)dealloc {
    self.viewModel.projectPreviewModel.drawingEnabled = NO;
    self.viewModel.projectPreviewModel.update();
}

- (void)loadView {
    [super loadView];
    self.view = [[UIView alloc] init];
    self.view.backgroundColor = [UIColor whiteColor];
    {
        BTMProjectPreview *view = [[BTMProjectPreview alloc] init];
        view.delegate = self;
        view.backgroundColor = [UIColor grayColor];
        [self.view addSubview:view];
        self.projectPreview = view;
    }
    {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor colorWithRed:(250.0/255.0) green:(250.0/255.0) blue:(250.0/255.0) alpha:0.4];
        [self.view addSubview:view];
        self.topToolbarView = view;
    }
    {
        UILabel *label = [[UILabel alloc] init];
        label.textColor = [UIColor cpRedColor];
        [self.topToolbarView addSubview:label];
        self.topToolbarViewInfoLabel = label;
    }
    {
        UIView *view = [[UIView alloc] init];
        [self.topToolbarView addSubview:view];
        self.buttonsContainerView = view;
    }
    {
        UIButton *button = [[UIButton alloc] init];
        button.contentEdgeInsets = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
        [button setTitleColor:[UIColor cpRedColor] forState:UIControlStateNormal];
        [button setTitleColor:[[UIColor cpRedColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(filtersUpdateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonsContainerView addSubview:button];
        self.filtersUpdateButton = button;
    }
    {
        UIButton *button = [[UIButton alloc] init];
        button.contentEdgeInsets = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
        [button setTitleColor:[UIColor cpRedColor] forState:UIControlStateNormal];
        [button setTitleColor:[[UIColor cpRedColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(renderButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonsContainerView addSubview:button];
        self.renderButton = button;
    }
    {
        UISegmentedControl *view = [[UISegmentedControl alloc] init];
        view.tintColor = [UIColor cpRedColor];
        [view addTarget:self action:@selector(filtersSegmentedControBTMessed:) forControlEvents:UIControlEventValueChanged];
        [self.topToolbarView addSubview:view];
        self.videoFiltersSegmentedControl = view;
    }
    {
        UISegmentedControl *view = [[UISegmentedControl alloc] init];
        view.tintColor = [UIColor cpRedColor];
        [view addTarget:self action:@selector(beatsSegmentedControBTMessed:) forControlEvents:UIControlEventValueChanged];
        [self.topToolbarView addSubview:view];
        self.audioFiltersSegmentedControl = view;
    }
    {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor colorWithRed:(250.0/255.0) green:(250.0/255.0) blue:(250.0/255.0) alpha:0.4];
        [self.view addSubview:view];
        self.bottomToolbarView = view;
    }
    {
        UISegmentedControl *view = [[UISegmentedControl alloc] initWithItems:@[@"D", @"Q-PD", @"Q-CSD", @"Q-BB", @"BBC-O", @"AC", @"X"]];
        view.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"beatDetectorType"];
        view.tintColor = [UIColor cpRedColor];
        [view addTarget:self action:@selector(beatDetectorTypeSegmentedControBTMessed:) forControlEvents:UIControlEventValueChanged];
        [self.bottomToolbarView addSubview:view];
        self.beatDetectorTypeSegmentedControl = view;
    }
    {
        UIButton *button = [[UIButton alloc] init];
        button.contentEdgeInsets = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
        [button setTitleColor:[UIColor cpRedColor] forState:UIControlStateNormal];
        [button setTitleColor:[[UIColor cpRedColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(videoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomToolbarView addSubview:button];
        self.videoButton = button;
    }
    {
        UIButton *button = [[UIButton alloc] init];
        button.contentEdgeInsets = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
        [button setTitleColor:[UIColor cpRedColor] forState:UIControlStateNormal];
        [button setTitleColor:[[UIColor cpRedColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(audioButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomToolbarView addSubview:button];
        self.audioButton = button;
    }
    {
        PlayerView *view = [[PlayerView alloc] init];
        view.windowDuration = 5.0;
        view.backgroundColor = [UIColor colorWithRed:(250.0/255.0) green:(250.0/255.0) blue:(250.0/255.0) alpha:0.4];
        [self.view addSubview:view];
        self.playerView = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    [self bindViewModel];
    [self.viewModel viewDidLoad];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    self.topToolbarView.left = 0.0;
    self.topToolbarView.rightForFixedLeft = self.view.width;
    self.topToolbarView.top = 0.0;
    
    self.topToolbarViewInfoLabel.left = 50.0;
    self.topToolbarViewInfoLabel.rightForFixedLeft = self.topToolbarViewInfoLabel.superview.width - 50.0;
    self.topToolbarViewInfoLabel.height = self.topToolbarView.height;
    [self.topToolbarViewInfoLabel sizeToFitSize:self.topToolbarViewInfoLabel.size];
    [self.topToolbarViewInfoLabel centerVerticaly];
    [self.topToolbarViewInfoLabel centerHorizontaly];
    
    self.videoFiltersSegmentedControl.left = 5.0;
    self.videoFiltersSegmentedControl.rightForFixedLeft = self.videoFiltersSegmentedControl.superview.width - 5.0;
    self.videoFiltersSegmentedControl.top = 5.0;
    self.videoFiltersSegmentedControl.height = 34.0;

    self.audioFiltersSegmentedControl.left = 5.0;
    self.audioFiltersSegmentedControl.rightForFixedLeft = self.audioFiltersSegmentedControl.superview.width - 5.0;
    self.audioFiltersSegmentedControl.top = self.videoFiltersSegmentedControl.bottom + 5.0;
    self.audioFiltersSegmentedControl.height = 34.0;
    
    [self.filtersUpdateButton sizeToFit];
    self.filtersUpdateButton.left = 0.0;
    self.filtersUpdateButton.top = 0.0;

    [self.renderButton sizeToFit];
    self.renderButton.left = self.filtersUpdateButton.right + 10.0;
    self.renderButton.top = 0.0;
    
    self.buttonsContainerView.top = self.audioFiltersSegmentedControl.bottom + 5.0;
    self.buttonsContainerView.width = self.renderButton.right;
    self.buttonsContainerView.height = MAX(self.filtersUpdateButton.height, self.renderButton.height);
    [self.buttonsContainerView centerHorizontaly];
    
    self.topToolbarView.height = self.buttonsContainerView.bottom + 5.0;
    
    self.bottomToolbarView.left = 0.0;
    self.bottomToolbarView.rightForFixedLeft = self.view.width;
    self.bottomToolbarView.height = 88.0;
    self.bottomToolbarView.bottomForFixedHeight = self.view.height;
    
    self.playerView.left = 0.0;
    self.playerView.rightForFixedLeft = self.view.width;
    self.playerView.height = 50.0;
    self.playerView.bottomForFixedHeight = self.bottomToolbarView.top;
    
    self.beatDetectorTypeSegmentedControl.width = self.bottomToolbarView.width;
    self.beatDetectorTypeSegmentedControl.height = 44.0;
    
    self.videoButton.left = 50.0;
    [self.videoButton sizeToFitSize:self.view.size];
    self.videoButton.top = 44.0;
    self.videoButton.height = self.bottomToolbarView.height / 2.0;
    
    [self.audioButton sizeToFitSize:self.view.size];
    self.audioButton.top = 44.0;
    self.audioButton.rightForFixedWidth = self.view.width - 50.0;
    self.audioButton.height = self.bottomToolbarView.height / 2.0;

    self.projectPreview.left = 0.0;
    self.projectPreview.rightForFixedLeft = self.view.width;
    self.projectPreview.top = 0.0;
    self.projectPreview.bottomForFixedTop = self.view.height;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [self.viewModel viewDidAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [self.viewModel viewWillDisappear];
}

#pragma mark - Setters/Getters

- (void)setViewModel:(BTMProjectViewModel *)viewModel {
    _viewModel = viewModel;
    [self bindViewModel];
}

#pragma mark - Actions

- (void)filtersUpdateButtonPressed:(id)sender {
    [self.viewModel filtersUpdateButtonPressed];
}

- (void)renderButtonPressed:(id)sender {
    [self.viewModel renderButtonPressed];
}

- (void)videoButtonPressed:(id)sender {
    self.viewModel.projectPreviewModel.pause = YES;
    CPImagePickerController *viewController = [[CPImagePickerController alloc] init];
    viewController.delegate = self;
    viewController.sourceType = CPImagePickerControllerSourceTypePhotoLibrary;
    viewController.mediaType = CPImagePickerControllerMediaTypeVideo;
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)audioButtonPressed:(id)sender {
    self.viewModel.projectPreviewModel.pause = YES;
    CPITunesSearchViewController *viewController = [[CPITunesSearchViewController alloc] init];
    viewController.delegate = self;
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)filtersSegmentedControBTMessed:(id)sender {
    [self.viewModel videoFilterButtonPressedAtIndex:self.videoFiltersSegmentedControl.selectedSegmentIndex];
}

- (void)beatsSegmentedControBTMessed:(id)sender {
    [self.viewModel audioFilterButtonPressedAtIndex:self.audioFiltersSegmentedControl.selectedSegmentIndex];
}

- (void)beatDetectorTypeSegmentedControBTMessed:(id)sender {
    [self.viewModel beatDetectorTypeSelectedAtIndex:self.beatDetectorTypeSegmentedControl.selectedSegmentIndex];
}

#pragma mark - ViewModel

- (void)bindViewModel {
    if (!self.isViewLoaded) {
        return;
    }
    __weak typeof (self) weakSelf = self;
    self.viewModel.update = ^{
        [weakSelf bindViewModel];
        [weakSelf.view setNeedsLayout];
        [weakSelf.view layoutIfNeeded];
    };
    self.topToolbarViewInfoLabel.hidden = self.viewModel.topToolbarInfoLabelHidden;
    self.topToolbarViewInfoLabel.text = self.viewModel.topToolbarInfoLabelText;
    
    self.filtersUpdateButton.hidden = self.viewModel.filtersUpdateButtonHidden;
    [self.filtersUpdateButton setTitle:self.viewModel.filtersUpdateButtonTitle forState:UIControlStateNormal];

    self.renderButton.hidden = self.viewModel.renderButtonHidden;
    [self.renderButton setTitle:self.viewModel.renderButtonTitle forState:UIControlStateNormal];

    [self.videoButton setTitle:self.viewModel.videoButtonTitle forState:UIControlStateNormal];
    [self.audioButton setTitle:self.viewModel.audioButtonTitle forState:UIControlStateNormal];
    self.projectPreview.viewModel = self.viewModel.projectPreviewModel;
    
    self.videoFiltersSegmentedControl.hidden = self.viewModel.videoFilterButtonsHidden;
    [self.videoFiltersSegmentedControl removeAllSegments];
    [self.viewModel.videoFilterButtonModels enumerateObjectsUsingBlock:^(BTMProjectVideoFilterButtonModel * _Nonnull viewModel, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.videoFiltersSegmentedControl insertSegmentWithTitle:viewModel.title atIndex:idx animated:NO];
        if (viewModel.selected) {
            self.videoFiltersSegmentedControl.selectedSegmentIndex = idx;
        }
    }];

    self.audioFiltersSegmentedControl.hidden = self.viewModel.audioFilterButtonsHidden;
    [self.audioFiltersSegmentedControl removeAllSegments];
    [self.viewModel.audioFilterButtonModels enumerateObjectsUsingBlock:^(BTMProjectAudioFilterButtonModel * _Nonnull viewModel, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.audioFiltersSegmentedControl insertSegmentWithTitle:viewModel.title atIndex:idx animated:NO];
        if (viewModel.selected) {
            self.audioFiltersSegmentedControl.selectedSegmentIndex = idx;
        }
    }];
    
    if (self.viewModel.audio) {
        self.playerView.hidden = NO;
        self.playerView.duration = self.viewModel.audioDuration;
        self.playerView.audio = self.viewModel.audio;
    }
    else {
        self.playerView.hidden = YES;
    }
}

#pragma mark - CPITunesSearchViewControllerDelegate

- (void)iTunesSearchViewController:(CPITunesSearchViewController *)controller didPickTrack:(CPITunesTrack *)track {
    self.viewModel.audioTrack = track;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)iTunesSearchViewControllerDidCancel:(CPITunesSearchViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CPImagePickerControllerDelegate

- (void)cpImagePickerController:(CPImagePickerController*)imagePickerController didSelectContentWithMediaInfo:(NSDictionary *)mediaInfo {
    if (imagePickerController.sourceType == CPImagePickerControllerSourceTypePhotoLibrary) {
        PHAsset *asset = mediaInfo[CPImagePickerControllerMediaInfoAssetKey];
        self.viewModel.videoAsset = asset;
    }
    else if (imagePickerController.sourceType == CPImagePickerControllerSourceTypeCamera) {
        NSURL *videoURL = [NSURL fileURLWithPath:mediaInfo[CPImagePickerControllerMediaInfoVideoPathKey]];
        self.viewModel.videoURL = videoURL;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cpImagePickerControllerAddContentFromCameraButtonPressed:(CPImagePickerController*)imagePickerController {
    CPImagePickerController *viewController = [[CPImagePickerController alloc] init];
    viewController.delegate = self;
    viewController.sourceType = CPImagePickerControllerSourceTypeCamera;
    viewController.mediaType = CPImagePickerControllerMediaTypeVideo;
    [imagePickerController presentViewController:viewController animated:YES completion:nil];
}

- (void)cpImagePickerControllerDidCancel:(CPImagePickerController *)imagePickerController {
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BTMProjectPreviewDelegate

- (void)projectPreview:(BTMProjectPreview *)view currentTimeChanged:(NSTimeInterval)time {
    if (self.playerView.audio) {
        self.playerView.currentTime = time;
    }
}

#pragma mark - Notifications

- (void)willResignActive:(NSNotification *)notification {
    [self.viewModel willResignActive];
}

- (void)didBecomeActive:(NSNotification *)notification {
    [self.viewModel didBecomeActive];
}

@end
