//
//  BTMEditViewController.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTMProjectViewModel;

@interface BTMProjectViewController : UIViewController

@property (nonatomic) BTMProjectViewModel *viewModel;

@end
