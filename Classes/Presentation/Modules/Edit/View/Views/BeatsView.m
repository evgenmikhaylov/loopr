//
//  BeatsView.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 11/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BeatsView.h"
#import "UIView+Frame.h"

@implementation BeatsView

#pragma mark - Drawings

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self drawBeatsInRect:rect];
}

- (void)drawBeatsInRect:(CGRect)rect {
    __block NSUInteger beatIndex = 1;
    [self.beatsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull beat, NSUInteger idx, BOOL * _Nonnull stop) {
        if (beat.floatValue > 0.0) {
            double time = ((double)idx / (double)self.beatsArray.count) * self.duration;
            CGFloat lineWidth = 1.0;
            CGFloat x = self.windowWidth * (time / self.windowDuration) - lineWidth / 2.0;
            CGPoint fromPoint = CGPointMake(x, 5.0);
            CGPoint toPoint = CGPointMake(x, self.height - 15.0);
            [self drawLineWithColor:[UIColor redColor] width:lineWidth fromPoint:fromPoint toPoint:toPoint];
            [self drawText:@(beatIndex).stringValue withColor:[UIColor redColor] fromPoint:toPoint];
            if (beatIndex % self.phraseLength == 0) {
                beatIndex = 1;
            } else {
                beatIndex++;
            }
        }
    }];
}

- (void)drawLineWithColor:(UIColor *)color width:(CGFloat)width fromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextSetLineWidth(context, width);
    CGContextMoveToPoint(context, fromPoint.x, fromPoint.y);
    CGContextAddLineToPoint(context, toPoint.x, toPoint.y);
    CGContextStrokePath(context);
}

- (void)drawText:(NSString *)text withColor:(UIColor *)color fromPoint:(CGPoint)point {
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.alignment = NSTextAlignmentCenter;
    NSDictionary* textFontAttributes = @{
                                         NSFontAttributeName: [UIFont systemFontOfSize:12.0],
                                         NSForegroundColorAttributeName: [UIColor redColor],
                                         };
    [text drawAtPoint:point withAttributes:textFontAttributes];
}

#pragma mark - Setters/Getters

- (void)setBeatsArray:(NSArray<NSNumber *> *)beatsArray {
    _beatsArray = beatsArray;
    [self setNeedsDisplay];
}

@end
