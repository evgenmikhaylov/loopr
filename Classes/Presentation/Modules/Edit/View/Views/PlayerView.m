//
//  PlayerView.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 21/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "PlayerView.h"
#import "BeatsView.h"
#import "BTMAudio.h"
#import "UIView+Frame.h"


@interface PlayerView ()

@property (nonatomic) UIScrollView *scrollView;

@property (nonatomic) BeatsView *beatsView;
@property (nonatomic) UIView *lineView;
@property (nonatomic) UIView *circleView;

@end

@implementation PlayerView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.duration = 0.0;
        self.windowDuration = 10.0;
        
        self.clipsToBounds = NO;
        self.backgroundColor = [UIColor lightGrayColor];
        
        self.scrollView = [[UIScrollView alloc] init];
        self.scrollView.userInteractionEnabled = NO;
        self.scrollView.clipsToBounds = NO;
        [self addSubview:self.scrollView];
        
        self.beatsView = [[BeatsView alloc] init];
        self.beatsView.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:self.beatsView];
        
        self.lineView = [[UIView alloc] init];
        self.lineView.backgroundColor = [UIColor blackColor];
        self.lineView.width = 1.0;
        [self.scrollView addSubview:self.lineView];
        
        self.circleView = [[UIView alloc] init];
        self.circleView.backgroundColor = [UIColor whiteColor];
        self.circleView.width = 20.0;
        self.circleView.height = self.circleView.width;
        self.circleView.layer.cornerRadius = self.circleView.width / 2.0;
        self.circleView.layer.borderWidth = 1.0;
        self.circleView.layer.borderColor = [UIColor blackColor].CGColor;
        [self.scrollView addSubview:self.circleView];
    }
    return self;
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];

    self.scrollView.left = 0.0;
    self.scrollView.top = 0.0;
    self.scrollView.size = self.size;
    
    self.beatsView.top = 0.0;
    self.beatsView.left = 0.0;
    self.beatsView.width = self.width * self.duration / self.windowDuration;
    self.beatsView.height = self.scrollView.height;
    self.beatsView.windowWidth = self.width;
    
    self.scrollView.contentSize = (CGSize){
        .width = self.beatsView.width,
        .height = self.height,
    };
    
    [self layoutLineView];
    [self layoutCircleView];    
}

- (void)layoutLineView {
    self.lineView.top = 0.0;
    self.lineView.bottomForFixedTop = self.height;
    self.lineView.centerX = self.width * (self.currentTime / self.windowDuration);
}

- (void)layoutCircleView {
    self.circleView.centerX = self.lineView.centerX;
    
    NSArray<NSNumber *> *beatsArray = self.beatsView.beatsArray;
    if (!beatsArray.count) {
        return;
    }
    
    int index = beatsArray.count * _currentTime / self.duration;
    int previosBeatIndex = index + 1;
    BOOL found = NO;
    while (found == NO) {
        previosBeatIndex--;
        if (previosBeatIndex < 0) {
            found = YES;
            break;
        }
        if (beatsArray[previosBeatIndex].floatValue > 0) {
            found = YES;
            break;
        };
    }
    
    found = NO;
    int nextBeatIndex = index - 1;
    while (found == NO) {
        nextBeatIndex++;
        if (nextBeatIndex >= beatsArray.count) {
            found = YES;
            break;
        }
        if(beatsArray[nextBeatIndex].floatValue > 0) {
            found = YES;
            break;
        }
    }
    
    float gravityFactor = 0.2;
    int beatsInterval = nextBeatIndex - previosBeatIndex;
    int relativeBeatsIndex = index - previosBeatIndex;
    float height = 240.0 * (gravityFactor * relativeBeatsIndex * (beatsInterval - relativeBeatsIndex) / (float)beatsArray.count);
    
    self.circleView.centerY = self.height - 5.0 - height;
}

- (void)updateScrollViewContentOffset {
    CGFloat offset = self.lineView.centerX - self.width / 2;
    if (offset > 0) {
        self.scrollView.contentOffset = (CGPoint){
            .x = offset,
        };
    }
    else {
        self.scrollView.contentOffset = (CGPoint){
            .x = 0.0,
        };
    }
}

#pragma mark - Setters/Getters

- (void)setCurrentTime:(NSTimeInterval)currentTime {
    _currentTime = currentTime;
    [self layoutLineView];
    [self layoutCircleView];
    [self updateScrollViewContentOffset];
}

- (void)setDuration:(NSTimeInterval)duration {
    _duration = duration;
    self.beatsView.duration = _duration;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setWindowDuration:(NSTimeInterval)windowDuration {
    _windowDuration = windowDuration;
    self.beatsView.windowDuration = _windowDuration;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setAudio:(BTMAudio *)audio {
    _audio = audio;
    self.beatsView.phraseLength = _audio.phraseLength;
    self.beatsView.beatsArray = _audio.filteredBeatsArray;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - Actions

- (void)tapGestureAction:(UITapGestureRecognizer *)gestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(playerViewTapped:)]) {
        [self.delegate playerViewTapped:self];
    }
}

- (void)panGestureAction:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:self];
    NSTimeInterval time = location.x * self.duration / self.width;
    if ([self.delegate respondsToSelector:@selector(playerViewPanned:withTime:)]) {
        [self.delegate playerViewPanned:self withTime:time];
    }
}

@end
