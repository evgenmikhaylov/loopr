//
//  BeatsView.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 11/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeatsView : UIView

@property (nonatomic) NSUInteger phraseLength;
@property (nonatomic) CGFloat windowWidth;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSTimeInterval windowDuration;

@property (nonatomic, copy) NSArray<NSNumber *> *beatsArray;

@end
