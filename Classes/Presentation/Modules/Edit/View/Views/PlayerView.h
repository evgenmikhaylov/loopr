//
//  PlayerView.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 21/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerView;

@protocol PlayerViewDelegate <NSObject>

- (void)playerViewTapped:(PlayerView *)playerView;
- (void)playerViewPanned:(PlayerView *)playerView withTime:(NSTimeInterval)time;

@end

@class BTMAudio;

@interface PlayerView : UIView

@property (nonatomic, weak) id<PlayerViewDelegate> delegate;
@property (nonatomic) NSTimeInterval currentTime;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSTimeInterval windowDuration;
@property (nonatomic, weak) BTMAudio *audio;

@end
