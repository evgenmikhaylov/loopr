//
//  BTMEditInteractor.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectInteractor.h"

#import "BTMProjectAudioFiltersFactory.h"
#import "BTMProjectVideoFiltersFactory.h"
#import "BTMBeatDetectionService.h"
#import "BTMProjectRenderService.h"
#import "BTMAudioStorage.h"
#import "BTMVideoStorage.h"
#import "BTMCacheService.h"
#import "MVZBeatDetector.h"
#import "BTMProject.h"
#import "BTMVideo.h"
#import "BTMAudio.h"

#import "PHAsset+Additions.h"

#import <ReactiveCocoa.h>
#import <AFNetworking.h>


NSString * const BTMProjectCacheKey = @"BTM_project";

@interface BTMProjectInteractor ()

@property (nonatomic) BTMVideoStorage *videoStorage;
@property (nonatomic) BTMAudioStorage *audioStorage;
@property (nonatomic) BTMBeatDetectionService *beatDetectionService;
@property (nonatomic) BTMCacheService *cacheService;
@property (nonatomic) BTMProjectRenderService *projectRenderService;

@end

@implementation BTMProjectInteractor

- (RACSignal *)rac_cacheProjectSignal:(BTMProject *)project {
    return [self.cacheService rac_cacheObjectSignal:project forKey:BTMProjectCacheKey];
}

- (RACSignal *)rac_projectSignal {
    @weakify(self)
    return [[self.cacheService rac_cachedObjectForKeySignal:BTMProjectCacheKey] flattenMap:^RACStream *(BTMProject *project) {
        @strongify(self)
        if (project == nil) {
            project = [[BTMProject alloc] init];
        }
        project.audioFilters= [BTMProjectAudioFiltersFactory projectAudioFilters];
        if ([[NSFileManager defaultManager] fileExistsAtPath:project.videoFiltersPath]) {
            NSURL *url = [NSURL fileURLWithPath:project.videoFiltersPath];
            project.videoFilters = [BTMProjectVideoFiltersFactory projectVideoFiltersWithFile:url];
            return [RACSignal return:project];
        }
        else {
            return [self rac_updateVideoFiltersForProjectSignal:project];
        }
    }];
}

- (RACSignal *)rac_updatedProjectSignal {
    @weakify(self)
    return [[self.cacheService rac_cachedObjectForKeySignal:BTMProjectCacheKey] flattenMap:^RACStream *(BTMProject *project) {
        @strongify(self)
        if (project == nil) {
            project = [[BTMProject alloc] init];
        }
        project.audioFilters= [BTMProjectAudioFiltersFactory projectAudioFilters];
        return [self rac_updateVideoFiltersForProjectSignal:project];
    }];
}

#pragma mark - Filters

- (RACSignal *)rac_updateVideoFiltersForProjectSignal:(BTMProject *)project {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURL *remoteURL = [NSURL URLWithString:@"https://dl.dropboxusercontent.com/s/2xir8qxf3ytszpe/VideoFilters.json?dl=0"];
        NSURL *localURL = [NSURL fileURLWithPath:project.videoFiltersPath];
        [[NSFileManager defaultManager] removeItemAtURL:localURL error:nil];
        NSURLRequest *request = [NSURLRequest requestWithURL:remoteURL];
        NSURLSessionDownloadTask *downloadTask = [[AFHTTPSessionManager manager] downloadTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
            return localURL;
        } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                project.videoFilters = [BTMProjectVideoFiltersFactory projectVideoFiltersWithFile:filePath];
                [subscriber sendNext:project];
                [subscriber sendCompleted];
            }
        }];
        [downloadTask resume];
        return nil;
    }];
}

#pragma mark - Video

- (RACSignal *)rac_updateProjectVideoSignal:(BTMProject *)project withPHAsset:(PHAsset *)asset {
    @weakify(self)
    return [[RACSignal defer:^RACSignal *{
        @strongify(self)
        CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMake(600 * asset.duration, 600));
        return [self.videoStorage rac_saveVideoWithPHAssetSignal:asset
                                                          toFile:project.videoPath 
                                                 withMaximumSize:960.0
                                                       timeRange:timeRange
                                                 framesPerSecond:project.videoFramesPerSecond];
    }] doCompleted:^{
        @strongify(self)
        BTMVideo *video = [[BTMVideo alloc] init];
        video.duration = CMTimeMake(600 * asset.duration, 600);
        project.video = video;
        [[self.cacheService rac_cacheObjectSignal:project forKey:BTMProjectCacheKey] subscribeCompleted:^{}];
    }];
}

- (RACSignal *)rac_updateProjectVideoSignal:(BTMProject *)project withURL:(NSURL *)url {
    @weakify(self)
    __block CMTime duration;
    return [[RACSignal defer:^RACSignal *{
        @strongify(self)
        AVURLAsset *asset = [AVURLAsset assetWithURL:url];
        duration = asset.duration;
        CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMake(600 * CMTimeGetSeconds(duration), 600));
        return [self.videoStorage rac_saveVideoWithAVAssetSignal:asset
                                                          toFile:project.videoPath 
                                                 withMaximumSize:640.0
                                                       timeRange:timeRange
                                                 framesPerSecond:project.videoFramesPerSecond];
    }] doCompleted:^{
        @strongify(self)
        BTMVideo *video = [[BTMVideo alloc] init];
        video.duration = duration;
        project.video = video;
        [[self.cacheService rac_cacheObjectSignal:project forKey:BTMProjectCacheKey] subscribeCompleted:^{}];
    }];
}

- (RACSignal *)rac_saveVideoWithURLToCameraRollSignal:(NSURL *)videoURL {
    return [[PHAsset rac_saveVideoToCameraRollSignal:videoURL withLocation:nil] deliverOnMainThread];
}

#pragma mark - Audio

- (RACSignal *)rac_updateProjectAudioSignal:(BTMProject *)project withURL:(NSURL *)url {
    @weakify(self)
    __block CMTime duration;
    return [[[RACSignal defer:^RACSignal *{
        @strongify(self)
        AVURLAsset *asset = [AVURLAsset assetWithURL:url];
        duration = asset.duration;
        return [self.audioStorage rac_saveAudioWithAVAssetSignal:asset toFile:project.audioPath];
    }] flattenMap:^RACStream *(NSString *filePath) {
        @strongify(self)
        return [self.beatDetectionService rac_processAudioWithURLSignal:[NSURL fileURLWithPath:filePath]];
    }] doNext:^(MVZBeatDetector *beatDetector) {
        @strongify(self)
        BTMAudio *audio = [[BTMAudio alloc] init];
        audio.duration = duration;
        audio.beatsArray = beatDetector.beatsArray;
        project.audio = audio;
        [[self.cacheService rac_cacheObjectSignal:project forKey:BTMProjectCacheKey] subscribeCompleted:^{}];
    }];
}

#pragma mark - Rendering

- (RACSignal *)rac_renderProjectSignal:(BTMProject *)project {
    __weak typeof (self) weakSelf = self;
    return [[self.projectRenderService rac_renderProjectSignal:project] then:^RACSignal *{
        return [weakSelf rac_saveVideoWithURLToCameraRollSignal:[NSURL fileURLWithPath:project.exportPath]];
    }];
}

- (RACSignal *)rac_renderingProgressSignal {
    return [self.projectRenderService rac_renderingProgressSignal];
}

- (RACSignal *)rac_cancelRenderingSignal {
    return [self.projectRenderService rac_cancelRenderingSignal];
}


@end
