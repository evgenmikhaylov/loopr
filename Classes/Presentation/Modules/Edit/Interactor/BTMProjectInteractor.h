//
//  BTMEditInteractor.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMBeatDetectionService;
@class BTMProjectRenderService;
@class BTMVideoStorage;
@class BTMAudioStorage;
@class BTMCacheService;
@class BTMProject;
@class RACSignal;
@class BTMVideo;
@class PHAsset;

@interface BTMProjectInteractor : NSObject

- (void)setVideoStorage:(BTMVideoStorage *)videoStorage;
- (void)setAudioStorage:(BTMAudioStorage *)audioStorage;
- (void)setBeatDetectionService:(BTMBeatDetectionService *)beatDetectionService;
- (void)setCacheService:(BTMCacheService *)cacheService;
- (void)setProjectRenderService:(BTMProjectRenderService *)projectRenderService;


- (RACSignal *)rac_cacheProjectSignal:(BTMProject *)project;
- (RACSignal *)rac_projectSignal;
- (RACSignal *)rac_updatedProjectSignal;

- (RACSignal *)rac_updateProjectVideoSignal:(BTMProject *)project withPHAsset:(PHAsset *)asset;
- (RACSignal *)rac_updateProjectVideoSignal:(BTMProject *)project withURL:(NSURL *)url;
- (RACSignal *)rac_saveVideoWithURLToCameraRollSignal:(NSURL *)videoURL;

- (RACSignal *)rac_updateProjectAudioSignal:(BTMProject *)project withURL:(NSURL *)url;

- (RACSignal *)rac_renderProjectSignal:(BTMProject *)project;
- (RACSignal *)rac_renderingProgressSignal;
- (RACSignal *)rac_cancelRenderingSignal;

@end
