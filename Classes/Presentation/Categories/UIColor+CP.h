//
//  UIColor+CP.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CP)

+ (UIColor*)cpRedColor;
+ (UIColor*)cpSplashRedColor;
+ (UIColor*)cpLightRedColor;
+ (UIColor*)cpDarkGrayColor;
+ (UIColor*)cpLightGrayColor;
+ (UIColor*)cpPlotLightGrayColor;
+ (UIColor*)cpPlotDarkGrayColor;
+ (UIColor*)cpLightBlackColor;
+ (UIColor*)cpLightGrayDividerColor;

@end
