//
//  UIColor+CP.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "UIColor+CP.h"

@implementation UIColor (CP)

+ (UIColor*)cpRedColor
{
    return [UIColor colorWithRed:(239.0f/255.0f) green:(83.0f/255.0f) blue:(80.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpSplashRedColor
{
    return [UIColor colorWithRed:(242.0f/255.0f) green:(45.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpLightRedColor
{
    return [UIColor colorWithRed:(244.0f/255.0f) green:(153.0f/255.0f) blue:(151.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpDarkGrayColor
{
    return [UIColor colorWithRed:(69.0f/255.0f) green:(90.0f/255.0f) blue:(100.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpLightGrayColor
{
    return [UIColor colorWithRed:(219.0f/255.0f) green:(219.0f/255.0f) blue:(219.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpPlotLightGrayColor
{
    return [UIColor colorWithRed:(225.0f/255.0f) green:(225.0f/255.0f) blue:(225.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpLightBlackColor
{
    return [UIColor colorWithRed:(63.0f/255.0f) green:(80.0f/255.0f) blue:(91.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpPlotDarkGrayColor
{
    return [UIColor colorWithRed:(199.0f/255.0f) green:(199.0f/255.0f) blue:(207.0f/255.0f) alpha:1.0f];
}

+ (UIColor*)cpLightGrayDividerColor
{
    return [UIColor colorWithRed:(197.0f/255.0f) green:(202.0f/255.0f) blue:(205.0f/255.0f) alpha:1.0f];
}



@end
