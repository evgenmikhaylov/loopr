//
//  UIFont+CP.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 8/10/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (CP)

+ (UIFont*)missionScriptFontWithSize:(CGFloat)fontSize;
+ (UIFont*)robotoMediumFontWithSize:(CGFloat)fontSize;
+ (UIFont*)robotoLightFontWithSize:(CGFloat)fontSize;
+ (UIFont*)robotoRegularFontWithSize:(CGFloat)fontSize;

@end
