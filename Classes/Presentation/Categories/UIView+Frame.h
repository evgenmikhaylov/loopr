//
//  UIView+Frame.h
//  Collaborate
//
//  Created by Evgeny Mikhaylov on 17/03/16.
//  Copyright © 2016 Trusted Insight, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGSize size;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat left;
@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat rightForFixedWidth;
@property (nonatomic) CGFloat rightForFixedLeft;
@property (nonatomic, readonly) CGFloat right;
@property (nonatomic) CGFloat bottomForFixedHeight;
@property (nonatomic) CGFloat bottomForFixedTop;
@property (nonatomic, readonly) CGFloat bottom;
@property (nonatomic) CGFloat centerX;
@property (nonatomic) CGFloat centerY;
@property (nonatomic) UIEdgeInsets margins;

@property (nonatomic) UIEdgeInsets edgeInsets;
@property (nonatomic) CGFloat topEdgeInset;
@property (nonatomic) CGFloat leftEdgeInset;
@property (nonatomic) CGFloat bottomEdgeInset;
@property (nonatomic) CGFloat rightEdgeInset;
@property (nonatomic, readonly) CGSize sizeWithEdgeInsets;

- (void)centerHorizontaly;
- (void)centerVerticaly;
- (void)sizeToFitSize:(CGSize)size;

- (CGFloat)dynamicOffsetFromView:(UIView *)view withConstant:(CGFloat)constant;
- (void)connectLeftToViewRight:(UIView *)view withOffset:(CGFloat)offset;
- (void)connectLeftToViewRight:(UIView *)view;
- (void)connectTopToViewBottom:(UIView *)view withOffset:(CGFloat)offset;
- (void)connectTopToViewBottom:(UIView *)view;

@end
