//
//  UIFont+CP.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 8/10/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "UIFont+CP.h"

@implementation UIFont (CP)

+ (UIFont*)missionScriptFontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Mission-Script" size:fontSize];
}

+ (UIFont*)robotoMediumFontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Roboto-Medium" size:fontSize];
}

+ (UIFont*)robotoLightFontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Roboto-Light" size:fontSize];
}

+ (UIFont*)robotoRegularFontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Roboto-Regular" size:fontSize];
}

@end
