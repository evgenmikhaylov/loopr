//
//  UIView+Frame.m
//  Collaborate
//
//  Created by Evgeny Mikhaylov on 17/03/16.
//  Copyright © 2016 Trusted Insight, Inc. All rights reserved.
//

#import "UIView+Frame.h"
#import <objc/runtime.h>

@implementation UIView (Frame)

- (void)setOrigin:(CGPoint)origin {
    
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin {
    
    return self.frame.origin;
}

- (void)setSize:(CGSize)size {
    
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size {
    
    return self.frame.size;
}

- (void)setWidth:(CGFloat)width {
    
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)width {
    
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height {
    
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height {
    
    return self.frame.size.height;
}

- (void)setX:(CGFloat)x {
    
    CGRect rect = self.frame;
    rect.origin.x = x;
    [self setFrame:rect];
}

- (CGFloat)x {
    
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y {
    
    CGRect rect = self.frame;
    rect.origin.y = y;
    [self setFrame:rect];
}

- (CGFloat)y {
    
    return self.frame.origin.y;
}

- (void)setLeft:(CGFloat)x {
    
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)left {
    
    return self.frame.origin.x;
}

- (void)setTop:(CGFloat)y {
    
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)top {
    
    return self.frame.origin.y;
}

- (void)setRightForFixedWidth:(CGFloat)rightForFixedWidth {
    
    self.left = rightForFixedWidth - self.width;
}

- (void)setRightForFixedLeft:(CGFloat)rightForFixedLeft {
    
    self.width = rightForFixedLeft - self.left;
}

- (CGFloat)rightForFixedWidth {
    
    return self.right;
}

- (CGFloat)rightForFixedLeft {
    
    return self.right;
}

- (CGFloat)right {
    
    return self.left + self.width;
}

- (void)setBottomForFixedHeight:(CGFloat)bottomForFixedHeight {
    
    self.top = bottomForFixedHeight - self.height;
}

- (void)setBottomForFixedTop:(CGFloat)bottomForFixedTop {
    
    self.height = bottomForFixedTop - self.top;
}

- (CGFloat)bottomForFixedHeight {
    
    return self.height;
}

- (CGFloat)bottomForFixedTop {
    
    return self.height;
}

- (CGFloat)bottom {
    
    return self.top + self.height;
}

- (void)setCenterX:(CGFloat)centerX {
    
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX {
    
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY {
    
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY {
    
    return self.center.y;
}

- (void)setMargins:(UIEdgeInsets)margins {
    
    CGRect frame = CGRectMake(margins.left,
                              margins.top,
                              self.superview.width - margins.left - margins.right,
                              self.superview.height - margins.top - margins.bottom);
    if (frame.size.width < 0) {
        frame.size.width = 0;
    }
    if (frame.size.height < 0) {
        frame.size.height = 0;
    }
    self.frame = frame;
}

- (UIEdgeInsets)margins {
    
    return (UIEdgeInsets){
        .top = self.top,
        .left = self.left,
        .bottom = self.superview.height - self.bottom,
        .right = self.superview.width - self.right,
    };
}


- (void)setEdgeInsets:(UIEdgeInsets)edgeInsets {
    objc_setAssociatedObject(self, @selector(edgeInsets), [NSValue valueWithUIEdgeInsets:edgeInsets], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIEdgeInsets)edgeInsets {
    return [objc_getAssociatedObject(self, @selector(edgeInsets)) UIEdgeInsetsValue];
}

- (void)setTopEdgeInset:(CGFloat)topEdgeInset {
    UIEdgeInsets edgeInsets = self.edgeInsets;
    edgeInsets.top = topEdgeInset;
    self.edgeInsets = edgeInsets;
}

- (CGFloat)topEdgeInset {
    return self.edgeInsets.top;
}

- (void)setLeftEdgeInset:(CGFloat)leftEdgeInset {
    UIEdgeInsets edgeInsets = self.edgeInsets;
    edgeInsets.left = leftEdgeInset;
    self.edgeInsets = edgeInsets;
}

- (CGFloat)leftEdgeInset {
    return self.edgeInsets.left;
}

- (void)setBottomEdgeInset:(CGFloat)bottomEdgeInset {
    UIEdgeInsets edgeInsets = self.edgeInsets;
    edgeInsets.bottom = bottomEdgeInset;
    self.edgeInsets = edgeInsets;
}

- (CGFloat)bottomEdgeInset {
    return self.edgeInsets.bottom;
}

- (void)setRightEdgeInset:(CGFloat)rightEdgeInset {
    UIEdgeInsets edgeInsets = self.edgeInsets;
    edgeInsets.right = rightEdgeInset;
    self.edgeInsets = edgeInsets;
}

- (CGFloat)rightEdgeInset {
    return self.edgeInsets.right;
}

- (CGSize)sizeWithEdgeInsets {
    return (CGSize){
        .width = self.edgeInsets.left + self.width + self.edgeInsets.right,
        .height = self.edgeInsets.top + self.height + self.edgeInsets.bottom,
    };
}


- (void)centerHorizontaly {
    
    self.centerX = self.superview.width / 2;
}

- (void)centerVerticaly {

    self.centerY = self.superview.height / 2;
}

- (void)sizeToFitSize:(CGSize)size {
    
    CGSize fitSize = [self sizeThatFits:size];
    self.size = (CGSize) {
        .width = MIN(size.width, fitSize.width),
        .height = MIN(size.height, fitSize.height),
    };
}

- (CGFloat)dynamicOffsetFromView:(UIView *)view withConstant:(CGFloat)constant {
    
    CGFloat dynamicOffset = 0.0;
    if ((self.hidden == NO && self.width > 0 && self.height > 0) && (view.hidden == NO && view.width > 0 && view.height > 0)) {
        dynamicOffset = constant;
    }
    return dynamicOffset;
}

- (void)connectLeftToViewRight:(UIView *)view withOffset:(CGFloat)offset {
    
    self.left = view.right + [self dynamicOffsetFromView:view withConstant:offset];
}

- (void)connectLeftToViewRight:(UIView *)view {

    self.left = view.right + [self dynamicOffsetFromView:view withConstant:self.leftEdgeInset + view.rightEdgeInset];
}

- (void)connectTopToViewBottom:(UIView *)view withOffset:(CGFloat)offset {
    
    self.top = view.bottom + [self dynamicOffsetFromView:view withConstant:offset];
}

- (void)connectTopToViewBottom:(UIView *)view {
    
    self.top = view.bottom + [self dynamicOffsetFromView:view withConstant:self.topEdgeInset + view.bottomEdgeInset];
}

@end
