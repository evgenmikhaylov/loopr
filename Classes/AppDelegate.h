//
//  AppDelegate.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 19/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end