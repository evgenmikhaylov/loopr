//
//  MVZBeatDetector.h
//  
//
//  Created by Evgeny Mikhaylov on 21/09/16.
//
//

#import <Foundation/Foundation.h>

#define DF_HFC (1)
#define DF_SPECDIFF (2)
#define DF_PHASEDEV (3)
#define DF_COMPLEXSD (4)
#define DF_BROADBAND (5)

typedef enum {
    BeatDetectorTypeDefault,
    BeatDetectorTypeQMPD,
    BeatDetectorTypeQMCSD,
    BeatDetectorTypeQMBB,
    BeatDetectorTypeBBC,
    BeatDetectorTypeBBCC,
    BeatDetectorTypeBBCX,
    
    // rejects
    BeatDetectorTypeQMBBT,
    BeatDetectorTypeQMHFC,
    BeatDetectorTypeQMSD,
    BeatDetectorTypeBR,
} BeatDetectorType;

@interface MVZBeatDetector : NSObject

- (void)startProcessingAudioFileWithURL:(NSURL *)fileURL withCompletionBlock:(void(^)())completionBlock;

@property (nonatomic, readonly) NSArray<NSNumber *> *energy1024Array;
@property (nonatomic, readonly) NSArray<NSNumber *> *energy44100Array;
@property (nonatomic, readonly) NSArray<NSNumber *> *energyPeakArray;
@property (nonatomic, readonly) NSArray<NSNumber *> *convArray;
@property (nonatomic, readonly) NSArray<NSNumber *> *beatsArray;

@property (nonatomic) BeatDetectorType type;

@end
