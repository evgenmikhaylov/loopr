/* -*- c-basic-offset: 4 indent-tabs-mode: nil -*-  vi:set ts=8 sts=4 sw=4: */

/*
    Vamp feature extraction plugin for the BeatRoot beat tracker.

    Centre for Digital Music, Queen Mary, University of London.
    This file copyright 2011 Simon Dixon, Chris Cannam and QMUL.
    
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.  See the file
    COPYING included with this distribution for more information.
*/

#include "BeatRootVampPlugin.h"
#include "BeatRootProcessor.h"

#include "Event.h"

BeatRootVampPlugin::BeatRootVampPlugin(float inputSampleRate) :
    Plugin(inputSampleRate),
    m_firstFrame(true)
{
    m_processor = new BeatRootProcessor(inputSampleRate, AgentParameters());
}

BeatRootVampPlugin::~BeatRootVampPlugin()
{
    delete m_processor;
}

string
BeatRootVampPlugin::getIdentifier() const
{
    return "beatroot";
}

string
BeatRootVampPlugin::getName() const
{
    return "BeatRoot Beat Tracker";
}

string
BeatRootVampPlugin::getDescription() const
{
    return "Identify beat locations in music";
}

string
BeatRootVampPlugin::getMaker() const
{
    return "Simon Dixon (plugin by Chris Cannam)";
}

int
BeatRootVampPlugin::getPluginVersion() const
{
    // Increment this each time you release a version that behaves
    // differently from the previous one
    return 1;
}

string
BeatRootVampPlugin::getCopyright() const
{
    return "GPL";
}

size_t
BeatRootVampPlugin::getPreferredBlockSize() const
{
    return m_processor->getFFTSize();
}

size_t 
BeatRootVampPlugin::getPreferredStepSize() const
{
    return m_processor->getHopSize();
}

size_t
BeatRootVampPlugin::getMinChannelCount() const
{
    return 1;
}

size_t
BeatRootVampPlugin::getMaxChannelCount() const
{
    return 1;
}

bool
BeatRootVampPlugin::initialise(size_t channels, size_t stepSize, size_t blockSize)
{
    if (channels < getMinChannelCount() ||
	channels > getMaxChannelCount()) {
	std::cerr << "BeatRootVampPlugin::initialise: Unsupported number ("
		  << channels << ") of channels" << std::endl;
	return false;
    }

    if (stepSize != getPreferredStepSize()) {
	std::cerr << "BeatRootVampPlugin::initialise: Unsupported step size "
		  << "for sample rate (" << stepSize << ", required step is "
		  << getPreferredStepSize() << " for rate " << m_inputSampleRate
		  << ")" << std::endl;
	return false;
    }

    if (blockSize != getPreferredBlockSize()) {
	std::cerr << "BeatRootVampPlugin::initialise: Unsupported block size "
		  << "for sample rate (" << blockSize << ", required size is "
		  << getPreferredBlockSize() << " for rate " << m_inputSampleRate
		  << ")" << std::endl;
	return false;
    }

    // Delete the processor that was created with default parameters
    // and used to determine the expected step and block size; replace
    // with one using the actual parameters we have
    delete m_processor;
    m_processor = new BeatRootProcessor(m_inputSampleRate, m_parameters);

    return true;
}

void
BeatRootVampPlugin::reset()
{
    m_processor->reset();
    m_firstFrame = true;
    m_origin = RealTime::zeroTime;
}

FeatureSet
BeatRootVampPlugin::process(const float *const *inputBuffers, RealTime timestamp)
{
    if (m_firstFrame) {
        m_origin = timestamp;
        m_firstFrame = false;
    }

    m_processor->processFrame(inputBuffers);
    return FeatureSet();
}

FeatureSet
BeatRootVampPlugin::getRemainingFeatures()
{
    EventList unfilled;
    EventList el = m_processor->beatTrack(&unfilled);

    Feature f;
    f.hasTimestamp = true;
    f.hasDuration = false;
    f.label = "";
    f.values.clear();

    FeatureSet fs;

    for (EventList::const_iterator i = el.begin(); i != el.end(); ++i) {
        f.timestamp = m_origin + RealTime::fromSeconds(i->time);
        fs[0].push_back(f);
    }

    for (EventList::const_iterator i = unfilled.begin(); 
         i != unfilled.end(); ++i) {
        f.timestamp = m_origin + RealTime::fromSeconds(i->time);
        fs[1].push_back(f);
    }

    return fs;
}
