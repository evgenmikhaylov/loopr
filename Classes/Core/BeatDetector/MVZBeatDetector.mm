//
//  MVZBeatDetector.m
//  
//
//  Created by Evgeny Mikhaylov on 21/09/16.
//
//

#import "MVZBeatDetector.h"
#import <AVFoundation/AVFoundation.h>

// Bridge
#import "VAMP-Bridge.h"

// QM
#import "BarBeatTrack.h"
#import "BeatTrack.h"

// BeatRoot
#import "BeatRootVampPlugin.h"

// BBC
#import "BBCPeaks.h"
#import "BBCRhythm.h"

int MVZLengthMultiplier = 1024;
long MVZSampleRate = 44100;

@interface MVZBeatDetector () {
    float *_energy1024;
    float *_energy44100;
    float *_energyPeak;
    float *_conv;
    float *_beats;
    int _tempo;
    int _length;
}

@end

@implementation MVZBeatDetector

- (void)dealloc {
    free(_energy1024);
    free(_energy44100);
    free(_energyPeak);
    free(_conv);
    free(_beats);
}

#pragma mark - Setters/Getters

- (NSArray<NSNumber *> *)energy1024Array {
    NSMutableArray<NSNumber *> *energy1024Array = [[NSMutableArray alloc] init];
    for (int i = 0; i < (_length / MVZLengthMultiplier); i++) {
        [energy1024Array addObject:@(_energy1024[i])];
    }
    return energy1024Array.copy;
}

- (NSArray<NSNumber *> *)energy44100Array {
    NSMutableArray<NSNumber *> *energy44100Array = [[NSMutableArray alloc] init];
    for (int i = 0; i < (_length / MVZLengthMultiplier); i++) {
        [energy44100Array addObject:@(_energy44100[i])];
    }
    return energy44100Array.copy;
}

- (NSArray<NSNumber *> *)energyPeakArray {
    NSMutableArray<NSNumber *> *energyPeakArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < ((_length / MVZLengthMultiplier) + 21); i++) {
        [energyPeakArray addObject:@(_energyPeak[i])];
    }
    return energyPeakArray.copy;
}

- (NSArray<NSNumber *> *)convArray {
    NSMutableArray<NSNumber *> *convArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < (_length / MVZLengthMultiplier); i++) {
        [convArray addObject:@(_conv[i])];
    }
    return convArray.copy;
}

- (NSArray<NSNumber *> *)beatsArray {
    NSMutableArray<NSNumber *> *beatsArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < (_length / MVZLengthMultiplier); i++) {
        [beatsArray addObject:@(_beats[i])];
    }
    return beatsArray.copy;
}

#pragma mark - Processing

- (void)startProcessingAudioFileWithURL:(NSURL *)fileURL withCompletionBlock:(void(^)())completionBlock {

    self.type = (BeatDetectorType)[[NSUserDefaults standardUserDefaults] integerForKey:@"beatDetectorType"];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:fileURL options:nil];
    AVAssetTrack *track = [[asset tracksWithMediaType:AVMediaTypeAudio] firstObject];
    NSError *assetReaderError;
    AVAssetReader *assetReader = [AVAssetReader assetReaderWithAsset:asset error:&assetReaderError];
    if (assetReaderError) {
        NSLog (@"error: %@", assetReaderError);
        return;
    }

    long sampleRate = MVZSampleRate;
    NSDictionary *audioSettings = @{
                                    AVFormatIDKey : @(kAudioFormatLinearPCM),
                                    AVSampleRateKey : @(sampleRate),
                                    AVNumberOfChannelsKey : @(1),
                                    AVLinearPCMIsFloatKey : @(YES),
                                    AVLinearPCMBitDepthKey : @(32),
                                    };
    AVAssetReaderAudioMixOutput *assetReaderOutput = [AVAssetReaderAudioMixOutput
                                                      assetReaderAudioMixOutputWithAudioTracks:asset.tracks
                                                      audioSettings:audioSettings];
    if (![assetReader canAddOutput:assetReaderOutput]) {
        NSLog (@"[assetReader canAddOutput:assetReaderOutput] failed");
        return;
    }
    [assetReader addOutput:assetReaderOutput];
    
    if (![assetReader startReading]) {
        NSLog (@"[assetReader startReading] failed");
        return;
    }
    
    CMSampleBufferRef sampleBuffer;
    int length = 0;
    
    int samplesCount = CMTimeGetSeconds(track.timeRange.duration) * sampleRate;
    float *samples = (float *)malloc((samplesCount) * sizeof(float));
    for(int i = 0; i < samplesCount; i++) {
        samples[i] = 0;
    }
    
    do {
        sampleBuffer = [assetReaderOutput copyNextSampleBuffer];
        if (sampleBuffer) {
            int bufferLength;

            CMBlockBufferRef buffer = CMSampleBufferGetDataBuffer(sampleBuffer);
            CMItemCount numSamplesInBuffer = CMSampleBufferGetNumSamples(sampleBuffer);
            
            AudioBufferList audioBufferList;
            CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(
                sampleBuffer,
                NULL,
                &audioBufferList,
                sizeof(audioBufferList),
                NULL,
                NULL,
                kCMSampleBufferFlag_AudioBufferList_Assure16ByteAlignment,
                &buffer
            );
            
            AudioBuffer audioBuffer = audioBufferList.mBuffers[0];
            float *bufferSamples = (float *)audioBuffer.mData;
            bufferLength = (int)numSamplesInBuffer;
            
            for(int i = 0; i < bufferLength; i++) {
                samples[i + length] = bufferSamples[i];
            }
            
            length += bufferLength;
            CFRelease(sampleBuffer);
        }
    } while (sampleBuffer);

    if (samples) {
        
        BBC::Rhythm bbcDetector(sampleRate);
        FeatureSet bbcFeatureSet = [self useVAMPPluginPort:&bbcDetector with:samples length:length];
        Feature tempo = bbcFeatureSet[7][0];
        Feature rhythmStrength = bbcFeatureSet[5][0];
        float estimatedTempo = tempo.values[0] ?: 120.0;
#ifdef DEBUG
        NSLog(@"Rhythm Strength: %f", rhythmStrength.values[0]);
        NSLog(@"Tempo: %f", estimatedTempo);
#endif
        
        Vamp::Plugin *tracker = nil;
        int outputIndex = 0;
        switch (self.type) {
            case BeatDetectorTypeQMBBT: {
                tracker = new BarBeatTracker(sampleRate);
                break;
            }
            case BeatDetectorTypeQMHFC: {
                tracker = new BeatTracker(sampleRate);
                ((BeatTracker *)tracker)->m_dfType = DF_HFC;
                ((BeatTracker *)tracker)->m_inputtempo = estimatedTempo;
                break;
            }
            case BeatDetectorTypeQMSD: {
                tracker = new BeatTracker(sampleRate);
                ((BeatTracker *)tracker)->m_dfType = DF_SPECDIFF;
                ((BeatTracker *)tracker)->m_inputtempo = estimatedTempo;
                break;
            }
            case BeatDetectorTypeQMPD: {
                tracker = new BeatTracker(sampleRate);
                ((BeatTracker *)tracker)->m_dfType = DF_PHASEDEV;
                ((BeatTracker *)tracker)->m_inputtempo = estimatedTempo;
                break;
            }
            case BeatDetectorTypeQMCSD: {
                tracker = new BeatTracker(sampleRate);
                ((BeatTracker *)tracker)->m_dfType = DF_COMPLEXSD;
                ((BeatTracker *)tracker)->m_inputtempo = estimatedTempo;
                break;
            }
            case BeatDetectorTypeQMBB: {
                tracker = new BeatTracker(sampleRate);
                ((BeatTracker *)tracker)->m_dfType = DF_BROADBAND;
                ((BeatTracker *)tracker)->m_inputtempo = estimatedTempo;
                break;
            }
            case BeatDetectorTypeBR: {
                tracker = new BeatRootVampPlugin(sampleRate);
                break;
            }
            case BeatDetectorTypeBBC: {
                tracker = new BBC::Rhythm(sampleRate);
                outputIndex = 1;
                break;
            }
            case BeatDetectorTypeBBCC: {
                tracker = new BBC::Rhythm(sampleRate);
                break;
            }
            case BeatDetectorTypeBBCX: {
                // with out powers combined...
                // ... we'll get shitty results, apparently
                // needs a little more thought
                std::vector<Feature> beats = bbcFeatureSet[0];
                std::vector<Feature>::iterator beatIterator = beats.begin();
                
                std::vector<Feature> onsets = bbcFeatureSet[1];
                std::vector<Feature>::iterator onsetIterator = onsets.begin();
                
                std::vector<Feature> combinedFeatures;
                do {
                    if (beatIterator == beats.end()) {
                        while (onsetIterator != onsets.end()) {
                            combinedFeatures.push_back(*onsetIterator);
                            ++onsetIterator;
                        }
                        break;
                    }
                    
                    if (onsetIterator == onsets.end()) {
                        while (beatIterator != beats.end()) {
                            combinedFeatures.push_back(*beatIterator);
                            ++beatIterator;
                        }
                        break;
                    }
                    
                    Feature beat = *beatIterator;
                    Feature onset = *onsetIterator;
                    if (beat.timestamp > onset.timestamp) {
                        combinedFeatures.push_back(onset);
                        ++onsetIterator;
                    }
                    else {
                        combinedFeatures.push_back(beat);
                        ++beatIterator;
                    }
                    
                } while (true);
                [self prepareBeats:&combinedFeatures length:length tempoHint:estimatedTempo];
                break;
            }
            case BeatDetectorTypeDefault: {
            default:
                [self processAudioSamples:samples withLength:length];
                break;
            }
        }
        
        if (tracker) {
            FeatureSet featureSet = [self useVAMPPluginPort:tracker with:samples length:length];
            FeatureList features = featureSet[outputIndex];
            [self prepareBeats:&features length:length tempoHint:estimatedTempo];
            free(tracker);
        }
    }
    
    free(samples);
    
    if (completionBlock) {
        completionBlock();
    }
}

- (FeatureSet)useVAMPPluginPort:(Vamp::Plugin *)tracker with:(float *)samples length:(int)length {
    RealTime time = RealTime::zeroTime;
    size_t frameSize = tracker->getPreferredBlockSize() ?: 1024;
    size_t stepSize = tracker->getPreferredStepSize() ?: 512;
    tracker->initialise(1, stepSize, frameSize);
    
    int steps = length / stepSize;
    for(int i = 0; i < steps; ++i) {
        float *currentBatch = samples + i * stepSize;
        tracker->process(&currentBatch, time);
        time = time + RealTime::frame2RealTime(stepSize, (int)MVZSampleRate);
    }
    
    FeatureSet featureSet = tracker->getRemainingFeatures();
    return featureSet;
}

- (void)prepareBeats:(FeatureList *)features length:(int)length tempoHint:(float)tempoHint {
    _length = length;
    int lengthWithMultiplier = length / MVZLengthMultiplier;
    _beats = (float *)calloc(lengthWithMultiplier, sizeof(float));
    
    long beatInterval = 60 * MVZSampleRate / tempoHint;
    long minFeatureGap = 0.5 * beatInterval;
    long maxFeatureGap = 2.0 * beatInterval;
    
    long previousFrame = 0;
    std::for_each(features->begin(), features->end(), [&](Feature feature){
        long featureFrame = RealTime::realTime2Frame(feature.timestamp, (int)MVZSampleRate);
        long gap = (featureFrame - previousFrame);
        long beatFrame = featureFrame / MVZLengthMultiplier;
        if (!previousFrame || (gap > minFeatureGap)) {
            previousFrame = featureFrame;
#ifdef DEBUG
            NSLog(@"got: %ld", beatFrame);
#endif
            _beats[beatFrame] = 1.0;
            
            if (gap > maxFeatureGap) {
                long currentFeatureFrame = featureFrame - beatInterval;
                gap -= beatInterval;
                
                while (gap >= beatInterval) {
                    long frame = currentFeatureFrame / MVZLengthMultiplier;
#ifdef DEBUG
                    NSLog(@"backfill: %ld", frame);
#endif
                    _beats[frame] = 1.0;
                    currentFeatureFrame -= beatInterval;
                    gap -= beatInterval;
                }
            }
        }
#ifdef DEBUG
        else {
            NSLog(@"skip: %ld", beatFrame);
        }
#endif
    });
}

- (void)processAudioSamples:(float *)samples withLength:(int)length {

    _length = length;
    
    int lengthWithMultiplier = length / MVZLengthMultiplier;

    _energy1024 = (float *)malloc(lengthWithMultiplier * sizeof(float));
    _energy44100 = (float *)malloc(lengthWithMultiplier * sizeof(float));
    _conv = (float *)malloc(lengthWithMultiplier * sizeof(float));
    _beats = (float *)malloc(lengthWithMultiplier * sizeof(float));
    _energyPeak = (float *)malloc((lengthWithMultiplier + 21) * sizeof(float));
    
    for(int i = 0; i < lengthWithMultiplier; i++) _energy1024[i] = 0.0f;
    for(int i = 0; i < lengthWithMultiplier; i++) _energy44100[i] = 0.0f;
    for(int i = 0; i < lengthWithMultiplier; i++) _conv[i] = 0.0f;
    for(int i = 0; i < lengthWithMultiplier; i++) _beats[i] = 0.0f;
    for(int i = 0; i < lengthWithMultiplier + 21; i++) _energyPeak[i] = 0.0f;
    
    int trainDimpSize = 108;
    
    for(int i = 0 ; i < lengthWithMultiplier ; i++) {
        _energy1024[i] = [self energyForSamples:samples offset:MVZLengthMultiplier * i window:4096.0f length:length];
    }
    
    _energy44100[0] = 0;
    float sum = 0.0f;
    for(int i = 0 ; i < 43 ; i++) {
        sum = sum + _energy1024[i];
    }
    _energy44100[0] = sum / 43.0f;
    for(int i = 1 ; i < lengthWithMultiplier ; i++) {
        sum = sum - _energy1024[i - 1] + _energy1024[i + 42];
        _energy44100[i] = sum / 43.0f;
    }
    
    for(int i = 21; i < lengthWithMultiplier; i++) {
        if (_energy1024[i] > 1.3f * _energy44100[i - 21]) {
            _energyPeak[i] = 1.0f;
        }
    }

    NSMutableArray<NSNumber *> *T = [[NSMutableArray alloc] init];
    int i_prec = 0;
    for(int i = 1 ; i < lengthWithMultiplier; i++) {
        if((_energyPeak[i] == 1) && (_energyPeak[i - 1] == 0))
        {
            int di = i - i_prec;
            if (di>5) {
                [T addObject:@(di)];
                i_prec = i;
            }
        }
    }
    
    int T_occ_max = 0;
    float T_occ_moy = 0.f;
    int occurences_T[86];
    for(int i = 0; i < 86; i++){
        occurences_T[i] = 0;
    }
    for(int i = 1; i < T.count; i++) {
        if(T[i].intValue <= 86) {
            occurences_T[T[i].intValue]++;
        }
    }
    int occ_max = 0;
    for(int i = 1; i < 86; i++) {
        if (occurences_T[i] > occ_max) {
            T_occ_max = i;
            occ_max = occurences_T[i];
        }
    }

    int voisin = T_occ_max - 1;
    if (occurences_T[T_occ_max + 1] > occurences_T[voisin]) {
        voisin = T_occ_max+1;
    }
    float div = occurences_T[T_occ_max] + occurences_T[voisin];
    
    if (div == 0){
        T_occ_moy = 0;
    }
    else {
        T_occ_moy = (float)(T_occ_max * occurences_T[T_occ_max] + (voisin) * occurences_T[voisin]) / div;
    }
    
    _tempo = (int) 60.f / (T_occ_moy * (1024.f/44100.f));

    float train_dimp[trainDimpSize];
    float espace = 0.f;
    train_dimp[0] = 1.f;
    for(int i=1; i < trainDimpSize; i++) {
        if (espace >= T_occ_moy) {
            train_dimp[i] = 1;
            espace = espace - T_occ_moy; // on garde le depassement
        }
        else{
            train_dimp[i] = 0;
        }
        espace += 1.f;
    }
    
    for(int i = 0; i < (lengthWithMultiplier - trainDimpSize); i++) {
        for(int j = 0; j < trainDimpSize; j++) {
            _conv[i] = _conv[i] + _energy1024[i+j] * train_dimp[j];
        }
    }
    [self normalizeSignal:_conv size:lengthWithMultiplier maxValue:1.f];

    for(int i = 1; i < lengthWithMultiplier ; i++){
        _beats[i]=0;
    }
    
    float max_conv=0.f;
    int max_conv_pos=0;
    for(int i = 1; i < lengthWithMultiplier; i++) {
        if(_conv[i]>max_conv)
        {
            max_conv = _conv[i];
            max_conv_pos = i;
        }
    }
    _beats[max_conv_pos] = 1.f;

    int i = max_conv_pos + T_occ_max;
    while((i < lengthWithMultiplier) && (_conv[i] > 0.f)) {
        int conv_max_pos_loc = [self searchMaxWithSignal:_conv position:i fenetreHalfSize:2];
        _beats[conv_max_pos_loc] = 1.f;
        i = conv_max_pos_loc + T_occ_max;
    }
    i = max_conv_pos - T_occ_max;
    while(i > 0) {
        int conv_max_pos_loc = [self searchMaxWithSignal:_conv position:i fenetreHalfSize:2];
        _beats[conv_max_pos_loc] = 1.f;
        i = conv_max_pos_loc - T_occ_max;
    }
}

#pragma mark - Helpers

- (float)energyForSamples:(float *)samples offset:(int)offset window:(int)window length:(int)length {
    float energy = 0.0f;
    for(int i = offset; ((i < offset + window) && i < length); i++) {
        SInt16 preparedSample = samples[i] * INT16_MAX;
        energy = energy + preparedSample * preparedSample / (float)window;
    }
    return energy;
}

- (void)normalizeSignal:(float *)signal size:(int)size maxValue:(float)maxValue {
    float max = 0.f;
    for(int i=0; i < size ;i++) {
        if (fabsf(signal[i]) > max) {
            max = fabsf(signal[i]);
        }
    }
    float ratio = maxValue / max;
    for(int i = 0; i < size ;i++) {
        signal[i] = signal[i] * ratio;
    }
}

- (int)searchMaxWithSignal:(float *)signal position:(int)position fenetreHalfSize:(int)fenetreHalfSize {
    float max = 0.f;
    int maxPosition = position;
    for(int i = position - fenetreHalfSize; i <= position + fenetreHalfSize; i++) {
        if (signal[i] > max) {
            max = signal[i];
            maxPosition = i;
        }
    }
    return maxPosition;
}

@end
