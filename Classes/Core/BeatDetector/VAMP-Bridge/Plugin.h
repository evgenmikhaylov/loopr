//
//  Plugin.hpp
//  Loopr
//
//  Created by Anton K on 7/12/17.
//  Copyright © 2017 Evgeny Mikhaylov. All rights reserved.
//

#ifndef Plugin_hpp
#define Plugin_hpp

#include "Feature.h"

namespace Vamp {
    class Plugin {
    public:
        Plugin(float sampleRate) : m_inputSampleRate(sampleRate) {};
        float m_inputSampleRate;
        
        virtual size_t getPreferredBlockSize() const = 0;
        virtual size_t getPreferredStepSize() const = 0;
        
        virtual FeatureSet getRemainingFeatures() = 0;
        virtual FeatureSet process(const float *const *inputBuffers, RealTime timestamp) = 0;
        virtual bool initialise(size_t inputChannels, size_t stepSize, size_t blockSize) = 0;
    };
}

#endif /* Plugin_hpp */
