//
//  Feature.h
//  Loopr
//
//  Created by Anton K on 7/12/17.
//  Copyright © 2017 Evgeny Mikhaylov. All rights reserved.
//

#ifndef Feature_h
#define Feature_h

#include <vector>
#include <map>
#include "RealTime.h"

struct Feature
{
    bool hasTimestamp;
    RealTime timestamp;
    
    bool hasDuration;
    RealTime duration;
    std::vector<float> values;
    std::string label;
    
    Feature() : hasTimestamp(false), hasDuration(false) {}
};

typedef std::vector<Feature> FeatureList;
typedef std::map<int, FeatureList> FeatureSet;

#endif /* Feature_h */
