/**
 * BBC Vamp plugin collection
 *
 * Copyright (c) 2011-2013 British Broadcasting Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "BBCRhythm.h"
/// @cond

using namespace BBC;

Rhythm::Rhythm(float inputSampleRate) : Plugin(inputSampleRate) {
    m_sampleRate = inputSampleRate;
    numBands = 7;
    bandHighFreq = NULL;
    calculateBandFreqs();
    
    // calculate and save half-hanny window
    halfHannLength = 12;
    halfHannWindow = new float[halfHannLength];
    for (int i = 0; i < halfHannLength; i++)
        halfHannWindow[i] = halfHanning((float) i);
    
    // calculate and save canny window
    cannyLength = 12;
    cannyShape = 4.f;
    cannyWindow = new float[cannyLength * 2 + 1];
    for (int i = cannyLength * -1; i < cannyLength + 1; i++)
        cannyWindow[i + cannyLength] = canny((float) i);
    
    // adjustables
    threshold = 1;
    average_window = 200;
    peak_window = 6;
    max_bpm = 320;
    min_bpm = 40;
}

Rhythm::~Rhythm() {
    delete[] halfHannWindow;
    delete[] cannyWindow;
    delete[] bandHighFreq;
}

string Rhythm::getIdentifier() const {
    return "bbc-rhythm";
}

string Rhythm::getName() const {
    return "Rhythm";
}

string Rhythm::getDescription() const {
    return "";
}

string Rhythm::getMaker() const {
    return "BBC";
}

int Rhythm::getPluginVersion() const {
    return 1;
}

string Rhythm::getCopyright() const {
    return "(c) 2013 British Broadcasting Corporation";
}


size_t Rhythm::getPreferredBlockSize() const {
    return 1024;
}

size_t Rhythm::getPreferredStepSize() const {
    return 256;
}

size_t Rhythm::getMinChannelCount() const {
    return 1;
}

size_t Rhythm::getMaxChannelCount() const {
    return 1;
}

bool Rhythm::initialise(size_t channels, size_t stepSize, size_t blockSize) {
    
    m_blockSize = blockSize;
    m_stepSize = stepSize;
    reset();
    
    return true;
}

void Rhythm::reset() {
    intensity.clear();
}

FeatureSet Rhythm::process(const float * const *inputBuffers,
                           RealTime timestamp) {
    FeatureSet output;
    float total = 0;
    int currentBand = 0;
    vector<float> bandTotal;
    
    // set band totals to zero
    for (int i = 0; i < numBands; i++)
        bandTotal.push_back(0.f);
    
    // for each frequency bin
    for (int i = 0; i < m_blockSize / 2; i++) {
        // get absolute value
        float binVal = abs(
                           complex<float>(inputBuffers[0][i * 2], inputBuffers[0][i * 2 + 1]));
        
        // add contents of this bin to total
        total += binVal;
        
        // find centre frequency of this bin
        float freq = (i + 1) * m_sampleRate / (float) m_blockSize;
        
        // locate which band this bin belongs in
        while (freq > bandHighFreq[currentBand]) {
            currentBand++;
            if (currentBand >= numBands)
                break;
        }
        
        // add bin value to relevent band
        bandTotal.at(currentBand) += binVal;
    }
    
    intensity.push_back(bandTotal);
    
    return output;
}

FeatureSet Rhythm::getRemainingFeatures() {
    FeatureSet output;
    int frames = intensity.size();
    
    if (frames == 0)
        return output;
    
    // find envelope by convolving each subband with half-hanning window
    vector<vector<float> > envelope;
    halfHannConvolve(envelope);
    
    // find onset curve by convolving each subband of envelope with canny window
    vector<float> onset;
    cannyConvolve(envelope, onset);
    
    // normalise onset curve
    vector<float> onsetNorm;
    normalise(onset, onsetNorm);
    
    // push normalised onset curve
    Feature f_onset;
    f_onset.hasTimestamp = true;
    for (unsigned i = 0; i < onsetNorm.size(); i++) {
        f_onset.timestamp = RealTime::frame2RealTime(i * m_stepSize,
                                                     m_sampleRate);
        f_onset.values.clear();
        f_onset.values.push_back(onsetNorm.at(i));
        output[3].push_back(f_onset);
    }
    
    // find moving average of onset curve and difference
    vector<float> onsetAverage;
    vector<float> onsetDiff;
    movingAverage(onsetNorm, average_window, threshold, onsetAverage, onsetDiff);
    
    // push moving average
    Feature f_avg;
    f_avg.hasTimestamp = true;
    for (unsigned i = 0; i < onsetAverage.size(); i++) {
        f_avg.timestamp = RealTime::frame2RealTime(i * m_stepSize, m_sampleRate);
        f_avg.values.clear();
        f_avg.values.push_back(onsetAverage.at(i));
        //output[1].push_back(f_avg);
    }
    
    // push difference from average
    Feature f_diff;
    f_diff.hasTimestamp = true;
    for (unsigned i = 0; i < onsetDiff.size(); i++) {
        f_diff.timestamp = RealTime::frame2RealTime(i * m_stepSize, m_sampleRate);
        f_diff.values.clear();
        f_diff.values.push_back(onsetDiff.at(i));
        output[2].push_back(f_diff);
    }
    
    // choose peaks
    vector<int> peaks;
    findOnsetPeaks(onsetDiff, peak_window, peaks);
    int onsetCount = (int) peaks.size();
    
    // push peaks
    Feature f_peak;
    f_peak.hasTimestamp = true;
    for (unsigned i = 0; i < peaks.size(); i++) {
        f_peak.timestamp = RealTime::frame2RealTime(peaks.at(i) * m_stepSize, m_sampleRate);
        output[1].push_back(f_peak);
    }
    
    // calculate average onset frequency
    float averageOnsetFreq = (float) onsetCount
    / (float) (frames * m_stepSize / m_sampleRate);
    Feature f_avgOnsetFreq;
    f_avgOnsetFreq.hasTimestamp = true;
    f_avgOnsetFreq.timestamp = RealTime::fromSeconds(0.0);
    f_avgOnsetFreq.values.push_back(averageOnsetFreq);
    output[4].push_back(f_avgOnsetFreq);
    
    // calculate rhythm strength
    float rhythmStrength = findMeanPeak(onset, peaks, 0);
    Feature f_rhythmStrength;
    f_rhythmStrength.hasTimestamp = true;
    f_rhythmStrength.timestamp = RealTime::fromSeconds(0.0);
    f_rhythmStrength.values.push_back(rhythmStrength);
    output[5].push_back(f_rhythmStrength);
    
    // find shift range for autocor
    float firstShift = (int) round(60.f / max_bpm * m_sampleRate / m_stepSize);
    float lastShift = (int) round(60.f / min_bpm * m_sampleRate / m_stepSize);
    
    int iter = 1;
    
    long expectedBeat = RealTime::realTime2Frame(output[1][0].timestamp, m_sampleRate);
    float expectedTempo = 0.0;
    
    // adjustables
    float beatInertia = 0.75;
    float tempoInertia = 0.95;
    
    do {
        // autocorrelation
        vector<float> autocor;
        autocorrelation(onsetDiff, firstShift, lastShift, autocor);
        Feature f_autoCor;
        f_autoCor.hasTimestamp = true;
        for (float shift = firstShift; shift < lastShift; shift++) {
            f_autoCor.timestamp = RealTime::frame2RealTime(shift * m_stepSize, m_sampleRate);
            f_autoCor.values.clear();
            int autocorIndex = shift - firstShift;
            if (autocorIndex < autocor.size()) {
                f_autoCor.values.push_back(autocor.at(autocorIndex));
            }
            output[6].push_back(f_autoCor);
        }
        
        // find peaks in autocor
        float percentile = 92;
        int autocorWindowLength = 3;
        vector<int> autocorPeaks;
        vector<int> autocorValleys;
        findCorrelationPeaks(autocor, percentile, autocorWindowLength, firstShift, autocorPeaks, autocorValleys);
        
        // find tempo from peaks
        float tempo = findTempo(autocorPeaks) * iter;
        if (tempo >= 180) {
            tempo /= 2.0;
        }
        if (expectedTempo) {
            tempo += (expectedTempo - tempo) * tempoInertia;
        }
        expectedTempo = tempo;
        
        Feature f_tempo;
        f_tempo.hasTimestamp = true;
        f_tempo.timestamp = RealTime::fromSeconds(0.0);
        f_tempo.values.push_back(tempo);
        output[7].push_back(f_tempo);
        
        // find beat peak
        int index = 0;
        long minDistance = INT16_MAX;
        for (unsigned i = 0; i < autocorPeaks.size(); i++) {
            long foundBeat = autocorPeaks.at(i) * m_stepSize;
            long distance = abs(foundBeat - expectedBeat);
            if (distance < minDistance) {
                index = i;
                minDistance = distance;
            }
        }
        
        Feature f_beat;
        f_beat.hasTimestamp = true;
        
        long selectedBeat = expectedBeat ?: (autocorPeaks.at(index) * m_stepSize);
        if (expectedBeat) {
            selectedBeat += (expectedBeat - selectedBeat) * beatInertia;
        }
        
        f_beat.timestamp = RealTime::frame2RealTime(selectedBeat, m_sampleRate);
        output[0].push_back(f_beat);
        
        float shiftStep = (m_sampleRate * 60.f / tempo) / m_stepSize;
        expectedBeat = selectedBeat + m_stepSize * shiftStep;
        
        firstShift += shiftStep;
        lastShift += shiftStep;
        ++iter;
        
    } while (lastShift < frames);
    
    return output;
}

/// @endcond

void Rhythm::calculateBandFreqs() {
    delete[] bandHighFreq;
    bandHighFreq = new float[numBands];
    
    for (int k = 0; k < numBands; k++) {
        bandHighFreq[k] = m_sampleRate / pow(2.f, numBands - k);
    }
}

float Rhythm::halfHanning(float n) {
    return 0.5f
    + 0.5f * cos(2.f * M_PI * (n / (2.f * (float) halfHannLength - 1.f)));
}

float Rhythm::canny(float n) {
    return n / (cannyShape * cannyShape)
    * exp(-1 * (n * n) / (2 * cannyShape * cannyShape));
}

float Rhythm::findRemainder(vector<int> peaks, int thisPeak) {
    float total = 0;
    for (unsigned i = 0; i < peaks.size(); i++) {
        float ratio = (float) peaks.at(i) / (float) thisPeak;
        total += abs(ratio - round(ratio));
    }
    return total;
}

float Rhythm::findTempo(vector<int> peaks) {
    if (peaks.empty()) return 0.f;
    float min = findRemainder(peaks, peaks.at(0));
    int minPos = 0;
    for (unsigned i = 1; i < peaks.size(); i++) {
        float result = findRemainder(peaks, peaks.at(i));
        if (result < min) {
            min = result;
            minPos = i;
        }
    }
    return 60.f / (peaks.at(minPos) * m_stepSize / m_sampleRate);
}

float Rhythm::findMeanPeak(vector<float> signal, vector<int> peaks, int shift) {
    float total = 0;
    for (unsigned i = 0; i < peaks.size(); i++)
        total += signal.at(peaks.at(i) - shift);
    return total / peaks.size();
}

void Rhythm::findCorrelationPeaks(vector<float> autocor_in, float percentile_in,
                                  int windowLength_in, int shift_in,
                                  vector<int>& peaks_out,
                                  vector<int>& valleys_out) {
    if (autocor_in.empty()) return;
    
    vector<float> autocorSorted(autocor_in);
    std::sort(autocorSorted.begin(), autocorSorted.end());
    float autocorThreshold = autocorSorted.at(
                                              percentile_in / 100.f * (autocorSorted.size() - 1));
    
    int autocorValleyPos = 0;
    float autocorValleyValue = autocorThreshold;
    
    for (unsigned i = 0; i < autocor_in.size(); i++) {
        bool success = true;
        
        // check for valley
        if (autocor_in.at(i) < autocorValleyValue) {
            autocorValleyPos = i;
            autocorValleyValue = autocor_in.at(i);
        }
        
        // if below the threshold, move onto next element
        if (autocor_in.at(i) < autocorThreshold)
            continue;
        
        // check for other peaks in the area
        for (int j = windowLength_in * -1; j < windowLength_in + 1; j++) {
            if (i + j >= 0 && i + j < autocor_in.size()) {
                if (autocor_in.at(i + j) > autocor_in.at(i))
                    success = false;
            }
        }
        
        // save peak and valley
        if (success) {
            peaks_out.push_back(shift_in + i);
            valleys_out.push_back(shift_in + autocorValleyPos);
            autocorValleyValue = autocor_in.at(i);
        }
    }
}

void Rhythm::autocorrelation(vector<float> signal_in, int startShift_in,
                             int endShift_in, vector<float>& autocor_out) {
    for (float shift = startShift_in; shift < endShift_in; shift++) {
        float result = 0;
        for (unsigned frame = 0; frame < signal_in.size(); frame++) {
            if (frame + shift < signal_in.size())
                result += signal_in.at(frame) * signal_in.at(frame + shift);
        }
        autocor_out.push_back(result / signal_in.size());
    }
}

void Rhythm::findOnsetPeaks(vector<float> onset_in, int windowLength_in,
                            vector<int>& peaks_out) {
    for (unsigned frame = 0; frame < onset_in.size(); frame++) {
        bool success = true;
        
        // ignore 0 values
        if (onset_in.at(frame) <= 0)
            continue;
        
        // if any frames within windowSize have a bigger value, this is not the peak
        for (int i = windowLength_in * -1; i < windowLength_in + 1; i++) {
            if (frame + i >= 0 && frame + i < onset_in.size()) {
                if (onset_in.at(frame + i) > onset_in.at(frame))
                    success = false;
            }
        }
        
        // push result out
        if (success) {
            peaks_out.push_back(frame);
        }
    }
}

void Rhythm::movingAverage(vector<float> signal_in, int windowLength_in,
                           float threshold_in, vector<float>& average_out,
                           vector<float>& difference_out) {
    float avgWindowLength = (windowLength_in * 2) + 1;
    for (unsigned frame = 0; frame < signal_in.size(); frame++) {
        float result = 0;
        for (int i = windowLength_in * -1; i < windowLength_in + 1; i++) {
            if (frame + i >= 0 && frame + i < signal_in.size())
                result += abs(signal_in.at(frame + i));
        }
        
        // calculate average and difference results
        float average = result / avgWindowLength + threshold_in;
        float difference = signal_in.at(frame) - average;
        if (difference < 0)
            difference = 0;
        
        average_out.push_back(average);
        difference_out.push_back(difference);
    }
}

void Rhythm::normalise(vector<float> signal_in, vector<float>& normalised_out) {
    // find mean
    float total = 0;
    for (unsigned i = 0; i < signal_in.size(); i++)
        total += signal_in.at(i);
    float mean = total / signal_in.size();
    
    // find std dev
    float std = 0;
    for (unsigned i = 0; i < signal_in.size(); i++)
        std += pow(signal_in.at(i) - mean, 2);
    std = sqrt(std / signal_in.size());
    
    // normalise and rectify
    for (unsigned i = 0; i < signal_in.size(); i++) {
        normalised_out.push_back((signal_in.at(i) - mean) / std);
        if (normalised_out.at(i) < 0)
            normalised_out.at(i) = 0;
    }
}

void Rhythm::halfHannConvolve(vector<vector<float> >& envelope_out) {
    for (unsigned frame = 0; frame < intensity.size(); frame++) {
        vector<float> frameResult;
        for (int subBand = 0; subBand < numBands; subBand++) {
            float result = 0;
            for (int shift = 0; shift < halfHannLength; shift++) {
                if (frame + shift < intensity.size())
                    result += intensity.at(frame + shift).at(subBand)
                    * halfHannWindow[shift];
            }
            frameResult.push_back(result);
        }
        envelope_out.push_back(frameResult);
    }
}

void Rhythm::cannyConvolve(vector<vector<float> > envelope_in,
                           vector<float>& onset_out) {
    for (unsigned frame = 0; frame < envelope_in.size(); frame++) {
        // reset feature details
        float sum = 0;
        
        // for each sub-band
        for (int subBand = 0; subBand < numBands; subBand++) {
            // convolve the canny window with the envelope of that sub-band
            for (int shift = cannyLength * -1; shift < cannyLength; shift++) {
                if (frame + shift >= 0 && frame + shift < envelope_in.size())
                    sum += envelope_in.at(frame + shift).at(subBand)
                    * cannyWindow[shift + cannyLength];
            }
        }
        
        // save result
        onset_out.push_back(sum);
    }
}
