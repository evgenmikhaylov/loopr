//
//  NSString+Time.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/20/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface NSString (Time)

+ (NSString *)stringWithTimeInterval:(NSTimeInterval)interval;
+ (NSString *)stringWithDate:(NSDate *)date dateFormat:(NSString *)dateFormat;
+ (NSString *)stringWithCMTime:(CMTime)time;
+ (NSString *)stringWithCMTimeRange:(CMTimeRange)timeRange;
- (CMTime)CMTime;
- (CMTimeRange)CMTimeRange;
- (NSString *)stringByRemovingOccurrencesOfStrings:(NSArray *)strings;

@end
