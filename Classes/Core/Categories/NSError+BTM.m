//
//  NSError+BTM.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "NSError+BTM.h"

NSString *const BTMErrorDomain = @"com.jetset.loopr.error";

@implementation NSError (BTM)

+ (NSError *)BTM_errorWithCode:(BTMError)code description:(NSString *)description {
    NSDictionary *userInfo = description ? @{NSLocalizedDescriptionKey:description} : nil;
    return [NSError errorWithDomain:BTMErrorDomain code:code userInfo:userInfo];
}

@end
