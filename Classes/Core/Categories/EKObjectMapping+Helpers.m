  /*************************************************************************
 * 
 * TRUSTED INSIGHT CONFIDENTIAL
 * 
 * Copyright (C) 2016 Trusted Insight, Inc. 
 * 
 * NOTICE:  All information contained herein is, and remains the property of Trusted Insight, Inc. 
 * The intellectual and technical concepts contained herein are proprietary to Trusted Insight, Inc. 
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected by 
 * trade secret or copyright law.
 *
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Trusted Insight, Inc.
 */
 
#import "EKObjectMapping+Helpers.h"
#import "NSString+Time.h"

@implementation EKObjectMapping (Helpers)

- (void)mapCGRectFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
        if ([value isKindOfClass:[NSString class]]) {
            CGRect rect = CGRectFromString(value);
            return [NSValue valueWithCGRect:rect];
        }
        return [NSValue valueWithCGRect:CGRectNull];
    } reverseBlock:^id _Nullable(id  _Nullable value) {
        if ([value isKindOfClass:[NSValue class]]) {
            CGRect rect = [value CGRectValue];
            if (!CGRectIsNull(rect)) {
                return NSStringFromCGRect(rect);
            }
        }
        return NSStringFromCGRect(CGRectNull);
    }];
}

- (void)mapCMTimeFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
        if ([value isKindOfClass:[NSString class]]) {
            CMTime time = [value CMTime];
            return [NSValue valueWithCMTime:time];
        }
        return [NSValue valueWithCMTime:kCMTimeInvalid];
    } reverseBlock:^id _Nullable(id  _Nullable value) {
        if ([value isKindOfClass:[NSValue class]]) {
            CMTime time = [value CMTimeValue];
            if (CMTIME_IS_VALID(time)) {
                return [NSString stringWithCMTime:time];
            }
        }
        return [NSString stringWithCMTime:kCMTimeInvalid];
    }];
}

- (void)mapCMTimeRangeFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
        if ([value isKindOfClass:[NSString class]]) {
            CMTimeRange timeRange = [value CMTimeRange];
            return [NSValue valueWithCMTimeRange:timeRange];
        }
        return [NSValue valueWithCMTimeRange:kCMTimeRangeInvalid];
    } reverseBlock:^id _Nullable(id  _Nullable value) {
        if ([value isKindOfClass:[NSValue class]]) {
            CMTimeRange timeRange = [value CMTimeRangeValue];
            if (CMTIMERANGE_IS_VALID(timeRange)) {
                return [NSString stringWithCMTimeRange:timeRange];
            }
        }
        return [NSString stringWithCMTimeRange:kCMTimeRangeInvalid];
    }];
}


- (void)mapURLFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:[EKMappingBlocks urlMappingBlock] reverseBlock:[EKMappingBlocks urlReverseMappingBlock]];
}

- (void)mapDateWithFormat:(NSString *)format fromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = EKISO_8601DateTimeFormat;
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFormatter.dateFormat = format;
    
    [self mapKeyPath:keyPath toProperty:property withDateFormatter:dateFormatter];
}

- (void)mapEnumerationWithKeyValueDictionary:(NSDictionary *)dictionary fromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    [self mapEnumerationWithKeyValueDictionary:dictionary defaultKey:nil defaultValue:nil fromKeyPath:keyPath toProperty:property];
}

- (void)mapEnumerationWithKeyValueDictionary:(NSDictionary *)dictionary
                                  defaultKey:(NSString *)defaultKey
                                 fromKeyPath:(NSString *)keyPath
                                  toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:^id(NSString *key, id value) {
        if ((value == nil || [value isEqual:[NSNull null]]) && defaultKey){
            value = defaultKey;
        }
        return dictionary[value];
    } reverseBlock:^id(id value) {
        if ([value isEqual:dictionary[defaultKey]]){
            return [NSNull null];
        }
        return [dictionary allKeysForObject:value].lastObject;
    }];
}

- (void)mapEnumerationWithKeyValueDictionary:(NSDictionary *)dictionary
                                  defaultKey:(NSString *)defaultKey
                                defaultValue:(NSNumber *)defaultValue
                                 fromKeyPath:(NSString *)keyPath
                                  toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:^id(NSString *key, id value) {
        if ((value == nil || [value isEqual:[NSNull null]]) && defaultKey){
            value = defaultKey;
        }
        return dictionary[value];
    } reverseBlock:^id(id value) {
        if ([value isEqual:defaultValue]){
            return [NSNull null];
        }
        return [dictionary allKeysForObject:value].lastObject;
    }];
}

- (void)mapDateWithTimeIntervalSince1970FromKeyPath:(NSString *)keyPath toProperty:(NSString *)property {
    [self mapKeyPath:keyPath toProperty:property withValueBlock:^id(NSString *key, id value) {
        return [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
    } reverseBlock:^id(id value) {
        NSDate *date = value;
        return @([date timeIntervalSince1970]);
    }];
}

@end
