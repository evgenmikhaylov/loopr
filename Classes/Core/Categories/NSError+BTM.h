//
//  NSError+BTM.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const BTMErrorDomain;

typedef NS_ENUM(NSInteger, BTMError) {
    BTMErrorCancelled = -20000,
    BTMErrorImageNotLoaded,
    BTMErrorVideoNotLoaded,
    BTMErrorVideoNotDecoded,
    BTMErrorAudioNotDecoded,
    BTMErrorShortVideo,
    BTMErrorLowMemory,
};

@interface NSError (BTM)

+ (NSError *)BTM_errorWithCode:(BTMError)code description:(NSString *)description;

@end
