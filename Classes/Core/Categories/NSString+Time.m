//
//  NSString+Time.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/20/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "NSString+Time.h"

@implementation NSString (Time)

+ (NSString *)stringWithTimeInterval:(NSTimeInterval)interval {
    
    int timeInterval = interval;
    int seconds = timeInterval%60;
    int minutes = timeInterval/60;
    int hours = timeInterval/(60*60);
    
    if (hours > 0){
        minutes = minutes%60;
        return [NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes, seconds];
    }
    else{
        return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    }
}

+ (NSString *)stringWithDate:(NSDate *)date dateFormat:(NSString *)dateFormat {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+ (NSString *)stringWithCMTime:(CMTime)time {
    
    NSDictionary *timeDictionary = CFBridgingRelease(CMTimeCopyAsDictionary(time, kCFAllocatorDefault));
    NSMutableString *timeString = [[NSMutableString alloc] init];
    [timeString appendString:@"{"];
    [timeDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (![timeString isEqualToString:@"{"]){
            [timeString appendString:@","];
        }
        NSString *keyValueString = [NSString stringWithFormat:@"%@=%@", key, obj];
        [timeString appendString:keyValueString];
    }];
    [timeString appendString:@"}"];
    return timeString.copy;
}

+ (NSString *)stringWithCMTimeRange:(CMTimeRange)timeRange {
    
    NSString *startString = [NSString stringWithCMTime:timeRange.start];
    NSString *durationString = [NSString stringWithCMTime:timeRange.duration];
    NSString *timeRangeString = [NSString stringWithFormat:@"{%@,%@}", startString, durationString];
    return timeRangeString;
}

- (CMTime)CMTime {
    
    NSString *timeString = [self stringByRemovingOccurrencesOfStrings:@[@"{",@"}"]];
    NSArray *components = [timeString componentsSeparatedByString:@","];
    if (components.count==0)
        return kCMTimeInvalid;
    
    NSMutableDictionary *timeDictionary = [[NSMutableDictionary alloc] init];
    [components enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSArray *valueAndKey = [obj componentsSeparatedByString:@"="];
        NSString *key = valueAndKey.firstObject;
        NSNumber *value = @([valueAndKey.lastObject integerValue]);
        if (key&&value){
            [timeDictionary setObject:value forKey:key];
        }
    }];
    if (timeDictionary.allValues.count==0)
        return kCMTimeInvalid;

    CMTime time = CMTimeMakeFromDictionary((__bridge CFDictionaryRef)timeDictionary.copy);
    return time;
}

- (CMTimeRange)CMTimeRange {
    
    NSArray *components = [self componentsSeparatedByString:@"},{"];
    NSString *startString = [NSString stringWithFormat:@"{%@}",[components.firstObject stringByRemovingOccurrencesOfStrings:@[@"{",@"}"]]];
    NSString *durationString = [NSString stringWithFormat:@"{%@}",[components.lastObject stringByRemovingOccurrencesOfStrings:@[@"{",@"}"]]];
    if (startString&&durationString){
        CMTime start = [startString CMTime];
        CMTime duration = [durationString CMTime];
        CMTimeRange timeRange = CMTimeRangeMake(start, duration);
        return timeRange;
    }
    else{
        return kCMTimeRangeInvalid;
    }
}

- (NSString *)stringByRemovingOccurrencesOfStrings:(NSArray *)strings {
    
    __block NSString *string = self;
    [strings enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        string = [string stringByReplacingOccurrencesOfString:obj withString:@""];
    }];
    return string.copy;
}


@end
