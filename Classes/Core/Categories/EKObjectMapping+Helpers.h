  /*************************************************************************
 * 
 * TRUSTED INSIGHT CONFIDENTIAL
 * 
 * Copyright (C) 2016 Trusted Insight, Inc. 
 * 
 * NOTICE:  All information contained herein is, and remains the property of Trusted Insight, Inc. 
 * The intellectual and technical concepts contained herein are proprietary to Trusted Insight, Inc. 
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected by 
 * trade secret or copyright law.
 *
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Trusted Insight, Inc.
 */
 
#import <EasyMapping/EasyMapping.h>

@interface EKObjectMapping (Helpers)

- (void)mapCGRectFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;

- (void)mapCMTimeFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;
- (void)mapCMTimeRangeFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;

- (void)mapURLFromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;

- (void)mapDateWithFormat:(NSString *)format fromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;
- (void)mapDateWithTimeIntervalSince1970FromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;

- (void)mapEnumerationWithKeyValueDictionary:(NSDictionary *)dictionary fromKeyPath:(NSString *)keyPath toProperty:(NSString *)property;
- (void)mapEnumerationWithKeyValueDictionary:(NSDictionary *)dictionary
                                  defaultKey:(NSString *)defaultKey
                                 fromKeyPath:(NSString *)keyPath
                                  toProperty:(NSString *)property;
- (void)mapEnumerationWithKeyValueDictionary:(NSDictionary *)dictionary
                                  defaultKey:(NSString *)defaultKey
                                defaultValue:(NSNumber *)defaultValue
                                 fromKeyPath:(NSString *)keyPath
                                  toProperty:(NSString *)property;

@end
