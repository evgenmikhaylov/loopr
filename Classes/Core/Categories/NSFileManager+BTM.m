//
//  NSFileManager+BTM.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 21/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "NSFileManager+BTM.h"
#import <ReactiveCocoa.h>
#import <MVZExtensions.h>

@implementation NSFileManager (BTM)

+ (NSString *)projectsDirectory {
    return [[self mvz_documentsDirectory] stringByAppendingPathComponent:@"Projects"];
}

+ (RACSignal *)rac_prepareDirectoryForFileAtPath:(NSString *)filePath {
    NSString *directoryPath = [filePath stringByDeletingLastPathComponent];
    return [[[NSFileManager defaultManager] mvz_createDirectoryAtPathSignal:directoryPath] then:^RACSignal *{
        return [[NSFileManager defaultManager] mvz_removeItemAtPathSignal:filePath];
    }];
}

@end
