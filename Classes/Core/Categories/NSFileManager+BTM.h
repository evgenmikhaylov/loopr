//
//  NSFileManager+BTM.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 21/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@interface NSFileManager (BTM)

+ (NSString *)projectsDirectory;
+ (RACSignal *)rac_prepareDirectoryForFileAtPath:(NSString *)filePath;

@end
