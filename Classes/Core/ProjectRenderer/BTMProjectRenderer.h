//
// Created by Evgeny Mikhaylov on 25/01/2017.
// Copyright (c) 2017 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class BTMProject;

@interface BTMProjectRenderer : NSObject

- (instancetype)initWithProject:(BTMProject *)project;
- (void)renderProjectWithProgressBlock:(void(^)(CGFloat progress))progressBlock
                       completionBlock:(void(^)(NSError *error))completionBlock;
- (void)cancelRendering;

@end
