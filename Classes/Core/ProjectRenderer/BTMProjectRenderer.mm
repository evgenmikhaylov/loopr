//
//  CPProjectExporter.mm
//  Cinepic
//
//  Created by EvgenyMikhaylov on 4/10/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "BTMProjectRenderer.h"
#import "CPVideoColorMatrix601ColorSwizzlingProgram.h"
#import "CPVideoColorMatrix709ColorSwizzlingProgram.h"
#import "CPGLVideoTexture.h"
#import "GLProgramCache.h"
#import "GLFrameBuffer.h"
#import "CPLineProgram.h"
#import "GLMatrix4x4.hpp"
#import "GLStateCache.h"
#import "GLVector3.hpp"
#import "GLContext.h"
#import "GLTexture.h"

#import "BTMVideoFilter.h"
#import "BTMProject.h"
#import "BTMVideo.h"
#import "BTMAudio.h"

#import "GLView.h"

#import "AVAudioPlayer+Additions.h"
#import "CoreGraphicsExtensions.h"
#import "AVAsset+Export.h"
#import "NSObject+CP.h"
#import "ColorUtils.h"
#import "NSError+CP.h"

@interface BTMProjectRenderer ()

@property (nonatomic) BTMProject *project;
@property (nonatomic) BOOL renderingCancelled;

@property (nonatomic) AVAssetWriter *assetWriter;
@property (nonatomic) AVAssetWriterInput *videoAssetWriterInput;
@property (nonatomic) AVAssetWriterInputPixelBufferAdaptor *assetWriterInputPixelBufferAdaptor;
@property (nonatomic) AVAssetReader *audioAssetReader;
@property (nonatomic) AVAssetReaderOutput *audioAssetReaderOutput;
@property (nonatomic) AVAssetWriterInput *audioAssetWriterInput;
@property (nonatomic) BOOL audioWritingFinished;
@property (nonatomic) BOOL videoWritingFinished;

@property (nonatomic) CMTime time;
@property (nonatomic) BOOL isFirstFrameDrawn;
@property (nonatomic, readonly) BOOL needsDrawNextFrame;
@property (nonatomic) NSUInteger indexOfNextBeat;
@property (nonatomic) CMTime nextBeatTime;
@property (nonatomic) CMTime videoTimeOffset;
@property (nonatomic) NSInteger currentCrossFadeFramesCount;
@property (nonatomic, readonly) CGFloat videoScale;

@property (nonatomic) CPVideoColorMatrix601ColorSwizzlingProgram *backgroundVideo601Program;
@property (nonatomic) CPVideoColorMatrix709ColorSwizzlingProgram *backgroundVideo709Program;

@property (nonatomic) GLFrameBuffer *exportFrameBuffer;
@property (nonatomic) CPGLVideoTexture *firstBackgroundTexture;
@property (nonatomic) CPGLVideoTexture *secondBackgroundTexture;

@property (nonatomic, copy) void(^exportVideoProgressBlock)(CGFloat progress);
@property (nonatomic, copy) void(^exportVideoCompletionBlock)(NSError *error);

@end

@implementation BTMProjectRenderer

- (instancetype)initWithProject:(BTMProject *)project {
    self = [super init];
    if (self) {
        self.project = project;
    }
    return self;
}

- (void)renderProjectWithProgressBlock:(void(^)(CGFloat progress))progressBlock
                       completionBlock:(void(^)(NSError *error))completionBlock {
    [self resetTime];
    self.isFirstFrameDrawn = NO;
    self.renderingCancelled = NO;
    self.audioWritingFinished = NO;
    self.videoWritingFinished = NO;
    self.exportVideoProgressBlock = progressBlock;
    self.exportVideoCompletionBlock = completionBlock;
    __weak typeof (self) weakSelf = self;
    [self createSecondBackgroundTextureWithOffset:CMTimeGetSeconds(self.time) completionBlock:^(CGSize videoSize) {
        [weakSelf exportVideoWithVideoSize:videoSize];
    }];
}

- (void)cancelRendering {
    self.renderingCancelled = YES;
    if (self.assetWriter.status == AVAssetWriterStatusWriting) {
        [self.assetWriter cancelWriting];
    }
}

- (void)dealloc {
    NSLog(@"RENDERER DEALLOCED");
}

#pragma mark - Setters/Getters

- (CGFloat)videoScale {
    CGFloat minScale = self.project.selectedVideoFilter.minScale;
    CGFloat maxScale = self.project.selectedVideoFilter.maxScale;
    CMTime startTime = [[self.project beatTimeValueAtIndex:(self.indexOfNextBeat - 1)] CMTimeValue];
    CMTime endTime = self.nextBeatTime;
    CMTime duration = CMTimeMake(endTime.value - startTime.value, endTime.timescale);
    CMTime transformedTime = CMTimeConvertScale(self.time, startTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
    CGFloat timeScale = (double)(transformedTime.value - startTime.value) / (double)duration.value;
    CGFloat videoScale = (maxScale - minScale) * timeScale + minScale;
    videoScale = MAX(videoScale, minScale);
    videoScale = MIN(videoScale, maxScale);
    return videoScale;
}

- (BOOL)needsDrawNextFrame {
    return self.time.value % (int)(self.project.selectedVideoFilter.timeStretch) == 0;
}

#pragma mark - Export

- (void)initializeVideoExport:(NSError **)outError {
    NSDictionary *outputSettings = @{
            AVVideoCodecKey : AVVideoCodecH264,
            AVVideoWidthKey : @(self.exportFrameBuffer.size.width),
            AVVideoHeightKey : @(self.exportFrameBuffer.size.height),
    };
    self.videoAssetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:outputSettings];

    NSDictionary *sourcePixelBufferAttributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
            [NSNumber numberWithInt:kCVPixelFormatType_32BGRA], kCVPixelBufferPixelFormatTypeKey,
            [NSNumber numberWithInt:self.exportFrameBuffer.size.width], kCVPixelBufferWidthKey,
            [NSNumber numberWithInt:self.exportFrameBuffer.size.height], kCVPixelBufferHeightKey,
            [NSNumber numberWithBool:YES], kCVPixelBufferOpenGLESCompatibilityKey,
            [NSNumber numberWithBool:YES], kCVPixelBufferOpenGLCompatibilityKey,
                    nil];
    self.assetWriterInputPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.videoAssetWriterInput
                                                                                                               sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    if ([self.assetWriter canAddInput:self.videoAssetWriterInput]){
        [self.assetWriter addInput:self.videoAssetWriterInput];
    }
    else{
        *outError = [NSError errorWithDomain:@"AVAssetWriter can't add output" code:100 userInfo:nil];
        return;
    }
}

- (void)initializeAudioExport:(NSError **)outError {
    if (!self.project.audio)
        return;

    AVURLAsset *audioAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:self.project.audioPath] options:nil];
    NSError *error = nil;
    self.audioAssetReader = [AVAssetReader assetReaderWithAsset:audioAsset error:&error];
    if (error){
        *outError = error;
        return;
    }

    self.audioAssetReaderOutput = [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:audioAsset.tracks audioSettings:nil];
    if ([self.audioAssetReader canAddOutput:self.audioAssetReaderOutput]) {
        [self.audioAssetReader addOutput:self.audioAssetReaderOutput];
    }
    else{
        error = [NSError errorWithDomain:@"AVAssetReader can't add output" code:100 userInfo:nil];
        *outError = error;
        return;
    }
    [self.audioAssetReader setTimeRange:self.project.audio.timeRange];

    AudioChannelLayout channelLayout;
    memset(&channelLayout, 0, sizeof(AudioChannelLayout));
    channelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
    NSDictionary *outputAudioSettings = @{
            AVFormatIDKey : @(kAudioFormatMPEG4AAC),
            AVNumberOfChannelsKey : @(1),
            AVSampleRateKey : @(44100.0),
            AVChannelLayoutKey : [NSData dataWithBytes:&channelLayout length:sizeof(AudioChannelLayout)],
            AVEncoderBitRateKey : @(64000),
    };
    self.audioAssetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:outputAudioSettings];

    if ([self.assetWriter canAddInput:self.audioAssetWriterInput]){
        [self.assetWriter addInput:self.audioAssetWriterInput];
    }
    else{
        error = [NSError errorWithDomain:@"AVAssetWriter can't add output" code:100 userInfo:nil];
        *outError = error;
        return;
    }
}

- (void)exportVideoWithVideoSize:(CGSize)videoSize {
    self.currentCrossFadeFramesCount = self.project.videoСrossFadeFramesCount;

    self.backgroundVideo601Program = [GLProgramCache programWithClass:[CPVideoColorMatrix601ColorSwizzlingProgram class]];
    self.backgroundVideo709Program = [GLProgramCache programWithClass:[CPVideoColorMatrix709ColorSwizzlingProgram class]];
    self.exportFrameBuffer = [GLFrameBuffer frameBufferWithFormat:GLFBOPixelFormatRGB565 andSize:videoSize];
    
    NSURL *exportURL = [NSURL fileURLWithPath:self.project.exportPath];

    NSError *error = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportURL.path]){
        [[NSFileManager defaultManager] removeItemAtPath:exportURL.path error:&error];
        if (error){
            if (self.exportVideoCompletionBlock){
                self.exportVideoCompletionBlock(error);
            }
            return;
        }
    }

    self.assetWriter = [[AVAssetWriter alloc] initWithURL:exportURL fileType:AVFileTypeMPEG4 error:&error];
    if (error){
        if (self.exportVideoCompletionBlock){
            self.exportVideoCompletionBlock(error);
        }
        return;
    }

    [self initializeAudioExport:&error];
    if (error){
        if (self.exportVideoCompletionBlock){
            self.exportVideoCompletionBlock(error);
        }
        return;
    }

    [self initializeVideoExport:&error];
    if (error){
        if (self.exportVideoCompletionBlock){
            self.exportVideoCompletionBlock(error);
        }
        return;
    }

    if (![self.assetWriter startWriting]){
        if (self.exportVideoCompletionBlock){
            NSError *cError = [NSError errorWithDomain:@"AVAssetWriter can't start writing" code:100 userInfo:nil];
            self.exportVideoCompletionBlock(cError);
        }
        return;
    }
    [self.assetWriter startSessionAtSourceTime:kCMTimeZero];
    [self.audioAssetReader startReading];

    __block BOOL audioWritingFinished = NO;

    __weak typeof (self) weakSelf = self;

    CMTime duration = self.project.audio.timeRange.duration;
    CMTime tDuration = CMTimeConvertScale(duration, self.time.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
    CMTime startTime = [[self.project beatTimeValueAtIndex:0] CMTimeValue];
    
    if (self.project.audio) {
        dispatch_queue_t writeAudioQueue = dispatch_queue_create("Writer Audio Queue", NULL);
        [self.audioAssetWriterInput requestMediaDataWhenReadyOnQueue:writeAudioQueue usingBlock:^{
            @autoreleasepool {
                [weakSelf renderAudioWithStartTime:startTime duration:tDuration];
            }
        }];
    }
    else {
        audioWritingFinished = YES;
    }

    dispatch_queue_t writeVideoQueue = dispatch_queue_create("Writer Video Queue", NULL);
    [self.videoAssetWriterInput requestMediaDataWhenReadyOnQueue:writeVideoQueue usingBlock:^{
        @autoreleasepool {
            [weakSelf renderVideoWithStartTime:startTime duration:tDuration];
        }
    }];
}

- (void)renderAudioWithStartTime:(CMTime)startTime duration:(CMTime)duration {
    while (!self.audioWritingFinished) {
        if ([self.audioAssetWriterInput isReadyForMoreMediaData] && !self.renderingCancelled) {
            CMSampleBufferRef nextBuffer = [self.audioAssetReaderOutput copyNextSampleBuffer];
            if (nextBuffer){
                CMTime presentationTime = CMTimeMake(self.time.value - startTime.value, self.time.timescale);
                CMSampleTimingInfo timing;
                CMSampleBufferGetSampleTimingInfo(nextBuffer, 0, &timing);
                timing.presentationTimeStamp = presentationTime;
                CMSampleBufferRef nBuffer;
                CMSampleBufferCreateCopyWithNewTiming(kCFAllocatorDefault, nextBuffer, 1, &timing, &nBuffer);
                if (![self.audioAssetWriterInput appendSampleBuffer:nBuffer]){
                    NSLog(@"error append audio");
                }
                CFRelease(nBuffer);
            }
            else {
                [self.audioAssetWriterInput markAsFinished];
                [self.audioAssetReader cancelReading];
                self.audioWritingFinished = YES;
                if (self.audioWritingFinished && self.videoWritingFinished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self finishExport];
                    });
                }
                break;
            }
        }
        else {
            if (self.assetWriter.status == AVAssetWriterStatusCancelled) {
                self.audioWritingFinished = YES;
                if (self.exportVideoCompletionBlock){
                    NSError *error = [NSError cp_errorWithCode:CPErrorCancelled description:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.exportVideoCompletionBlock(error);
                    });
                }
                break;
            }
        }
    }
}

- (void)renderVideoWithStartTime:(CMTime)startTime duration:(CMTime)duration {
    while (!self.videoWritingFinished){
        if ([self.videoAssetWriterInput isReadyForMoreMediaData] && !self.renderingCancelled){
            [GLContext lock];
            [self draw];
            [GLContext unlock];
            if (!self.isFirstFrameDrawn) {
                continue;
            }
            CVPixelBufferRef pixel_buffer = NULL;
            CVReturn status = CVPixelBufferPoolCreatePixelBuffer(NULL, [self.assetWriterInputPixelBufferAdaptor pixelBufferPool], &pixel_buffer);
            if ((pixel_buffer == NULL) || (status != kCVReturnSuccess)){
                NSLog(@"NO PIXEL BUFFER");
                continue;
            }
            else{
                CVPixelBufferLockBaseAddress(pixel_buffer, 0);
                GLubyte *pixelBufferData = (GLubyte *)CVPixelBufferGetBaseAddress(pixel_buffer);
                [GLContext lock];
                [self.exportFrameBuffer bind];
                glReadPixels(0, 0, (GLsizei)self.exportFrameBuffer.size.width, (GLsizei)self.exportFrameBuffer.size.height, GL_RGBA, GL_UNSIGNED_BYTE, pixelBufferData);
                [self.exportFrameBuffer unbind];
                [GLContext unlock];
            }
            CMTime time = self.time;
            CMTime presentationTime = CMTimeMake(time.value - startTime.value, time.timescale);
            if ([self.assetWriterInputPixelBufferAdaptor appendPixelBuffer:pixel_buffer withPresentationTime:presentationTime]){
                [self updateTime];
            }
            else{
                NSLog(@"Problem appending pixel buffer at time: %lld", presentationTime.value);
            }
            CVPixelBufferUnlockBaseAddress(pixel_buffer, 0);
            CVPixelBufferRelease(pixel_buffer);
            [self updateTime];
            
            if (self.exportVideoProgressBlock){
                CGFloat progress = (CGFloat)(CMTimeGetSeconds(self.time) / CMTimeGetSeconds(duration));
                if (progress >= 0.0f && progress <= 1.0f){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.exportVideoProgressBlock(progress);
                    });
                }
            }
        }
        else {
            if (self.assetWriter.status == AVAssetWriterStatusCancelled) {
                self.videoWritingFinished = YES;
                if (self.exportVideoCompletionBlock){
                    NSError *cError = [NSError errorWithDomain:CPErrorDomain code:CPErrorCancelled userInfo:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.exportVideoCompletionBlock(cError);
                    });
                }
                break;
            }
        }
    }
}

- (void)finishExport {
    __weak typeof (self) weakSelf = self;
    self.audioWritingFinished = YES;
    self.videoWritingFinished = YES;
    [self.assetWriter finishWritingWithCompletionHandler:^{
        NSError *error = nil;
        if (weakSelf.assetWriter.status == AVAssetWriterStatusFailed) {
            error = weakSelf.assetWriter.error;
        }
        if (self.exportVideoCompletionBlock){
            dispatch_async(dispatch_get_main_queue(), ^{
                self.exportVideoCompletionBlock(error);
            });
        }
    }];
}

#pragma mark - Time

- (void)updateTime {
    NSValue *beatTimeValue = [self.project beatTimeValueAtIndex:self.indexOfNextBeat];
    if (beatTimeValue) {
        CMTime beatTime = [beatTimeValue CMTimeValue];
        CMTime tBeatTime = CMTimeConvertScale(beatTime, self.time.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (tBeatTime.value == self.time.value) {
            if ([self.project isLastBeatIndex:self.indexOfNextBeat]) {
                [self.audioAssetWriterInput markAsFinished];
                [self.audioAssetReader cancelReading];
                [self.videoAssetWriterInput markAsFinished];
                [self finishExport];
            }
            else {
                NSValue *nextTimeValue = [self.project beatTimeValueAtIndex:(self.indexOfNextBeat + 1)];
                if (nextTimeValue) {
                    CMTime nextTime = [nextTimeValue CMTimeValue];
                    self.nextBeatTime = nextTime;
                    self.indexOfNextBeat++;
                    NSTimeInterval videoInterval = CMTimeGetSeconds(self.project.video.duration) * self.project.selectedVideoFilter.timeStretch / 2.0
                    - (CMTimeGetSeconds(self.nextBeatTime) - CMTimeGetSeconds(self.videoTimeOffset));
                    NSTimeInterval beatInterval = CMTimeGetSeconds(nextTime) - CMTimeGetSeconds(self.nextBeatTime);
                    if (videoInterval < beatInterval) {
                        self.videoTimeOffset = self.nextBeatTime;
                    }
                }
            }
            self.currentCrossFadeFramesCount = self.project.videoСrossFadeFramesCount;
        }
    }
    self.time = CMTimeMake(self.time.value + 1, (int32_t)self.project.videoFramesPerSecond);
}

- (void)resetTime {
    self.time = [[self.project beatTimeValueAtIndex:0] CMTimeValue];
    [self resetBeatTime];
}

- (void)resetBeatTime {
    self.indexOfNextBeat = 0;
    self.videoTimeOffset = [[self.project beatTimeValueAtIndex:self.indexOfNextBeat] CMTimeValue];
    self.indexOfNextBeat++;
    self.nextBeatTime = [[self.project beatTimeValueAtIndex:self.indexOfNextBeat] CMTimeValue];
}


#pragma mark - Textures

- (void)createFirstBackgroundTextureWithOffset:(NSTimeInterval)offset completionBlock:(void(^)(CGSize videoSize))completionBlock {
    [self.firstBackgroundTexture stopReading];
    self.firstBackgroundTexture = nil;
    CPGLVideoTexture *videoTexture = [[CPGLVideoTexture alloc] init];
    videoTexture.currentURL = [NSURL fileURLWithPath:self.project.videoPath];
    videoTexture.shouldRepeat = YES;
    self.firstBackgroundTexture = videoTexture;
    __weak typeof (videoTexture) weakVideoTexture = videoTexture;
    [videoTexture startReadingWithOffset:offset completionBlock:^{
        CGSize videoSize = weakVideoTexture.assetVideoTrack.naturalSize;
        if (completionBlock) {
            completionBlock(videoSize);
        }
    }];
}

- (void)createSecondBackgroundTextureWithOffset:(NSTimeInterval)offset completionBlock:(void(^)(CGSize videoSize))completionBlock {
    [self.secondBackgroundTexture stopReading];
    self.secondBackgroundTexture = nil;
    CPGLVideoTexture *videoTexture = [[CPGLVideoTexture alloc] init];
    videoTexture.currentURL = [NSURL fileURLWithPath:self.project.videoPath];
    videoTexture.shouldRepeat = YES;
    self.secondBackgroundTexture = videoTexture;
    __weak typeof (videoTexture) weakVideoTexture = videoTexture;
    [videoTexture startReadingWithOffset:offset completionBlock:^{
        CGSize videoSize = weakVideoTexture.assetVideoTrack.naturalSize;
        if (completionBlock) {
            completionBlock(videoSize);
        }
    }];
}

#pragma mark - Draw

- (void)draw {
    [self.exportFrameBuffer bind];
    [GLStateCache setViewportSize:self.exportFrameBuffer.size];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    [self drawBackground];
    [self.exportFrameBuffer unbind];
}

- (void)drawBackground {
    CGFloat videoScale = self.videoScale;
    if (self.currentCrossFadeFramesCount > 0) {
        NSTimeInterval offset = 2.0 * (CMTimeGetSeconds(self.nextBeatTime) - CMTimeGetSeconds(self.videoTimeOffset)) / self.project.selectedVideoFilter.timeStretch;
        CGFloat topTextureAlpha = (float)self.currentCrossFadeFramesCount / (float)self.project.videoСrossFadeFramesCount;
        if (self.indexOfNextBeat % 2 == 1) {
            [self drawVideoTexture:self.secondBackgroundTexture withScale:videoScale alpha:1.0];
            if (self.firstBackgroundTexture) {
                [self drawVideoTexture:self.firstBackgroundTexture withScale:self.project.selectedVideoFilter.maxScale alpha:topTextureAlpha];
            }
            if (self.currentCrossFadeFramesCount == 1) {
                [self createFirstBackgroundTextureWithOffset:offset completionBlock:nil];
            }
        }
        else {
            [self drawVideoTexture:self.firstBackgroundTexture withScale:videoScale alpha:1.0];
            if (self.secondBackgroundTexture) {
                [self drawVideoTexture:self.secondBackgroundTexture withScale:self.project.selectedVideoFilter.maxScale alpha:topTextureAlpha];
            }
            if (self.currentCrossFadeFramesCount == 1) {
                [self createSecondBackgroundTextureWithOffset:offset completionBlock:nil];
            }
        }
        self.currentCrossFadeFramesCount--;
    }
    else {
        CPGLVideoTexture *backgroundTexture = (self.indexOfNextBeat % 2 == 1) ? self.secondBackgroundTexture : self.firstBackgroundTexture;
        [self drawVideoTexture:backgroundTexture withScale:videoScale alpha:1.0];
    }
}

- (void)drawVideoTexture:(CPGLVideoTexture *)videoTexture withScale:(CGFloat)scale alpha:(CGFloat)alpha {
    if (self.project.selectedVideoFilter.videoMode == BTMVideoFilterVideoModeStillFrame || !self.needsDrawNextFrame) {
        [self drawVideoTextureWithCurrentFrame:videoTexture scale:scale alpha:alpha];
    }
    else {
        [self drawVideoTextureWithNextFrame:videoTexture scale:scale alpha:alpha];
    }
}

- (void)drawVideoTextureWithNextFrame:(CPGLVideoTexture *)videoTexture scale:(CGFloat)scale alpha:(CGFloat)alpha {
    __weak typeof (self) weakSelf = self;
    [videoTexture readNextFrameWithCompletionBlock:^(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle) {
        [weakSelf drawBackgroundVideoWithTexture:texture
                                           scale:scale
                                           alpha:alpha
                                     colorMatrix:colorMatrix
                             chromaTextureHandle:chromaTextureHandle
                              lumiaTextureHandle:lumiaTextureHandle];
    }];
}

- (void)drawVideoTextureWithCurrentFrame:(CPGLVideoTexture *)videoTexture scale:(CGFloat)scale alpha:(CGFloat)alpha {
    __weak typeof (self) weakSelf = self;
    [videoTexture readCurrentFrameWithCompletionBlock:^(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle) {
        [weakSelf drawBackgroundVideoWithTexture:texture
                                           scale:scale
                                           alpha:alpha
                                     colorMatrix:colorMatrix
                             chromaTextureHandle:chromaTextureHandle
                              lumiaTextureHandle:lumiaTextureHandle];
    }];
}

- (void)drawBackgroundVideoWithTexture:(CPGLVideoTexture *)texture
                                 scale:(CGFloat)scale
                                 alpha:(CGFloat)alpha
                           colorMatrix:(GLVideoTextureColorMatrix)colorMatrix
                   chromaTextureHandle:(GLenum)chromaTextureHandle
                    lumiaTextureHandle:(GLenum)lumiaTextureHandle {
    
    CGRect cropRect = CGRectMake(0.0, 0.0, 1.0, 1.0);
    CGRect scaledCropRect = rectApplyingScale(cropRect, 1.0 / scale);

    float texCoordinateMinX = CGRectGetMinX(scaledCropRect);
    float texCoordinateMaxX = CGRectGetMaxX(scaledCropRect);
    float texCoordinateMinY = CGRectGetMinY(scaledCropRect);
    float texCoordinateMaxY = CGRectGetMaxY(scaledCropRect);

    float vertices[] =
            {
                    -1.0f, 1.0f, texCoordinateMinX, texCoordinateMaxY,
                    1.0f, 1.0f, texCoordinateMaxX, texCoordinateMaxY,
                    -1.0f, -1.0f, texCoordinateMinX, texCoordinateMinY,
                    1.0f, -1.0f, texCoordinateMaxX, texCoordinateMinY,
            };

    GLProgram<CPVideoProgramProtocol> *program = (colorMatrix == GLVideoTextureColorMatrix601) ? self.backgroundVideo601Program : self.backgroundVideo709Program;
    [GLStateCache useGLProgram:program];
    [program setUniform1i:program.uniformSamplerY value:chromaTextureHandle];
    [program setUniform1i:program.uniformSamplerUV value:lumiaTextureHandle];
    [program setUniform1f:program.uniformAlpha value:alpha];
    [program setUniformMatrix4x4:program.uniformProjectionMatrix matrix:GLMatrix4x4::GLMatrix4x4()];
    [program setUniformMatrix4x4:program.uniformModelViewMatrix matrix:GLMatrix4x4::GLMatrix4x4()];

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glVertexAttribPointer(program.attribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4, &vertices[0]);
    glVertexAttribPointer(program.attribTexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4, &vertices[2]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisable(GL_BLEND);
    
    self.isFirstFrameDrawn = YES;
}

@end

