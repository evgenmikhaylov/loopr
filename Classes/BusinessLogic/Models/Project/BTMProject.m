//
//  BTMProject.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProject.h"

#import "BTMAudioFilter.h"
#import "BTMVideoFilter.h"
#import "BTMVideo.h"
#import "BTMAudio.h"

#import "NSFileManager+BTM.h"

#import <AVFoundation/AVFoundation.h>

@interface BTMProject ()

@property (nonatomic) NSString *projectID;
@property (nonatomic) NSUInteger selectedAudioFilterIndex;
@property (nonatomic) NSUInteger selectedVideoFilterIndex;

@end

@implementation BTMProject

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapKeyPath:@"projectID" toProperty:@"projectID"];
    [mapping mapKeyPath:@"selectedAudioFilterIndex" toProperty:@"selectedAudioFilterIndex"];
    [mapping mapKeyPath:@"selectedVideoFilterIndex" toProperty:@"selectedVideoFilterIndex"];
    [mapping hasOne:[BTMAudio class] forKeyPath:@"audio" forProperty:@"audio"];
    [mapping hasMany:[BTMAudioFilter class] forKeyPath:@"audioFilters" forProperty:@"audioFilters"];
    [mapping hasOne:[BTMVideo class] forKeyPath:@"video" forProperty:@"video"];
    [mapping hasMany:[BTMVideoFilter class] forKeyPath:@"videoFilters" forProperty:@"videoFilters"];
    [mapping mapKeyPath:@"mute" toProperty:@"mute"];
    return mapping;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.projectID = [[NSUUID UUID] UUIDString];
    }
    return self;
}

#pragma mark - Audio

- (void)setSelectedAudioFilter:(BTMAudioFilter *)selectedAudioFilter {
    if (![self.audioFilters containsObject:selectedAudioFilter]) {
        self.selectedAudioFilterIndex = 0;
        return;
    }
    self.selectedAudioFilterIndex = [self.audioFilters indexOfObject:selectedAudioFilter];
    self.audio.beatsDivider = selectedAudioFilter.beatsDivider;
}

- (BTMAudioFilter *)selectedAudioFilter {
    if (self.audioFilters.count == 0) {
        return nil;
    }
    if (self.selectedAudioFilterIndex >= self.audioFilters.count) {
        self.selectedAudioFilterIndex = 0;
    }
    return self.audioFilters[self.selectedAudioFilterIndex];
}

- (NSString *)audioPath {
    return [[[NSFileManager projectsDirectory] stringByAppendingPathComponent:self.projectID] stringByAppendingPathComponent:@"Audio.m4a"];
}

- (NSValue *)beatTimeValueAtIndex:(NSUInteger)index {
    if (index >= self.audio.filteredBeats.count) {
        return nil;
    }
    NSNumber *beatTime = self.audio.filteredBeats[index];
    int32_t timeScale = (int32_t)self.videoFramesPerSecond;
    int64_t value = beatTime.doubleValue * timeScale;
    CMTime time = CMTimeMake(value, timeScale);
    return [NSValue valueWithCMTime:time];
}

- (BOOL)isLastBeatIndex:(NSUInteger)index {
    if (index == (self.audio.filteredBeats.count - 1) || index >= self.audio.filteredBeats.count) {
        return YES;
    }
    return NO;
}

#pragma mark - Video

- (void)setSelectedVideoFilter:(BTMVideoFilter *)selectedVideoFilter {
    if (![self.videoFilters containsObject:selectedVideoFilter]) {
        self.selectedVideoFilterIndex = 0;
        return;
    }
    self.selectedVideoFilterIndex = [self.videoFilters indexOfObject:selectedVideoFilter];
}

- (BTMVideoFilter *)selectedVideoFilter {
    if (self.videoFilters.count == 0) {
        return nil;
    }
    if (self.selectedVideoFilterIndex >= self.videoFilters.count) {
        self.selectedVideoFilterIndex = 0;
    }
    return self.videoFilters[self.selectedVideoFilterIndex];
}

- (NSUInteger)videoFramesPerSecond {
    return 60;
}

- (NSUInteger)videoСrossFadeFramesCount {
    if (self.selectedVideoFilter.transitionMode == BTMVideoFilterTransitionModeCrossFade) {
        NSUInteger framesCount = self.selectedVideoFilter.crossFadeFramesCount * self.videoFramesPerSecond / 60.0;
        return framesCount;
    }
    return 1;
}

- (NSString *)videoPath {
    return [[[NSFileManager projectsDirectory] stringByAppendingPathComponent:self.projectID] stringByAppendingPathComponent:@"Video.mp4"];
}

- (NSString *)videoFiltersPath {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"VideoFilters.json"];
}

#pragma mark - Export

- (NSString *)exportPath {
    return [[[NSFileManager projectsDirectory] stringByAppendingPathComponent:self.projectID] stringByAppendingPathComponent:@"Rendered.mp4"];
}

@end
