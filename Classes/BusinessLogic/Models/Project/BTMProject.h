//
//  BTMProject.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "CPBaseObject.h"
#import <CoreMedia/CoreMedia.h>

@class BTMAudioFilter;
@class BTMVideoFilter;
@class BTMAudio;
@class BTMVideo;

NS_ASSUME_NONNULL_BEGIN

@interface BTMProject : CPBaseObject

@property (nonatomic, readonly) NSString *projectID;


@property (nonatomic) BTMAudio *audio;
@property (nonatomic) NSArray<BTMAudioFilter *> *audioFilters;
@property (nonatomic) BTMAudioFilter *selectedAudioFilter;
@property (nonatomic) BOOL mute;
@property (nonatomic, readonly) NSString *audioPath;
- (NSValue *)beatTimeValueAtIndex:(NSUInteger)index;
- (BOOL)isLastBeatIndex:(NSUInteger)index;

@property (nonatomic) BTMVideo *video;
@property (nonatomic) NSArray<BTMVideoFilter *> *videoFilters;
@property (nonatomic) BTMVideoFilter *selectedVideoFilter;
@property (nonatomic, readonly) NSUInteger videoFramesPerSecond;
@property (nonatomic, readonly) NSUInteger videoСrossFadeFramesCount;
@property (nonatomic, readonly) NSString *videoPath;
@property (nonatomic, readonly) NSString *videoFiltersPath;


@property (nonatomic, readonly) NSString *exportPath;

@end

NS_ASSUME_NONNULL_END
