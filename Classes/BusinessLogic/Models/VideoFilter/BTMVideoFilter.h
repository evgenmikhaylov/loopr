//
//  BTMVideoFilter.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 23/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "CPBaseObject.h"

typedef NS_ENUM(NSUInteger, BTMVideoFilterVideoMode) {
    BTMVideoFilterVideoModeStillFrame,
    BTMVideoFilterVideoModeLinear,
    BTMVideoFilterVideoModeEaseIn,
    BTMVideoFilterVideoModeEaseOut,
    BTMVideoFilterVideoModeEaseInOut,
    BTMVideoFilterVideoModeCustom,
};

typedef NS_ENUM(NSUInteger, BTMVideoFilterTransitionMode) {
    BTMVideoFilterTransitionModeCrossFade,
    BTMVideoFilterTransitionModeCrossCut,
};

typedef NS_ENUM(NSUInteger, BTMVideoFilterScaleMode) {
    BTMVideoFilterScaleModeLinear,
    BTMVideoFilterScaleModeEaseIn,
    BTMVideoFilterScaleModeEaseOut,
    BTMVideoFilterScaleModeEaseInOut,
    BTMVideoFilterScaleModeCustom,
};

@interface BTMVideoFilter : CPBaseObject

@property (nonatomic) NSString *name;
@property (nonatomic) CGFloat timeStretch;
@property (nonatomic) BTMVideoFilterVideoMode videoMode;
@property (nonatomic) BTMVideoFilterTransitionMode transitionMode;
@property (nonatomic) NSUInteger crossFadeFramesCount;
@property (nonatomic) CGFloat minScale;
@property (nonatomic) CGFloat maxScale;
@property (nonatomic) BTMVideoFilterScaleMode scaleMode;

@end
