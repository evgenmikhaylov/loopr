//
//  BTMProjectVideoFiltersFactory.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 23/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMVideoFilter;

@interface BTMProjectVideoFiltersFactory : NSObject

+ (NSArray<BTMVideoFilter *> *)projectVideoFiltersWithFile:(NSURL *)fileURL;

@end
