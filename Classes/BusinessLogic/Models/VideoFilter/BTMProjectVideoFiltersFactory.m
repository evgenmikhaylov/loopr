//
//  BTMProjectVideoFiltersFactory.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 23/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectVideoFiltersFactory.h"
#import "BTMVideoFilter.h"

@implementation BTMProjectVideoFiltersFactory

+ (NSArray<BTMVideoFilter *> *)projectVideoFiltersWithFile:(NSURL *)fileURL {
    NSMutableArray<BTMVideoFilter *> *filters = [[NSMutableArray alloc] init];
    NSData *data = [NSData dataWithContentsOfURL:fileURL];
    if (!data) {
        return nil;
    }
    NSDictionary *filtersDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    [filtersDictionary[@"filters"] enumerateObjectsUsingBlock:^(id filterDictionary, NSUInteger idx, BOOL *stop) {
        BTMVideoFilter *filter = [BTMVideoFilter objectWithProperties:filterDictionary];
        [filters addObject:filter];
    }];
    return filters.copy;
}

@end
