//
//  BTMVideoFilter.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 23/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "EKObjectMapping+Helpers.h"
#import "BTMVideoFilter.h"

@implementation BTMVideoFilter

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"time_stretch" toProperty:@"timeStretch"];
    [mapping mapEnumerationWithKeyValueDictionary:@{
                                                    @"still_frame": @(BTMVideoFilterVideoModeStillFrame),
                                                    @"linear": @(BTMVideoFilterVideoModeLinear),
                                                    @"easein": @(BTMVideoFilterVideoModeEaseIn),
                                                    @"easeout": @(BTMVideoFilterVideoModeEaseOut),
                                                    @"easein_easeout": @(BTMVideoFilterVideoModeEaseInOut),
                                                    @"custom": @(BTMVideoFilterVideoModeCustom),
                                                    }
                                       defaultKey:@"linear"
                                      fromKeyPath:@"video_mode"
                                       toProperty:@"videoMode"];
    [mapping mapEnumerationWithKeyValueDictionary:@{
                                                    @"cross_fade": @(BTMVideoFilterTransitionModeCrossFade),
                                                    @"cross_cut": @(BTMVideoFilterTransitionModeCrossCut),
                                                    }
                                       defaultKey:@"cross_cut"
                                      fromKeyPath:@"transition_mode"
                                       toProperty:@"transitionMode"];
    [mapping mapKeyPath:@"cross_fade_frames_count" toProperty:@"crossFadeFramesCount"];
    [mapping mapKeyPath:@"min_scale" toProperty:@"minScale"];
    [mapping mapKeyPath:@"max_scale" toProperty:@"maxScale"];
    [mapping mapEnumerationWithKeyValueDictionary:@{
                                                    @"linear": @(BTMVideoFilterScaleModeLinear),
                                                    @"easein": @(BTMVideoFilterScaleModeEaseIn),
                                                    @"easeout": @(BTMVideoFilterScaleModeEaseOut),
                                                    @"easein_easeout": @(BTMVideoFilterScaleModeEaseInOut),
                                                    @"custom": @(BTMVideoFilterScaleModeCustom),
                                                    }
                                       defaultKey:@"linear"
                                      fromKeyPath:@"scale_mode"
                                       toProperty:@"scaleMode"];
    return mapping;
}

@end
