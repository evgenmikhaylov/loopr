//
//  BTMAudioFilter.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 24/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMAudioFilter.h"

@implementation BTMAudioFilter

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"beatsDivider" toProperty:@"beatsDivider"];
    return mapping;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.beatsDivider = 1;
    }
    return self;
}

@end
