//
//  BTMAudioFilter.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 24/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "CPBaseObject.h"

@interface BTMAudioFilter : CPBaseObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSUInteger beatsDivider;

@end
