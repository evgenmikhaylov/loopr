//
//  BTMProjectAudioFiltersFactory.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 24/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMAudioFilter;

@interface BTMProjectAudioFiltersFactory : NSObject

+ (NSArray<BTMAudioFilter *> *)projectAudioFilters;

@end
