//
//  BTMAudio.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMAudio.h"
#import "EKObjectMapping+Helpers.h"

@interface BTMAudio ()

@property (nonatomic) CMTimeRange timeRange;
@property (nonatomic) NSArray<NSNumber *> *beats;
@property (nonatomic) NSArray<NSNumber *> *filteredBeats;
@property (nonatomic) NSArray<NSNumber *> *filteredBeatsArray;

@end

@implementation BTMAudio

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapCMTimeRangeFromKeyPath:@"timeRange" toProperty:@"timeRange"];
    [mapping mapCMTimeFromKeyPath:@"duration" toProperty:@"duration"];
    [mapping mapKeyPath:@"beats" toProperty:@"beats"];
    [mapping mapKeyPath:@"beatsDivider" toProperty:@"beatsDivider"];
    
    [mapping mapKeyPath:@"beatsArray" toProperty:@"beatsArray"];
    return mapping;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.beatsDivider = 1;
    }
    return self;
}

#pragma mark - Setters/Getters

- (void)setBeats:(NSArray<NSNumber *> *)beats {
    _beats = beats;
}

- (void)setFilteredBeats:(NSArray<NSNumber *> *)filteredBeats {
    _filteredBeats = filteredBeats;
    [self updateTimeRange];
}

- (void)setBeatsDivider:(NSUInteger)beatsDivider {
    _beatsDivider = beatsDivider;
    [self updateFilteredBeatsArray];
}

- (void)setBeatsArray:(NSArray<NSNumber *> *)beatsArray {
    _beatsArray = beatsArray;
    self.beats = [self beatsWithBeatsArray:_beatsArray];
    [self updateFilteredBeatsArray];
}

- (void)setFilteredBeatsArray:(NSArray<NSNumber *> *)filteredBeatsArray {
    _filteredBeatsArray = filteredBeatsArray;
    self.filteredBeats = [self beatsWithBeatsArray:_filteredBeatsArray];
}

- (NSUInteger)phraseLength {
    return 8;
}

#pragma mark - Helpers

- (void)updateTimeRange {
    int32_t timeScale = 600;
    NSTimeInterval firstBeatTime = [_filteredBeats.firstObject doubleValue];
    NSTimeInterval lastBeatTime = [_filteredBeats.lastObject doubleValue];
    NSTimeInterval audioDuration = lastBeatTime - firstBeatTime;
    self.timeRange = CMTimeRangeMake(CMTimeMake(timeScale * firstBeatTime, timeScale), CMTimeMake(timeScale * audioDuration, timeScale));
}

- (NSArray<NSNumber *> *)beatsWithBeatsArray:(NSArray<NSNumber *> *)beatsArray {
    if (beatsArray.count == 0) {
        return @[];
    }
    NSMutableArray<NSNumber *> *beats = [[NSMutableArray alloc] init];
    [beatsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull beat, NSUInteger idx, BOOL * _Nonnull stop) {
        if (beat.intValue > 0) {
            double time = CMTimeGetSeconds(self.duration) * (double)idx / (double)beatsArray.count;
            [beats addObject:@(time)];
        }
    }];
    NSUInteger remainder = beats.count % self.phraseLength;
    [beats removeObjectsInRange:NSMakeRange(beats.count - remainder, remainder)];
    return beats.copy;
}

- (void)updateFilteredBeats {
    NSMutableArray *filteredBeats = [[NSMutableArray alloc] init];
    [self.beats enumerateObjectsUsingBlock:^(NSNumber * _Nonnull beat, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx % _beatsDivider == 0) {
            [filteredBeats addObject:beat];
        }
    }];
    self.filteredBeats = filteredBeats.copy;
}

- (void)updateFilteredBeatsArray {
    NSMutableArray *filteredBeatsArray = [[NSMutableArray alloc] init];
    __block NSUInteger beatIndex = 0;
    [self.beatsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull beat, NSUInteger idx, BOOL * _Nonnull stop) {
        if (beat.intValue > 0) {
            beatIndex++;
            if (beatIndex % self.beatsDivider == 0) {
                [filteredBeatsArray addObject:beat];
            }
            else {
                [filteredBeatsArray addObject:@0];
            }
        }
        else {
            [filteredBeatsArray addObject:beat];
        }
    }];
    self.filteredBeatsArray = filteredBeatsArray.copy;
}

@end
