//
//  BTMAudio.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "CPBaseObject.h"
#import <CoreMedia/CoreMedia.h>

@interface BTMAudio : CPBaseObject

@property (nonatomic, readonly) CMTimeRange timeRange;
@property (nonatomic) CMTime duration;

@property (nonatomic) NSUInteger beatsDivider;
@property (nonatomic) NSArray<NSNumber *> *beatsArray;
@property (nonatomic, readonly) NSUInteger phraseLength;

@property (nonatomic, readonly) NSArray<NSNumber *> *beats;
@property (nonatomic, readonly) NSArray<NSNumber *> *filteredBeats;
@property (nonatomic, readonly) NSArray<NSNumber *> *filteredBeatsArray;

@end
