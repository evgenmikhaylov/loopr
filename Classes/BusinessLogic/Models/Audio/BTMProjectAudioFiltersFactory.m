//
//  BTMProjectAudioFiltersFactory.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 24/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectAudioFiltersFactory.h"
#import "BTMAudioFilter.h"

@implementation BTMProjectAudioFiltersFactory

+ (NSArray<BTMAudioFilter *> *)projectAudioFilters {
    NSMutableArray<BTMAudioFilter *> *filters = [[NSMutableArray alloc] init];
    {
        BTMAudioFilter *filter = [[BTMAudioFilter alloc] init];
        filter.name = @"Beats/1";
        filter.beatsDivider = 1;
        [filters addObject:filter];
    }
    {
        BTMAudioFilter *filter = [[BTMAudioFilter alloc] init];
        filter.name = @"Beats/2";
        filter.beatsDivider = 2;
        [filters addObject:filter];
    }
//    {
//        BTMAudioFilter *filter = [[BTMAudioFilter alloc] init];
//        filter.name = @"Beats/3";
//        [filters addObject:filter];
//    }
//    {
//        BTMAudioFilter *filter = [[BTMAudioFilter alloc] init];
//        filter.name = @"Beats/4";
//        [filters addObject:filter];
//    }
    return filters.copy;
}
@end
