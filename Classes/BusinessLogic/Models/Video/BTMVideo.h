//
//  BTMVideo.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "CPBaseObject.h"
#import <CoreMedia/CoreMedia.h>

@interface BTMVideo : CPBaseObject

@property (nonatomic) CGRect cropRect;
@property (nonatomic) CMTime duration;

@end
