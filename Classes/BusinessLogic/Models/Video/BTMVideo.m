//
//  BTMVideo.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMVideo.h"
#import "BTMVideoFilter.h"
#import "EKObjectMapping+Helpers.h"
#import "NSFileManager+BTM.h"

@interface BTMVideo ()

@end

@implementation BTMVideo

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapCMTimeFromKeyPath:@"duration" toProperty:@"duration"];
    [mapping mapCGRectFromKeyPath:@"cropRect" toProperty:@"cropRect"];
    return mapping;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.cropRect = CGRectZero;
    }
    return self;
}

@end
