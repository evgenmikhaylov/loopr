//
// Created by Evgeny Mikhaylov on 25/01/2017.
// Copyright (c) 2017 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMProject;
@class RACSignal;

@interface BTMProjectRenderService : NSObject

- (RACSignal *)rac_renderProjectSignal:(BTMProject *)project;
- (RACSignal *)rac_renderingProgressSignal;
- (RACSignal *)rac_cancelRenderingSignal;

@end
