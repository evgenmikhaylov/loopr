//
// Created by Evgeny Mikhaylov on 25/01/2017.
// Copyright (c) 2017 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMProjectRenderService.h"
#import "BTMProjectRenderer.h"
#import "BTMProject.h"
#import <ReactiveCocoa.h>

@interface BTMProjectRenderService ()

@property (nonatomic) BTMProjectRenderer *renderer;
@property (nonatomic) RACSubject *rac_renderingProgressSubject;

@end

@implementation BTMProjectRenderService

- (RACSignal *)rac_renderProjectSignal:(BTMProject *)project {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        self.rac_renderingProgressSubject = [RACSubject subject];
        self.renderer = [[BTMProjectRenderer alloc] initWithProject:project];
        [self.renderer renderProjectWithProgressBlock:^(CGFloat progress) {
            [self.rac_renderingProgressSubject sendNext:@(progress)];
        } completionBlock:^(NSError *error) {
            [subscriber sendNext:nil];
            [subscriber sendCompleted];
            [self.rac_renderingProgressSubject sendCompleted];
            self.rac_renderingProgressSubject = nil;
            self.renderer = nil;
        }];
        return nil;
    }];
}

- (RACSignal *)rac_renderingProgressSignal {
    return self.rac_renderingProgressSubject;
}

- (RACSignal *)rac_cancelRenderingSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self.renderer cancelRendering];
        [subscriber sendNext:nil];
        [subscriber sendCompleted];
        return nil;
    }];
}

@end
