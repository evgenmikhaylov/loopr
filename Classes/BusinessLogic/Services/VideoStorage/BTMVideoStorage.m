//
//  BTMVideoStorage.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMVideoStorage.h"
#import "BTMVideoService.h"

#import "NSFileManager+BTM.h"

#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <Photos/Photos.h>
#import <MVZExtensions.h>
#import <ReactiveCocoa.h>

@interface BTMVideoStorage ()

@property (nonatomic) BTMVideoService *videoService;

@end

@implementation BTMVideoStorage

- (instancetype)initWithVideoService:(BTMVideoService *)service {
    self = [super init];
    if (self) {
        self.videoService = service;
    }
    return self;
}

- (RACSignal *)rac_saveVideoWithPHAssetSignal:(PHAsset *)photosAsset
                                       toFile:(NSString *)filePath
                              withMaximumSize:(CGFloat)maximumSize
                                    timeRange:(CMTimeRange)timeRange
                              framesPerSecond:(NSUInteger)framesPerSecond {
    @weakify(self)
    return [[NSFileManager rac_prepareDirectoryForFileAtPath:filePath] then:^RACSignal *{
        @strongify(self)
        return [self.videoService rac_exportVideoPHAssetSignal:photosAsset
                                                        toFile:filePath
                                               withMaximumSize:maximumSize
                                                     timeRange:timeRange
                                               framesPerSecond:framesPerSecond];
    }];
}

- (RACSignal *)rac_saveVideoWithAVAssetSignal:(AVAsset *)asset
                                       toFile:(NSString *)filePath
                              withMaximumSize:(CGFloat)maximumSize
                                    timeRange:(CMTimeRange)timeRange
                              framesPerSecond:(NSUInteger)framesPerSecond {
    @weakify(self)
    return [[NSFileManager rac_prepareDirectoryForFileAtPath:filePath] then:^RACSignal *{
        @strongify(self)
        return [self.videoService rac_exportVideoAVAssetSignal:asset
                                                        toFile:filePath
                                               withMaximumSize:maximumSize
                                                     timeRange:timeRange
                                               framesPerSecond:framesPerSecond];
    }];
}

@end
