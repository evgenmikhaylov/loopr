//
//  BTMVideoStorage.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@class BTMVideoService;
@class RACSignal;
@class PHAsset;
@class AVAsset;

@interface BTMVideoStorage : NSObject

- (instancetype)initWithVideoService:(BTMVideoService *)service;

- (RACSignal *)rac_saveVideoWithPHAssetSignal:(PHAsset *)photosAsset
                                       toFile:(NSString *)filePath
                              withMaximumSize:(CGFloat)maximumSize
                                    timeRange:(CMTimeRange)timeRange
                              framesPerSecond:(NSUInteger)framesPerSecond;
- (RACSignal *)rac_saveVideoWithAVAssetSignal:(AVAsset *)asset
                                       toFile:(NSString *)filePath
                              withMaximumSize:(CGFloat)maximumSize
                                    timeRange:(CMTimeRange)timeRange
                              framesPerSecond:(NSUInteger)framesPerSecond;

@end
