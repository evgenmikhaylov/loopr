//
//  BTMCacheService.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 21/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMCacheService.h"
#import "CPBaseObject.h"
#import <ReactiveCocoa.h>
#import <PINCache.h>


@implementation BTMCacheService

- (RACSignal *)rac_cacheObjectSignal:(CPBaseObject *)object forKey:(NSString *)key {
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[PINCache sharedCache] setObject:object forKey:key block:^(PINCache * _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
            [subscriber sendNext:object];
            [subscriber sendCompleted];
        }];
        return nil;
    }] subscribeOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh]] deliverOnMainThread];
}

- (RACSignal *)rac_cachedObjectForKeySignal:(NSString *)key {
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[PINCache sharedCache] objectForKey:key block:^(PINCache * _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
            [subscriber sendNext:object];
            [subscriber sendCompleted];
        }];
        return nil;
    }] subscribeOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh]] deliverOnMainThread];
}


@end
