//
//  BTMCacheService.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 21/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CPBaseObject;
@class RACSignal;

@interface BTMCacheService : NSObject

- (RACSignal *)rac_cacheObjectSignal:(CPBaseObject *)object forKey:(NSString *)key;
- (RACSignal *)rac_cachedObjectForKeySignal:(NSString *)key;

@end
