//
//  BTMServicesFactory.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMServicesFactory.h"

#import "BTMBeatDetectionService.h"
#import "BTMProjectRenderService.h"
#import "BTMCacheService.h"
#import "BTMVideoService.h"
#import "BTMVideoStorage.h"
#import "BTMAudioService.h"
#import "BTMAudioStorage.h"

@implementation BTMServicesFactory

+ (BTMVideoService *)videoService {
    BTMVideoService *service = [[BTMVideoService alloc] init];
    return service;
}

+ (BTMVideoStorage *)videoStorage {
    BTMVideoStorage *storage = [[BTMVideoStorage alloc] initWithVideoService:[self videoService]];
    return storage;
}

+ (BTMAudioService *)audioService {
    BTMAudioService *service = [[BTMAudioService alloc] init];
    return service;
}

+ (BTMAudioStorage *)audioStorage {
    BTMAudioStorage *storage = [[BTMAudioStorage alloc] initWithAudioService:[self audioService]];
    return storage;
}

+ (BTMBeatDetectionService *)beatDetectionService {
    BTMBeatDetectionService *service = [[BTMBeatDetectionService alloc] init];
    return service;
}

+ (BTMCacheService *)cacheService {
    BTMCacheService *service = [[BTMCacheService alloc] init];
    return service;
}

+ (BTMProjectRenderService *)projectRenderService {
    BTMProjectRenderService *service = [[BTMProjectRenderService alloc] init];
    return service;
}


@end
