//
//  BTMAudioService.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMAudioService.h"

#import "AVAsset+Export.h"
#import "NSError+BTM.h"
#import "PHAsset+CP.h"
#import <ReactiveCocoa.h>

@implementation BTMAudioService

- (RACSignal *)rac_exportAudioAVAssetSignal:(AVAsset *)asset toFile:(NSString *)filePath {
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [asset exportM4AAudioToFile:filePath completionBlock:^(NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                [subscriber sendNext:filePath];
                [subscriber sendCompleted];
            }
        }];
        return nil;
    }] deliverOnMainThread] catch:^RACSignal *(NSError *error) {
        NSString *description = NSLocalizedString(@"Can not decode audio.", nil);
        return [RACSignal error:[NSError BTM_errorWithCode:BTMErrorAudioNotDecoded description:description]];
    }];
}

@end
