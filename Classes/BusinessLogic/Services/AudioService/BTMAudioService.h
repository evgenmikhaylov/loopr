//
//  BTMAudioService.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@class AVAsset;

@interface BTMAudioService : NSObject

- (RACSignal *)rac_exportAudioAVAssetSignal:(AVAsset *)asset toFile:(NSString *)filePath;

@end
