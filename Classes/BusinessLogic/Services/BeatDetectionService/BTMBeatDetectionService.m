//
//  BTMBeatDetectionService.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMBeatDetectionService.h"
#import "MVZBeatDetector.h"
#import <AVFoundation/AVFoundation.h>
#import <ReactiveCocoa.h>

@implementation BTMBeatDetectionService

- (RACSignal *)rac_processAudioWithURLSignal:(NSURL *)url {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        MVZBeatDetector *beatDetector = [[MVZBeatDetector alloc] init];
        [beatDetector startProcessingAudioFileWithURL:url withCompletionBlock:^{
            [subscriber sendNext:beatDetector];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

@end
