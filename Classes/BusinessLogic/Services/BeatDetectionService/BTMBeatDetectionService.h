//
//  BTMBeatDetectionService.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@interface BTMBeatDetectionService : NSObject

- (RACSignal *)rac_processAudioWithURLSignal:(NSURL *)url;

@end
