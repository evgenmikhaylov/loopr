//
//  BTMServicesFactory.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMBeatDetectionService;
@class BTMProjectRenderService;
@class BTMVideoService;
@class BTMVideoStorage;
@class BTMAudioService;
@class BTMAudioStorage;
@class BTMCacheService;

@interface BTMServicesFactory : NSObject

+ (BTMVideoService *)videoService;
+ (BTMVideoStorage *)videoStorage;
+ (BTMAudioService *)audioService;
+ (BTMAudioStorage *)audioStorage;
+ (BTMBeatDetectionService *)beatDetectionService;
+ (BTMCacheService *)cacheService;
+ (BTMProjectRenderService *)projectRenderService;

@end
