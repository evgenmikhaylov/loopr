//
//  BTMAudioStorage.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTMAudioService;
@class RACSignal;
@class AVAsset;

@interface BTMAudioStorage : NSObject

- (instancetype)initWithAudioService:(BTMAudioService *)service;

- (RACSignal *)rac_saveAudioWithAVAssetSignal:(AVAsset *)asset
                                       toFile:(NSString *)filePath;

@end
