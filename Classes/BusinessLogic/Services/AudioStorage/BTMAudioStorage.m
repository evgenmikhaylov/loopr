//
//  BTMAudioStorage.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMAudioStorage.h"
#import "BTMAudioService.h"

#import "NSFileManager+BTM.h"

#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MVZExtensions.h>
#import <ReactiveCocoa.h>

@interface BTMAudioStorage ()

@property (nonatomic) BTMAudioService *audioService;

@end

@implementation BTMAudioStorage

- (instancetype)initWithAudioService:(BTMAudioService *)service {
    self = [super init];
    if (self) {
        self.audioService = service;
    }
    return self;
}

- (RACSignal *)rac_saveAudioWithAVAssetSignal:(AVAsset *)asset
                                       toFile:(NSString *)filePath {
    @weakify(self)
    return [[NSFileManager rac_prepareDirectoryForFileAtPath:filePath] then:^RACSignal *{
        @strongify(self)
        return [self.audioService rac_exportAudioAVAssetSignal:asset toFile:filePath];
    }];
}

@end
