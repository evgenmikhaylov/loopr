//
//  BTMVideoService.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@class RACSignal;
@class PHAsset;
@class AVAsset;

@interface BTMVideoService : NSObject

- (RACSignal *)rac_exportVideoPHAssetSignal:(PHAsset *)photosAsset
                                     toFile:(NSString *)filePath
                            withMaximumSize:(CGFloat)maximumSize
                                  timeRange:(CMTimeRange)timeRange
                            framesPerSecond:(NSUInteger)framesPerSecond;
- (RACSignal *)rac_exportVideoAVAssetSignal:(AVAsset *)asset
                                     toFile:(NSString *)filePath
                            withMaximumSize:(CGFloat)maximumSize
                                  timeRange:(CMTimeRange)timeRange
                            framesPerSecond:(NSUInteger)framesPerSecond;

@end
