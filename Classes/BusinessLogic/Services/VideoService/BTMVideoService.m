//
//  BTMVideoService.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 14/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "BTMVideoService.h"

#import "AVAsset+Export.h"
#import "NSError+BTM.h"
#import "PHAsset+CP.h"
#import <ReactiveCocoa.h>

@implementation BTMVideoService

- (RACSignal *)rac_exportVideoPHAssetSignal:(PHAsset *)photosAsset
                                     toFile:(NSString *)filePath
                            withMaximumSize:(CGFloat)maximumSize
                                  timeRange:(CMTimeRange)timeRange
                            framesPerSecond:(NSUInteger)framesPerSecond {
    @weakify(self)
    return [[photosAsset cp_requestVideoAVAssetSignal] flattenMap:^RACStream *(AVAsset *asset) {
        @strongify(self)
        return [self rac_exportVideoAVAssetSignal:asset
                                           toFile:filePath
                                  withMaximumSize:maximumSize
                                        timeRange:timeRange
                                  framesPerSecond:framesPerSecond];
    }];
}

- (RACSignal *)rac_exportVideoAVAssetSignal:(AVAsset *)asset
                                     toFile:(NSString *)filePath
                            withMaximumSize:(CGFloat)maximumSize
                                  timeRange:(CMTimeRange)timeRange
                            framesPerSecond:(NSUInteger)framesPerSecond {
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [asset exportVideoToFile:filePath withMaximumSize:maximumSize timeRange:timeRange framesPerSecond:framesPerSecond completionBlock:^(NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                [subscriber sendNext:filePath];
                [subscriber sendCompleted];
            }
        }];
        return nil;
    }] deliverOnMainThread] catch:^RACSignal *(NSError *error) {
        NSString *description = NSLocalizedString(@"Can not decode video.", nil);
        return [RACSignal error:[NSError BTM_errorWithCode:BTMErrorVideoNotDecoded description:description]];
    }];
}

@end
