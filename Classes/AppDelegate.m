//
//  AppDelegate.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 19/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "AppDelegate.h"
#import "CPITunesSearchViewController.h"
#import "BTMProjectFactory.h"
#import "BTMProjectViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [BTMProjectFactory viewController];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
