//
//  MVZAlertController.h
//  RideRequest
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MVZAlertControllerModel;

@interface MVZAlertController : UIAlertController

+ (instancetype)alertControllerWithViewModel:(MVZAlertControllerModel *)viewModel;

@end
