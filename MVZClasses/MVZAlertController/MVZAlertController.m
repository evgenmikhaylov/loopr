//
//  MVZAlertController.m
//  RideRequest
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZAlertController.h"
#import "MVZAlertAction.h"
#import "MVZAlertControllerModel.h"

@interface MVZAlertControllerWindowRootViewController : UIViewController

@end

@implementation MVZAlertControllerWindowRootViewController

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end

@interface MVZAlertController ()

@property (nonatomic) UIWindow *alertWindow;

@end

@implementation MVZAlertController

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self bindViewModel];
    });
}

+ (void)bindViewModel {
    [MVZAlertControllerModel sharedInstance].show = ^{
        MVZAlertController *alertController = [MVZAlertController alertControllerWithViewModel:[MVZAlertControllerModel sharedInstance]];
        [alertController show];
    };
}

+ (instancetype)alertControllerWithViewModel:(MVZAlertControllerModel *)viewModel {
    UIAlertControllerStyle alertControllerStyle;
    switch (viewModel.style) {
        case MVZAlertControllerModelStyleAlert:         alertControllerStyle = UIAlertControllerStyleAlert;        break;
        case MVZAlertControllerModelStyleActionSheet:   alertControllerStyle = UIAlertControllerStyleActionSheet;  break;
    }
    MVZAlertController *alertController = [self alertControllerWithTitle:viewModel.title
                                                                 message:viewModel.message
                                                          preferredStyle:alertControllerStyle];
    [viewModel.actionModels enumerateObjectsUsingBlock:^(MVZAlertControllerActionModel * _Nonnull actionModel, NSUInteger idx, BOOL * _Nonnull stop) {
        [alertController addAction:[MVZAlertAction actionWithViewModel:actionModel]];
    }];
    return alertController;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.alertWindow.hidden = YES;
    self.alertWindow = nil;
}

- (void)show {
    self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.alertWindow.windowLevel = UIWindowLevelAlert;
    self.alertWindow.rootViewController = [[MVZAlertControllerWindowRootViewController alloc] init];
    [self.alertWindow makeKeyAndVisible];
    [self.alertWindow.rootViewController presentViewController:self animated:YES completion:nil];
}

- (void)hide {
    [self.alertWindow.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
