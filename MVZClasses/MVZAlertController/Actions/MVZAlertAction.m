//
//  MVZAlertAction.m
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZAlertAction.h"
#import "MVZAlertControllerActionModel.h"

@implementation MVZAlertAction

+ (instancetype)actionWithViewModel:(MVZAlertControllerActionModel *)viewModel {
    UIAlertActionStyle actionStyle;
    switch (viewModel.style) {
        case MVZAlertControllerActionModelStyleDefault:     actionStyle = UIAlertActionStyleDefault;        break;
        case MVZAlertControllerActionModelStyleCancel:      actionStyle = UIAlertActionStyleCancel;         break;
        case MVZAlertControllerActionModelStyleDestructive: actionStyle = UIAlertActionStyleDestructive;    break;
    }
    MVZAlertAction *action = [self actionWithTitle:viewModel.title style:actionStyle handler:^(UIAlertAction *action) {
        viewModel.action();
    }];
    return action;
}

@end
