//
//  MVZAlertControllerActionModel.m
//  RideRequest
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZAlertControllerActionModel.h"

@implementation MVZAlertControllerActionModel

- (void (^)())action {
    if (!_action) {
        _action = ^{
        };
    }
    return _action;
}

@end
