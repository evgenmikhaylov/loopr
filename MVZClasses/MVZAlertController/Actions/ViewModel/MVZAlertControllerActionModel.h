//
//  MVZAlertControllerActionModel.h
//  RideRequest
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, MVZAlertControllerActionModelStyle) {
    MVZAlertControllerActionModelStyleDefault,
    MVZAlertControllerActionModelStyleCancel,
    MVZAlertControllerActionModelStyleDestructive,
};

@interface MVZAlertControllerActionModel : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) MVZAlertControllerActionModelStyle style;
@property (nonatomic, copy) void(^action)();

@end
