//
//  MVZAlertControllerModel.m
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZAlertControllerModel.h"

@implementation MVZAlertControllerModel

+ (instancetype)sharedInstance {    
    static MVZAlertControllerModel *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (void)showError:(NSError *)error {
    MVZAlertControllerModel *viewModel = [MVZAlertControllerModel sharedInstance];
    viewModel.title = NSLocalizedString(@"Error", nil);
    viewModel.message = error.localizedDescription;
    viewModel.style = MVZAlertControllerModelStyleAlert;
    
    NSMutableArray<MVZAlertControllerActionModel *> *actionModels = [[NSMutableArray alloc] init];
    {
        MVZAlertControllerActionModel *actionModel = [[MVZAlertControllerActionModel alloc] init];
        actionModel.title = NSLocalizedString(@"OK", nil);
        [actionModels addObject:actionModel];
    }
    viewModel.actionModels = actionModels;
    viewModel.show();
}

- (void (^)())show {
    if (!_show) {
        _show = ^{
        };
    }
    return _show;
}

@end
