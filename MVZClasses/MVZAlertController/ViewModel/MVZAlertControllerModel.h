//
//  MVZAlertControllerModel.h
//
//  Created by Evgeny Mikhaylov on 13/09/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZAlertControllerActionModel.h"
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, MVZAlertControllerModelStyle) {
    MVZAlertControllerModelStyleAlert,
    MVZAlertControllerModelStyleActionSheet,
};

@interface MVZAlertControllerModel : NSObject

+ (instancetype)sharedInstance;

+ (void)showError:(NSError *)error;

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *message;
@property (nonatomic) MVZAlertControllerModelStyle style;
@property (nonatomic) NSArray<MVZAlertControllerActionModel *> *actionModels;
@property (nonatomic, copy) void(^show)();

@end
