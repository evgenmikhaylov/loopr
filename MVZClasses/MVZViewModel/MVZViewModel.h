//
//  MVZViewModel.h
//  SplitPic
//
//  Created by Evgeny Mikhaylov on 03/11/2016.
//  Copyright © 2016 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MVZViewModel : NSObject

@property (nonatomic, copy) void(^update)();

@end

NS_ASSUME_NONNULL_END
