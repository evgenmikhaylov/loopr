//
//  MVZViewModel.m
//  SplitPic
//
//  Created by Evgeny Mikhaylov on 03/11/2016.
//  Copyright © 2016 rosberry. All rights reserved.
//

#import "MVZViewModel.h"

@implementation MVZViewModel

- (void (^)())update {
    if (!_update) {
        _update = ^{
        };
    }
    return _update;
}

@end
