//
//  MVZActivityIndicator.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZActivityIndicator.h"
#import "MVZActivityIndicatorModel.h"

@implementation MVZActivityIndicator

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self bindViewModel];
    });
}

+ (void)bindViewModel {
    [MVZActivityIndicatorModel sharedInstance].show = ^{
        [MVZActivityIndicator show];
    };
    [MVZActivityIndicatorModel sharedInstance].showWithProgress = ^(CGFloat progress){
        [MVZActivityIndicator showWithProgress:progress];
    };
    [MVZActivityIndicatorModel sharedInstance].showWithProgressAndText = ^(CGFloat progress, NSString *text){
        [MVZActivityIndicator showWithProgress:progress text:text];
    };
    [MVZActivityIndicatorModel sharedInstance].showSuccessWithText = ^(NSString *text){
        [MVZActivityIndicator showSuccessWithText:text];
    };
    [MVZActivityIndicatorModel sharedInstance].dismiss = ^{
        [MVZActivityIndicator dismiss];
    };
}

@end
