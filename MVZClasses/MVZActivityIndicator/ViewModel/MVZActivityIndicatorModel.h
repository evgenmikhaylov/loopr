//
//  MVZActivityIndicatorModel.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MVZActivityIndicatorModel : NSObject

+ (instancetype)sharedInstance;

+ (void)show;
+ (void)showWithProgress:(CGFloat)progress;
+ (void)showWithProgress:(CGFloat)progress text:(NSString *)text;
+ (void)showSuccessWithText:(NSString*)text;
+ (void)showErrorWithText:(NSString*)text;
+ (void)dismiss;

@property (nonatomic, copy) void(^show)();
@property (nonatomic, copy) void(^showWithProgress)(CGFloat progress);
@property (nonatomic, copy) void(^showWithProgressAndText)(CGFloat progress, NSString *text);
@property (nonatomic, copy) void(^showSuccessWithText)(NSString *text);
@property (nonatomic, copy) void(^showErrorWithText)(NSString *text);
@property (nonatomic, copy) void(^dismiss)();

@end

NS_ASSUME_NONNULL_END
