//
//  MVZActivityIndicatorModel.m
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "MVZActivityIndicatorModel.h"

@implementation MVZActivityIndicatorModel

+ (instancetype)sharedInstance {
    static MVZActivityIndicatorModel *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (void)show {
    [MVZActivityIndicatorModel sharedInstance].show();
}

+ (void)showWithProgress:(CGFloat)progress {
    [MVZActivityIndicatorModel sharedInstance].showWithProgress(progress);
}

+ (void)showWithProgress:(CGFloat)progress text:(NSString *)text {
    [MVZActivityIndicatorModel sharedInstance].showWithProgressAndText(progress, text);
}

+ (void)showSuccessWithText:(NSString*)text {
    [MVZActivityIndicatorModel sharedInstance].showSuccessWithText(text);
}

+ (void)showErrorWithText:(NSString*)text {
    [MVZActivityIndicatorModel sharedInstance].showErrorWithText(text);
}

+ (void)dismiss {
    [MVZActivityIndicatorModel sharedInstance].dismiss();
}

- (void (^)())show {
    if (!_show) {
        _show = ^{
        };
    }
    return _show;
}

- (void (^)(CGFloat progress))showWithProgress {
    if (!_showWithProgress) {
        _showWithProgress = ^(CGFloat progress){
        };
    }
    return _showWithProgress;
}

- (void (^)(CGFloat progress, NSString *text))showWithProgressAndText {
    if (!_showWithProgressAndText) {
        _showWithProgressAndText = ^(CGFloat progress, NSString *text){
        };
    }
    return _showWithProgressAndText;
}

- (void (^)(NSString *text))showSuccessWithText {
    if (!_showSuccessWithText) {
        _showSuccessWithText = ^(NSString *text){
        };
    }
    return _showSuccessWithText;
}

- (void (^)(NSString *text))showErrorWithText {
    if (!_showErrorWithText) {
        _showErrorWithText = ^(NSString *text){
        };
    }
    return _showErrorWithText;
}

- (void (^)())dismiss {
    if (!_dismiss) {
        _dismiss = ^{
        };
    }
    return _dismiss;
}

@end
