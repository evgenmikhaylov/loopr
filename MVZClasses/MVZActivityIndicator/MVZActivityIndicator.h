//
//  MVZActivityIndicator.h
//  Loopr
//
//  Created by Evgeny Mikhaylov on 15/11/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <ActivityIndicator/ActivityIndicator.h>

@interface MVZActivityIndicator : ActivityIndicator

@end
