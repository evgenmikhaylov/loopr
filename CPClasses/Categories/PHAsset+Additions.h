//
//  PHAsset+Additions.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 6/11/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Photos/Photos.h>

@class RACSignal;

@interface PHAsset (Additions)

+ (PHAsset*)lastAssetWithType:(PHAssetMediaType)type;
+ (PHAsset*)assetFromLocalIdentifier:(NSString *)localIdentifier;

+ (void)requestLastImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage *))resultHandler;
+ (void)saveImageToCameraRoll:(UIImage *)image
                 withLocation:(CLLocation *)location
              completionBlock:(void(^)(PHAsset *asset, NSError *error))completionBlock;
+ (void)saveVideoToCameraRoll:(NSURL *)videoURL
                     location:(CLLocation *)location
              completionBlock:(void(^)(PHAsset *asset, NSError *error))completionBlock;

- (void)requestImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage *))resultHandler;

+ (RACSignal *)rac_lastAssetWithType:(PHAssetMediaType)type;
+ (RACSignal *)rac_requestLastImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode;
+ (RACSignal *)rac_saveImageToCameraRollSignal:(UIImage *)image withLocation:(CLLocation *)location;
+ (RACSignal *)rac_saveVideoToCameraRollSignal:(NSURL *)videoURL withLocation:(CLLocation *)location;

- (RACSignal *)rac_requestImageWithSizeSignal:(CGSize)size contentMode:(PHImageContentMode)contentMode;
- (RACSignal *)rac_requestAVAssetForVideoSignal;

@end
