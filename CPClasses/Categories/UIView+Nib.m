//
//  UIView+Nib.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/3/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "UIView+Nib.h"

@implementation UIView (Nib)

+ (id)loadFromNib
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
}

@end
