//
//  UIImage+Crop.h
//  CircleCam
//
//  Created by Paul Bar on 2/20/13.
//  Copyright (c) 2013 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Crop)

- (UIImage*)fixOrientation;
- (UIImage*)squareImage;
- (UIImage*)cropWithAbsoluteRect:(CGRect)rect;
- (UIImage*)crop:(CGRect)rect;
- (CGRect)cropRectInContainerWithSize:(CGSize)containerSize;
+ (CGRect)cropRectForImageWithSize:(CGSize)imageSize inContainerWithSize:(CGSize)containerSize;
+ (CGRect)cropRectForImageWithSize:(CGSize)imageSize scale:(CGFloat)scale inContainerWithSize:(CGSize)containerSize;

@end
