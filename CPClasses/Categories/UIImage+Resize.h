//
//  UIImage+Resize.h
//  Peek
//
//  Created by EvgenyMikhaylov on 10/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RACSignal;

@interface UIImage (Resize)

- (UIImage *)resizeImageWithMaxDimension:(CGFloat)maxDimension;
- (UIImage *)resizeImageWithMinDimension:(CGFloat)minDimension;
- (RACSignal *)rac_resizeImageWithMaxDimensionSignal:(CGFloat)maxDimension;
- (RACSignal *)rac_resizeImageWithMinDimensionSignal:(CGFloat)minDimension;

@end
