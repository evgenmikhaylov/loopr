//
//  NSError+CP.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 12/4/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "NSError+CP.h"

@implementation NSError (CP)

+ (NSError *)cp_errorWithCode:(CPError)code description:(NSString *)description {
    
    NSDictionary *userInfo = description ? @{NSLocalizedDescriptionKey:description} : nil;
    return [NSError errorWithDomain:CPErrorDomain code:code userInfo:userInfo];
}

@end
