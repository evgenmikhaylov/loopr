//
//  PHAsset+CP.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 12/17/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "PHAsset+CP.h"
#import "PHAsset+Additions.h"
#import "UIImage+Resize.h"
#import "NSError+CP.h"
#import <ReactiveCocoa.h>

@implementation PHAsset (CP)

+ (RACSignal *)cp_requestLastAssetImageOrWatermarkWithSizeSignal:(CGSize)size {
    
    __block PHAsset *lastAsset = nil;
    return [[[[[PHAsset rac_lastAssetWithType:PHAssetMediaTypeImage]
               flattenMap:^RACStream *(PHAsset *asset) {
                   lastAsset = asset;
                   if (asset) {
                       return [asset rac_requestImageWithSizeSignal:size contentMode:PHImageContentModeAspectFill];
                   }
                   else {
                       return [RACSignal return:nil];
                   }
               }] map:^id(UIImage *image) {
                   if (!image){
                       image = [UIImage imageNamed:@"watermark.jpg"];
                   }
                   return RACTuplePack(lastAsset, image);
               }] subscribeOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh]] deliverOnMainThread];
}

- (RACSignal *)cp_requestAssetImageWithSizeSignal:(CGSize)size {
    
    return [[[[[self rac_requestImageWithSizeSignal:size contentMode:PHImageContentModeAspectFill]
              flattenMap:^RACStream *(UIImage *image) {
                  if (image) {
                      return [image rac_resizeImageWithMinDimensionSignal:size.width];
                  }
                  else {
                      return [RACSignal return:nil];
                  }
              }] flattenMap:^RACStream *(UIImage *image) {
                  if (image) {
                      return [RACSignal return:image];
                  }
                  else {
                      NSString *errorDescription = NSLocalizedString(@"Can not load image", nil);
                      NSError *error = [NSError cp_errorWithCode:CPErrorImageNotLoaded description:errorDescription];
                      return [RACSignal error:error];
                  }
              }] subscribeOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh]] deliverOnMainThread];
}

- (RACSignal *)cp_requestVideoAVAssetSignal{
    
    return [[[[self rac_requestAVAssetForVideoSignal]
              flattenMap:^RACStream *(AVAsset *asset) {
                  if (asset) {
                      return [RACSignal return:asset];
                  }
                  else {
                      NSString *description = NSLocalizedString(@"Can not load video", nil);
                      NSError *error = [NSError cp_errorWithCode:CPErrorVideoNotLoaded description:description];
                      return [RACSignal error:error];
                  }
              }] subscribeOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh]] deliverOnMainThread];
}

@end
