//
//  NSError+CP.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 12/4/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const CPErrorDomain = @"com.jetset.cinepic.error";

typedef NS_ENUM(NSInteger, CPError) {
    CPErrorCancelled = -20000,
    CPErrorImageNotLoaded,
    CPErrorVideoNotLoaded,
    CPErrorVideoNotDecoded,
    CPErrorShortVideo,
    CPErrorLowMemory,
};

@interface NSError (CP)

+ (NSError *)cp_errorWithCode:(CPError)code description:(NSString *)description;

@end
