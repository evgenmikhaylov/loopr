//
//  NSBundle+Additions.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/20/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Additions)

+ (NSString*)bundleName;

@end
