//
//  AVAudioPlayer+Additions.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 4/3/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVAudioPlayer (Additions)

@property (nonatomic) BOOL mute;

@end
