//
//  AVAudioPlayer+Additions.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 4/3/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "AVAudioPlayer+Additions.h"

@implementation AVAudioPlayer (Additions)

- (void)setMute:(BOOL)mute
{
    float volume = mute ? 0.0f : 1.0f;
    [self setVolume:volume];
}

- (BOOL)mute
{
    if (self.volume>0.0f){
        return NO;
    }
    return YES;
}

@end
