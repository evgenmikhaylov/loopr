//
//  NSBundle+Additions.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/20/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "NSBundle+Additions.h"

@implementation NSBundle (Additions)

+ (NSString*)bundleName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}

@end
