//
//  PHAsset+CP.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 12/17/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <Photos/Photos.h>

@class RACSignal;

@interface PHAsset (CP)

+ (RACSignal *)cp_requestLastAssetImageOrWatermarkWithSizeSignal:(CGSize)size;
- (RACSignal *)cp_requestAssetImageWithSizeSignal:(CGSize)size;
- (RACSignal *)cp_requestVideoAVAssetSignal;

@end
