//
//  NSObject+CP.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/6/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CP)

- (BOOL)isNull;

@end
