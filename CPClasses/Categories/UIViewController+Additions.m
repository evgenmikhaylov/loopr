//
//  UIViewController+Additions.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 5/20/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "UIViewController+Additions.h"

@implementation UIViewController (Additions)

- (BOOL)isVisible {
    
    BOOL visible = self.isViewLoaded && self.view.window;
    return visible;
}

- (BOOL)canPerformSegueWithIdentifier:(NSString *)identifier {
    
    NSArray *segueTemplates = [self valueForKey:@"storyboardSegueTemplates"];
    NSArray *filteredArray = [segueTemplates filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", identifier]];
    return filteredArray.count>0;
}

- (BOOL)restorationIdentifierIsEqual:(NSString *)identifier {
    
    return [self.restorationIdentifier isEqualToString:identifier];
}

@end
