//
//  AVAsset+Export.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/10/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "AVAsset+Export.h"
#import <ReactiveCocoa.h>

CGFloat RadiansToDegrees(CGFloat radians) {
    
    return radians * 180 / M_PI;
};

@implementation AVAsset (Export)

- (UIImageOrientation)videoOrientation {
    
    AVAssetTrack *videoTrack = [[self tracksWithMediaType:AVMediaTypeVideo] firstObject];
    if (videoTrack){
        CGAffineTransform preferredTransform = [videoTrack preferredTransform];
        CGFloat videoAngleInDegree = RadiansToDegrees(atan2(preferredTransform.b, preferredTransform.a));
        switch ((int)videoAngleInDegree) {
            case 0:
                return UIImageOrientationRight;
                break;
            case 90:
                return UIImageOrientationUp;
                break;
            case 180:
                return UIImageOrientationLeft;
                break;
            case -90:
                return UIImageOrientationDown;
                break;
            default:
                return UIImageOrientationRight;
                break;
        }
    }
    else{
        return UIImageOrientationUp;
    }
}

- (AVAssetExportSession *)exportVideoToFile:(NSString *)outputPath
                              withTimeRange:(CMTimeRange)timeRange
                            completionBlock:(void (^)(NSError *error))completionBlock {
    
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputPath]){
        [[NSFileManager defaultManager] removeItemAtPath:outputPath error:&error];
        if (error){
            if (completionBlock){
                completionBlock(error);
            }
            return nil;
        }
    }
    
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:self presetName:AVAssetExportPresetHighestQuality];
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.outputURL = [NSURL fileURLWithPath:outputPath];
    exportSession.timeRange = timeRange;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch(exportSession.status){
            case AVAssetExportSessionStatusFailed:{
                if (completionBlock){
                    completionBlock(exportSession.error);
                }
                return;
            }
                break;
            case AVAssetExportSessionStatusCancelled:
            case AVAssetExportSessionStatusCompleted:{
                if (completionBlock){
                    completionBlock(nil);
                }
            }
                break;
            default:{
                NSError *unknownError = [NSError errorWithDomain:@"Unknown export error" code:100 userInfo:nil];
                if (completionBlock){
                    completionBlock(unknownError);
                }
                return;
            }
                break;
        }
    }];
    return exportSession;
}

- (AVAssetExportSession *)exportVideoToFile:(NSString *)outputPath
                            withMaximumSize:(CGFloat)maximumSize
                                  timeRange:(CMTimeRange)timeRange
                            framesPerSecond:(NSUInteger)framesPerSecond
                            completionBlock:(void (^)(NSError *error))completionBlock {
    
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputPath]){
        [[NSFileManager defaultManager] removeItemAtPath:outputPath error:&error];
        if (error){
            if (completionBlock){
                completionBlock(error);
            }
            return nil;
        }
    }
    
    AVAssetTrack *videoTrack = [[self tracksWithMediaType:AVMediaTypeVideo] firstObject];
    if (!videoTrack){
        NSString *infoString = NSLocalizedString(@"No video track", nil);
        error = [NSError errorWithDomain:@"com.jetset.videoExport" code:100 userInfo:@{NSLocalizedDescriptionKey:infoString}];
        if (completionBlock){
            completionBlock(error);
        }
        return nil;
    }
    
    CGSize naturalSize = videoTrack.naturalSize;
    UIImageOrientation orientation = [self videoOrientation];
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGSize renderSize = naturalSize;
    CGFloat scale = maximumSize/naturalSize.width;
    
    switch (orientation) {
        case UIImageOrientationUp:{
            transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(M_PI_2), CGAffineTransformMakeTranslation(naturalSize.height*scale, 0));
            transform = CGAffineTransformScale(transform, scale, scale);
            renderSize = CGSizeMake(naturalSize.height*scale, naturalSize.width*scale);
        }
            break;
        case UIImageOrientationLeft:{
            transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(M_PI), CGAffineTransformMakeTranslation(naturalSize.width*scale, naturalSize.height*scale));
            transform = CGAffineTransformScale(transform, scale, scale);
            renderSize = CGSizeMake(naturalSize.width*scale, naturalSize.height*scale);
        }
            break;
        case UIImageOrientationRight:{
            transform = CGAffineTransformMakeScale(scale, scale);
            renderSize = CGSizeMake(naturalSize.width*scale, naturalSize.height*scale);
        }
            break;
        case UIImageOrientationDown:{
            transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(-M_PI_2), CGAffineTransformMakeTranslation(0, naturalSize.width*scale));
            transform = CGAffineTransformScale(transform, scale, scale);
            renderSize = CGSizeMake(naturalSize.height*scale, naturalSize.width*scale);
        }
            break;
        default:
            break;
    }
    renderSize.width = [self multipleOfNumber:renderSize.width multiple:16];
    renderSize.height = [self multipleOfNumber:renderSize.height multiple:16];
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
    [layerInstruction setTransform:transform atTime:kCMTimeZero];
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.layerInstructions = @[layerInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, self.duration);
    
    [compositionVideoTrack insertTimeRange:timeRange ofTrack:videoTrack atTime:kCMTimeZero error:&error];
    
    if (error){
        if (completionBlock){
            completionBlock(error);
        }
        return nil;
    }
    
    AVAssetTrack *audioTrack = [[self tracksWithMediaType:AVMediaTypeAudio] firstObject];
    if (audioTrack) {
        AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVAssetExportPresetPassthrough preferredTrackID:kCMPersistentTrackID_Invalid];
        [compositionAudioTrack insertTimeRange:timeRange ofTrack:audioTrack atTime:kCMTimeZero error:&error];
        if (error){
            if (completionBlock){
                completionBlock(error);
            }
            return nil;
        }
    }
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.instructions = @[instruction];
    videoComposition.renderSize = renderSize;
    videoComposition.frameDuration = CMTimeMake(1, (int32_t)framesPerSecond);
    
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:composition presetName:AVAssetExportPresetHighestQuality];
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.videoComposition = videoComposition;
    exportSession.outputURL = [NSURL fileURLWithPath:outputPath];
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch(exportSession.status){
            case AVAssetExportSessionStatusFailed:{
                if (completionBlock){
                    completionBlock(exportSession.error);
                }
                return;
            }
                break;
            case AVAssetExportSessionStatusCancelled:
            case AVAssetExportSessionStatusCompleted:{
                if (completionBlock){
                    completionBlock(nil);
                }
            }
                break;
            default:{
                NSError *unknownError = [NSError errorWithDomain:@"Unknown export error" code:100 userInfo:nil];
                if (completionBlock){
                    completionBlock(unknownError);
                }
                return;
            }
                break;
        }
    }];
    return exportSession;
}

- (AVAssetExportSession *)exportM4AAudioToFile:(NSString *)outputPath completionBlock:(void (^)(NSError *error))completionBlock {
    
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputPath]){
        [[NSFileManager defaultManager] removeItemAtPath:outputPath error:&error];
        if (error){
            if (completionBlock){
                completionBlock(error);
            }
            return nil;
        }
    }
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:self presetName:AVAssetExportPresetAppleM4A];
    exportSession.outputFileType = @"com.apple.m4a-audio";
    exportSession.outputURL = [NSURL fileURLWithPath:outputPath];
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch (exportSession.status){
            case AVAssetExportSessionStatusFailed:{
                if (completionBlock){
                    completionBlock(exportSession.error);
                }
                return;
            }
                break;
            case AVAssetExportSessionStatusCompleted:{
                if (completionBlock){
                    completionBlock(nil);
                }
            }
                break;
            default:{
                NSError *unknownError = [NSError errorWithDomain:@"Unknown export error" code:100 userInfo:nil];
                if (completionBlock){
                    completionBlock(unknownError);
                }
                return;
            }
                break;
        }
    }];
    return exportSession;
}

- (void)exportAudioToFile:(NSString *)outputPath completionBlock:(void (^)(NSError *error))completionBlock {
    
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputPath]){
        [[NSFileManager defaultManager] removeItemAtPath:outputPath error:&error];
        if (error){
            if (completionBlock){
                completionBlock(error);
            }
            return;
        }
    }
    
    AVAssetReader *assetReader = [AVAssetReader assetReaderWithAsset:self error:&error];
    if (error){
        if (completionBlock){
            completionBlock(error);
        }
        return;
    }
    
    AVAssetReaderOutput *assetReaderOutput = [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:self.tracks audioSettings:nil];
    if (![assetReader canAddOutput:assetReaderOutput]) {
        if (completionBlock){
            error = [NSError errorWithDomain:@"AVAssetReaderOutput can't add output" code:100 userInfo:nil];
            completionBlock(error);
        }
        return;
    }
    [assetReader addOutput:assetReaderOutput];
    
    AVAssetWriter *assetWriter = [AVAssetWriter assetWriterWithURL:[NSURL fileURLWithPath:outputPath] fileType:AVFileTypeCoreAudioFormat error:&error];
    if (error){
        if (completionBlock){
            completionBlock(error);
        }
        return;
    }

    AudioChannelLayout channelLayout;
    memset(&channelLayout, 0, sizeof(AudioChannelLayout));
    channelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo;
    NSDictionary *outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
                                    [NSNumber numberWithFloat:44100.0], AVSampleRateKey,
                                    [NSNumber numberWithInt:2], AVNumberOfChannelsKey,
                                    [NSData dataWithBytes:&channelLayout length:sizeof(AudioChannelLayout)], AVChannelLayoutKey,
                                    [NSNumber numberWithInt:16], AVLinearPCMBitDepthKey,
                                    [NSNumber numberWithBool:NO], AVLinearPCMIsNonInterleaved,
                                    [NSNumber numberWithBool:NO], AVLinearPCMIsFloatKey,
                                    [NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey,
                                    nil];
    
    AVAssetWriterInput *assetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:outputSettings];
    if ([assetWriter canAddInput:assetWriterInput]){
        [assetWriter addInput:assetWriterInput];
    }
    else{
        if (completionBlock){
            error = [NSError errorWithDomain:@"AVAssetReaderOutput can't add output" code:100 userInfo:nil];
            completionBlock(error);
        }
        return;
    }
    
    assetWriterInput.expectsMediaDataInRealTime = NO;
    
    [assetWriter startWriting];
    [assetReader startReading];
    
    AVAssetTrack *soundTrack = self.tracks.firstObject;
    CMTime startTime = CMTimeMake(0, soundTrack.naturalTimeScale);
    [assetWriter startSessionAtSourceTime:startTime];
    
    dispatch_queue_t mediaInputQueue = dispatch_queue_create("mediaInputQueue", NULL);
    [assetWriterInput requestMediaDataWhenReadyOnQueue:mediaInputQueue usingBlock:^{
        while (assetWriterInput.readyForMoreMediaData) {
            CMSampleBufferRef nextBuffer = [assetReaderOutput copyNextSampleBuffer];
            if (nextBuffer) {
                [assetWriterInput appendSampleBuffer:nextBuffer];
            } else {
                [assetWriterInput markAsFinished];
                [assetWriter finishWritingWithCompletionHandler:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (assetWriter.status == AVAssetWriterStatusFailed) {
                            if (completionBlock){
                                completionBlock(assetWriter.error);
                            }
                        }
                        else{
                            if (completionBlock){
                                completionBlock(nil);
                            }
                        }
                    });
                }];
                [assetReader cancelReading];
                break;
            }
        }
    }];
}

- (UIImage *)videoPreviewWithMaximumSize:(CGSize)maximumSize time:(CMTime)time error:(NSError **)error {
    
    AVAssetImageGenerator *imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:self];
    imageGenerator.appliesPreferredTrackTransform = YES;
    imageGenerator.maximumSize = maximumSize;
    CGImageRef cgImage = [imageGenerator copyCGImageAtTime:time actualTime:nil error:error];
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return image;
}

#pragma mark - Helpers

- (NSUInteger)multipleOfNumber:(NSUInteger)number multiple:(NSUInteger)multiple {
    
    NSUInteger a = number / multiple;
    return a * multiple;
}

@end
