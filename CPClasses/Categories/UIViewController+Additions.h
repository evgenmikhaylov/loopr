//
//  UIViewController+Additions.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 5/20/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Additions)

- (BOOL)isVisible;
- (BOOL)canPerformSegueWithIdentifier:(NSString *)identifier;
- (BOOL)restorationIdentifierIsEqual:(NSString *)identifier;

@end
