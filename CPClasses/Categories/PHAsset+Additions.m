//
//  PHAsset+Additions.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 6/11/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "PHAsset+Additions.h"
#import <ReactiveCocoa.h>

@implementation PHAsset (Additions)

+ (PHAsset *)lastAssetWithType:(PHAssetMediaType)type {
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:type options:fetchOptions];
    return fetchResult.lastObject;
}

+ (PHAsset *)assetFromLocalIdentifier:(NSString *)localIdentifier {
    
    if(!localIdentifier){
        return nil;
    }
    PHFetchResult *result = [PHAsset fetchAssetsWithLocalIdentifiers:@[localIdentifier] options:nil];
    return result.firstObject;
}

+ (void)requestLastImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage *))resultHandler {
    
    PHAsset *asset = [PHAsset lastAssetWithType:PHAssetMediaTypeImage];
    if (asset){
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.networkAccessAllowed = YES;
        options.synchronous = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[PHImageManager defaultManager] requestImageForAsset:asset
                                                       targetSize:size
                                                      contentMode:contentMode
                                                          options:options
                                                    resultHandler:^(UIImage *result, NSDictionary *info) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            if (resultHandler){
                                                                resultHandler(result);
                                                            }
                                                        });
                                                    }];
        });
    }
    else{
        if (resultHandler){
            resultHandler(nil);
        }
    }
}

+ (void)saveImageToCameraRoll:(UIImage *)image
                 withLocation:(CLLocation *)location
              completionBlock:(void(^)(PHAsset *asset, NSError *error))completionBlock {

    __block PHObjectPlaceholder *placeholderAsset = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *newAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        newAssetRequest.location = location;
        newAssetRequest.creationDate = [NSDate date];
        placeholderAsset = newAssetRequest.placeholderForCreatedAsset;
    } completionHandler:^(BOOL success, NSError *error) {
        if(success){
            PHAsset *asset = [self assetFromLocalIdentifier:placeholderAsset.localIdentifier];
            completionBlock(asset, nil);
        }
        else {
            completionBlock(nil, error);
        }
    }];
}

+ (void)saveVideoToCameraRoll:(NSURL *)videoURL
                     location:(CLLocation *)location
              completionBlock:(void(^)(PHAsset *asset, NSError *error))completionBlock {
    
    __block PHObjectPlaceholder *placeholderAsset = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *newAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:videoURL];
        newAssetRequest.location = location;
        newAssetRequest.creationDate = [NSDate date];
        placeholderAsset = newAssetRequest.placeholderForCreatedAsset;
    } completionHandler:^(BOOL success, NSError *error) {
        if(success){
            PHAsset *asset = [self assetFromLocalIdentifier:placeholderAsset.localIdentifier];
            completionBlock(asset, nil);
        }
        else {
            completionBlock(nil, error);
        }
    }];
}

- (void)requestImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage *))resultHandler {
    
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = YES;
    options.synchronous = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[PHImageManager defaultManager] requestImageForAsset:self
                                                   targetSize:size
                                                  contentMode:contentMode
                                                      options:options
                                                resultHandler:^(UIImage *result, NSDictionary *info) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (resultHandler){
                                                            resultHandler(result);
                                                        }
                                                    });
                                                }];
    });
}

#pragma mark - RACSignals

+ (RACSignal *)rac_lastAssetWithType:(PHAssetMediaType)type {

    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PHAsset *asset = [self lastAssetWithType:type];
        [subscriber sendNext:asset];
        [subscriber sendCompleted];
        return nil;
    }];
}

+ (RACSignal *)rac_requestLastImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode {
    
    __block PHAsset *lastAsset = nil;
    return [[[self rac_lastAssetWithType:PHAssetMediaTypeImage]
            flattenMap:^RACStream *(PHAsset *asset) {
                lastAsset = asset;
                if (asset) {
                    return [asset rac_requestImageWithSizeSignal:size contentMode:contentMode];
                }
                else {
                    return [RACSignal return:nil];
                }
            }] map:^id(UIImage *image) {
                return RACTuplePack(lastAsset, image);
            }];
}

+ (RACSignal *)rac_saveImageToCameraRollSignal:(UIImage *)image withLocation:(CLLocation *)location {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self saveImageToCameraRoll:image withLocation:location completionBlock:^(PHAsset *asset, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                [subscriber sendNext:asset];
                [subscriber sendCompleted];
            }
        }];
        return nil;
    }];
}

+ (RACSignal *)rac_saveVideoToCameraRollSignal:(NSURL *)videoURL withLocation:(CLLocation *)location {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self saveVideoToCameraRoll:videoURL location:location completionBlock:^(PHAsset *asset, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                [subscriber sendNext:asset];
                [subscriber sendCompleted];
            }
        }];
        return nil;
    }];
}

- (RACSignal *)rac_requestImageWithSizeSignal:(CGSize)size contentMode:(PHImageContentMode)contentMode {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PHImageManager *imageManager = [PHImageManager defaultManager];
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.networkAccessAllowed = YES;
        options.synchronous = YES;
        PHImageRequestID requestID = [[PHImageManager defaultManager] requestImageForAsset:self
                                                                                targetSize:size
                                                                               contentMode:contentMode
                                                                                   options:options
                                                                             resultHandler:^(UIImage *result, NSDictionary *info) {
                                                                                 [subscriber sendNext:result];
                                                                                 [subscriber sendCompleted];
                                                                             }];
        return [RACDisposable disposableWithBlock:^{
            [imageManager cancelImageRequest:requestID];
        }];
    }];
}

- (RACSignal *)rac_requestAVAssetForVideoSignal {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PHImageManager *imageManager = [PHImageManager defaultManager];
        PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
        options.networkAccessAllowed = YES;
        PHImageRequestID requestID = [imageManager requestAVAssetForVideo:self
                                                                  options:options
                                                            resultHandler:^(AVAsset *videoAsset, AVAudioMix *audioMix, NSDictionary *info) {
                                                                [subscriber sendNext:videoAsset];
                                                                [subscriber sendCompleted];
                                                            }];
        return [RACDisposable disposableWithBlock:^{
            [imageManager cancelImageRequest:requestID];
        }];
    }];
}

@end
