//
//  NSObject+CP.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/6/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "NSObject+CP.h"

@implementation NSObject (CP)

- (BOOL)isNull
{
    return [self isEqual:[NSNull null]];
}

@end
