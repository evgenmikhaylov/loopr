//
//  UIImage+Crop.m
//  CircleCam
//
//  Created by Paul Bar on 2/20/13.
//  Copyright (c) 2013 Rosberry. All rights reserved.
//

#import "UIImage+Crop.h"

@implementation UIImage (Crop)

- (UIImage*)fixOrientation
{
    CGRect rect = (CGRect){
        .origin = CGPointZero,
        .size = self.size
    };
    UIGraphicsBeginImageContext(rect.size);
    [self drawInRect:rect];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}

- (UIImage*)squareImage
{
    CGAffineTransform transform;
    switch (self.imageOrientation){
        case UIImageOrientationLeft:    transform = CGAffineTransformMakeRotation(M_PI_2);  break;
        case UIImageOrientationRight:   transform = CGAffineTransformMakeRotation(-M_PI_2); break;
        case UIImageOrientationDown:    transform = CGAffineTransformMakeRotation(-M_PI);   break;
        default:                        transform = CGAffineTransformIdentity;              break;
    };
    CGSize actualSize = CGSizeApplyAffineTransform(self.size, transform);
    actualSize = CGSizeMake(fabs(actualSize.width), fabs(actualSize.height));
    CGRect cropRect = CGRectZero;
    if (actualSize.width>actualSize.height){
        cropRect = (CGRect){
            .origin.x = (int)(0.5f*(actualSize.width-actualSize.height)),
            .origin.y = 0,
            .size.width = actualSize.height,
            .size.height = actualSize.height,
        };
    }else{
        cropRect = (CGRect){
            .origin.x = 0,
            .origin.y = (int)(0.5f*(actualSize.height-actualSize.width)),
            .size.width = actualSize.width,
            .size.height = actualSize.width,
        };
    }
    return [self crop:cropRect];
}

- (UIImage *)cropWithAbsoluteRect:(CGRect)rect
{
    CGRect cropRect = {
        .origin.x = rect.origin.x*self.size.width,
        .origin.y = rect.origin.y*self.size.height,
        .size.width = rect.size.width*self.size.width,
        .size.height = rect.size.height*self.size.height,
    };
    return [self crop:cropRect];
}

- (UIImage *)crop:(CGRect)rect
{
    UIImage *fixedImage = [self fixOrientation];
    CGImageRef imageRef = CGImageCreateWithImageInRect([fixedImage CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:fixedImage.scale orientation:fixedImage.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

- (CGRect)cropRectInContainerWithSize:(CGSize)containerSize
{
    return [UIImage cropRectForImageWithSize:self.size inContainerWithSize:containerSize];
}

+ (CGRect)cropRectForImageWithSize:(CGSize)imageSize inContainerWithSize:(CGSize)containerSize
{
    return [self cropRectForImageWithSize:imageSize scale:1.0f inContainerWithSize:containerSize];
}

+ (CGRect)cropRectForImageWithSize:(CGSize)imageSize scale:(CGFloat)scale inContainerWithSize:(CGSize)containerSize
{
    CGFloat imageAspectRatio = imageSize.width/imageSize.height;
    CGFloat containerAspectRatio = containerSize.width/containerSize.height;
    CGRect cropRect = CGRectZero;
    CGFloat width = (imageAspectRatio > containerAspectRatio) ? (containerAspectRatio / imageAspectRatio) : 1.0f;
    CGFloat height = (imageAspectRatio > containerAspectRatio) ? 1.0f : (imageAspectRatio / containerAspectRatio);
    width /= scale;
    height /= scale;
    cropRect = (CGRect){
        .origin.x = (1.0f - width) / 2.0f,
        .origin.y = (1.0f - height) / 2.0f,
        .size.width = width,
        .size.height = height,
    };
    return cropRect;
}


@end
