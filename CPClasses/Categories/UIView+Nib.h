//
//  UIView+Nib.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/3/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Nib)

+ (id)loadFromNib;

@end
