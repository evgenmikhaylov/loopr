//
//  AVAsset+Export.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/10/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVAsset (Export)

- (UIImageOrientation)videoOrientation;
- (AVAssetExportSession *)exportVideoToFile:(NSString *)outputPath
                              withTimeRange:(CMTimeRange)timeRange
                            completionBlock:(void (^)(NSError *error))completionBlock;
- (AVAssetExportSession *)exportVideoToFile:(NSString *)outputPath
                            withMaximumSize:(CGFloat)maximumSize
                                  timeRange:(CMTimeRange)timeRange
                            framesPerSecond:(NSUInteger)framesPerSecond
                            completionBlock:(void (^)(NSError *error))completionBlock;
- (AVAssetExportSession *)exportM4AAudioToFile:(NSString *)outputPath completionBlock:(void (^)(NSError *error))completionBlock;
- (UIImage *)videoPreviewWithMaximumSize:(CGSize)maximumSize time:(CMTime)time error:(NSError **)error;

@end
