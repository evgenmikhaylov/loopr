//
//  UIImage+Resize.m
//  Peek
//
//  Created by EvgenyMikhaylov on 10/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "UIImage+Resize.h"
#import <ReactiveCocoa.h>

@implementation UIImage (Resize)

- (UIImage *)resizeImageWithMaxDimension:(CGFloat)maxDimension {
    
    CGFloat imageMaxDimension = MAX(self.size.width, self.size.height);
    if (imageMaxDimension < maxDimension) {
        return self;
    }
    
    CGFloat aspectRatio = self.size.width / self.size.height;
    CGSize imageSize = (aspectRatio > 1) ? CGSizeMake(maxDimension, maxDimension / aspectRatio) : CGSizeMake(maxDimension * aspectRatio, maxDimension);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 1.0);
    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

- (UIImage *)resizeImageWithMinDimension:(CGFloat)minDimension {
    
    CGFloat imageMinDimension = MIN(self.size.width, self.size.height);
    if (imageMinDimension < minDimension) {
        return self;
    }
    
    CGFloat aspectRatio = self.size.width/self.size.height;
    CGSize imageSize = (aspectRatio > 1) ? CGSizeMake(minDimension * aspectRatio, minDimension) : CGSizeMake(minDimension, minDimension / aspectRatio);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 1.0);
    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

- (RACSignal *)rac_resizeImageWithMaxDimensionSignal:(CGFloat)maxDimension {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        UIImage *image = [self resizeImageWithMaxDimension:maxDimension];
        [subscriber sendNext:image];
        [subscriber sendCompleted];
        return nil;
    }];
}

- (RACSignal *)rac_resizeImageWithMinDimensionSignal:(CGFloat)minDimension {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        UIImage *image = [self resizeImageWithMinDimension:minDimension];
        [subscriber sendNext:image];
        [subscriber sendCompleted];
        return nil;
    }];
}

@end
