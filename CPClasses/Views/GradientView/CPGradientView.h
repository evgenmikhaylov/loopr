//
//  CPGradientView.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 5/19/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPGradientView : UIView

@property (nonatomic) CAGradientLayer *gradientLayer;

@end
