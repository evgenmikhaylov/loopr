//
//  CPGradientView.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 5/19/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPGradientView.h"

@interface CPGradientView ()


@end

@implementation CPGradientView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.gradientLayer = [CAGradientLayer layer];
    [self.layer addSublayer:self.gradientLayer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.gradientLayer.frame = self.bounds;
}

@end
