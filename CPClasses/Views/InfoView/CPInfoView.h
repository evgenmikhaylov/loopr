//
//  CPInfoView.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/18/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPInfoView;

@protocol CPInfoViewDelegate <NSObject>

@optional
- (void)cpInfoViewPressed:(CPInfoView*)infoView;

@end

@interface CPInfoView : UIView

@property (weak, nonatomic) id<CPInfoViewDelegate> delegate;

@property (nonatomic) NSString *text;
@property (nonatomic) NSAttributedString *attributedText;
@property (nonatomic) UIColor *textColor;
@property (nonatomic) UIFont *font;
@property (nonatomic) BOOL enabled;

@end
