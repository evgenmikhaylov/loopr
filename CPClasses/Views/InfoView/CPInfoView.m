//
//  CPInfoView.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/18/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPInfoView.h"
#import "UIColor+CP.h"

@interface CPInfoView ()

@property (nonatomic) UIButton *button;

@end

@implementation CPInfoView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.button = [[UIButton alloc] init];
    self.button.exclusiveTouch = YES;
    [self.button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.button.titleLabel.numberOfLines = 0;
    [self addSubview:self.button];
    
    self.button.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.button
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0f
                                                      constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.button
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0f
                                                      constant:0.0f]];
}

- (void)buttonPressed:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(cpInfoViewPressed:)]){
        [self.delegate cpInfoViewPressed:self];
    }
}

#pragma mark -
#pragma mark Setters

- (void)setText:(NSString *)text
{
    _text = text;
    [self.button setAttributedTitle:nil forState:UIControlStateNormal];
    [self.button setTitle:text forState:UIControlStateNormal];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    _attributedText = attributedText;
    [self.button setTitle:nil forState:UIControlStateNormal];
    [self.button setAttributedTitle:_attributedText forState:UIControlStateNormal];
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    [self.button setTitleColor:_textColor forState:UIControlStateNormal];
    [self.button setTitleColor:[_textColor colorWithAlphaComponent:0.5f] forState:UIControlStateHighlighted];
}

- (void)setFont:(UIFont *)font
{
    _font = font;
    self.button.titleLabel.font = _font;
}

- (void)setEnabled:(BOOL)enabled
{
    _enabled = enabled;
    self.button.userInteractionEnabled = _enabled;
}

@end
