//
//  ZoomView.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/25/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZoomView;

@protocol ZoomViewDelegate <NSObject>

@optional
- (void)zoomViewWillBeginZooming:(ZoomView*)view;
- (void)zoomViewDidEndZooming:(ZoomView*)view;
- (void)zoomView:(ZoomView*)view didChangeVisibleRect:(CGRect)rect;

@end

@interface ZoomView : UIView

@property (weak, nonatomic) id<ZoomViewDelegate> delegate;

@property (nonatomic) UIImage *image;
@property (nonatomic) CGSize contentSize;
@property (nonatomic) CGRect visibleRect;

@end
