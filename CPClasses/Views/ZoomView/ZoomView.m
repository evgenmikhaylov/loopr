//
//  ZoomView.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/25/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "ZoomView.h"

@interface ZoomView () <UIScrollViewDelegate>

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIImageView *contentView;

@property (nonatomic) CGFloat defaultZoomScale;

@end

@implementation ZoomView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.alwaysBounceHorizontal = YES;
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:self.scrollView];

    self.contentView = [[UIImageView alloc] init];
    [self.scrollView addSubview:self.contentView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (CGRectIsEmpty(self.bounds))
        return;
    
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = self.contentSize;
    
    CGFloat imageAspectRatio = self.contentSize.width/self.contentSize.height;
    CGFloat scrollViewAspectRatio = CGRectGetWidth(self.scrollView.frame)/CGRectGetHeight(self.scrollView.frame);
    CGFloat minimumZoomScale;
    if (imageAspectRatio>scrollViewAspectRatio){
        self.defaultZoomScale = CGRectGetWidth(self.scrollView.frame)/self.contentSize.width;
        minimumZoomScale = self.defaultZoomScale*(imageAspectRatio/scrollViewAspectRatio);
    }else{
        self.defaultZoomScale = CGRectGetHeight(self.scrollView.frame)/self.contentSize.height;
        minimumZoomScale = self.defaultZoomScale/(imageAspectRatio/scrollViewAspectRatio);
    }
    self.scrollView.minimumZoomScale = minimumZoomScale;
    self.scrollView.maximumZoomScale = minimumZoomScale*5.0f;
    if (CGRectIsEmpty(_visibleRect)){
        self.scrollView.zoomScale = minimumZoomScale;
        [self resetContentOffset];
    }else{
        [self updateZoomScale];
    }
}

#pragma mark -
#pragma mark Methods

- (void)resetContentOffset
{
    CGFloat offsetX = 0.0f;
    if (CGRectGetWidth(self.contentView.frame)>CGRectGetWidth(self.scrollView.frame)){
        offsetX = 0.5f*(CGRectGetWidth(self.contentView.frame)-CGRectGetWidth(self.scrollView.frame));
    }
    CGFloat offsetY = 0.0f;
    if (CGRectGetHeight(self.contentView.frame)>CGRectGetHeight(self.scrollView.frame)){
        offsetY = 0.5f*(CGRectGetHeight(self.contentView.frame)-CGRectGetHeight(self.scrollView.frame));
    }
    [self.scrollView setContentOffset:CGPointMake(offsetX, offsetY)];
}

- (void)centerScrollViewContent
{
    CGRect contentViewFrame = self.contentView.frame;
    if (CGRectGetWidth(contentViewFrame)<CGRectGetWidth(self.scrollView.frame)) {
        contentViewFrame.origin.x = (CGRectGetWidth(self.scrollView.frame)-CGRectGetWidth(contentViewFrame))/2;
    }else {
        contentViewFrame.origin.x = 0;
    }
    if (CGRectGetHeight(contentViewFrame)<CGRectGetHeight(self.scrollView.frame)) {
        contentViewFrame.origin.y = (CGRectGetHeight(self.scrollView.frame)-CGRectGetHeight(contentViewFrame))/2;
    }else {
        contentViewFrame.origin.y = 0;
    }
    self.contentView.frame = contentViewFrame;
}

- (void)scrollViewVisibleRectChanged
{
    if (isinf(self.defaultZoomScale))
        return;
    
    CGFloat imageAspectRatio = self.contentSize.width/self.contentSize.height;
    CGFloat scrollViewAspectRatio = CGRectGetWidth(self.scrollView.frame)/CGRectGetHeight(self.scrollView.frame);
    CGFloat minimumZoomScale;
    if (imageAspectRatio>scrollViewAspectRatio){
        minimumZoomScale = self.defaultZoomScale*(imageAspectRatio/scrollViewAspectRatio);
    }else{
        minimumZoomScale = self.defaultZoomScale/(imageAspectRatio/scrollViewAspectRatio);
    }
    CGFloat scale = self.scrollView.zoomScale/minimumZoomScale;
    if (scale<1.0f){
        self.scrollView.zoomScale = minimumZoomScale;
    }
    CGRect visibleRect = [self.scrollView convertRect:self.scrollView.bounds toView:self.contentView];
    _visibleRect = CGRectApplyAffineTransform(visibleRect, CGAffineTransformMakeScale(1.0f/self.contentSize.width, 1.0f/self.contentSize.height));
    if (self.scrollView.isZooming||self.scrollView.isDragging||self.scrollView.isDecelerating){
        if ([self.delegate respondsToSelector:@selector(zoomView:didChangeVisibleRect:)]){
            [self.delegate zoomView:self didChangeVisibleRect:_visibleRect];
        }
    }
}

#pragma mark -
#pragma mark Setters

- (void)setExclusiveTouch:(BOOL)exclusiveTouch
{
    [super setExclusiveTouch:exclusiveTouch];
    self.scrollView.exclusiveTouch = exclusiveTouch;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.contentView.image = _image;
    self.contentSize = _image.size;
}

- (void)setContentSize:(CGSize)contentSize
{
    _contentSize = contentSize;
    self.contentView.transform = CGAffineTransformIdentity;
    self.contentView.frame = (CGRect){
        .origin = CGPointZero,
        .size = _contentSize,
    };
    [self setNeedsLayout];
}

- (void)setVisibleRect:(CGRect)visibleRect
{
    _visibleRect = visibleRect;
    [self updateZoomScale];
}

#pragma mark -
#pragma mark Helpers

- (void)updateZoomScale
{
    if (isnan(_visibleRect.origin.x)
        || isnan(_visibleRect.origin.y)
        || isnan(_visibleRect.size.width)
        || isnan(_visibleRect.size.height)
        || CGRectIsEmpty(_visibleRect)
        || CGSizeEqualToSize(self.contentSize, CGSizeZero))
        return;
    
    CGFloat zoomScale = (CGRectGetWidth(self.frame)/CGRectGetWidth(_visibleRect))/self.contentSize.width;
    CGPoint contentOffset = (CGPoint){
        .x = zoomScale*self.contentSize.width*CGRectGetMinX(_visibleRect),
        .y = zoomScale*self.contentSize.height*CGRectGetMinY(_visibleRect),
    };
    self.scrollView.zoomScale = zoomScale;
    self.scrollView.contentOffset = contentOffset;
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContent];
    [self scrollViewVisibleRectChanged];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.contentView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self scrollViewVisibleRectChanged];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (!scrollView.zooming){
        if ([self.delegate respondsToSelector:@selector(zoomViewWillBeginZooming:)]){
            [self.delegate zoomViewWillBeginZooming:self];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!scrollView.zooming){
        if ([self.delegate respondsToSelector:@selector(zoomViewDidEndZooming:)]){
            [self.delegate zoomViewDidEndZooming:self];
        }
    }
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    if (!scrollView.dragging){
        if ([self.delegate respondsToSelector:@selector(zoomViewWillBeginZooming:)]){
            [self.delegate zoomViewWillBeginZooming:self];
        }
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if (!scrollView.dragging){
        if ([self.delegate respondsToSelector:@selector(zoomViewDidEndZooming:)]){
            [self.delegate zoomViewDidEndZooming:self];
        }
    }
}

@end
