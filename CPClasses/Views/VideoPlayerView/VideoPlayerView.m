//
//  VideoPlayerView.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/19/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "VideoPlayerView.h"
#import <RACEXTScope.h>

static void *VideoPlayerViewStatusObservationContext = &VideoPlayerViewStatusObservationContext;
static void *VideoPlayerViewRateObservationContext = &VideoPlayerViewRateObservationContext;
static void *VideoPlayerViewCurrentItemObservationContext = &VideoPlayerViewCurrentItemObservationContext;

@interface VideoPlayerView ()

@property (nonatomic) AVPlayerItem *playerItem;
@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVAsset *videoAsset;
@property (nonatomic) CMTimeRange timeRange;
@property (nonatomic) NSTimer *timer;


@end

@implementation VideoPlayerView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.player removeObserver:self forKeyPath:@"currentItem"];
    self.playerItem = nil;
    self.player = nil;
}

+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.backgroundColor = [UIColor blackColor];
    _timeRange = kCMTimeRangeInvalid;
    self.muted = YES;
}

- (void)prepareToPlay
{
    [self prepareToPlayWithTimeRange:kCMTimeRangeInvalid];
}

- (void)prepareToPlayWithTimeRange:(CMTimeRange)timeRange
{
    if (self.videoAsset){
        [self playWithTimeRange:timeRange];
    }
    else if (self.videoURL){
        self.videoAsset = [AVURLAsset assetWithURL:self.videoURL];
        [self playWithTimeRange:timeRange];
    }
    else if (self.photoLibraryAsset){
        @weakify(self)
        [[PHImageManager defaultManager] requestAVAssetForVideo:self.photoLibraryAsset options:nil resultHandler:^(AVAsset *videoAsset, AVAudioMix *audioMix, NSDictionary *info) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                self.videoAsset = videoAsset;
                [self playWithTimeRange:timeRange];
            });
        }];
    }
}

- (void)play
{
    [self playWithTimeRange:kCMTimeRangeInvalid];
}

- (void)playWithTimeRange:(CMTimeRange)timeRange
{
    _timeRange = timeRange;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        if (self.playerItem){
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
        }
        self.playerItem = [AVPlayerItem playerItemWithAsset:self.videoAsset];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
        [self.player removeObserver:self forKeyPath:@"currentItem"];
        self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
        self.player.muted = self.muted;
        [self.player addObserver:self forKeyPath:@"currentItem" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:VideoPlayerViewCurrentItemObservationContext];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (CMTIMERANGE_IS_VALID(self.timeRange)){
                [self seekToStart];
            }else{
                [self.player play];
            }
        });
    });
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    if (CMTIMERANGE_IS_VALID(self.timeRange)){
        [self.player seekToTime:self.timeRange.start toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }else{
        [self.player seekToTime:kCMTimeZero toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }
    [self.player play];
    if ([self.delegate respondsToSelector:@selector(videoPlayerViewDidFinishPlayingVideoWithURL:)]){
        [self.delegate videoPlayerViewDidFinishPlayingVideoWithURL:self.videoURL];
    }
}

- (void)seekToStart
{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
    [self.player seekToTime:self.timeRange.start toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    [self.player play];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f/10.0f target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
}

- (void)timerAction
{
    if (CMTIME_COMPARE_INLINE(self.player.currentTime, >=, CMTimeRangeGetEnd(self.timeRange))){
        [self seekToStart];
    }
}

- (void)stop
{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark -
#pragma mark Setters

- (void)setAsset:(AVAsset *)asset
{
    _videoAsset = asset;
}

- (void)setTimeRange:(CMTimeRange)timeRange
{
    _timeRange = timeRange;
    [self seekToStart];
}

#pragma mark -
#pragma mark Getters

- (AVAsset *)asset
{
    return _videoAsset;
}

#pragma mark -
#pragma mark Asset Key Value Observing

- (void)observeValueForKeyPath:(NSString*) path
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    if (context == VideoPlayerViewCurrentItemObservationContext)
    {
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        if (newPlayerItem != (id)[NSNull null]){
            AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self layer];
            playerLayer.player = self.player;
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        }
    }
    else
    {
        [super observeValueForKeyPath:path ofObject:object change:change context:context];
    }
}

@end
