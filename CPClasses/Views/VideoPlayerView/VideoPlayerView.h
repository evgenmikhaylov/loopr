//
//  VideoPlayerView.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/19/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

@protocol VideoPlayerViewDelegate <NSObject>

@optional
- (void)videoPlayerViewDidFinishPlayingVideoWithURL:(NSURL*)url;

@end

@interface VideoPlayerView : UIView

@property (weak, nonatomic) id<VideoPlayerViewDelegate> delegate;
@property (nonatomic, readonly) AVPlayerItem *playerItem;
@property (nonatomic, readonly) AVPlayer *player;
@property (nonatomic) NSURL *videoURL;
@property (nonatomic) PHAsset *photoLibraryAsset;
@property (nonatomic) AVAsset *asset;
@property (nonatomic) BOOL muted;

- (void)prepareToPlay;
- (void)prepareToPlayWithTimeRange:(CMTimeRange)timeRange;
- (void)setTimeRange:(CMTimeRange)timeRange;
- (void)stop;

@end
