//
//  GLStateCache.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLStateCache.h"
#import "GLProgram.h"

#if PROFILE_STATE_CACHE
static long long __redundantCall = 0;
#endif

@implementation GLStateCache

+ (void)reset
{
    [self setViewportSize:CGSizeZero];
    [self setClearColorRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
    [self useGLProgram:nil];
    [self bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
    [self bindRenderbuffer:GL_RENDERBUFFER framebuffer:0];
}

+ (void)setViewportSize:(CGSize)size
{
#if USE_STATE_CACHE
    static CGSize _viewPortSize = (CGSize){0.0f, 0.0f};
    
    if (!CGSizeEqualToSize(size, _viewPortSize))
    {
        _viewPortSize = size;
#endif
        glViewport(0, 0, (GLsizei)size.width, (GLsizei)size.height);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

+ (void)setClearColorRed:(CGFloat)red green:(GLfloat)green blue:(GLfloat)blue alpha:(GLfloat)alpha
{
#if USE_STATE_CACHE
    static GLfloat _red = -1.0f, _green = -1.0f, _blue = -1.0f, _alpha = -1.0f;
    
    if (_red != red || _green != green || _blue != blue || _alpha != alpha)
    {
        _red = red;
        _green = green;
        _blue = blue;
        _alpha = alpha;
#endif
        glClearColor(red, green, blue, alpha);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

+ (void)useGLProgram:(GLProgram *)program
{
#if USE_STATE_CACHE
    static __unsafe_unretained GLProgram* _program = nil;
    
    if (_program != program)
    {
        _program = program;
#endif
        [program use];
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

+ (void)bindFramebuffer:(GLuint)target framebuffer:(GLuint)framebuffer
{
#if USE_STATE_CACHE
    static GLuint _target = 0, _framebuffer = 0;
    
    if (_target != target || _framebuffer != framebuffer)
    {
        _target = target;
        _framebuffer = framebuffer;
#endif
        glBindFramebuffer(target, framebuffer);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

+ (void)bindRenderbuffer:(GLuint)target framebuffer:(GLuint)renderbuffer
{
#if USE_STATE_CACHE
    static GLuint _target = 0, _renderbuffer = 0;
    
    if (_target != target || _renderbuffer != renderbuffer)
    {
        _target = target;
        _renderbuffer = renderbuffer;
#endif
        glBindRenderbuffer(target, renderbuffer);
#if USE_STATE_CACHE
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; }
#endif
}

#if PROFILE_STATE_CACHE
+ (void)incRedundantCall
{
    __redundantCall++;
}

+ (void)showStatistics
{
    NSLog(@"Redundant Call save by State Cache: %lld",__redundantCall);
}
#endif

@end
