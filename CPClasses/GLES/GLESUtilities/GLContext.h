//
//  GLContext.h
//  SplitPic
//
//  Created by Admin on 03/05/2013.
//  Copyright (c) 2013 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>


#if DEBUG
#define CHECK_GL_ERROR() \
{\
    GLenum err = glGetError();\
    if (err != GL_NO_ERROR)\
        printf("Error(%s-%d). glError: 0x%04X\n",__FILE__,__LINE__, err);\
}
#else
#define CHECK_GL_ERROR()
#endif


@interface GLContext : NSObject

+ (BOOL)supportExtensionTextureRG;
+ (BOOL)supportDiscardFramebuffer;
+ (GLint)maximumTextureSize;

+ (CGSize)sizeThatFitInMaxSupportSize:(CGSize)inputSize;
+ (CGFloat)scale;

+ (EAGLContext *)context;
+ (CVOpenGLESTextureCacheRef)videoTextureCache;

+ (void)setContext;

+ (void)lock;
+ (void)unlock;

+ (void)perfromBlockWithLockContext:(dispatch_block_t)block;

@end
