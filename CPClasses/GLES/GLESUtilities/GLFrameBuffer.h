//
//  GLFrameBuffer.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/25/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

@class GLTexture;
@class GLProgram;

typedef enum
{
    GLFBOPixelFormatRGBA8888 = 0,
    GLFBOPixelFormatRGB565,
    GLFBOPixelFormatR8
} GLFBOPixelFormat;

@interface GLFrameBuffer : NSObject

@property (nonatomic,readonly) GLuint handle;
@property (nonatomic,readonly) GLuint texHandle;
@property (nonatomic,readonly) CGSize size;

+ (instancetype)frameBufferWithFormat:(GLFBOPixelFormat)format andSize:(CGSize)size;
+ (instancetype)frameBufferWithGLTexture:(GLTexture *)texture;

- (void)bind;
- (void)unbind;

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit;

- (void)imageFromFramebufferWithBlock:(void(^)(BOOL completed, UIImage* image))block;

@end
