//
//  GLTexture.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/31/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLTexture.h"
#import "GLContext.h"
#import "GLFrameBuffer.h"
#import "GLProgram.h"

@interface GLTexture ()
{
    GLTextureFormat textureFormat;
}

@end

@implementation GLTexture

+ (instancetype)texture
{
    GLTexture* texture = [[self alloc] init];
    texture->textureFormat = GLTextureFormatRGB565;
    return texture;
}

+ (instancetype)textureWithTextureFormat:(GLTextureFormat)textureFormat
{
    GLTexture* texture = [self texture];
    texture->textureFormat = textureFormat;
    return texture;
}

+ (instancetype)textureWithImage:(UIImage *)image andTextureFormat:(GLTextureFormat)textureFormat
{
    GLTexture* texture = [self texture];
    texture->textureFormat = textureFormat;
    [texture loadTextureWithImage:image size:image.size error:nil];
    return texture;
}

+ (instancetype)emptyTextureWithSize:(CGSize)size andTextureFormat:(GLTextureFormat)textureFormat
{
    GLTexture* texture = [self texture];
    texture->textureFormat = textureFormat;
    texture->_size = size;
    [GLContext lock];
    
    glGenTextures(1, &texture->_texHandle);
    glBindTexture(GL_TEXTURE_2D, texture->_texHandle);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    if (textureFormat == GLTextureFormatRGBA8888)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)size.width, (GLsizei)size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLsizei)size.width, (GLsizei)size.height, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, NULL);
    }
    
    texture->_isLoaded = YES;
    glBindTexture(GL_TEXTURE_2D, 0);
    
    [GLContext unlock];
    return texture;
}

- (void)dealloc
{
    [self clearTexture];
}

- (NSString *)description
{
    return [[super description] stringByAppendingFormat:@", isLoaded = %d, size = %@, handle = %d",self.isLoaded,NSStringFromCGSize(self.size),self.texHandle];
}

- (BOOL)loadTextureWithImage:(UIImage *)image size:(CGSize)size error:(NSError **)error
{
    [self clearTexture];
    
    if (image == nil)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLTextureLoaderDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Image is nil"}];
        }
        
        return NO;
    }
    
    CGImageAlphaInfo info = CGImageGetAlphaInfo(image.CGImage);
    size = [GLContext sizeThatFitInMaxSupportSize:size];
    
    BOOL hasAlpha = ((info == kCGImageAlphaPremultipliedLast) || (info == kCGImageAlphaPremultipliedFirst) || (info == kCGImageAlphaLast) || (info == kCGImageAlphaFirst) ? YES : NO);
    
    if (CGImageGetColorSpace(image.CGImage))
    {
        if (hasAlpha)
        {
            info = kCGImageAlphaPremultipliedLast;
        }
        else
        {
            info = kCGImageAlphaNoneSkipLast;
        }
    }
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation)
    {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformRotate(CGAffineTransformTranslate(transform, size.width, size.height), M_PI);
            break;
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformRotate(CGAffineTransformTranslate(transform, size.width, 0.0f), M_PI_2);
            break;
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformRotate(CGAffineTransformTranslate(transform, 0.0f, size.height), -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (image.imageOrientation)
    {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformScale(CGAffineTransformTranslate(transform, size.width, 0.0f), -1.0f, 1.0f);
            break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformScale(CGAffineTransformTranslate(transform, size.height, 0.0f), -1.0f, 1.0f);
            break;
        default:
            break;
    }
    
    void* data = malloc(size.width * size.height * 4);
    
    if (data == NULL)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLTextureLoaderDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed allocate memory for image"}];
        }
        
        return NO;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(data, size.width, size.height, 8, 4 * size.width, colorSpace, info | kCGBitmapByteOrder32Big);
    
    if (context == NULL)
    {
        CGColorSpaceRelease(colorSpace);
        
        if (data)
        {
            free(data);
        }
        
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLTextureLoaderDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed create CGContext"}];
        }
        
        return NO;
    }
    
    CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
    CGContextConcatCTM(context, transform);
    
    switch (image.imageOrientation)
    {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.height, size.width), image.CGImage);
            break;
        default:
            CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), image.CGImage);
            break;
    }
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    [GLContext lock];
    
    glGenTextures(1, &_texHandle);
    glBindTexture(GL_TEXTURE_2D, _texHandle);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    if (textureFormat == GLTextureFormatRGBA8888)
    {
        if ([self.dataProviderDelegate respondsToSelector:@selector(glTexture:didLoadData:)])
        {
            @autoreleasepool
            {
                [self.dataProviderDelegate glTexture:self didLoadData:[NSData dataWithBytesNoCopy:data length:size.width * size.height * 4 freeWhenDone:NO]];
            }
        }
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)size.width, (GLsizei)size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        free(data);
    }
    else
    {
        void* tempData = malloc(size.width * size.height * 2);
        
        if (tempData == NULL)
        {
            free(data);
            
            if (error != NULL)
            {
                *error = [NSError errorWithDomain:@"GLTextureLoaderDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Failed allocate memory for converting texture"}];
            }
            
            return NO;
        }
        
        unsigned int* inPixel32 = (unsigned int*)data;
        unsigned short* outPixel16 = (unsigned short*)tempData;
        
        // Convert RGBA8888 to RGB565
        for (unsigned long i = 0; i < size.width * size.height; ++i, ++inPixel32)
        {
            *outPixel16++ = ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | ((((*inPixel32 >> 8) & 0xFF) >> 2) << 5) | ((((*inPixel32 >> 16) & 0xFF) >> 3) << 0);
        }
        
        free(data);
        
        if ([self.dataProviderDelegate respondsToSelector:@selector(glTexture:didLoadData:)])
        {
            @autoreleasepool
            {
                [self.dataProviderDelegate glTexture:self didLoadData:[NSData dataWithBytesNoCopy:tempData length:size.width * size.height * 2 freeWhenDone:NO]];
            }
        }
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLsizei)size.width, (GLsizei)size.height, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, tempData);
        free(tempData);
    }
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
#if 0
    NSString* path = [NSTemporaryDirectory() stringByAppendingString:@"RGB565.bin"];
    FILE* file = fopen([path UTF8String], "w");
    fwrite(tempData, size.width * size.height * 2, 1, file);
    fclose(file);
#endif
    
    _size = size;
    _isLoaded = YES;
    
    [GLContext unlock];
    
    return YES;
}

- (void)loadTextureAsyncWithImage:(UIImage *)image size:(CGSize)size andCompletionBlock:(void(^)(BOOL completed, NSError* error))block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError* error = nil;
        BOOL result = [self loadTextureWithImage:image size:size error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            block(result, error);
        });
    });
}

- (BOOL)loadTextureWithData:(NSData *)data size:(CGSize)size error:(NSError **)error
{
    UIImage* image = [UIImage imageWithData:data];
    return [self loadTextureWithImage:image size:size error:error];
}

- (void)loadTextureAsyncWithData:(NSData *)data size:(CGSize)size andCompletionBlock:(void(^)(BOOL completed, NSError* error))block
{
    UIImage* image = [UIImage imageWithData:data];
    [self loadTextureAsyncWithImage:image size:size andCompletionBlock:block];
}

- (BOOL)loadTextureWithRAWData:(NSData *)data size:(CGSize)size error:(NSError **)error
{
    [self clearTexture];
    
    if (data)
    {
        size = [GLContext sizeThatFitInMaxSupportSize:size];
        
        [GLContext lock];
        
        glGenTextures(1, &_texHandle);
        glBindTexture(GL_TEXTURE_2D, _texHandle);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        
        if (textureFormat == GLTextureFormatRGBA8888)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)size.width, (GLsizei)size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.bytes);
        }
        else
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLsizei)size.width, (GLsizei)size.height, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, data.bytes);
        }
        
        glBindTexture(GL_TEXTURE_2D, 0);
        
        _size = size;
        _isLoaded = YES;
        
        [GLContext unlock];
        
        return YES;
    }
    else
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLTextureLoaderDomain" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Data is nil"}];
        }
        
        return NO;
    }
}

- (void)loadTextureAsyncWithRAWData:(NSData *)data size:(CGSize)size andCompletionBlock:(void(^)(BOOL completed, NSError* error))block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError* error = nil;
        BOOL result = [self loadTextureWithRAWData:data size:size error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            block(result,error);
        });
    });
}

- (void)clearTexture
{
    _isLoaded = NO;
    
    if (_texHandle)
    {
        [GLContext perfromBlockWithLockContext:^()
         {
             glDeleteTextures(1, &_texHandle);
             _texHandle = 0;
         }];
    }
}

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit
{
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, self.texHandle);
    [program setUniform1i:uniform value:unit - GL_TEXTURE0];
}

#if DEBUG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (id)debugQuickLookObject
{
    GLFrameBuffer* fbo = [GLFrameBuffer frameBufferWithGLTexture:self];
    return [fbo performSelector:NSSelectorFromString(@"debugQuickLookObject")];
}
#pragma clang diagnostic pop
#endif

@end
