//
//  GLView.h
//  SplitPic
//
//  Created by Andrey Konoplyankin on 10/14/13.
//  Copyright (c) 2013 Andrey. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSInteger, GLViewOptions)
{
    GLViewOptionsNone = 1 << 0,
    GLViewOptionsDepthBuffer = 1 << 1,
    GLViewOptionsMSAA = 1 << 2,
};

@protocol GLViewDelegate;

@interface GLView : UIView

@property (nonatomic,weak) IBOutlet id<GLViewDelegate> delegate;

@property (nonatomic,readonly) CGSize sizeInPixels;
@property (nonatomic,readwrite) GLViewOptions options;

- (BOOL)display;
- (void)bindDrawable;

- (UIImage *)snapshot;

@end

@protocol GLViewDelegate <NSObject>
@optional

- (void)glView:(GLView *)view didRecreateDisplayFramebuffer:(CGSize)sizeInPixels;

@required

- (void)glView:(GLView *)view drawInRect:(CGRect)rect;

@end