//
//  GLVector3.h
//  GLESUtilities
//
//  Created by Andrey on 15/06/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#ifndef __SPAPP__GLVECTOR3___
#define __SPAPP__GLVECTOR3___

class GLVector3
{
    friend class GLMatrix4x4;
    
public:
    
    float v[3];
    
    GLVector3(float x = 0.0f, float y = 0.0f, float z = 0.0f);
    ~GLVector3();
    
    float length();
    
    GLVector3 negative();
    GLVector3 normalize();
    
    static GLVector3 Add(GLVector3 left, GLVector3 right);
    static GLVector3 CrossProduct(GLVector3 left, GLVector3 right);
    static float DotProduct(GLVector3 left, GLVector3 right);
};

#endif /* defined(__SPAPP__GLVECTOR3___) */
