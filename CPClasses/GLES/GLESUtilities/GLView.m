//
//  GLView.m
//  SplitPic
//
//  Created by Andrey Konoplyankin on 10/14/13.
//  Copyright (c) 2013 Andrey. All rights reserved.
//

#import "GLView.h"
#import "GLContext.h"
#import "GLLog.h"
#import "GLStateCache.h"

@interface GLView ()
{
    GLuint colorRenderBuffer, depthRenderBuffer, displayFrameBuffer;
    GLuint msaaColorRenderbuffer, msaaFrameBuffer;
    
    GLsizei numberMSAASamples;
}

@end

@implementation GLView

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

#pragma mark -
#pragma mark Designated Initializer

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.contentScaleFactor = [GLContext scale];
    self.opaque = YES;
    
    CAEAGLLayer* eaglLayer = (CAEAGLLayer *)self.layer;
    eaglLayer.opaque = YES;
    eaglLayer.drawableProperties = @{kEAGLDrawablePropertyRetainedBacking:@(NO),kEAGLDrawablePropertyColorFormat:kEAGLColorFormatRGBA8};
    
    numberMSAASamples = 2;
    
    [GLContext perfromBlockWithLockContext:^()
     {
         [self destroyDisplayFramebuffer];
         [self createDisplayFramebuffer];
     }];
}

- (void)dealloc
{
    GLLog(GLLogVerboseLevelWarning, @"%s",__func__);
    
    [GLContext perfromBlockWithLockContext:^()
     {
         [self destroyDisplayFramebuffer];
     }];
}

#pragma mark -
#pragma mark Setters

- (void)setFrame:(CGRect)frame
{
    CGSize size = self.bounds.size;
    
    [super setFrame:frame];
    
    if (!CGSizeEqualToSize(self.bounds.size, size))
    {
        [self recreateDisplayFramebuffer];
    }
}

- (void)setBounds:(CGRect)bounds
{
    BOOL recreateDisplayFramebuffer = !CGSizeEqualToSize(self.bounds.size, bounds.size);
    [super setBounds:bounds];
    
    if (recreateDisplayFramebuffer)
    {
        [self recreateDisplayFramebuffer];
    }
}

- (void)setOptions:(GLViewOptions)options
{
    if (self.options != options)
    {
        _options = options;
        [self recreateDisplayFramebuffer];
    }
}

#pragma mark -
#pragma mark Create/Destroy main framebuffer

- (void)recreateDisplayFramebuffer
{
    [GLContext perfromBlockWithLockContext:^()
     {
         [GLStateCache reset];
         [self destroyDisplayFramebuffer];
         [self createDisplayFramebuffer];
     }];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(glView:didRecreateDisplayFramebuffer:)])
    {
        [self.delegate glView:self didRecreateDisplayFramebuffer:self.sizeInPixels];
    }
}

- (void)createDisplayFramebuffer
{
    if (self.bounds.size.width == 0.0f || self.bounds.size.height == 0.0f)
    {
        return;
    }
    
    glGenFramebuffers(1, &displayFrameBuffer);
    [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:displayFrameBuffer];
    
    glGenRenderbuffers(1, &colorRenderBuffer);
    [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:colorRenderBuffer];
    
    [[GLContext context] renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    GLint backingWidth, backingHeight;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    if ((backingWidth == 0) || (backingHeight == 0))
    {
        [self destroyDisplayFramebuffer];
        return;
    }
    
    _sizeInPixels.width = (CGFloat)backingWidth;
    _sizeInPixels.height = (CGFloat)backingHeight;
    
    GLLog(GLLogVerboseLevelWarning, @"Backing width: %d, height: %d", backingWidth, backingHeight);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
    
    if (self.options & GLViewOptionsMSAA)
    {
        GLint maxSamplesAllowed = 0;
        glGetIntegerv(GL_MAX_SAMPLES_APPLE, &maxSamplesAllowed);
        
        glGenFramebuffers(1, &msaaFrameBuffer);
        NSAssert(msaaFrameBuffer, @"Can't create default MSAA frame buffer");
        
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:msaaFrameBuffer];
        glBindFramebuffer(GL_FRAMEBUFFER, msaaFrameBuffer);
        
        glGenRenderbuffers(1, &msaaColorRenderbuffer);
        NSAssert(msaaColorRenderbuffer, @"Can't create MSAA color buffer");
        
        [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:msaaColorRenderbuffer];
        
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, MIN(maxSamplesAllowed, numberMSAASamples), GL_RGBA8_OES , backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, msaaColorRenderbuffer);
#if DEBUG
        GLuint framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        NSAssert(framebufferStatus == GL_FRAMEBUFFER_COMPLETE, @"Failure with MSAA framebuffer generation, error - 0x%X", framebufferStatus);
#endif
    }
    
    if (self.options & GLViewOptionsDepthBuffer)
    {
        glGenRenderbuffers(1, &depthRenderBuffer);
        [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:depthRenderBuffer];
        
        if (self.options & GLViewOptionsMSAA)
        {
            GLint maxSamplesAllowed = 0;
            glGetIntegerv(GL_MAX_SAMPLES_APPLE, &maxSamplesAllowed);
            glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, MIN(maxSamplesAllowed, numberMSAASamples), GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        }
        else
        {
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        }
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:colorRenderBuffer];
    }
    
#if DEBUG
    GLuint framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    NSAssert(framebufferStatus == GL_FRAMEBUFFER_COMPLETE, @"Failure with display framebuffer generation for display of size: %f, %f", self.bounds.size.width, self.bounds.size.height);
#endif
}

- (void)destroyDisplayFramebuffer
{
    if (displayFrameBuffer)
    {
        glDeleteFramebuffers(1, &displayFrameBuffer);
        displayFrameBuffer = 0;
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
    }
    
    if (colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer = 0;
        [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:0];
    }
    
    if (depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
        [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:0];
    }
    
    if (msaaColorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &msaaColorRenderbuffer);
        msaaColorRenderbuffer = 0;
        [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:0];
    }
    
    if (msaaFrameBuffer)
    {
        glDeleteFramebuffers(1, &msaaFrameBuffer);
        msaaFrameBuffer = 0;
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
    }
}

#pragma mark -
#pragma mark Display

- (BOOL)display
{
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground)
    {
        [GLContext lock];
        
        [self bindDrawable];
        [self.delegate glView:self drawInRect:self.bounds];
        [self presentFramebuffer];
        
        [GLContext unlock];
        
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)bindDrawable
{
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:msaaFrameBuffer];
    }
    else
    {
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:displayFrameBuffer];
    }
}

- (void)presentFramebuffer
{
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache bindFramebuffer:GL_READ_FRAMEBUFFER_APPLE framebuffer:msaaFrameBuffer];
        [GLStateCache bindFramebuffer:GL_DRAW_FRAMEBUFFER_APPLE framebuffer:displayFrameBuffer];
        glResolveMultisampleFramebufferAPPLE();
    }
    
    if ([GLContext supportDiscardFramebuffer])
    {
        if (self.options & GLViewOptionsMSAA)
        {
            if (self.options & GLViewOptionsDepthBuffer)
            {
                GLenum attachments[] = {GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT};
                glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 2, attachments);
            }
            else
            {
                GLenum attachments[] = {GL_COLOR_ATTACHMENT0};
                glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, attachments);
            }
            
            [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:colorRenderBuffer];
        }
        else if (self.options & GLViewOptionsDepthBuffer)
        {
            GLenum attachments[] = {GL_DEPTH_ATTACHMENT};
            glDiscardFramebufferEXT(GL_FRAMEBUFFER, 1, attachments);
        }
    }
    
    [GLStateCache bindRenderbuffer:GL_RENDERBUFFER framebuffer:colorRenderBuffer];
    [[GLContext context] presentRenderbuffer:GL_RENDERBUFFER];
    
    if (self.options & GLViewOptionsMSAA)
    {
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:msaaFrameBuffer];
    }
}

#pragma mark -

static void GLViewDataProviderReleaseCallback(void *info, const void *data, size_t size)
{
    free((void *)data);
}

- (UIImage *)snapshot
{
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground)
    {
        CGSize size = self.sizeInPixels;
        NSUInteger dataSize = size.width * size.height * 4;
        GLubyte* data = malloc(dataSize);
        
        [GLContext lock];
        
        [self.delegate glView:self drawInRect:self.bounds];
        
        if (self.options & GLViewOptionsMSAA)
        {
            [GLStateCache bindFramebuffer:GL_READ_FRAMEBUFFER_APPLE framebuffer:msaaFrameBuffer];
            [GLStateCache bindFramebuffer:GL_DRAW_FRAMEBUFFER_APPLE framebuffer:displayFrameBuffer];
            glResolveMultisampleFramebufferAPPLE();
            
            [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:displayFrameBuffer];
        }
        
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glReadPixels(0, 0, size.width, size.height, GL_RGBA, GL_UNSIGNED_BYTE, data);
        
        [GLContext unlock];
        
        CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, data, dataSize, GLViewDataProviderReleaseCallback);
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        CGImageRef cgImage = CGImageCreate(size.width, size.height, 8, 32, 4 * size.width, colorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaLast, dataProvider, NULL, NO, kCGRenderingIntentDefault);
        UIImage* image = [UIImage imageWithCGImage:cgImage scale:1.0f orientation:UIImageOrientationDownMirrored];
        
        CGDataProviderRelease(dataProvider);
        CGColorSpaceRelease(colorSpace);
        CGImageRelease(cgImage);
        
        return image;
    }
    else
    {
        return nil;
    }
}

#if DEBUG

- (id)debugQuickLookObject
{
    return [self snapshot];
}

#endif

@end
