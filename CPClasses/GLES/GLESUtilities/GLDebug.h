//
//  GLDebug.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 4/10/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//


#import "GLContext.h"

#define DEBUG_OPENGL_CALL 1

#if DEBUG_OPENGL_CALL

#define glCreateProgram()                       glCreateProgram();                      CHECK_GL_ERROR();
#define glCreateShader(a)                       glCreateShader(a);                      CHECK_GL_ERROR();
#define glAttachShader(a, b)                    glAttachShader(a, b);                   CHECK_GL_ERROR();
#define glShaderSource(a, b, c, d)              glShaderSource(a, b, c, d);             CHECK_GL_ERROR();
#define glCompileShader(a)                      glCompileShader(a);                     CHECK_GL_ERROR();
#define glGetShaderiv(a, b, c)                  glGetShaderiv(a, b, c);                 CHECK_GL_ERROR();
#define glGetShaderInfoLog(a, b, c, d)          glGetShaderInfoLog(a, b, c, d);         CHECK_GL_ERROR();
#define glBindAttribLocation(a, b, c)           glBindAttribLocation(a, b, c);          CHECK_GL_ERROR();
#define glGetUniformLocation(a, b)              glGetUniformLocation(a, b);             CHECK_GL_ERROR();
#define glLinkProgram(a)                        glLinkProgram(a);                       CHECK_GL_ERROR();
#define glGetProgramiv(a, b, c)                 glGetProgramiv(a, b, c);                CHECK_GL_ERROR();
#define glDeleteShader(a)                       glDeleteShader(a);                      CHECK_GL_ERROR();
#define glUseProgram(a)                         glUseProgram(a);                        CHECK_GL_ERROR();
#define glValidateProgram(a)                    glValidateProgram(a);                   CHECK_GL_ERROR();
#define glGetProgramInfoLog(a, b, c, d)         glGetProgramInfoLog(a, b, c, d);        CHECK_GL_ERROR();
#define glDeleteProgram(a)                      glDeleteProgram(a);                     CHECK_GL_ERROR();


#define glUniform1i(a, b)                       glUniform1i(a, b);                      CHECK_GL_ERROR();
#define glUniform1f(a, b)                       glUniform1f(a, b);                      CHECK_GL_ERROR();
#define glUniform2f(a,b,c)                      glUniform2f(a,b,c);                     CHECK_GL_ERROR();
#define glUniformMatrix4fv(a, b, c, d)          glUniformMatrix4fv(a, b, c, d);         CHECK_GL_ERROR();


#define glGenBuffers(a,b)                       glGenBuffers(a,b);                      CHECK_GL_ERROR();
#define glDeleteBuffers(a, b)                   glDeleteBuffers(a, b);                  CHECK_GL_ERROR();
#define glBindBuffer(a,b)                       glBindBuffer(a,b);                      CHECK_GL_ERROR();
#define glBufferData(a, b, c, d)                glBufferData(a, b, c, d);               CHECK_GL_ERROR();
#define glMapBufferOES(a, b)                    glMapBufferOES(a, b);                   CHECK_GL_ERROR();
#define glUnmapBufferOES(a)                     glUnmapBufferOES(a);                    CHECK_GL_ERROR();


#define glGenVertexArraysOES(a, b)              glGenVertexArraysOES(a, b);             CHECK_GL_ERROR();
#define glDeleteVertexArraysOES(a, b)           glDeleteVertexArraysOES(a, b);          CHECK_GL_ERROR();
#define glBindVertexArrayOES(a)                 glBindVertexArrayOES(a);                CHECK_GL_ERROR();


#define glGenFramebuffers(a, b)                 glGenFramebuffers(a, b);                CHECK_GL_ERROR();
#define glBindFramebuffer(a, b)                 glBindFramebuffer(a, b);                CHECK_GL_ERROR();
#define glDeleteFramebuffers(a, b)              glDeleteFramebuffers(a, b);             CHECK_GL_ERROR();
#define glFramebufferTexture2D(a, b, c, d, e)   glFramebufferTexture2D(a, b, c, d, e);  CHECK_GL_ERROR();
//#define glCheckFramebufferStatus(a)             glCheckFramebufferStatus(a); CHECK_GL_ERROR();

#define glGenRenderbuffers(a, b)                glGenRenderbuffers(a, b);               CHECK_GL_ERROR();
#define glBindRenderbuffer(a, b)                glBindRenderbuffer(a, b);               CHECK_GL_ERROR();
#define glGetRenderbufferParameteriv(a, b, c)   glGetRenderbufferParameteriv(a, b, c);  CHECK_GL_ERROR();
#define glFramebufferRenderbuffer(a, b, c, d)   glFramebufferRenderbuffer(a, b, c, d);  CHECK_GL_ERROR();
#define glDeleteRenderbuffers(a, b)             glDeleteRenderbuffers(a, b);            CHECK_GL_ERROR();


#define glPixelStorei(a, b)                     glPixelStorei(a, b);                    CHECK_GL_ERROR();
#define glGenTextures(a, b)                     glGenTextures(a, b);                    CHECK_GL_ERROR();
#define glActiveTexture(a)                      glActiveTexture(a);                     CHECK_GL_ERROR();
#define glBindTexture(a, b)                     glBindTexture(a, b);                    CHECK_GL_ERROR();
#define glDeleteTextures(a, b)                  glDeleteTextures(a, b);                 CHECK_GL_ERROR();
#define glTexParameterf(a, b, c)                glTexParameterf(a, b, c);               CHECK_GL_ERROR();
#define glTexParameteri(a, b, c)                glTexParameteri(a, b, c);               CHECK_GL_ERROR();
#define glTexImage2D(a, b, c, d, e, f, g, i, k) glTexImage2D(a, b, c, d, e, f, g, i, k); CHECK_GL_ERROR();


#define glEnableVertexAttribArray(a)            glEnableVertexAttribArray(a);           CHECK_GL_ERROR();
#define glVertexAttribPointer(a, b, c, d, e, f) glVertexAttribPointer(a, b, c, d, e, f); CHECK_GL_ERROR();

#define glDrawArrays(a, b, c)                   glDrawArrays(a, b, c);                  CHECK_GL_ERROR();

#define glGetIntegerv(a, b)                     glGetIntegerv(a, b);                    CHECK_GL_ERROR();
#define glReadPixels(a, b, c, d, e, f, g)       glReadPixels(a, b, c, d, e, f, g);      CHECK_GL_ERROR();

#define glViewport(a, b, c, d)                  glViewport(a, b, c, d);                 CHECK_GL_ERROR();
#define glClearColor(a, b, c, d)                glClearColor(a, b, c, d);               CHECK_GL_ERROR();
#define glClear(a)                              glClear(a);                             CHECK_GL_ERROR();


#define glEnable(a)                             glEnable(a);                            CHECK_GL_ERROR();
#define glBlendFunc(a, b)                       glBlendFunc(a, b);                      CHECK_GL_ERROR();
#define glBlendFuncSeparate(a, b, c, d)         glBlendFuncSeparate(a, b, c, d);        CHECK_GL_ERROR();

#endif // #if DEBUG_OPENGL_CALL
