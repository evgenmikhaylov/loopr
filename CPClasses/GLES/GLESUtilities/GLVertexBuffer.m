//
//  GLVertexBuffer.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLVertexBuffer.h"
#import "GLContext.h"

@implementation GLVertexBuffer

+ (instancetype)vertexBufferWithDataSize:(NSUInteger)size
{
    return [[self alloc] initWithDataSize:size];
}

- (instancetype)initWithDataSize:(NSUInteger)size
{
    self = [super init];
    if (self)
    {
        _dataSize = size;
        
        NSAssert(size != 0, @"Data size must be > 0");
        [self createVBOWithDataSize:size];
    }
    return self;
}

- (void)dealloc
{
    [self deleteVBO];
}

#pragma mark -
#pragma mark Create/Delete VBO

- (void)createVBOWithDataSize:(NSUInteger)size
{
    void* data = calloc(size, 1);
    NSAssert(data, @"Failed allocate memory for size %zd", size);
    
    [GLContext perfromBlockWithLockContext:^
     {
         glGenBuffers(1, &_vboHandle);
         
         NSAssert(_vboHandle != 0, @"Failed create VBO");
         
         glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
         glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
         glBindBuffer(GL_ARRAY_BUFFER, 0);
     }];
    
    free(data);
}

- (void)deleteVBO
{
    if (self.vboHandle)
    {
        [GLContext perfromBlockWithLockContext:^()
         {
             glDeleteBuffers(1, &_vboHandle);
             _vboHandle = 0;
         }];
    }
}

#pragma mark -
#pragma mark Bind/Unbind

- (void)bind
{
    glBindBuffer(GL_ARRAY_BUFFER, self.vboHandle);
}

- (void)unbind
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#pragma mark -
#pragma mark Map Vertex

- (BOOL)mapVertexDataWithBlock:(void(^)(void* data))block
{
    void* data = glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
    if (data)
    {
        block(data);
        glUnmapBufferOES(GL_ARRAY_BUFFER);
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)lockGLContextAndMapVertexDataWithBlock:(void(^)(void* data))block
{
    __block BOOL result = NO;
    [GLContext perfromBlockWithLockContext:^()
     {
         [self bind];
         result = [self mapVertexDataWithBlock:block];
         [self unbind];
     }];
    return result;
}

#pragma mark -

- (NSString *)description
{
    [GLContext setContext];
    
    NSMutableString* string = [NSMutableString stringWithFormat:@"<%@: %p> vboHandle = %zd, size = %zd, data:\n", NSStringFromClass([self class]), self, self.vboHandle, self.dataSize];
    
    void (^block)() = ^(void *data)
    {
        for (NSInteger i = 0; i < self.dataSize / sizeof(float); i += 2)
        {
            float* p = (float *)data + i;
            [string appendFormat:@"{%.2f, %.2f}\n", p[0], p[1]];
        }
    };
    
    [self bind];
    if (![self mapVertexDataWithBlock:block])
    {
        [string appendString:@"Failed to map buffer!"];
    }
    [self unbind];
    
    return [NSString stringWithString:string];
}

@end


@implementation GLVertexBuffer (Deprecated)

+ (instancetype)vertexBuffer
{
    return [[self alloc] init];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self createVBO];
    }
    return self;
}

#pragma mark -
#pragma mark Create VBO

- (void)createVBO
{
    [GLContext perfromBlockWithLockContext:^()
     {
         glGenBuffers(1, &_vboHandle);
     }];
}

#pragma mark -
#pragma mark Setup Vertex

- (void)setupVertexDataWithSize:(GLsizei)size data:(void *)data freeWhenDone:(BOOL)freeWhenDone
{
    _dataSize = size;
    
    [GLContext perfromBlockWithLockContext:^()
     {
         [self bind];
         glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
         [self unbind];
     }];
    
    if (freeWhenDone)
    {
        free(data);
    }
}

@end


