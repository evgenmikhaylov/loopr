//
//  GLVideoTexture.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, GLVideoTextureColorMatrix)
{
    GLVideoTextureColorMatrix601 = 0,
    GLVideoTextureColorMatrix709
};

@protocol GLVideoTextureDataOutput;

@interface GLVideoTexture : NSObject

@property (nonatomic,weak) id<GLVideoTextureDataOutput> dataOutput;

- (instancetype)initWithURL:(NSURL *)url completionBlock:(void(^)())block;

- (void)resetPlayback;
- (BOOL)readNextFrame;

@end

@protocol GLVideoTextureDataOutput <NSObject>
@required

- (void)glVideoTexture:(GLVideoTexture *)texture didOutputPixelBufferWithSize:(CGSize)size colorMatrix:(GLVideoTextureColorMatrix)colorMatrix chromaTextureHandle:(GLenum)chromaTextureHandle lumiaTextureHandle:(GLenum)lumiaTextureHandle;

@end
