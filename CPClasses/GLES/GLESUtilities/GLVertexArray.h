//
//  GLVertexArray.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

@interface GLVertexArray : NSObject

@property (nonatomic,readonly) GLuint vboHandle, vaoHandle;
@property (nonatomic,readonly) NSUInteger dataSize;

+ (instancetype)vertexArrayWithDataSize:(NSUInteger)size;

- (void)bind;
- (void)unbind;

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size stride:(GLuint)stride offset:(GLuint)offset;
- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size;

- (BOOL)mapVertexDataWithBlock:(void(^)(void* data))block;
- (BOOL)lockGLContextAndMapVertexDataWithBlock:(void(^)(void* data))block;

@end


@interface GLVertexArray (Deprecated)

+ (instancetype)vertexArray __attribute__ ((deprecated("Use [GLVertexArray vertexBufferWithDataSize:] instead")));
- (instancetype)init __attribute__ ((deprecated("Use [GLVertexArray vertexBufferWithDataSize:] instead")));

- (void)setupVertexDataWithSize:(GLsizei)size data:(void *)data freeWhenDone:(BOOL)freeWhenDone __attribute__ ((deprecated("Use [GLVertexArray vertexBufferWithDataSize:] instead")));
- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size stride:(GLuint)stride pointer:(void *)pointer __attribute__ ((deprecated("Use [GLVertexArray setupVertexAttrib:size:stride:offset:] instead")));

@end
