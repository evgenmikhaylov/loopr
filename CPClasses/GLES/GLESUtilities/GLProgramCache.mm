//
//  GLProgramCache.m
//  SplitPic
//
//  Created by Andrey Konoplyankin on 6/9/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLProgramCache.h"
#import "GLProgram.h"

@interface GLProgram (Private)
+ (instancetype)program;
@end

@interface GLProgramCache ()

@property (nonatomic) NSMutableDictionary* programs;

@end

@implementation GLProgramCache

+ (instancetype)sharedInstance
{
    static GLProgramCache* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (id)programWithClass:(id)className
{
    GLProgramCache* cache = [self sharedInstance];
    NSString* key = NSStringFromClass(className);
    
    if (cache.programs[key])
    {
        return cache.programs[key];
    }
    else
    {
        GLProgram* program = nil;
        if ([className respondsToSelector:@selector(program)])
        {
            program = [className program];
        }
        else
        {
            program = [[className alloc] init];
        }
        
        cache.programs[key] = program;
        return program;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.programs = [NSMutableDictionary dictionary];
    }
    return self;
}

@end
