//
//  GLStateCache.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

#define USE_STATE_CACHE 1
#define PROFILE_STATE_CACHE 0

@class GLProgram;

@interface GLStateCache : NSObject

+ (void)reset;

+ (void)setViewportSize:(CGSize)size;
+ (void)setClearColorRed:(CGFloat)red green:(GLfloat)green blue:(GLfloat)blue alpha:(GLfloat)alpha;

+ (void)useGLProgram:(GLProgram *)program;

+ (void)bindFramebuffer:(GLuint)target framebuffer:(GLuint)framebuffer;
+ (void)bindRenderbuffer:(GLuint)target framebuffer:(GLuint)renderbuffer;

#if PROFILE_STATE_CACHE
+ (void)incRedundantCall;
+ (void)showStatistics;
#endif

@end
