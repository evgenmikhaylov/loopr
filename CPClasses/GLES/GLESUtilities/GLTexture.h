//
//  GLTexture.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/31/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

@class GLProgram;

@protocol GLTextureDataProvider;

typedef enum
{
    GLTextureFormatRGBA8888 = 0,
    GLTextureFormatRGB565,
} GLTextureFormat;

@interface GLTexture : NSObject

+ (instancetype)texture;
+ (instancetype)textureWithTextureFormat:(GLTextureFormat)textureFormat;
+ (instancetype)textureWithImage:(UIImage *)image andTextureFormat:(GLTextureFormat)textureFormat;
+ (instancetype)emptyTextureWithSize:(CGSize)size andTextureFormat:(GLTextureFormat)textureFormat;

@property (nonatomic) GLuint texHandle;
@property (nonatomic,readonly) BOOL isLoaded;
@property (nonatomic,readonly) CGSize size;
@property (nonatomic,weak) id<GLTextureDataProvider> dataProviderDelegate;

- (BOOL)loadTextureWithImage:(UIImage *)image size:(CGSize)size error:(NSError **)error;

- (void)loadTextureAsyncWithImage:(UIImage *)image size:(CGSize)size andCompletionBlock:(void(^)(BOOL completed, NSError* error))block;

- (BOOL)loadTextureWithData:(NSData *)data size:(CGSize)size error:(NSError **)error;
- (void)loadTextureAsyncWithData:(NSData *)data size:(CGSize)size andCompletionBlock:(void(^)(BOOL completed, NSError* error))block;

- (BOOL)loadTextureWithRAWData:(NSData *)data size:(CGSize)size error:(NSError **)error;
- (void)loadTextureAsyncWithRAWData:(NSData *)data size:(CGSize)size andCompletionBlock:(void(^)(BOOL completed, NSError* error))block;

- (void)clearTexture;

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit;

@end

@protocol GLTextureDataProvider <NSObject>
@optional

- (void)glTexture:(GLTexture *)texture didLoadData:(NSData *)rawData;

@end
