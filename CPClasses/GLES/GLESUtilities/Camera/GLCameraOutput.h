//
//  GLCamera.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

extern NSInteger const GLCameraOutputCantAddInputDeviceErrorCode;
extern NSInteger const GLCameraOutputFailedInitializeAVCaptureSessionErrorCode;

@class GLProgram;

@interface GLCameraOutput : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic) CGFloat scale;
@property (nonatomic) CGSize size;
@property (nonatomic) BOOL isInitialize;

+ (AVAuthorizationStatus)authorizationStatus;
+ (void)requestAccessWithCompletionHandler:(void (^)(BOOL granted))handler;

+ (instancetype)cameraWithSize:(CGSize)size error:(NSError **)error;

- (instancetype)initWithSize:(CGSize)size error:(NSError **)error;
- (instancetype)init __attribute__((unavailable("Use -initWithSize:")));

- (GLint)texHandle;

- (void)startRunning;
- (void)stopRunning;
- (BOOL)isRunning;

- (void)pause;
- (void)resume;

- (BOOL)frontCameraAvailable;
- (BOOL)backCameraAvailable;

- (BOOL)canSetFocus;

/**
 Set camera focus to specific point.
 
 @param point Should be from {0,0} (top-left) to {1,1} (bottom-right).
 */

- (BOOL)setFocusAtPoint:(CGPoint)point;

- (BOOL)canSetExposureWithCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode;

/**
 Set camera exposure to specific point.
 
 @param point Should be from {0,0} (top-left) to {1,1} (bottom-right).
 */

- (BOOL)setExposureAtPoint:(CGPoint)point withCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode;

- (BOOL)canFlipCamera;
- (AVCaptureDevicePosition)flipCamera;
- (AVCaptureDevicePosition)captureDevicePosition;

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit;

@end
