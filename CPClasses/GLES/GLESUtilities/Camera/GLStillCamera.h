//
//  GLStillCamera.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLCameraOutput.h"

extern NSInteger const GLStillCameraCaptureConnectionNotFoundErrorCode;
extern NSInteger const GLStillCameraCaptureCantCameraAccessErrorCode;
extern NSInteger const GLStillCameraCaptureStillImageOutputNotFoundErrorCode;
extern NSInteger const GLStillCameraCaptureBeingCapturedErrorCode;

@interface GLStillCamera : GLCameraOutput

- (BOOL)flashAvailable;
- (BOOL)setFlashMode:(AVCaptureFlashMode)flashMode;
- (AVCaptureFlashMode)flashMode;

- (void)captureImageWithBlock:(void(^)(BOOL completed, NSData* imageData, NSError* error))block;

@end
