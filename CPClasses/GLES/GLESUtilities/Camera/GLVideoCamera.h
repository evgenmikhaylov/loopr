//
//  GLVideoCamera.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLCameraOutput.h"

@interface GLVideoCamera : GLCameraOutput

- (BOOL)startRecordingWithURL:(NSURL *)videoURL error:(NSError **)error;
- (void)stopRecordingWithBlock:(void(^)(void))block;

@end
