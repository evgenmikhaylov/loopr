//
//  GLStillCamera+SimulatorSupport.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 4/1/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLStillCamera+SimulatorSupport.h"

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@implementation GLStillCamera (SimulatorSupport)

#if TARGET_IPHONE_SIMULATOR

+ (void)exchangeInstanceMethod:(SEL)originMethod new:(SEL)newMethod
{
    Method _origMethod = class_getInstanceMethod([self class], originMethod);
    Method _newMethod = class_getInstanceMethod([self class], newMethod);
    method_exchangeImplementations(_origMethod, _newMethod);
}

+ (void)load
{
    /* Exchange -captureImageWithBlock: */
    [self exchangeInstanceMethod:@selector(captureImageWithBlock:) new:@selector(_captureImageWithBlock:)];
}

- (void)_captureImageWithBlock:(void(^)(BOOL completed, NSData* imageData, NSError* error))block
{
    CGRect bounds = CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
    
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, 0.0f);
    
    [[UIColor colorWithRed:(arc4random() % 255) / 255.0f green:(arc4random() % 255) / 255.0f blue:(arc4random() % 255) / 255.0f alpha:1.0f] set];
    UIRectFill(bounds);
    
    [[UIColor colorWithRed:(arc4random() % 255) / 255.0f green:(arc4random() % 255) / 255.0f blue:(arc4random() % 255) / 255.0f alpha:1.0f] set];
    UIRectFill(CGRectInset(bounds, 20.0f, 20.0f));
    
    [[UIColor colorWithRed:(arc4random() % 255) / 255.0f green:(arc4random() % 255) / 255.0f blue:(arc4random() % 255) / 255.0f alpha:1.0f] set];
    CGContextAddEllipseInRect(UIGraphicsGetCurrentContext(), CGRectInset(bounds, 60.0f, 60.0f));
    CGContextFillPath(UIGraphicsGetCurrentContext());
    
    NSData* data = UIImageJPEGRepresentation(UIGraphicsGetImageFromCurrentImageContext(), 0.7f);
    UIGraphicsEndImageContext();
    
    block(YES,data,nil);
}

#endif

@end
