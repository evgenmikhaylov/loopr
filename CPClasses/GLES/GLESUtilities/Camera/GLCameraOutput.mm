//
//  GLCamera.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLCameraOutput.h"
#import "GLCameraOutputProgram.h"
#import "GLContext.h"
#import "GLFrameBuffer.h"
#import "GLLog.h"
#import "GLProgram.h"
#import "GLProgramCache.h"
#import "GLStateCache.h"
#import "GLVertexArray.h"
#include <sys/sysctl.h>
#include <sys/utsname.h>

NSInteger const GLCameraOutputCantAddInputDeviceErrorCode = 100;
NSInteger const GLCameraOutputFailedInitializeAVCaptureSessionErrorCode = 101;

NS_INLINE CGRect GLCameraOutputRectByCalculateAspectOfSizes(CGSize outSize, CGSize inputSize)
{
    CGFloat aspect = MAX(outSize.width / inputSize.width, outSize.height / inputSize.height);
    inputSize.width *= aspect;
    inputSize.height *= aspect;
    return CGRectMake(-(inputSize.width - outSize.width), -(inputSize.height - outSize.height), outSize.width, outSize.height);
}

NS_INLINE NSString* GLCameraOutputPlatformString()
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char* machine = (char *)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString* platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}


@interface GLCameraOutput ()
{
    CVOpenGLESTextureRef lumaTexture;
    CVOpenGLESTextureRef chromaTexture;
    
    CGSize outputSize;
    
    BOOL isPaused;
    BOOL supportsFullYUVRange;
}

@property (nonatomic) GLCameraOutputProgram* cameraOutputProgram;
@property (nonatomic) GLFrameBuffer* fbo;
@property (nonatomic) GLVertexArray* vertexArray;

@property (nonatomic) AVCaptureSession* session;
@property (nonatomic) AVCaptureDevice* captureDevice;
@property (nonatomic) AVCaptureDeviceInput* captureDeviceInput;

@end

@implementation GLCameraOutput

+ (AVAuthorizationStatus)authorizationStatus
{
    return [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
}

+ (void)requestAccessWithCompletionHandler:(void (^)(BOOL granted))handler
{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:handler];
}

+ (instancetype)cameraWithSize:(CGSize)size error:(NSError **)error
{
    return [[self alloc] initWithSize:size error:error];
}

- (instancetype)initWithSize:(CGSize)size error:(NSError **)error
{
    self = [super init];
    if (self)
    {
        self.scale = 1.0f;
        self.size = size;
        
        NSError* initializeSessionError = nil;
        
        if ([self initializeSession:&initializeSessionError])
        {
            _isInitialize = YES;
            
            [GLContext perfromBlockWithLockContext:^()
             {
                 self.cameraOutputProgram = [GLCameraOutputProgram programWithSupportsFullYUVRange:supportsFullYUVRange];
             }];
        }
        else
        {
            _isInitialize = NO;
            
            GLLog(GLLogVerboseLevelError, @"GLCameraOutput::Error = %@",initializeSessionError);
            
            if (error != NULL)
            {
                *error = initializeSessionError;
            }
        }
    }
    return self;
}

- (void)dealloc
{
    [self tearDownSession];
}

#pragma mark -
#pragma mark Setters / Getters

- (void)setSize:(CGSize)size
{
    if (!CGSizeEqualToSize(size, self.size) && !CGSizeEqualToSize(size, CGSizeZero))
    {
        _size = size;
        
        self.fbo = [GLFrameBuffer frameBufferWithFormat:GLFBOPixelFormatRGB565 andSize:size];
        [self setupVAO];
    }
}

- (void)setScale:(CGFloat)scale
{
    _scale = MAX(MIN(scale,2.0f),1.0f); // clampf
}

- (GLint)texHandle
{
    return self.fbo.texHandle;
}

#pragma mark -

- (void)cleanUpTextures
{
    [GLContext lock];
    
    if (lumaTexture)
    {
        CFRelease(lumaTexture);
        lumaTexture = NULL;
    }
    
    if (chromaTexture)
    {
        CFRelease(chromaTexture);
        chromaTexture = NULL;
    }
    
    // Periodic texture cache flush every frame
    CVOpenGLESTextureCacheFlush([GLContext videoTextureCache], 0);
    
    [GLContext unlock];
}

- (void)setupVAO
{
    if (CGSizeEqualToSize(self.size, CGSizeZero) || CGSizeEqualToSize(outputSize, CGSizeZero))
    {
        return;
    }
    
    CGRect rect = GLCameraOutputRectByCalculateAspectOfSizes(self.size, outputSize);
    GLfloat aspectX = (rect.size.width / self.size.width) - (rect.origin.x / self.size.width);
    GLfloat aspectY = (rect.size.height / self.size.height) - (rect.origin.y / self.size.height);
    
    self.vertexArray = [GLVertexArray vertexArrayWithDataSize:sizeof(GLfloat) * 16];
    [GLContext perfromBlockWithLockContext:^()
     {
         [self.vertexArray mapVertexDataWithBlock:^(void *data)
          {
              GLfloat* p = (GLfloat *)data;
              p[0] = -aspectX;   p[1] = aspectY;
              p[2] = 0.0f;       p[3] = 1.0f;
              p[4] = aspectX;    p[5] = aspectY;
              p[6] = 0.0f;       p[7] = 0.0f;
              p[8] = -aspectX;   p[9] = -aspectY;
              p[10] = 1.0f;      p[11] = 1.0f;
              p[12] = aspectX;   p[13] = -aspectY;
              p[14] = 1.0f;      p[15] = 0.0f;
          }];
     }];
    [self.vertexArray setupVertexAttrib:self.cameraOutputProgram.attribPosition size:2 stride:sizeof(float) * 4 offset:0];
    [self.vertexArray setupVertexAttrib:self.cameraOutputProgram.attribTexCoord size:2 stride:sizeof(float) * 4 offset:8];
}

#pragma mark -
#pragma mark Session

- (BOOL)initializeSession:(NSError **)error
{
    AVCaptureDevicePosition position = AVCaptureDevicePositionUnspecified;
    
    if ([self backCameraAvailable])
    {
        position = AVCaptureDevicePositionBack;
    }
    else if ([self frontCameraAvailable])
    {
        position = AVCaptureDevicePositionFront;
    }
    
    if (position != AVCaptureDevicePositionUnspecified)
    {
        NSError* captureDeviceError = nil;
        self.captureDevice = [self captureDeviceWithPosition:position];
        self.captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:&captureDeviceError];
        
        if (self.captureDeviceInput)
        {
            self.session = [[AVCaptureSession alloc] init];
            self.session.sessionPreset = AVCaptureSessionPresetPhoto;
            
            if ([self.session canAddInput:self.captureDeviceInput])
            {
                [self.session addInput:self.captureDeviceInput];
                
                AVCaptureVideoDataOutput* output = [[AVCaptureVideoDataOutput alloc] init];
                output.alwaysDiscardsLateVideoFrames = NO;
                
                for (NSNumber* currentPixelFormat in output.availableVideoCVPixelFormatTypes)
                {
                    if ([currentPixelFormat intValue] == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
                    {
                        supportsFullYUVRange = YES;
                    }
                }
                
                if (supportsFullYUVRange)
                {
                    output.videoSettings = @{(id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)};
                }
                else
                {
                    output.videoSettings = @{(id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)};
                }
                
                // For iPhone 4 needs use main queue
                if ([GLCameraOutputPlatformString() rangeOfString:@"iPhone3,"].location != NSNotFound)
                {
                    [output setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
                }
                else
                {
                    [output setSampleBufferDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
                }
                
                [self.session addOutput:output];
                return YES;
            }
            else
            {
                self.session = nil;
                
                if (error != NULL)
                {
                    *error = [NSError errorWithDomain:@"GLCameraOutput" code:GLCameraOutputCantAddInputDeviceErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Can't add device input"}];
                }
                return NO;
            }
        }
        else
        {
            if (error != NULL)
            {
                *error = captureDeviceError;
            }
            return NO;
        }
    }
    else
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLCameraOutput" code:GLCameraOutputFailedInitializeAVCaptureSessionErrorCode userInfo:@{NSLocalizedDescriptionKey:@"Failed initialize AVCaptureSession"}];
        }
        return NO;
    }
}

- (void)tearDownSession
{
    [self stopRunning];
    [self cleanUpTextures];
    
    [[self.session.inputs copy] enumerateObjectsUsingBlock:^(AVCaptureInput* input, NSUInteger index, BOOL *stop)
     {
         [self.session removeInput:input];
     }];
    
    [[self.session.outputs copy] enumerateObjectsUsingBlock:^(AVCaptureOutput* output, NSUInteger index, BOOL *stop)
     {
         [self.session removeOutput:output];
     }];
    
    self.session = nil;
}

- (void)startRunning
{
    if (self.session && !self.session.isRunning)
    {
        GLLog(GLLogVerboseLevelWarning, @"GLCameraOutput::StartRunning");
        isPaused = NO;
        [self.session startRunning];
    }
}

- (void)stopRunning
{
    if (self.session && self.session.isRunning)
    {
        GLLog(GLLogVerboseLevelWarning, @"GLCameraOutput::StopRunning");
        [self.session stopRunning];
        [self cleanUpTextures];
    }
}

- (BOOL)isRunning
{
    return self.session.isRunning;
}

- (void)pause
{
    isPaused = YES;
}

- (void)resume
{
    isPaused = NO;
}

#pragma mark -
#pragma mark Camera

- (BOOL)frontCameraAvailable
{
    static BOOL frontCameraAvailable = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        frontCameraAvailable = [self captureDeviceWithPosition:AVCaptureDevicePositionFront] != nil;
    });
    return frontCameraAvailable;
}

- (BOOL)backCameraAvailable
{
    static BOOL backCameraAvailable = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        backCameraAvailable = [self captureDeviceWithPosition:AVCaptureDevicePositionBack] != nil;
    });
    return backCameraAvailable;
}

- (BOOL)canFlipCamera
{
    return ([self frontCameraAvailable] && [self backCameraAvailable]);
}

- (AVCaptureDevicePosition)flipCamera
{
    if ([self canFlipCamera])
    {
        BOOL pause = isPaused;
        if (!pause)
        {
            [self pause];
        }
        
        AVCaptureDevicePosition position = (self.captureDevice.position == AVCaptureDevicePositionBack)?(AVCaptureDevicePositionFront):(AVCaptureDevicePositionBack);
        AVCaptureDevice* captureDevice = [self captureDeviceWithPosition:position];
        AVCaptureDeviceInput* captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
        
        [self.session beginConfiguration];
        [self.session removeInput:self.captureDeviceInput];
        
        if ([self.session canAddInput:captureDeviceInput])
        {
            [self.session addInput:captureDeviceInput];
            self.captureDevice = captureDevice;
            self.captureDeviceInput = captureDeviceInput;
        }
        else
        {
            [self.session addInput:self.captureDeviceInput];
        }
        
        [self.session commitConfiguration];
        
        if (!pause)
        {
            [self resume];
        }
        
        return position;
    }
    else
    {
        return self.captureDevice.position;
    }
}

- (AVCaptureDevicePosition)captureDevicePosition
{
    return self.captureDevice.position;
}

- (AVCaptureDevice *)captureDeviceWithPosition:(AVCaptureDevicePosition)position
{
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice* device in devices)
    {
        if ([device position] == position)
        {
            return device;
        }
    }
    return nil;
}

- (BOOL)canSetFocus
{
    return ([self.captureDevice isFocusPointOfInterestSupported] && [self.captureDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus]);
}

- (BOOL)setFocusAtPoint:(CGPoint)point
{
    if ([self canSetFocus])
    {
        if ([self.captureDevice lockForConfiguration:nil])
        {
            [self.captureDevice setFocusPointOfInterest:CGPointMake(point.x / self.scale + (self.scale - 1.0f) / (self.scale * 2.0), point.y / self.scale + (self.scale - 1.0f) / (self.scale * 2.0))];
            [self.captureDevice setFocusMode:AVCaptureFocusModeAutoFocus];
            [self.captureDevice unlockForConfiguration];
            
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)canSetExposureWithCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode
{
    return ([self.captureDevice isExposurePointOfInterestSupported] && [self.captureDevice isExposureModeSupported:captureExposureMode]);
}

- (BOOL)setExposureAtPoint:(CGPoint)point withCaptureExposureMode:(AVCaptureExposureMode)captureExposureMode
{
    if ([self canSetExposureWithCaptureExposureMode:captureExposureMode])
    {
        if ([self.captureDevice lockForConfiguration:nil])
        {
            [self.captureDevice setExposurePointOfInterest:CGPointMake(point.x / self.scale + (self.scale - 1.0f) / (self.scale * 2.0), point.y / self.scale + (self.scale - 1.0f) / (self.scale * 2.0))];
            [self.captureDevice setExposureMode:captureExposureMode];
            [self.captureDevice unlockForConfiguration];
            
            return YES;
        }
    }
    
    return NO;
}

#pragma mark -
#pragma mark Binding

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit
{
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, self.fbo.texHandle);
    [program setUniform1i:uniform value:unit - GL_TEXTURE0];
}

#pragma mark -
#pragma mark Draw

- (void)drawImage
{
    if (self.vertexArray)
    {
        [self.fbo bind];
        [GLStateCache useGLProgram:self.cameraOutputProgram];
        
        [GLStateCache setViewportSize:self.size];
        
        [self.cameraOutputProgram setUniform1f:self.cameraOutputProgram.uniformScale value:self.scale];
        
        [self.cameraOutputProgram setUniform1i:self.cameraOutputProgram.uniformSamplerY value:0];
        [self.cameraOutputProgram setUniform1i:self.cameraOutputProgram.uniformSamplerUV value:1];
        
        [self.vertexArray bind];
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        [self.vertexArray unbind];
        
        [self.fbo unbind];
    }
    else
    {
        GLLog(GLLogVerboseLevelWarning, @"WARNING: call - %s without VAO!",__func__);
    }
}

#pragma mark -
#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if (self.session == nil || isPaused || CGSizeEqualToSize(self.size, CGSizeZero) || [GLCameraOutput authorizationStatus] != AVAuthorizationStatusAuthorized)
    {
        return;
    }
    
    CVReturn err;
    CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    size_t width = CVPixelBufferGetWidth(pixelBuffer);
    size_t height = CVPixelBufferGetHeight(pixelBuffer);
    
    if (outputSize.width != height || outputSize.height != width)
    {
        outputSize = CGSizeMake(height, width);
        GLLog(GLLogVerboseLevelWarning, @"outputSize = %@",NSStringFromCGSize(outputSize));
        [self setupVAO];
    }
    
    if (![GLContext videoTextureCache])
    {
        GLLog(GLLogVerboseLevelError, @"No video texture cache");
        return;
    }
    
    [self cleanUpTextures];
    
    [GLContext lock];
    
    // CVOpenGLESTextureCacheCreateTextureFromImage will create GLES texture
    // optimally from CVImageBufferRef.
    
    // Y-plane
    glActiveTexture(GL_TEXTURE0);
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       [GLContext videoTextureCache],
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_LUMINANCE,
                                                       (GLsizei)width,
                                                       (GLsizei)height,
                                                       GL_LUMINANCE,
                                                       GL_UNSIGNED_BYTE,
                                                       0,
                                                       &lumaTexture);
    if (err)
    {
        GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    
    glBindTexture(CVOpenGLESTextureGetTarget(lumaTexture), CVOpenGLESTextureGetName(lumaTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    // UV-plane
    glActiveTexture(GL_TEXTURE1);
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       [GLContext videoTextureCache],
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_LUMINANCE_ALPHA,
                                                       (GLsizei)width / 2,
                                                       (GLsizei)height / 2,
                                                       GL_LUMINANCE_ALPHA,
                                                       GL_UNSIGNED_BYTE,
                                                       1,
                                                       &chromaTexture);
    if (err)
    {
        GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    
    glBindTexture(CVOpenGLESTextureGetTarget(chromaTexture), CVOpenGLESTextureGetName(chromaTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    [self drawImage];
    
    [GLContext unlock];
}

@end
