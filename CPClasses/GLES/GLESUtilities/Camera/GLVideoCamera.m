//
//  GLVideoCamera.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLVideoCamera.h"
#import "GLLog.h"

#import <AVFoundation/AVFoundation.h>

@interface GLCameraOutput (Private)

@property (nonatomic) AVCaptureSession* session;

@end

@interface GLVideoCamera ()
{
    BOOL isRecording;
}

@property (nonatomic) NSURL* videoURL;
@property (nonatomic) AVAssetWriter* assetWriter;
@property (nonatomic) AVAssetWriterInput* assetWriterVideoInput;
@property (nonatomic) AVAssetWriterInputPixelBufferAdaptor* assetWriterPixelBufferInput;

@end

@implementation GLVideoCamera

- (BOOL)startRecordingWithURL:(NSURL *)videoURL error:(NSError **)error
{
    if (self.session && !isRecording)
    {
        [[NSFileManager defaultManager] removeItemAtURL:videoURL error:nil];
        
        self.videoURL = videoURL;
        
        self.assetWriter = [AVAssetWriter assetWriterWithURL:videoURL fileType:AVFileTypeQuickTimeMovie error:nil];
        self.assetWriter.movieFragmentInterval = CMTimeMakeWithSeconds(1.0, 1000);
        
        NSDictionary* settings = @{
                                   AVVideoCodecKey:AVVideoCodecH264,
                                   AVVideoWidthKey:@(640.0f),
                                   AVVideoHeightKey:@(640.0f)
                                   };
        
        self.assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:settings];
        self.assetWriterVideoInput.expectsMediaDataInRealTime = YES;
        
        // You need to use BGRA for the video in order to get realtime encoding. I use a color-swizzling shader to line up glReadPixels' normal RGBA output with the movie input's BGRA.
        NSDictionary* sourcePixelBufferAttributesDictionary = @{
                                                                (id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_32BGRA),
                                                                (id)kCVPixelBufferWidthKey:@(640.0f),
                                                                (id)kCVPixelBufferHeightKey:@(640.0f)
                                                                };
        
        self.assetWriterPixelBufferInput = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.assetWriterVideoInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
        
        [self.assetWriter addInput:self.assetWriterVideoInput];
        [self.assetWriter startWriting];
        [self.assetWriter startSessionAtSourceTime:kCMTimeZero];
        
        isRecording = YES;
        
        return YES;
    }
    else
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:@"GLVideoCameraDomain" code:110 userInfo:@{NSLocalizedDescriptionKey:@"Failed to start recording!"}];
        }
        return NO;
    }
}

- (void)stopRecordingWithBlock:(void(^)(void))block
{
    if (isRecording)
    {
        isRecording = NO;
        [self.assetWriterVideoInput markAsFinished];
        [self.assetWriter finishWritingWithCompletionHandler:^{
            block();
        }];
    }
    else
    {
        block();
    }
}

#pragma mark -
#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    [super captureOutput:captureOutput didOutputSampleBuffer:sampleBuffer fromConnection:connection];
    
    if (isRecording)
    {
        static int frame = 0;
        
        CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        BOOL append = [self.assetWriterPixelBufferInput appendPixelBuffer:pixelBuffer withPresentationTime:CMTimeMake(frame++, 30)];
        
        GLLog(GLLogVerboseLevelWarning, @"frame = %d, append = %d",frame,append);
    }
}

@end
