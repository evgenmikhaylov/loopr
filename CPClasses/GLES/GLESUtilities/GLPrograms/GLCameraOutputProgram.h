//
//  VideoStreamProgram.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 4/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLProgram.h"

@interface GLCameraOutputProgram : GLProgram

@property (nonatomic,readonly) GLuint attribPosition, attribTexCoord;
@property (nonatomic,readonly) GLuint uniformScale, uniformSamplerY, uniformSamplerUV;

+ (instancetype)programWithSupportsFullYUVRange:(BOOL)programWithSupportsFullYUVRange;

@end
