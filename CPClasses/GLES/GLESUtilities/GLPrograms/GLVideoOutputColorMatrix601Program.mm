//
//  VideoOutputColorMatrix601Program.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#import "GLVideoOutputColorMatrix601Program.h"

NSString* GLVideoOutputColorMatrix601ProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 inputTextureCoordinate;
 
 varying vec2 textureCoordinate;
 
 void main()
 {
     gl_Position = position;
     textureCoordinate = inputTextureCoordinate;
 }
 );

NSString* GLVideoOutputColorMatrix601ProgramFragmentShader = SHADER_STRING
(
 uniform sampler2D SamplerY;
 uniform sampler2D SamplerUV;
 
 varying highp vec2 textureCoordinate;
 
 const mediump mat3 convertMatrix = mat3(
                                         1.164,  1.164, 1.164,
                                         0.0, -0.392, 2.017,
                                         1.596, -0.813,   0.0
                                         );
 
 void main()
 {
     mediump vec3 yuv;
     lowp vec3 rgb;
     
     // Subtract constants to map the video range start at 0
     yuv.x = texture2D(SamplerY, textureCoordinate).r - (16.0/255.0);
     yuv.yz = texture2D(SamplerUV, textureCoordinate).rg - vec2(0.5, 0.5);
     
     rgb = convertMatrix * yuv;
     
     gl_FragColor = vec4(rgb, 1.0);
 }
 );

@implementation GLVideoOutputColorMatrix601Program
{
    GLuint attribPosition, attribTexCoord;
    GLuint uniformSamplerY, uniformSamplerUV;
}

+ (instancetype)program
{
    GLVideoOutputColorMatrix601Program* program = [[self alloc] initWithVertexShaderString:GLVideoOutputColorMatrix601ProgramVertexShader fragmentShaderString:GLVideoOutputColorMatrix601ProgramFragmentShader];
    [program addAttribute:@"position"];
    [program addAttribute:@"inputTextureCoordinate"];
    if ([program link])
    {
        program->uniformSamplerY = [program uniformIndex:@"SamplerY"];
        program->uniformSamplerUV = [program uniformIndex:@"SamplerUV"];
        
        program->attribPosition = [program attributeIndex:@"position"];
        program->attribTexCoord = [program attributeIndex:@"inputTextureCoordinate"];
        
        glEnableVertexAttribArray(program->attribPosition);
        glEnableVertexAttribArray(program->attribTexCoord);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    return program;
}

#pragma mark -
#pragma mark GLVideoOutputProgramProtocol

- (GLuint)attribPosition
{
    return attribPosition;
}

- (GLuint)attribTexCoord
{
    return attribTexCoord;
}

- (GLuint)uniformSamplerY
{
    return uniformSamplerY;
}

- (GLuint)uniformSamplerUV
{
    return uniformSamplerUV;
}

@end
