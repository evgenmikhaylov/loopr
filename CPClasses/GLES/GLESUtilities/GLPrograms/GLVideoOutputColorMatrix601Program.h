//
//  VideoOutputColorMatrix601Program.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#import "GLProgram.h"
#import "GLVideoOutputProgramProtocol.h"

@interface GLVideoOutputColorMatrix601Program : GLProgram <GLVideoOutputProgramProtocol>

+ (instancetype)program;

@end
