//
//  GLVertexBuffer.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

@interface GLVertexBuffer : NSObject

@property (nonatomic,readonly) GLuint vboHandle;
@property (nonatomic,readonly) NSUInteger dataSize;

+ (instancetype)vertexBufferWithDataSize:(NSUInteger)size;

- (void)bind;
- (void)unbind;

- (BOOL)mapVertexDataWithBlock:(void(^)(void* data))block;
- (BOOL)lockGLContextAndMapVertexDataWithBlock:(void(^)(void* data))block;

@end


@interface GLVertexBuffer (Deprecated)

+ (instancetype)vertexBuffer __attribute__ ((deprecated("Use [GLVertexBuffer vertexBufferWithDataSize:] instead")));
- (instancetype)init __attribute__ ((deprecated("Use [GLVertexBuffer vertexBufferWithDataSize:] instead")));

- (void)setupVertexDataWithSize:(GLsizei)size data:(void *)data freeWhenDone:(BOOL)freeWhenDone __attribute__ ((deprecated("Use [GLVertexBuffer vertexBufferWithDataSize:] instead")));

@end
