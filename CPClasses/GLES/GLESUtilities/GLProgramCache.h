//
//  GLProgramCache.h
//  SplitPic
//
//  Created by Andrey Konoplyankin on 6/9/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLProgramCache : NSObject

+ (id)programWithClass:(id)className;

@end
