//
//  GLFrameBuffer.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/25/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLFrameBuffer.h"
#import "GLContext.h"
#import "GLLog.h"
#import "GLStateCache.h"
#import "GLProgram.h"
#import "GLTexture.h"

typedef NS_ENUM(NSUInteger, GLFrameBufferFormat)
{
    GLFrameBufferFormatDefault = 0,
    GLFrameBufferFormatWithGLTexture
};

@interface GLFrameBuffer ()
{
    GLFrameBufferFormat frameBufferFormat;
    GLFBOPixelFormat pixelFormat;
    BOOL isInitialized;
}

@end

@implementation GLFrameBuffer

+ (instancetype)frameBufferWithFormat:(GLFBOPixelFormat)format andSize:(CGSize)size
{
    return [[self alloc] initWithPixelFormat:format andSize:size];
}

+ (instancetype)frameBufferWithGLTexture:(GLTexture *)texture
{
    return [[self alloc] initWithGLTexture:texture];
}

- (id)initWithPixelFormat:(GLFBOPixelFormat)format andSize:(CGSize)size
{
    self = [super init];
    if (self)
    {
        NSAssert(size.width != 0.0f && size.height != 0.0f, @"Width and height cannot be 0!");
        NSAssert(format == GLFBOPixelFormatRGB565 || format == GLFBOPixelFormatRGBA8888 || format == GLFBOPixelFormatR8, @"Unsupported pixel format!");
        
        frameBufferFormat = GLFrameBufferFormatDefault;
        pixelFormat = format;
        _size = [GLContext sizeThatFitInMaxSupportSize:size];
        
        if (pixelFormat == GLFBOPixelFormatR8 && ![GLContext supportExtensionTextureRG])
        {
            GLLog(GLLogVerboseLevelWarning, @"WARNING: R8 pixel format unsupported, will be use RGB565");
            pixelFormat = GLFBOPixelFormatRGB565;
        }
        
        [self createFBO];
    }
    return self;
}

- (id)initWithGLTexture:(GLTexture *)texture
{
    self = [super init];
    if (self)
    {
        NSAssert(texture != nil, @"Pass nil");
        
        frameBufferFormat = GLFrameBufferFormatWithGLTexture;
        _size = texture.size;
        _texHandle = texture.texHandle;
        
        [GLContext lock];
        
        glGenFramebuffers(1, &_handle);
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:self.handle];
        
        glBindTexture(GL_TEXTURE_2D, texture.texHandle);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.texHandle, 0);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
        {
            isInitialized = YES;
            GLLog(GLLogVerboseLevelWarning, @"GLFramebuffer::FBO Created with GLTexture - %@", texture);
        }
        else
        {
            GLLog(GLLogVerboseLevelError, @"GLFramebuffer::Failed Create FBO");
        }
        
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
        glBindTexture(GL_TEXTURE_2D, 0);
        
        [GLContext unlock];
    }
    return self;
}

- (void)dealloc
{
    [self destroyFBO];
}

- (NSString *)pixelFormatString
{
    if (frameBufferFormat == GLFrameBufferFormatWithGLTexture)
    {
        return @"GLTexture";
    }
    else
    {
        switch (pixelFormat)
        {
            case GLFBOPixelFormatRGB565:
                return @"RGB565";
            case GLFBOPixelFormatRGBA8888:
                return @"RGBA8888";
            case GLFBOPixelFormatR8:
                return @"R8";
            default:
                return @"Unknown";
        }
    }
}

- (NSString *)description
{
    return [[super description] stringByAppendingFormat:@" %@, handle = %d, texHandle = %d, size = {%0.2f,%0.2f}",
            [self pixelFormatString],
            self.handle,
            self.texHandle,
            self.size.width,
            self.size.height];
}

- (void)bind
{
    if (isInitialized)
    {
        [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:self.handle];
    }
    else
    {
#if DEBUG
        [NSException raise:NSInternalInconsistencyException format:@"Perform -bind method without fbo!"];
#endif
    }
}

- (void)unbind
{
    [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
}

- (void)bindTextureToGLProgram:(GLProgram *)program uniform:(GLuint)uniform textureUnit:(GLenum)unit
{
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, self.texHandle);
    [program setUniform1i:uniform value:unit - GL_TEXTURE0];
}

- (void)createFBO
{
    [GLContext lock];
    
    glGenFramebuffers(1, &_handle);
    glGenTextures(1, &_texHandle);
    
    [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:self.handle];
    
    GLuint format = 0;
    GLuint type = 0;
    
    switch (pixelFormat)
    {
        case GLFBOPixelFormatRGBA8888:
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        case GLFBOPixelFormatRGB565:
            format = GL_RGB;
            type = GL_UNSIGNED_SHORT_5_6_5;
            break;
        case GLFBOPixelFormatR8:
            format = GL_RED_EXT;
            type = GL_UNSIGNED_BYTE;
            break;
    }
    
    glBindTexture(GL_TEXTURE_2D, _texHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, format, _size.width, _size.height, 0, format, type, NULL);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texHandle, 0);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
    {
        isInitialized = YES;
        GLLog(GLLogVerboseLevelWarning, @"GLFramebuffer::FBO Created - %@, size - {%0.2f,%0.2f}",[self pixelFormatString],_size.width,_size.height);
    }
    else
    {
        GLLog(GLLogVerboseLevelError, @"GLFramebuffer::Failed Create FBO");
    }
    
    [GLStateCache bindFramebuffer:GL_FRAMEBUFFER framebuffer:0];
    glBindTexture(GL_TEXTURE_2D, 0);
    
    [GLContext unlock];
}

- (void)destroyFBO
{
    if (isInitialized)
    {
        [GLContext lock];
        
        if (_handle)
        {
            glDeleteFramebuffers(1, &_handle);
            _handle = 0;
        }
        
        if (frameBufferFormat != GLFrameBufferFormatWithGLTexture && _texHandle)
        {
            glDeleteTextures(1, &_texHandle);
            _texHandle = 0;
        }
        
        GLLog(GLLogVerboseLevelWarning, @"GLFramebuffer::Delete FBO success");
        
        [GLContext unlock];
    }
}

void dataProviderReleaseCallback(void *info, const void *data, size_t size)
{
    free((void *)data);
}

- (void)imageFromFramebufferWithBlock:(void(^)(BOOL completed, UIImage* image))block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CGSize size = self.size;
        
        NSUInteger totalBytesForImage = (int)size.width * (int)size.height * 4;
        GLubyte* rawImagePixels = (GLubyte *)malloc(totalBytesForImage);
        
        [GLContext perfromBlockWithLockContext:^()
         {
             [self bind];
             glReadPixels(0, 0, (int)size.width, (int)size.height, GL_RGBA, GL_UNSIGNED_BYTE, rawImagePixels);
             [self unbind];
         }];
        
        CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rawImagePixels, totalBytesForImage, dataProviderReleaseCallback);
        CGColorSpaceRef defaultRGBColorSpace = CGColorSpaceCreateDeviceRGB();
        
        CGImageRef cgImageFromBytes = CGImageCreate((int)size.width, (int)size.height, 8, 32, 4 * (int)size.width, defaultRGBColorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaLast, dataProvider, NULL, NO, kCGRenderingIntentDefault);
        
        UIImage* image = [UIImage imageWithCGImage:cgImageFromBytes];
        
        CGDataProviderRelease(dataProvider);
        CGColorSpaceRelease(defaultRGBColorSpace);
        CGImageRelease(cgImageFromBytes);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            block(YES,image);
        });
    });
}

#if DEBUG
- (id)debugQuickLookObject
{
    [GLContext setContext];
    
    CGSize size = self.size;
    
    NSUInteger totalBytesForImage = (int)size.width * (int)size.height * 4;
    GLubyte* rawImagePixels = (GLubyte *)malloc(totalBytesForImage);
    
    [self bind];
    glReadPixels(0, 0, (int)size.width, (int)size.height, GL_RGBA, GL_UNSIGNED_BYTE, rawImagePixels);
    [self unbind];
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rawImagePixels, totalBytesForImage, dataProviderReleaseCallback);
    CGColorSpaceRef defaultRGBColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGImageRef cgImageFromBytes = CGImageCreate((int)size.width, (int)size.height, 8, 32, 4 * (int)size.width, defaultRGBColorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaLast, dataProvider, NULL, NO, kCGRenderingIntentDefault);
    
    UIImage* image = [UIImage imageWithCGImage:cgImageFromBytes scale:1.0f orientation:UIImageOrientationUp];
    
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(defaultRGBColorSpace);
    CGImageRelease(cgImageFromBytes);
    
    return image;
}
#endif

@end
