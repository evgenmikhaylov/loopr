//
//  GLLog.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 11/12/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/EAGL.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(GLint, GLLogVerboseLevel)
{
    GLLogVerboseLevelNothing = 0,
    GLLogVerboseLevelError,
    GLLogVerboseLevelWarning
};

#if __cplusplus
extern "C" {
#endif
    
    void GLLogSetVerboseLevel(GLLogVerboseLevel level);
    void GLLog(GLLogVerboseLevel level, NSString *format, ...);
    
#if __cplusplus
}
#endif
