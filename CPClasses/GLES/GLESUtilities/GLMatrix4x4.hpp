//
//  GLMatrix4x4.h
//  SPApp
//
//  Created by Andrey Konoplyankin on 5/30/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#ifndef __SPAPP__GLMATRIX4X4___
#define __SPAPP__GLMATRIX4X4___

class GLMatrix4x4
{
    float m[16] __attribute__((aligned(16)));
    
public:
    
    GLMatrix4x4();
    GLMatrix4x4(const GLMatrix4x4& right);
    ~GLMatrix4x4();
    
    static GLMatrix4x4 Ortho(float left, float right, float bottom, float top, float nearZ = -1.0f, float farZ = 1.0f);
    static GLMatrix4x4 Frustum(float left, float right, float bottom, float top, float nearZ = -1.0f, float farZ = 1.0f);
    static GLMatrix4x4 Perspective(float fovyRadians, float aspect, float nearZ = -1.0f, float farZ = 1.0f);
    
    static GLMatrix4x4 LookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ);
    
    static GLMatrix4x4 Translate(float x, float y = 0.0f, float z = 0.0f);
    
    static GLMatrix4x4 RotationX(float radians);
    static GLMatrix4x4 RotationY(float radians);
    static GLMatrix4x4 RotationZ(float radians);
    
    static GLMatrix4x4 Scale(float scale);
    static GLMatrix4x4 Scale(float x, float y, float z = 1.0f);
    
    void setFOV(float fov);
    const float* pointer() const;
    
    GLMatrix4x4& operator*= (const GLMatrix4x4& right);
    GLMatrix4x4& operator* (const GLMatrix4x4& right);
    GLMatrix4x4& operator= (const GLMatrix4x4& right);
    bool operator== (const GLMatrix4x4& right);
    bool operator!= (const GLMatrix4x4& right);
};

#endif /* defined(__SPAPP__GLMATRIX4X4___) */
