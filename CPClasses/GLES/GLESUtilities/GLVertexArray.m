//
//  GLVertexArray.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 5/8/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLVertexArray.h"
#import "GLContext.h"

@implementation GLVertexArray

+ (instancetype)vertexArrayWithDataSize:(NSUInteger)size
{
    return [[self alloc] initWithDataSize:size];
}

- (instancetype)initWithDataSize:(NSUInteger)size
{
    self = [super init];
    if (self)
    {
        _dataSize = size;
        
        NSAssert(size != 0, @"Data size must be > 0");
        [self createVAOWithDataSize:size];
    }
    return self;
}

- (void)dealloc
{
    [self deleteVAO];
}

#pragma mark -
#pragma mark Create/Delete VAO

- (void)createVAOWithDataSize:(NSUInteger)size
{
    void* data = calloc(size, 1);
    NSAssert(data, @"Failed allocate memory for size %zd", size);
    
    [GLContext perfromBlockWithLockContext:^
     {
         glGenVertexArraysOES(1, &_vaoHandle);
         glGenBuffers(1, &_vboHandle);
         
         NSAssert(_vaoHandle != 0 || _vboHandle != 0, @"Failed create VBO and VAO");
         
         glBindVertexArrayOES(_vaoHandle);
         glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
         glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         glBindVertexArrayOES(0);
     }];
    
    free(data);
}

- (void)deleteVAO
{
    [GLContext perfromBlockWithLockContext:^()
     {
         if (_vaoHandle)
         {
             glDeleteVertexArraysOES(1, &_vaoHandle);
             _vaoHandle = 0;
         }
         
         if (_vboHandle)
         {
             glDeleteBuffers(1, &_vboHandle);
             _vboHandle = 0;
         }
     }];
}

#pragma mark -
#pragma mark Bind/Unbind

- (void)bind
{
    glBindVertexArrayOES(_vaoHandle);
}

- (void)unbind
{
    glBindVertexArrayOES(0);
}

#pragma mark -
#pragma mark Setup Data

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size stride:(GLuint)stride offset:(GLuint)offset
{
    [GLContext perfromBlockWithLockContext:^()
     {
         [self bind];
         
         glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
         glVertexAttribPointer(attrib, size, GL_FLOAT, GL_FALSE, stride, (void *)offset);
         glEnableVertexAttribArray(attrib);
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         [self unbind];
     }];
}

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size
{
    [self setupVertexAttrib:attrib size:size stride:0 offset:0];
}

#pragma mark -
#pragma mark Map Data

- (BOOL)mapVertexDataWithBlock:(void(^)(void* data))block
{
    BOOL result = NO;
    glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
    
    void* data = glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
    if (data)
    {
        block(data);
        glUnmapBufferOES(GL_ARRAY_BUFFER);
        result = YES;
    }
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return result;
}

- (BOOL)lockGLContextAndMapVertexDataWithBlock:(void(^)(void* data))block
{
    __block BOOL result = NO;
    [GLContext perfromBlockWithLockContext:^()
     {
         result = [self mapVertexDataWithBlock:block];
     }];
    return result;
}

#pragma mark -

- (NSString *)description
{
    [GLContext setContext];
    
    NSMutableString* string = [NSMutableString stringWithFormat:@"<%@: %p> vboHandle = %zd, vaoHandle = %zd, size = %zd, data:\n", NSStringFromClass([self class]), self, self.vboHandle, self.vaoHandle, self.dataSize];
    
    void (^block)() = ^(void *data)
    {
        for (NSInteger i = 0; i < self.dataSize / sizeof(float); i += 2)
        {
            float* p = (float *)data + i;
            [string appendFormat:@"{%.2f, %.2f}\n", p[0], p[1]];
        }
    };
    
    if (![self mapVertexDataWithBlock:block])
    {
        [string appendString:@"Failed to map buffer!"];
    }
    
    return [NSString stringWithString:string];
}

@end


@implementation GLVertexArray (Deprecated)

+ (instancetype)vertexArray
{
    return [[self alloc] init];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self createVAO];
    }
    return self;
}

#pragma mark -
#pragma mark Create VAO

- (void)createVAO
{
    [GLContext perfromBlockWithLockContext:^()
     {
         glGenVertexArraysOES(1, &_vaoHandle);
         glGenBuffers(1, &_vboHandle);
     }];
}

#pragma mark -
#pragma mark Setup Vertex

- (void)setupVertexDataWithSize:(GLsizei)size data:(void *)data freeWhenDone:(BOOL)freeWhenDone
{
    _dataSize = size;
    
    [GLContext perfromBlockWithLockContext:^()
     {
         [self bind];
         
         glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
         glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         [self unbind];
     }];
    
    if (freeWhenDone)
    {
        free(data);
    }
}

- (void)setupVertexAttrib:(GLuint)attrib size:(GLuint)size stride:(GLuint)stride pointer:(void *)pointer
{
    [GLContext perfromBlockWithLockContext:^()
     {
         [self bind];
         
         glBindBuffer(GL_ARRAY_BUFFER, _vboHandle);
         glVertexAttribPointer(attrib, size, GL_FLOAT, GL_FALSE, stride, pointer);
         glEnableVertexAttribArray(attrib);
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         [self unbind];
     }];
}

@end
