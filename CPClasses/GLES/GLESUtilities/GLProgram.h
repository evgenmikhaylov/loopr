//  This is Jeff LaMarche's GLProgram OpenGL shader wrapper class from his OpenGL ES 2.0 book.
//  A description of this can be found at his page on the topic:
//  http://iphonedevelopment.blogspot.com/2010/11/opengl-es-20-for-ios-chapter-4.html
//  I've extended this to be able to take programs as NSStrings in addition to files, for baked-in shaders

#import <Foundation/Foundation.h>

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @ STRINGIZE2(text)

class GLMatrix4x4;
class GLVector3;

@interface GLProgram : NSObject 

@property(nonatomic, readonly, getter = isInitialized) BOOL initialized;

- (instancetype)initWithVertexShaderString:(NSString *)vShaderString fragmentShaderString:(NSString *)fShaderString;
- (instancetype)initWithVertexShaderFileName:(NSString *)vShaderFileName fragmentShaderFileName:(NSString *)fShaderFileName;

- (void)addAttribute:(NSString *)attributeName;
- (GLuint)attributeIndex:(NSString *)attributeName;
- (GLuint)uniformIndex:(NSString *)uniformName;

- (BOOL)link;
- (void)use;

- (void)validate;

- (void)setUniform1f:(GLint)location value:(GLfloat)value;
- (void)setUniform1i:(GLint)location value:(GLint)value;
- (void)setUniform2f:(GLint)location value:(CGPoint)value;
- (void)setUniform3f:(GLint)location value:(GLVector3)vector;
- (void)setUniformMatrix4x4:(GLint)location matrix:(const GLMatrix4x4 &)matrix;

@end
