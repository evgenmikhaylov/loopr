//  This is Jeff LaMarche's GLProgram OpenGL shader wrapper class from his OpenGL ES 2.0 book.
//  A description of this can be found at his page on the topic:
//  http://iphonedevelopment.blogspot.com/2010/11/opengl-es-20-for-ios-chapter-4.html


#import "GLProgram.h"
#import "GLLog.h"
#import "GLStateCache.h"
#import "GLContext.h"
#include "GLVector3.hpp"
#include "GLMatrix4x4.hpp"

typedef void (*GLInfoFunction)(GLuint program, GLenum pname, GLint* params);
typedef void (*GLLogFunction) (GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);

@interface GLProgram()
{
    NSMutableArray* attributes;
    NSMutableArray* uniforms;
    NSMutableDictionary* uniformsCache;
    GLuint program, vertShader, fragShader;
}

@end

@implementation GLProgram

- (instancetype)initWithVertexShaderString:(NSString *)vShaderString fragmentShaderString:(NSString *)fShaderString;
{
    self = [super init];
    if (self)
    {
        attributes = [NSMutableArray array];
        uniforms = [NSMutableArray array];
        uniformsCache = [NSMutableDictionary dictionary];
        
        program = glCreateProgram();
        
        if ([self compileShader:&vertShader type:GL_VERTEX_SHADER string:vShaderString] == NO)
        {
            GLLog(GLLogVerboseLevelError, @"Failed to compile vertex shader");
        }
        
        if ([self compileShader:&fragShader type:GL_FRAGMENT_SHADER string:fShaderString] == NO)
        {
            GLLog(GLLogVerboseLevelError, @"Failed to compile fragment shader");
        }
        
        glAttachShader(program, vertShader);
        glAttachShader(program, fragShader);
    }
    return self;
}

- (instancetype)initWithVertexShaderFileName:(NSString *)vShaderFileName fragmentShaderFileName:(NSString *)fShaderFileName;
{
    NSString* vertShaderPath = [[NSBundle mainBundle] pathForResource:vShaderFileName ofType:@"vsh"];
    NSString* vertexShaderString = [NSString stringWithContentsOfFile:vertShaderPath encoding:NSUTF8StringEncoding error:nil];
    
    NSString* fragShaderPath = [[NSBundle mainBundle] pathForResource:fShaderFileName ofType:@"fsh"];
    NSString* fragmentShaderString = [NSString stringWithContentsOfFile:fragShaderPath encoding:NSUTF8StringEncoding error:nil];
    
    self = [self initWithVertexShaderString:vertexShaderString fragmentShaderString:fragmentShaderString];
    if (self)
    {
        
    }
    return self;
}

- (void)dealloc
{
    [GLContext lock];
    
    if (vertShader)
    {
        glDeleteShader(vertShader);
    }
    
    if (fragShader)
    {
        glDeleteShader(fragShader);
    }
    
    if (program)
    {
        glDeleteProgram(program);
    }
    
    [GLContext unlock];
}

#pragma mark -
#pragma mark Compile

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type string:(NSString *)shaderString
{
    GLint status;
    const GLchar* source = (GLchar*)[shaderString UTF8String];
    
    if (!source)
    {
        GLLog(GLLogVerboseLevelError, @"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    
	if (status != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0)
		{
			GLchar *log = (GLchar *)malloc(logLength);
			glGetShaderInfoLog(*shader, logLength, &logLength, log);
			GLLog(GLLogVerboseLevelError, @"Shader compile log:\n%s", log);
			free(log);
		}
	}
    
    return status == GL_TRUE;
}

#pragma mark -
#pragma mark Attrib / Uniforms

- (void)addAttribute:(NSString *)attributeName
{
    if (![attributes containsObject:attributeName])
    {
        [attributes addObject:attributeName];
        glBindAttribLocation(program, (GLuint)[attributes indexOfObject:attributeName], [attributeName UTF8String]);
    }
}

- (GLuint)attributeIndex:(NSString *)attributeName
{
    return (GLuint)[attributes indexOfObject:attributeName];
}

- (GLuint)uniformIndex:(NSString *)uniformName
{
    return glGetUniformLocation(program, [uniformName UTF8String]);
}

#pragma mark -
#pragma mark Link & Use

- (BOOL)link
{
    GLint status;
    
    glLinkProgram(program);
    
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        return NO;
    }
    
    if (vertShader)
    {
        glDeleteShader(vertShader);
        vertShader = 0;
    }
    
    if (fragShader)
    {
        glDeleteShader(fragShader);
        fragShader = 0;
    }
    
    _initialized = YES;
    
    return YES;
}

- (void)use
{
    glUseProgram(program);
}

#pragma mark -
#pragma mark Log & Validate

- (NSString *)logForOpenGLObject:(GLuint)object infoCallback:(GLInfoFunction)infoFunc logFunc:(GLLogFunction)logFunc
{
    GLint logLength = 0, charsWritten = 0;
    
    infoFunc(object, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength < 1)
    {
        return nil;
    }
    
    GLchar* logBytes = (GLchar *)malloc(logLength);
    logFunc(object, logLength, &charsWritten, logBytes);
    NSString* log = [[NSString alloc] initWithBytes:logBytes length:logLength encoding:NSUTF8StringEncoding];
    free(logBytes);
    return log;
}

- (NSString *)vertexShaderLog
{
    return [self logForOpenGLObject:vertShader infoCallback:(GLInfoFunction)&glGetProgramiv logFunc:(GLLogFunction)&glGetProgramInfoLog];
}

- (NSString *)fragmentShaderLog
{
    return [self logForOpenGLObject:fragShader infoCallback:(GLInfoFunction)&glGetProgramiv logFunc:(GLLogFunction)&glGetProgramInfoLog];
}

- (NSString *)programLog
{
    return [self logForOpenGLObject:program infoCallback:(GLInfoFunction)&glGetProgramiv logFunc:(GLLogFunction)&glGetProgramInfoLog];
}

- (void)validate
{
	GLint logLength;
	
	glValidateProgram(program);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
	if (logLength > 0)
	{
		GLchar* log = (GLchar *)malloc(logLength);
		glGetProgramInfoLog(program, logLength, &logLength, log);
		GLLog(GLLogVerboseLevelWarning, @"Program validate log:\n%s", log);
		free(log);
	}
}

#pragma mark -
#pragma mark Set Uniforms

- (void)setUniform1f:(GLint)location value:(GLfloat)value
{
#if USE_STATE_CACHE
    NSNumber* number = uniformsCache[@(location)];
    if (!number || ![number isKindOfClass:[NSNumber class]] || [number floatValue] != value)
    {
#endif
        glUniform1f(location, value);
#if USE_STATE_CACHE
        uniformsCache[@(location)] = @(value);
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniform1i:(GLint)location value:(GLint)value
{
#if USE_STATE_CACHE
    NSNumber* number = uniformsCache[@(location)];
    if (!number || ![number isKindOfClass:[NSNumber class]] || [number integerValue] != value)
    {
#endif
        glUniform1i(location, value);
#if USE_STATE_CACHE
        uniformsCache[@(location)] = @(value);
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniform2f:(GLint)location value:(CGPoint)point
{
#if USE_STATE_CACHE
    NSValue* value = uniformsCache[@(location)];
    if (!value || ![value isKindOfClass:[NSValue class]] || !CGPointEqualToPoint(point, [value CGPointValue]))
    {
#endif
        glUniform2f(location, point.x, point.y);
#if USE_STATE_CACHE
        uniformsCache[@(location)] = [NSValue valueWithCGPoint:point];
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniform3f:(GLint)location value:(GLVector3)vector
{
#if USE_STATE_CACHE
    NSData* data = uniformsCache[@(location)];
    if (!data || ![[data class] isSubclassOfClass:[NSData class]] || memcmp(data.bytes, vector.v, sizeof(float)*3) != 0)
    {
#endif
        glUniform3f(location, vector.v[0], vector.v[1], vector.v[2]);
#if USE_STATE_CACHE
        uniformsCache[@(location)] = [NSData dataWithBytes:vector.v length:(sizeof(float)*3)];
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

- (void)setUniformMatrix4x4:(GLint)location matrix:(const GLMatrix4x4 &)matrix
{
#if USE_STATE_CACHE
    NSData* data = uniformsCache[@(location)];
    if (!data || ![[data class] isSubclassOfClass:[NSData class]] || memcmp(data.bytes, matrix.pointer(), sizeof(float) * 16) != 0)
    {
#endif
        glUniformMatrix4fv(location, 1, GL_FALSE, matrix.pointer());
#if USE_STATE_CACHE
        uniformsCache[@(location)] = [NSData dataWithBytes:matrix.pointer() length:(sizeof(float) * 16)];
    }
#endif
#if PROFILE_STATE_CACHE
    else { [GLStateCache incRedundantCall]; };
#endif
}

@end
