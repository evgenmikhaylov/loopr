//
//  GLVector3.cpp
//  GLESUtilities
//
//  Created by Andrey on 15/06/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#include "GLVector3.hpp"
#include <stdio.h>
#include <math.h>

GLVector3::GLVector3(float x, float y, float z)
{
    v[0] = x;
    v[1] = y;
    v[2] = z;
}

GLVector3::~GLVector3()
{
    
}

float GLVector3::length()
{
    return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

GLVector3 GLVector3::negative()
{
    GLVector3 vector;
    vector.v[0] = -v[0];
    vector.v[1] = -v[1];
    vector.v[2] = -v[2];
    return vector;
}

GLVector3 GLVector3::normalize()
{
    float scale = 1.0f / length();
    
    GLVector3 vector;
    vector.v[0] = v[0] * scale;
    vector.v[1] = v[1] * scale;
    vector.v[2] = v[2] * scale;
    return vector;
}

GLVector3 GLVector3::Add(GLVector3 left, GLVector3 right)
{
    GLVector3 vector;
    vector.v[0] = left.v[0] + right.v[0];
    vector.v[1] = left.v[1] + right.v[1];
    vector.v[2] = left.v[2] + right.v[2];
    return vector;
}

GLVector3 GLVector3::CrossProduct(GLVector3 left, GLVector3 right)
{
    GLVector3 vector;
    vector.v[0] = left.v[1] * right.v[2] - left.v[2] * right.v[1];
    vector.v[1] = left.v[2] * right.v[0] - left.v[0] * right.v[2];
    vector.v[2] = left.v[0] * right.v[1] - left.v[1] * right.v[0];
    return vector;
}

float GLVector3::DotProduct(GLVector3 left, GLVector3 right)
{
    return left.v[0] * right.v[0] + left.v[1] * right.v[1] + left.v[2] * right.v[2];
}
