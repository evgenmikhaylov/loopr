//
//  GLContext.m
//  SplitPic
//
//  Created by Admin on 03/05/2013.
//  Copyright (c) 2013 Andrey. All rights reserved.
//

#import "GLContext.h"
#import "GLLog.h"

@interface GLContext ()
{
    dispatch_semaphore_t semaphore;
    CVOpenGLESTextureCacheRef videoTextureCache;
}

@property (nonatomic) EAGLContext* context;
@property (atomic) BOOL appActive;

@end

@implementation GLContext

+ (BOOL)supportExtensionTextureRG
{
    static BOOL supportExtensionTextureRG = NO;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        [self setContext];
        supportExtensionTextureRG = (strstr((char*)glGetString(GL_EXTENSIONS), "GL_EXT_texture_rg")) != NULL;
    });
    return supportExtensionTextureRG;
}

+ (BOOL)supportDiscardFramebuffer
{
    static BOOL supportDiscardFramebuffer = NO;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        [self setContext];
        supportDiscardFramebuffer = (strstr((char*)glGetString(GL_EXTENSIONS), "GL_EXT_discard_framebuffer")) != NULL;
    });
    return supportDiscardFramebuffer;
}

+ (GLint)maximumTextureSize
{
    static GLint maxTextureSize = 0;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        [self setContext];
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
    });
    return maxTextureSize;
}

+ (CGSize)sizeThatFitInMaxSupportSize:(CGSize)inputSize
{
    GLfloat maxTextureSize = [GLContext maximumTextureSize];
    if ((inputSize.width < maxTextureSize) && (inputSize.height < maxTextureSize))
    {
        return CGSizeMake((int)inputSize.width, (int)inputSize.height);
    }
    
    CGSize adjustedSize;
    if (inputSize.width > inputSize.height)
    {
        adjustedSize.width = (int)maxTextureSize;
        adjustedSize.height = (int)((maxTextureSize / inputSize.width) * inputSize.height);
    }
    else
    {
        adjustedSize.height = (int)maxTextureSize;
        adjustedSize.width = (int)((maxTextureSize / inputSize.height) * inputSize.width);
    }
    
    GLLog(GLLogVerboseLevelError, @"%s: WARNING: size bigger that device support, check new size! inputSize - %@, adjustedSize - %@",__func__,NSStringFromCGSize(inputSize),NSStringFromCGSize(adjustedSize));
    
    return adjustedSize;
}

+ (CGFloat)scale
{
    return [UIScreen mainScreen].scale;
}

+ (instancetype)sharedInstance
{
    static GLContext* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        semaphore = dispatch_semaphore_create(1);
        
        self.appActive = YES;
        self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        [EAGLContext setCurrentContext:self.context];
        
        CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, self.context, NULL, &videoTextureCache);
        if (err)
        {
            GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreate %d", err);
        }
    }
    return self;
}

- (void)dealloc
{
    [EAGLContext setCurrentContext:nil];
    self.context = nil;
}

#pragma mark -
#pragma mark Static methods

+ (CVOpenGLESTextureCacheRef)videoTextureCache
{
    return [GLContext sharedInstance]->videoTextureCache;
}

+ (EAGLContext *)context
{
    return [[GLContext sharedInstance] context];
}

+ (void)setContext
{
    if ([EAGLContext currentContext] != self.context)
    {
        [EAGLContext setCurrentContext:self.context];
    }
}

#pragma mark -
#pragma mark Lock / Unlock Context

+ (void)lock
{
    NSDate* date = nil;
    while ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        if (date == nil)
        {
            date = [NSDate dateWithTimeIntervalSinceNow:0.1];
        }
        
        [NSThread sleepUntilDate:date];
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:0.1];
    }
    
    dispatch_semaphore_wait([GLContext sharedInstance]->semaphore, DISPATCH_TIME_FOREVER);
    [self setContext];
}

+ (void)unlock
{
    dispatch_semaphore_signal([GLContext sharedInstance]->semaphore);
}

+ (void)perfromBlockWithLockContext:(dispatch_block_t)block
{
    [self lock];
    {
        block();
    }
    [self unlock];
}

@end
