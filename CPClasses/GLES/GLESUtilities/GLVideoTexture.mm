//
//  GLVideoTexture.m
//  SPApp
//
//  Created by Andrey Konoplyankin on 3/26/14.
//  Copyright (c) 2014 rosberry. All rights reserved.
//

#import "GLVideoTexture.h"
#import "GLContext.h"
#import "GLLog.h"
#import "GLProgram.h"
#import "GLFrameBuffer.h"

#import <AVFoundation/AVFoundation.h>

@interface GLVideoTexture () <AVPlayerItemOutputPullDelegate>
{
    CVOpenGLESTextureRef _lumaTexture;
    CVOpenGLESTextureRef _chromaTexture;
    
    CGFloat preferredRotation;
}

@property (nonatomic) AVPlayerItemVideoOutput* videoOutput;
@property (nonatomic) AVPlayer* player;

@end

@implementation GLVideoTexture

- (instancetype)initWithURL:(NSURL *)url completionBlock:(void(^)())block
{
    self = [super init];
    if (self)
    {
        // Setup AVPlayerItemVideoOutput with the required pixelbuffer attributes.
        NSDictionary* pixBuffAttributes = @{(id)kCVPixelBufferPixelFormatTypeKey: @(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)};
        self.videoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:pixBuffAttributes];
        [self.videoOutput setDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        
        self.player = [[AVPlayer alloc] init];
        //self.player.volume = 0.0f;
        
        AVPlayerItem* item = [AVPlayerItem playerItemWithURL:url];
        AVAsset* asset = [item asset];
        
        [asset loadValuesAsynchronouslyForKeys:@[@"tracks"] completionHandler:^()
         {
             if ([asset statusOfValueForKey:@"tracks" error:nil] == AVKeyValueStatusLoaded)
             {
                 NSArray* tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
                 if ([tracks count] > 0)
                 {
                     // Choose the first video track.
                     AVAssetTrack* videoTrack = [tracks objectAtIndex:0];
                     [videoTrack loadValuesAsynchronouslyForKeys:@[@"preferredTransform"] completionHandler:^()
                      {
                          if ([videoTrack statusOfValueForKey:@"preferredTransform" error:nil] == AVKeyValueStatusLoaded)
                          {
                              CGAffineTransform preferredTransform = [videoTrack preferredTransform];
                              
                              /*
                               The orientation of the camera while recording affects the orientation of the images received from an AVPlayerItemVideoOutput. Here we compute a rotation that is used to correctly orientate the video.
                               */
                              
                              preferredRotation = -1.0f * atan2(preferredTransform.b, preferredTransform.a);
                              
                              //[self addDidPlayToEndTimeNotificationForPlayerItem:item];
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                                  [item addOutput:self.videoOutput];
                                  [self.player replaceCurrentItemWithPlayerItem:item];
                                  [self.videoOutput requestNotificationOfMediaDataChangeWithAdvanceInterval:0.3];
                                  [self.player play];
                                  
                                  [self resetPlayback];
                                  
                                  if (block)
                                  {
                                      block();
                                  }
                              });
                          }
                          
                      }];
                 }
             }
         }];
    }
    return self;
}

- (void)dealloc
{
    GLLog(GLLogVerboseLevelWarning, @"%s",__func__);
    
    [self cleanUpTextures];
}

- (void)resetPlayback
{
    
}

- (void)cleanUpTextures
{
    if (_lumaTexture)
    {
        CFRelease(_lumaTexture);
        _lumaTexture = NULL;
    }
    
    if (_chromaTexture)
    {
        CFRelease(_chromaTexture);
        _chromaTexture = NULL;
    }
    
    // Periodic texture cache flush every frame
    CVOpenGLESTextureCacheFlush([GLContext videoTextureCache], 0);
}

- (BOOL)readNextFrame
{
    CMTime outputItemTime = [self.player currentTime];
    
    //if ([self.videoOutput hasNewPixelBufferForItemTime:outputItemTime])
    {
        CVPixelBufferRef pixelBuffer = [self.videoOutput copyPixelBufferForItemTime:outputItemTime itemTimeForDisplay:NULL];
        if (pixelBuffer)
        {
            CVReturn err;
            GLsizei width = (GLsizei)CVPixelBufferGetWidth(pixelBuffer);
            GLsizei height = (GLsizei)CVPixelBufferGetHeight(pixelBuffer);
            
            [self cleanUpTextures];
            
            /*
             CVOpenGLESTextureCacheCreateTextureFromImage will create GLES texture optimally from CVPixelBufferRef.
             */
            
            /*
             Create Y and UV textures from the pixel buffer. These textures will be drawn on the frame buffer Y-plane.
             */
            
            glActiveTexture(GL_TEXTURE0);
            err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                               [GLContext videoTextureCache],
                                                               pixelBuffer,
                                                               NULL,
                                                               GL_TEXTURE_2D,
                                                               GL_RED_EXT,
                                                               width,
                                                               height,
                                                               GL_RED_EXT,
                                                               GL_UNSIGNED_BYTE,
                                                               0,
                                                               &_lumaTexture);
            if (err)
            {
                GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
            }
            
            glBindTexture(CVOpenGLESTextureGetTarget(_lumaTexture), CVOpenGLESTextureGetName(_lumaTexture));
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            // UV-plane.
            glActiveTexture(GL_TEXTURE1);
            err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                               [GLContext videoTextureCache],
                                                               pixelBuffer,
                                                               NULL,
                                                               GL_TEXTURE_2D,
                                                               GL_RG_EXT,
                                                               width / 2,
                                                               height / 2,
                                                               GL_RG_EXT,
                                                               GL_UNSIGNED_BYTE,
                                                               1,
                                                               &_chromaTexture);
            if (err)
            {
                GLLog(GLLogVerboseLevelError, @"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
            }
            
            glBindTexture(CVOpenGLESTextureGetTarget(_chromaTexture), CVOpenGLESTextureGetName(_chromaTexture));
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            if (CVBufferGetAttachment(pixelBuffer, kCVImageBufferYCbCrMatrixKey, NULL) == kCVImageBufferYCbCrMatrix_ITU_R_601_4)
            {
                [self.dataOutput glVideoTexture:self didOutputPixelBufferWithSize:CGSizeMake(width, height) colorMatrix:GLVideoTextureColorMatrix601 chromaTextureHandle:0 lumiaTextureHandle:1];
            }
            else
            {
                [self.dataOutput glVideoTexture:self didOutputPixelBufferWithSize:CGSizeMake(width, height) colorMatrix:GLVideoTextureColorMatrix709 chromaTextureHandle:0 lumiaTextureHandle:1];
            }
            
            CVPixelBufferRelease(pixelBuffer);
            
            return YES;
        }
    }
    
    return NO;
}

@end
