//
//  CPVideoColorMatrix601Program.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#import "GLProgram.h"
#import "CPVideoProgramProtocol.h"

@interface CPVideoColorMatrix601Program : GLProgram <CPVideoProgramProtocol>

+ (instancetype)program;

@end
