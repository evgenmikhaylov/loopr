//
//  CPImageProgram.m
//  Cinepic
//
//  Created by Anton on 12.08.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "CPImageProgram.h"
#import "GLContext.h"

NSString* CPImageProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 inputTextureCoordinate;
 
 uniform mat4 projectionMatrix;
 uniform mat4 modelViewMatrix;

 varying vec2 textureCoordinate;
 
 void main()
 {
     gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz,1.0);
     textureCoordinate = inputTextureCoordinate;
 }
 );

NSString* CPImageProgramFragmentShader = SHADER_STRING
(
 uniform sampler2D inputImageTexture;
 uniform highp float alpha;
 varying highp vec2 textureCoordinate;
 
 lowp float clamp_to_border()
 {
     bvec2 out1 = greaterThan(textureCoordinate, vec2(1.0, 1.0));
     bvec2 out2 = lessThan(textureCoordinate, vec2(0.0, 0.0));
     return float(!(any(out1) || any(out2)));
 }
 
 void main()
 {
     gl_FragColor = vec4(texture2D(inputImageTexture, textureCoordinate).rgb, alpha)*clamp_to_border();
 }
 );

@implementation CPImageProgram

+ (instancetype)program
{
    [GLContext lock];
    
    CPImageProgram *program = [[self alloc] initWithVertexShaderString:CPImageProgramVertexShader fragmentShaderString:CPImageProgramFragmentShader];
    [program addAttribute:@"position"];
    [program addAttribute:@"inputTextureCoordinate"];
    if ([program link])
    {
        program->_uniformInputImageTexture = [program uniformIndex:@"inputImageTexture"];
        program->_attribPosition = [program attributeIndex:@"position"];
        program->_attribTexCoord = [program attributeIndex:@"inputTextureCoordinate"];
        program->_uniformProjectionMatrix = [program uniformIndex:@"projectionMatrix"];
        program->_uniformModelViewMatrix = [program uniformIndex:@"modelViewMatrix"];
        program->_uniformAlpha = [program uniformIndex:@"alpha"];

        glEnableVertexAttribArray(program->_attribPosition);
        glEnableVertexAttribArray(program->_attribTexCoord);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    [GLContext unlock];
    
    return program;
}

@end
