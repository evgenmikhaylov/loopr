//
//  CPLineProgram.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/27/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPLineProgram.h"
#import "GLContext.h"

NSString* CPLineProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 
 void main()
 {
     gl_Position = position;
 }
 );

NSString* CPLineProgramFragmentShader = SHADER_STRING
(
 uniform highp vec3 color;

 void main()
 {
     gl_FragColor = vec4(color.r, color.g, color.b, 1.0);
 }
);

@implementation CPLineProgram

+ (instancetype)program
{
    [GLContext lock];
    
    CPLineProgram* program = [[self alloc] initWithVertexShaderString:CPLineProgramVertexShader fragmentShaderString:CPLineProgramFragmentShader];
    [program addAttribute:@"position"];
    if ([program link])
    {
        program->_attribPosition = [program attributeIndex:@"position"];
        program->_uniformColor = [program uniformIndex:@"color"];
        glEnableVertexAttribArray(program->_attribPosition);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    [GLContext unlock];
    
    return program;
}

@end
