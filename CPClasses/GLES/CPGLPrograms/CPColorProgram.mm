//
//  CPColorProgram.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 4/29/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPColorProgram.h"
#import "GLContext.h"
#include "GLVector3.hpp"

NSString* CPColorProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 uniform mat4 projectionMatrix;
 uniform mat4 modelViewMatrix;
 
 void main()
 {
     gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz,1.0);
 }
 );

NSString* CPColorProgramFragmentShader = SHADER_STRING
(
 uniform highp vec3 color;
 uniform highp float alpha;
 
 void main()
 {
     gl_FragColor = vec4(color.r, color.g, color.b, alpha);
 }
 );

@implementation CPColorProgram

- (instancetype)initWithVertexShaderString:(NSString *)vShaderString fragmentShaderString:(NSString *)fShaderString
{
    self = [super initWithVertexShaderString:vShaderString fragmentShaderString:fShaderString];
    if (self) {
    }
    return self;
}

+ (instancetype)program
{
    [GLContext lock];
    
    CPColorProgram* program = [[self alloc] initWithVertexShaderString:CPColorProgramVertexShader fragmentShaderString:CPColorProgramFragmentShader];
    [program addAttribute:@"position"];
    if ([program link])
    {
        program->_attribPosition = [program attributeIndex:@"position"];
        program->_uniformProjectionMatrix = [program uniformIndex:@"projectionMatrix"];
        program->_uniformModelViewMatrix = [program uniformIndex:@"modelViewMatrix"];
        program->_uniformAlpha = [program uniformIndex:@"alpha"];
        program->_uniformColor = [program uniformIndex:@"color"];
        glEnableVertexAttribArray(program->_attribPosition);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    [GLContext unlock];
    
    return program;
}

@end
