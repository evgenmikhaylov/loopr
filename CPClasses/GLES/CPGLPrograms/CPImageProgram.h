//
//  CPImageProgram.h
//  Cinepic
//
//  Created by Anton on 12.08.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GLProgram.h"

@interface CPImageProgram : GLProgram

+ (instancetype)program;

@property (nonatomic,readonly) GLuint attribPosition, attribTexCoord;
@property (nonatomic,readonly) GLuint uniformProjectionMatrix, uniformModelViewMatrix, uniformInputImageTexture, uniformAlpha;

@end
