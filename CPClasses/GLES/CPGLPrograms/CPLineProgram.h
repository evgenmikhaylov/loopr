//
//  CPLineProgram.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/27/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "GLProgram.h"

@interface CPLineProgram : GLProgram

+ (instancetype)program;

@property (nonatomic,readonly) GLuint attribPosition, attribTexCoord, uniformColor;

@end