//
//  CPImageColorSwizzlingProgram.h
//  Cinepic
//
//  Created by Anton on 14.08.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GLProgram.h"

@interface CPImageColorSwizzlingProgram : GLProgram

+ (instancetype)program;

@property (nonatomic,readonly) GLuint attribPosition, attribTexCoord;
@property (nonatomic,readonly) GLuint uniformProjectionMatrix, uniformModelViewMatrix, uniformInputImageTexture, uniformAlpha;

@end
