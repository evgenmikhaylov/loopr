//
//  CPVideoColorMatrix709ColorSwizzlingProgram.m
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#import "CPVideoColorMatrix709ColorSwizzlingProgram.h"
#import "GLContext.h"

NSString* CPVideoColorMatrix709ColorSwizzlingProgramVertexShader = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 inputTextureCoordinate;
 
 uniform mat4 projectionMatrix;
 uniform mat4 modelViewMatrix;
 
 varying vec2 textureCoordinate;
 
 void main()
 {
     gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz,1.0);
     textureCoordinate = inputTextureCoordinate;
 }
 );

NSString* CPVideoColorMatrix709ColorSwizzlingProgramFragmentShader = SHADER_STRING
(
 uniform sampler2D SamplerY;
 uniform sampler2D SamplerUV;
 uniform highp float alpha;
 varying highp vec2 textureCoordinate;
 
 const mediump mat3 convertMatrix = mat3(
                                         1.164,  1.164, 1.164,
                                         0.0, -0.213, 2.112,
                                         1.793, -0.533,   0.0
                                         );
 
 lowp float clamp_to_border()
 {
     bvec2 out1 = greaterThan(textureCoordinate, vec2(1.0, 1.0));
     bvec2 out2 = lessThan(textureCoordinate, vec2(0.0, 0.0));
     return float(!(any(out1) || any(out2)));
 }
 
 void main()
 {
     mediump vec3 yuv;
     lowp vec3 rgb;
     
     // Subtract constants to map the video range start at 0
     yuv.x = texture2D(SamplerY, textureCoordinate).r - (16.0/255.0);
     yuv.yz = texture2D(SamplerUV, textureCoordinate).rg - vec2(0.5, 0.5);
     
     rgb = convertMatrix * yuv;
     
     gl_FragColor = vec4(rgb.bgr, alpha)*clamp_to_border();
 }
 );

@implementation CPVideoColorMatrix709ColorSwizzlingProgram
{
    GLuint attribPosition, attribTexCoord;
    GLuint uniformSamplerY, uniformSamplerUV, uniformAlpha, uniformProjectionMatrix, uniformModelViewMatrix;
}

+ (instancetype)program
{
    [GLContext lock];
    
    CPVideoColorMatrix709ColorSwizzlingProgram* program = [[self alloc] initWithVertexShaderString:CPVideoColorMatrix709ColorSwizzlingProgramVertexShader
                                                                              fragmentShaderString:CPVideoColorMatrix709ColorSwizzlingProgramFragmentShader];
    [program addAttribute:@"position"];
    [program addAttribute:@"inputTextureCoordinate"];
    if ([program link])
    {
        program->uniformSamplerY = [program uniformIndex:@"SamplerY"];
        program->uniformSamplerUV = [program uniformIndex:@"SamplerUV"];
        
        program->attribPosition = [program attributeIndex:@"position"];
        program->attribTexCoord = [program attributeIndex:@"inputTextureCoordinate"];
        
        program->uniformAlpha = [program uniformIndex:@"alpha"];
        program->uniformProjectionMatrix = [program uniformIndex:@"projectionMatrix"];
        program->uniformModelViewMatrix = [program uniformIndex:@"modelViewMatrix"];
        
        glEnableVertexAttribArray(program->attribPosition);
        glEnableVertexAttribArray(program->attribTexCoord);
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"Failed link program %@",NSStringFromClass([self class])];
    }
    
    [GLContext unlock];
    
    return program;
}

#pragma mark -
#pragma mark VideoOutputProgramProtocol

- (GLuint)attribPosition
{
    return attribPosition;
}

- (GLuint)attribTexCoord
{
    return attribTexCoord;
}

- (GLuint)uniformSamplerY
{
    return uniformSamplerY;
}

- (GLuint)uniformSamplerUV
{
    return uniformSamplerUV;
}

- (GLuint)uniformAlpha
{
    return uniformAlpha;
}

- (GLuint)uniformProjectionMatrix
{
    return uniformProjectionMatrix;
}

- (GLuint)uniformModelViewMatrix
{
    return uniformModelViewMatrix;
}

@end
