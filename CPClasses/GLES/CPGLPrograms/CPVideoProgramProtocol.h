//
//  CPVideoProgramProtocol.h
//  GLESUtilities
//
//  Created by Andrey Konoplyankin on 6/14/14.
//  Copyright (c) 2014 Andrey Konoplyankin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CPVideoProgramProtocol <NSObject>

- (GLuint)attribPosition;
- (GLuint)attribTexCoord;
- (GLuint)uniformSamplerY;
- (GLuint)uniformSamplerUV;
- (GLuint)uniformAlpha;
- (GLuint)uniformProjectionMatrix;
- (GLuint)uniformModelViewMatrix;

@end
