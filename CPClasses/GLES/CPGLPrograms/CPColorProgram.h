//
//  CPColorProgram.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 4/29/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "GLProgram.h"

@interface CPColorProgram : GLProgram

+ (instancetype)program;

@property (nonatomic,readonly) GLuint attribPosition;
@property (nonatomic,readonly) GLuint uniformProjectionMatrix, uniformModelViewMatrix, uniformAlpha, uniformColor;

@end
