//
//  CoreGraphicsExtensions.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/02/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CoreGraphicsExtensions.h"

CGRect rectApplyingScale(CGRect rect, CGFloat scale) {
    
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGRect scaledRect = CGRectApplyAffineTransform(rect, CGAffineTransformMakeScale(scale, scale));
    scaledRect.origin.x = center.x - CGRectGetWidth(scaledRect) / 2;
    scaledRect.origin.y = center.y - CGRectGetHeight(scaledRect) / 2;
    return scaledRect;
}
