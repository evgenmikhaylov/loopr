//
//  CoreGraphicsExtensions.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/02/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

CG_EXTERN CGRect rectApplyingScale(CGRect rect, CGFloat scale);