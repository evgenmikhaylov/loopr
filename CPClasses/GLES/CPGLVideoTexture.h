//
//  CPGLVideoTexture.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/19/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLVideoTexture.h"
#import <AVFoundation/AVFoundation.h>

@class CPGLVideoTexture;

@protocol CPGLVideoTextureDataOutput <NSObject>

@optional
- (void)cpGLVideoTexture:(CPGLVideoTexture *)texture didOutputPixelBufferWithSize:(CGSize)size
             colorMatrix:(GLVideoTextureColorMatrix)colorMatrix
     chromaTextureHandle:(GLenum)chromaTextureHandle
      lumiaTextureHandle:(GLenum)lumiaTextureHandle;

@end

@interface CPGLVideoTexture : NSObject

@property (nonatomic, weak) id<CPGLVideoTextureDataOutput> dataOutput;

@property (nonatomic, readonly) BOOL loaded;
@property (nonatomic, readonly) BOOL isPlaybackInProgress;
@property (nonatomic, readonly) AVAssetTrack *assetVideoTrack;
@property (nonatomic, readonly) AVAsset *asset;
@property (nonatomic) BOOL shouldRepeat;
@property (nonatomic) BOOL pause;
@property (nonatomic) NSURL *currentURL;


- (instancetype)initWithURL:(NSURL *)url;

- (void)startReadingFromBeginnigWithCompletionBlock:(void(^)())completionBlock;
- (void)startReadingWithOffset:(NSTimeInterval)offset completionBlock:(void(^)())completionBlock;
- (void)pauseReading;
- (void)stopReading;
- (BOOL)readCurrentFrame;
- (BOOL)readCurrentFrameWithCompletionBlock:(void(^)(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle))completionBlock;
- (BOOL)readNextFrame;
- (BOOL)readNextFrameWithCompletionBlock:(void(^)(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle))completionBlock;


@end
