//
//  CPGLVideoTexture.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 2/19/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPGLVideoTexture.h"
#import "GLContext.h"
#import "GLProgram.h"
#import "GLFrameBuffer.h"

#import <AVFoundation/AVFoundation.h>
#import <RACEXTScope.h>

@interface CPGLVideoTexture () <AVPlayerItemOutputPullDelegate>
{
    CVOpenGLESTextureRef _lumaTexture;
    CVOpenGLESTextureRef _chromaTexture;
    
    CFAbsoluteTime lastTime;
    long long currentFrameIndex;
    CGFloat startReadingTime;
}

@property (nonatomic) BOOL isPlaybackInProgress;
@property (nonatomic) BOOL loaded;
@property (nonatomic) CMSampleBufferRef lastSampleBuffer;
@property (nonatomic) AVAssetReaderTrackOutput *assetOutput;
@property (nonatomic) AVAssetTrack *assetVideoTrack;
@property (nonatomic) AVAsset *asset;
@property (nonatomic) AVAssetReader *assetReader;
@property (nonatomic, copy) void(^readNextFrameCompletionBlock)(CPGLVideoTexture *, CGSize, GLVideoTextureColorMatrix, GLenum, GLenum);

@end

@implementation CPGLVideoTexture

- (instancetype)initWithURL:(NSURL *)url
{
    self = [super init];
    if (self)
    {
        _currentURL = url;
        self.shouldRepeat = NO;
        self.loaded = NO;
    }
    return self;
}

- (void)dealloc
{
    [self cleanUpTextures];
    if (_lastSampleBuffer)
        CFRelease(_lastSampleBuffer), _lastSampleBuffer = NULL;
    [self clean];
    [self setCurrentURL:nil];
}

- (void)clean
{
    currentFrameIndex = 0;
    [self setAssetVideoTrack:nil];
    [self setAssetOutput:nil];
    [self setAsset:nil];
    [self setAssetReader:nil];
    if (_lastSampleBuffer)
        CFRelease(_lastSampleBuffer), _lastSampleBuffer = NULL;
}

- (void)cleanReader
{
    currentFrameIndex = 0;
    [self setAssetVideoTrack:nil];
    [self setAssetOutput:nil];
    [self setAssetReader:nil];
    if (_lastSampleBuffer)
        CFRelease(_lastSampleBuffer), _lastSampleBuffer = NULL;
}

- (void)startReadingFromBeginnigWithCompletionBlock:(void(^)())completionBlock
{
    [self startReadingWithOffset:0.0 completionBlock:completionBlock];
}

- (void)startReadingWithOffset:(NSTimeInterval)offset completionBlock:(void(^)())completionBlock
{
    [self clean];
    self.isPlaybackInProgress = NO;
    NSDictionary *inputOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    self.asset = [[AVURLAsset alloc] initWithURL:self.currentURL options:inputOptions];
    AVKeyValueStatus tracksStatus = [self.asset statusOfValueForKey:@"tracks" error:nil];
    if (tracksStatus == AVKeyValueStatusLoaded) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self prepareReaderWithOffset:offset];
            if (completionBlock){
                completionBlock();
            }
        });
    }
    else {
        @weakify(self);
        [self.asset loadValuesAsynchronouslyForKeys:[NSArray arrayWithObject:@"tracks"] completionHandler: ^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self prepareReaderWithOffset:offset];
                self.loaded = YES;
                if (completionBlock){
                    completionBlock();
                }
            });
        }];
    }

}

- (void)prepareReaderWithOffset:(NSTimeInterval)offset {
    
    NSArray *videoTracks = [self.asset tracksWithMediaType:AVMediaTypeVideo];
    if (videoTracks.count > 0) {
        NSDictionary *pixBuffAttributes = @{(id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)};
        self.assetVideoTrack = [videoTracks firstObject];
        self.assetOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:self.assetVideoTrack outputSettings:pixBuffAttributes];
        self.assetReader = [[AVAssetReader alloc] initWithAsset:self.asset error:nil];
        if ([self.assetReader canAddOutput:self.assetOutput] && ![self.assetReader.outputs containsObject:self.assetOutput]){
            [self.assetReader addOutput:self.assetOutput];
            NSTimeInterval duration = CMTimeGetSeconds(self.asset.duration) - offset;
            [self assetReaderSetTimeRangeFromTime:offset withDuration:duration];
            [self.assetReader startReading];
            self.isPlaybackInProgress = YES;
        }
        else {
            NSLog(@"OOOOO!!!");
        }
    }
}

- (void)pauseReading
{
    [self.assetReader cancelReading];
    [self cleanReader];
    self.isPlaybackInProgress = NO;
    self.loaded = NO;
}

- (void)stopReading
{
    [self.assetReader cancelReading];
    [self clean];
    self.isPlaybackInProgress = NO;
    self.loaded = NO;
}

- (void)playbackIsFinished
{
    self.isPlaybackInProgress = NO;
    self.loaded = NO;
    [self clean];
}

- (BOOL)assetReaderSetTimeRangeFromTime:(double)startTime withDuration:(double)duration {
    if (self.assetReader.status == AVAssetReaderStatusReading)
        return NO;
    startReadingTime = startTime;
    CMTimeScale scale = self.asset.duration.timescale;
    CMTime start = CMTimeMake(startTime*scale, scale);
    CMTime dur = CMTimeMake(duration*scale, scale);
    CMTimeRange range = CMTimeRangeMake(start, dur);
    [self.assetReader setTimeRange:range];
    return YES;
}

- (void)cleanUpTextures
{
    if (_lumaTexture)
    {
        CFRelease(_lumaTexture);
        _lumaTexture = NULL;
    }
    
    if (_chromaTexture)
    {
        CFRelease(_chromaTexture);
        _chromaTexture = NULL;
    }
    
    // Periodic texture cache flush every frame
    CVOpenGLESTextureCacheFlush([GLContext videoTextureCache], 0);
}

- (BOOL)readCurrentFrame
{
    return [self drawLastSampleBuffer];
}

- (BOOL)readCurrentFrameWithCompletionBlock:(void(^)(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle))completionBlock
{
    self.readNextFrameCompletionBlock = completionBlock;
    BOOL readCurrentFrame = [self readCurrentFrame];
    if (!readCurrentFrame) {
        return [self readNextFrame];
    }
    return readCurrentFrame;
    
}

- (BOOL)readNextFrame
{
    if (!self.assetReader){
        return [self drawLastSampleBuffer];
    }
    
    if (self.assetReader.status==AVAssetReaderStatusReading){
        if (self.pause){
            if (!_lastSampleBuffer){
                _lastSampleBuffer = [self.assetOutput copyNextSampleBuffer];
            }
        }
        else{
            if (_lastSampleBuffer){
                CFRelease(_lastSampleBuffer),
                _lastSampleBuffer = NULL;
            }
            _lastSampleBuffer = [self.assetOutput copyNextSampleBuffer];
        }
        if (_lastSampleBuffer){
            return [self drawLastSampleBuffer];
        }
    }
    else if (self.assetReader.status==AVAssetReaderStatusCompleted){
        if (self.shouldRepeat){
            [self.assetReader cancelReading];
            [self startReadingFromBeginnigWithCompletionBlock:nil];
        }
        else{
            [self playbackIsFinished];
        }
    }
    return NO;
}

- (BOOL)readNextFrameWithCompletionBlock:(void(^)(CPGLVideoTexture *texture, CGSize size, GLVideoTextureColorMatrix colorMatrix, GLenum chromaTextureHandle, GLenum lumiaTextureHandle))completionBlock {
    
    self.readNextFrameCompletionBlock = completionBlock;
    return [self readNextFrame];
}

- (BOOL)drawLastSampleBuffer
{
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(_lastSampleBuffer);
    
    if (pixelBuffer){
        
        CVReturn err;
        size_t width = CVPixelBufferGetWidth(pixelBuffer);
        size_t height = CVPixelBufferGetHeight(pixelBuffer);
        
        [self cleanUpTextures];
        
        glActiveTexture(GL_TEXTURE0);
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                           [GLContext videoTextureCache],
                                                           pixelBuffer,
                                                           NULL,
                                                           GL_TEXTURE_2D,
                                                           GL_RED_EXT,
                                                           (int)width,
                                                           (int)height,
                                                           GL_RED_EXT,
                                                           GL_UNSIGNED_BYTE,
                                                           0,
                                                           &_lumaTexture);
        if (err){
            NSLog(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
        }
        
        glBindTexture(CVOpenGLESTextureGetTarget(_lumaTexture), CVOpenGLESTextureGetName(_lumaTexture));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        // UV-plane.
        glActiveTexture(GL_TEXTURE1);
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                           [GLContext videoTextureCache],
                                                           pixelBuffer,
                                                           NULL,
                                                           GL_TEXTURE_2D,
                                                           GL_RG_EXT,
                                                           (int)width / 2,
                                                           (int)height / 2,
                                                           GL_RG_EXT,
                                                           GL_UNSIGNED_BYTE,
                                                           1,
                                                           &_chromaTexture);
        if (err){
            NSLog(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
        }
        
        glBindTexture(CVOpenGLESTextureGetTarget(_chromaTexture), CVOpenGLESTextureGetName(_chromaTexture));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        GLVideoTextureColorMatrix colorMatrix;
        if (CVBufferGetAttachment(pixelBuffer, kCVImageBufferYCbCrMatrixKey, NULL) == kCVImageBufferYCbCrMatrix_ITU_R_601_4){
            colorMatrix = GLVideoTextureColorMatrix601;
        }
        else {
            colorMatrix = GLVideoTextureColorMatrix709;
        }
        if ([self.dataOutput respondsToSelector:@selector(cpGLVideoTexture:didOutputPixelBufferWithSize:colorMatrix:chromaTextureHandle:lumiaTextureHandle:)]) {
            [self.dataOutput cpGLVideoTexture:self
                 didOutputPixelBufferWithSize:CGSizeMake(width, height)
                                  colorMatrix:colorMatrix
                          chromaTextureHandle:0
                           lumiaTextureHandle:1];
        }
        if (self.readNextFrameCompletionBlock) {
            self.readNextFrameCompletionBlock(self, CGSizeMake(width, height), colorMatrix, 0, 1);
        }
        return YES;
    }
    self.readNextFrameCompletionBlock = nil;
    return NO;
}

@end
