//
//  CPITunesSearchBarViewOutput.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CPITunesSearchBarViewOutput <NSObject>

- (void)closeButtonPressed;
- (void)doneButtonPressed;
- (void)searchButtonPressed;
- (void)textDidChange:(NSString *)text;

@end
