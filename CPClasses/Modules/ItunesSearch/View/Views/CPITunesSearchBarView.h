//
//  CPITunesSearchBarView.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPITunesSearchBarViewModel;

@interface CPITunesSearchBarView : UIView

@property (nonatomic, readonly) UIButton *closeButton;
@property (nonatomic, readonly) UIButton *doneButton;
@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UISearchBar *searchBar;

@property (nonatomic) CPITunesSearchBarViewModel *viewModel;

@end
