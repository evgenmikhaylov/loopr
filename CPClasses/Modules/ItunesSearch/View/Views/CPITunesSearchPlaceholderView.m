//
//  CPITunesSearchPlaceholderView.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchPlaceholderView.h"
#import "UIColor+CP.h"
#import "UIFont+CP.h"
#import <ReactiveCocoa.h>
#import <Framer.h>

@interface CPITunesSearchPlaceholderView ()

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *label;

@property (nonatomic, readonly) CGFloat labelTopInset;

@end

@implementation CPITunesSearchPlaceholderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.imageView];

        self.label = [[UILabel alloc] init];
        self.label.numberOfLines = 2;
        self.label.textColor = [UIColor colorWithRed:(209.0/255.0) green:(209.0/255.0) blue:(209.0/255.0) alpha:1.0];
        self.label.font = [UIFont robotoRegularFontWithSize:20.0];
        self.label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.label];
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize labelSize = [self.label sizeThatFits:size];
    CGSize imageViewSize = [self.imageView sizeThatFits:size];
    return (CGSize){
        .width = MIN(MAX(labelSize.width, imageViewSize.width), 178.0),
        .height = labelSize.height + self.labelTopInset + imageViewSize.height,
    };
}

- (void)layoutSubviews {
    [super layoutSubviews];

    @weakify(self)
    [self.label installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.right(0.0);
        framer.sizeThatFits(self.frame.size);
        framer.bottom(0.0);
    }];
    [self.imageView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.right(0.0);
        framer.top(0.0);
        framer.bottom_to(self.label.nui_top, self.labelTopInset);
    }];
}

#pragma mark - Getters

- (CGFloat)labelTopInset {    
    return 17.0;
}

@end
