//
//  CPITunesSearchBarView.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchBarView.h"

#import "CPITunesSearchBarViewModel.h"

#import "UIColor+CP.h"
#import "UIFont+CP.h"

#import <ReactiveCocoa.h>
#import <Framer.h>

@interface CPITunesSearchBarView () <UISearchBarDelegate>

@property (nonatomic) UIView *topContainerView;
@property (nonatomic) UIView *bottomContainerView;
@property (nonatomic) UIButton *closeButton;
@property (nonatomic) UIButton *doneButton;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UISearchBar *searchBar;

@end

@implementation CPITunesSearchBarView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.topContainerView = [[UIView alloc] init];
        [self addSubview:self.topContainerView];

        self.bottomContainerView = [[UIView alloc] init];
        [self addSubview:self.bottomContainerView];
        
        self.closeButton = [[UIButton alloc] init];
        [self.closeButton setImage:[UIImage imageNamed:@"iconSearchCancel"] forState:UIControlStateNormal];
        [self.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.topContainerView addSubview:self.closeButton];

        self.doneButton = [[UIButton alloc] init];
        [self.doneButton setImage:[UIImage imageNamed:@"iconSearchDoneP"] forState:UIControlStateNormal];
        [self.doneButton addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.topContainerView addSubview:self.doneButton];

        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = [UIColor cpDarkGrayColor];
        self.titleLabel.font = [UIFont robotoMediumFontWithSize:16.0];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.topContainerView addSubview:self.titleLabel];
        
        self.searchBar = [[UISearchBar alloc] init];
        self.searchBar.delegate = self;
        self.searchBar.placeholder = NSLocalizedString(@"Track / artist name", nil);
        for (UIView *subview in [[self.searchBar.subviews lastObject] subviews]) {
            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                subview.alpha = 0.0;
                break;
            }
        }
        UITextField *searchTextField = [self.searchBar valueForKey:@"_searchField"];
        searchTextField.font = [UIFont robotoRegularFontWithSize:14.0];
        searchTextField.textColor = [UIColor cpDarkGrayColor];
        searchTextField.backgroundColor = [UIColor colorWithRed:(3.0/255.0f) green:(3.0/255.0f) blue:(3.0/255.0f) alpha:0.09];
        [self.bottomContainerView addSubview:self.searchBar];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    @weakify(self)
    [self.topContainerView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.right(0.0);
        framer.top(0.0);
        framer.height(self.frame.size.height / 2);
    }];
    [self.bottomContainerView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.right(0.0);
        framer.top_to(self.topContainerView.nui_bottom, 0.0);
        framer.bottom(0.0);
    }];
    [self.closeButton installFrames:^(NUIFramer * _Nonnull framer) {
        framer.super_centerY(0.0);
        framer.left(1.0);
        framer.width(45.0);
        framer.height(45.0);
    }];
    [self.doneButton installFrames:^(NUIFramer * _Nonnull framer) {
        framer.super_centerY(0.0);
        framer.right(1.0);
        framer.width(45.0);
        framer.height(45.0);
    }];
    [self.titleLabel installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.super_centerY(0.0);
        framer.super_centerX(0.0);
        framer.sizeThatFits((CGSize){
            .width = CGRectGetMinX(self.doneButton.frame) - CGRectGetMaxX(self.closeButton.frame),
            .height = CGRectGetHeight(self.topContainerView.frame),
        });
    }];
    [self.searchBar installFrames:^(NUIFramer * _Nonnull framer) {
        framer.edges(UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0));
    }];
}

#pragma mark - Setters/Getters

- (void)setViewModel:(CPITunesSearchBarViewModel *)viewModel {
    _viewModel = viewModel;
    self.titleLabel.text = _viewModel.title;
    self.searchBar.text = _viewModel.text;
    RAC(self.doneButton, enabled) = RACObserve(self.viewModel, doneButtonEnabled);
    RAC(self.closeButton, hidden) = RACObserve(self.viewModel, closeButtonHidden);
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - Actions

- (void)closeButtonPressed:(id)sender {
    [self.viewModel closeButtonPressed];
}

- (void)doneButtonPressed:(id)sender {
    [self.viewModel doneButtonPressed];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.viewModel textDidChange:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.viewModel searchButtonPressed];
}

@end
