//
//  CPITunesSearchViewOutput.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CPITunesTrack;

@protocol CPITunesSearchViewOutput <NSObject>

- (void)viewDidLoad;
- (void)viewWillAppear;
- (void)didTriggerPullToRefreshEvent;
- (void)didTriggerInfiniteScrollEvent;

@end
