//
//  CPITunesSearchViewTableViewCellItemsBuilder.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchViewTableViewCellItemsBuilder.h"

#import "CPITunesSearchTrackTableViewCellModel.h"
#import "CPITunesSearchTrackTableViewCellItem.h"
#import "CPITunesSearchViewOutput.h"
#import "CPITunesTrack.h"

#import <RSBTableViewManager/RSBTableViewSectionItem.h>
#import <ReactiveCocoa/RACEXTScope.h>

@implementation CPITunesSearchViewTableViewCellItemsBuilder

- (NSArray<RSBTableViewCellItem *> *)cellItemsWithCellModels:(nullable NSArray<CPITunesSearchTrackTableViewCellModel *> *)cellModels {
    NSMutableArray *cellItems = [[NSMutableArray alloc] init];
    [cellModels enumerateObjectsUsingBlock:^(CPITunesSearchTrackTableViewCellModel * _Nonnull cellModel, NSUInteger idx, BOOL * _Nonnull stop) {
        CPITunesSearchTrackTableViewCellItem *cellItem = [[CPITunesSearchTrackTableViewCellItem alloc] init];
        cellItem.cellModel = cellModel;
        [cellItems addObject:cellItem];
    }];
    return cellItems.copy;
}

- (BOOL)cellItem:(RSBTableViewCellItem *)cellItem containsCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel {
    if ([cellItem isKindOfClass:[CPITunesSearchTrackTableViewCellItem class]]) {
        CPITunesSearchTrackTableViewCellItem *trackCellItem = (id)cellItem;
        return [cellModel isEqual:trackCellItem.cellModel];
    }
    return NO;
}

@end
