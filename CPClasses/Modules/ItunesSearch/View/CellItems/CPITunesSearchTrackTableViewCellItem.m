//
//  CPITunesSearchTrackTableViewCellItem.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchTrackTableViewCellItem.h"

#import "CPITunesSearchTrackTableViewCellModel.h"
#import "CPITunesSearchTrackTableViewCell.h"
#import "CPITunesTrack.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation CPITunesSearchTrackTableViewCellItem

#pragma RSBTableViewCellItemProtocol

- (void)configureCell:(CPITunesSearchTrackTableViewCell *)cell inTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [super configureCell:cell inTableView:tableView atIndexPath:indexPath];
    cell.cellModel = self.cellModel;
}

- (CGFloat)heightInTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    return self.cellModel.height;
}

- (Class)registeredTableViewCellClass {
    return [CPITunesSearchTrackTableViewCell class];
}

- (void)didSelectInTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    if (self.cellModel.selected) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        self.cellModel.selected = NO;
    }
    else {
        self.cellModel.selected = YES;
    }
}

- (void)didDeselectInTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    self.cellModel.selected = NO;
}

@end
