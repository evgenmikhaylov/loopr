//
//  CPITunesSearchTrackTableViewCellItem.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <RSBTableViewManager/RSBTableViewCellItem.h>

@class CPITunesSearchTrackTableViewCellModel;

@interface CPITunesSearchTrackTableViewCellItem : RSBTableViewCellItem

@property (nonatomic) CPITunesSearchTrackTableViewCellModel *cellModel;

@end
