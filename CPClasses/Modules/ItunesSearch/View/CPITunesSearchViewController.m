//
//  CPiTunesSearchViewController.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchViewController.h"

#import "CPITunesSearchViewTableViewCellItemsBuilder.h"
#import "CPITunesSearchTrackTableViewCellModel.h"
#import "CPITunesSearchTrackTableViewCell.h"
#import "CPITunesSearchPlaceholderView.h"
#import "CPITunesSearchViewModel.h"
#import "CPITunesSearchBarView.h"
#import "CPITunesTrack.h"

#import "UIColor+CP.h"
#import "UIFont+CP.h"

#import <RSBPullToRefresh/UIScrollView+RefreshControls.h>
#import <RSBTableViewManager.h>
#import <ReactiveCocoa.h>
#import <Framer.h>

@interface CPITunesSearchViewController ()

@property (nonatomic) CPITunesSearchViewModel *viewModel;

@property (nonatomic, nonnull) UIView *placeholdersContainerView;
@property (nonatomic, nonnull) UILabel *noResultsLabel;
@property (nonatomic, nonnull) UILabel *noInternetLabel;
@property (nonatomic, nonnull) CPITunesSearchPlaceholderView *placeholderView;
@property (nonatomic, nonnull) CPITunesSearchBarView *searchBar;
@property (nonatomic, nonnull) UITableView *tableView;

@property (nonatomic, nonnull) RSBTableViewManager *tableViewManager;
@property (nonatomic, nonnull) RSBTableViewSectionItem *tableViewSectionItem;
@property (nonatomic, nonnull) CPITunesSearchViewTableViewCellItemsBuilder *tableViewCellItemsBuilder;

@property (nonatomic) CGRect keyboardFrame;

@end

@implementation CPITunesSearchViewController

- (void)dealloc {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.placeholdersContainerView = [[UIView alloc] init];
    [self.view addSubview:self.placeholdersContainerView];
    
    self.placeholderView = [[CPITunesSearchPlaceholderView alloc] init];
    self.placeholderView.label.text = self.viewModel.placeholderTitle;
    self.placeholderView.imageView.image = self.viewModel.placeholderImage;
    [self.placeholdersContainerView addSubview:self.placeholderView];

    self.noResultsLabel = [[UILabel alloc] init];
    self.noResultsLabel.text = self.viewModel.noResultsTitle;
    self.noResultsLabel.numberOfLines = 1;
    self.noResultsLabel.textColor = [UIColor colorWithRed:(209.0/255.0) green:(209.0/255.0) blue:(209.0/255.0) alpha:1.0];
    self.noResultsLabel.font = [UIFont robotoRegularFontWithSize:20.0];
    self.noResultsLabel.textAlignment = NSTextAlignmentCenter;
    [self.placeholdersContainerView addSubview:self.noResultsLabel];

    self.noInternetLabel = [[UILabel alloc] init];
    self.noInternetLabel.text = self.viewModel.noInternetTitle;
    self.noInternetLabel.numberOfLines = 1;
    self.noInternetLabel.textColor = [UIColor colorWithRed:(209.0/255.0) green:(209.0/255.0) blue:(209.0/255.0) alpha:1.0];
    self.noInternetLabel.font = [UIFont robotoRegularFontWithSize:20.0];
    self.noInternetLabel.textAlignment = NSTextAlignmentCenter;
    [self.placeholdersContainerView addSubview:self.noInternetLabel];
    
    self.searchBar = [[CPITunesSearchBarView alloc] init];
    self.searchBar.backgroundColor = [UIColor colorWithRed:(250.0/255.0) green:(250.0/255.0) blue:(250.0/255.0) alpha:0.9];
    self.searchBar.viewModel = self.viewModel.searchBarViewModel;
    [self.view addSubview:self.searchBar];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    @weakify(self)
    [self.tableView addPullToRefreshWithActionHandler:^{
        @strongify(self)
        [self.viewModel didTriggerPullToRefreshEvent];
    }];
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        @strongify(self)
        [self.viewModel didTriggerInfiniteScrollEvent];
    }];
    self.tableViewManager = [[RSBTableViewManager alloc] initWithTableView:self.tableView];
    
    [[RACObserve(self.viewModel, triggerPullToRefresh) ignore:@NO] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView triggerPullToRefresh];
    }];
    RAC(self.tableView.pullToRefreshControl, enabled) = RACObserve(self.viewModel, enablePullToRefresh);
    [[RACObserve(self.viewModel, endLoadingPullToRefresh) ignore:@NO] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView.pullToRefreshControl endLoading];
    }];
    [[RACObserve(self.viewModel, endLoadingInfiniteScrolling) ignore:@NO] subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView reloadData];
        [self.tableView.infiniteScrollingRefreshControl endLoading];
    }];
    [RACObserve(self.viewModel, hideKeyboard) subscribeNext:^(id x) {
        @strongify(self)
        if ([x boolValue]) {
            [self.searchBar.searchBar resignFirstResponder];
        } else {
            [self.searchBar.searchBar becomeFirstResponder];
        }
    }];
    RAC(self.placeholderView, hidden) = RACObserve(self.viewModel, hidePlaceholder);
    RAC(self.noResultsLabel, hidden) = RACObserve(self.viewModel, hideNoResultsTitle);
    RAC(self.noInternetLabel, hidden) = RACObserve(self.viewModel, hideNoInternetTitle);
    RAC(self.tableView.infiniteScrollingRefreshControl, enabled) = RACObserve(self.viewModel, enableInfiniteScrolling);
    [[RACObserve(self.viewModel, closeModule) ignore:@NO] subscribeNext:^(id x) {
        @strongify(self)
        if ([self.delegate respondsToSelector:@selector(iTunesSearchViewControllerDidCancel:)]) {
            [self.delegate iTunesSearchViewControllerDidCancel:self];
        }
    }];
    [self.viewModel.rac_firstSongsSignal subscribeNext:^(NSArray<CPITunesSearchTrackTableViewCellModel *> *cellModels) {
        @strongify(self)
        [self displayCellModels:cellModels];
    }];
    [self.viewModel.rac_nextSongsSignal subscribeNext:^(NSArray<CPITunesSearchTrackTableViewCellModel *> *cellModels) {
        @strongify(self)
        [self addCellModels:cellModels];
    }];
    [self.viewModel.rac_songUpdatedSignal subscribeNext:^(CPITunesSearchTrackTableViewCellModel *cellModel) {
        @strongify(self)
        [self reloadCellModel:cellModel];
    }];
    [self.viewModel.rac_selectSongSignal subscribeNext:^(CPITunesSearchTrackTableViewCellModel *cellModel) {
        @strongify(self)
        [self selectCellModel:cellModel];
    }];
    [self.viewModel.rac_donePressedSignal subscribeNext:^(CPITunesSearchTrackTableViewCellModel *cellModel) {
        @strongify(self)
        if ([self.delegate respondsToSelector:@selector(iTunesSearchViewController:didPickTrack:)]) {
            [self.delegate iTunesSearchViewController:self didPickTrack:cellModel.object];
        }
    }];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    [self.viewModel viewDidLoad];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    @weakify(self)
    [self.searchBar installFrames:^(NUIFramer * _Nonnull framer) {
        framer.left(0.0);
        framer.right(0.0);
        framer.top([UIApplication sharedApplication].statusBarFrame.size.height);
        framer.height(88.0);
    }];
    [self.placeholdersContainerView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.right(0.0);
        framer.top_to(self.searchBar.nui_bottom, 0.0);
        framer.bottom(self.keyboardFrame.size.height);
    }];
    [self.placeholderView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.sizeThatFits(self.placeholdersContainerView.frame.size);
        framer.super_centerY(0.0);
        framer.super_centerX(0.0);
    }];
    [self.noResultsLabel installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.sizeThatFits(self.placeholdersContainerView.frame.size);
        framer.super_centerY(0.0);
        framer.super_centerX(0.0);
    }];
    [self.noInternetLabel installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.sizeThatFits(self.placeholdersContainerView.frame.size);
        framer.super_centerY(0.0);
        framer.super_centerX(0.0);
    }];
    [self.tableView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.right(0.0);
        framer.top_to(self.searchBar.nui_bottom, 0.0);
        framer.bottom(0.0);
    }];
    UIEdgeInsets tableViewInsets = (UIEdgeInsets){
        .bottom = self.keyboardFrame.size.height,
    };
    self.tableView.contentInset = tableViewInsets;
    self.tableView.scrollIndicatorInsets = tableViewInsets;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.viewModel viewWillAppear];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Setters/Getters

- (CPITunesSearchViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel = [[CPITunesSearchViewModel alloc] init];
    }
    return _viewModel;
}

- (CPITunesSearchViewTableViewCellItemsBuilder *)tableViewCellItemsBuilder {
    if (!_tableViewCellItemsBuilder) {
        _tableViewCellItemsBuilder = [[CPITunesSearchViewTableViewCellItemsBuilder alloc] init];
        _tableViewCellItemsBuilder.output = self.viewModel;
    }
    return _tableViewCellItemsBuilder;
}

- (RSBTableViewSectionItem *)tableViewSectionItem {
    if (!_tableViewSectionItem) {
        _tableViewSectionItem = [[RSBTableViewSectionItem alloc] init];
    }
    return _tableViewSectionItem;
}

#pragma mark - TableView

- (void)displayCellModels:(NSArray<CPITunesSearchTrackTableViewCellModel *> *)cellModels {
    self.tableViewSectionItem.cellItems = [self.tableViewCellItemsBuilder cellItemsWithCellModels:cellModels];
    self.tableViewManager.sectionItems = @[self.tableViewSectionItem];
}

- (void)addCellModels:(NSArray<CPITunesSearchTrackTableViewCellModel *> *)cellModels {
    NSArray<RSBTableViewCellItem *> *cellItems = [self.tableViewCellItemsBuilder cellItemsWithCellModels:cellModels];
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(self.tableViewSectionItem.cellItems.count, cellItems.count)];
    [self.tableViewManager insertCellItems:cellItems
                             toSectionItem:self.tableViewSectionItem
                                 atIndexes:indexes 
                          withRowAnimation:UITableViewRowAnimationNone];
}

- (void)reloadCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel {
    [self.tableViewSectionItem.cellItems enumerateObjectsUsingBlock:^(RSBTableViewCellItem   * _Nonnull cellItem, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.tableViewCellItemsBuilder cellItem:cellItem containsCellModel:cellModel]) {
            NSUInteger section = [self.tableViewManager.sectionItems indexOfObject:self.tableViewSectionItem];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:section];
            CPITunesSearchTrackTableViewCell *cell = (id)[self.tableView cellForRowAtIndexPath:indexPath];
            if (cell) {
                cell.cellModel = cellModel;
            }
            *stop = YES;
        }
    }];
}

- (void)selectCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel {
    [self.tableViewSectionItem.cellItems enumerateObjectsUsingBlock:^(RSBTableViewCellItem   * _Nonnull cellItem, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.tableViewCellItemsBuilder cellItem:cellItem containsCellModel:cellModel]) {
            NSUInteger section = [self.tableViewManager.sectionItems indexOfObject:self.tableViewSectionItem];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:section];
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            *stop = YES;
        }
    }];
}

#pragma mark - Notifications

- (void)keyboardWillShowNotification:(NSNotification *)notification {
    
    NSDictionary *info = notification.userInfo;
    self.keyboardFrame = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification {

    self.keyboardFrame = CGRectZero;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

@end
