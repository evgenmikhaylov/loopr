//
//  CPITunesSearchTrackTableViewCell.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSBLoaderView;
@class CPITunesSearchTrackTableViewCellModel;

@interface CPITunesSearchTrackTableViewCell : UITableViewCell

@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UILabel *subtitleLabel;
@property (nonatomic, readonly) UIImageView *trackImageView;
@property (nonatomic, readonly) RSBLoaderView *loaderView;

@property (nonatomic) CPITunesSearchTrackTableViewCellModel *cellModel;

@end
