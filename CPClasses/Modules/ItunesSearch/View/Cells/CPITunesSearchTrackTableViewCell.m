//
//  CPITunesSearchTrackTableViewCell.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchTrackTableViewCell.h"

#import "CPITunesSearchTrackTableViewCellModel.h"
#import "UIColor+CP.h"
#import "UIFont+CP.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <ReactiveCocoa.h>
#import <RSBLoaderView.h>
#import <Framer.h>

@interface CPITunesSearchTrackTableViewCell ()

@property (nonatomic) UIView *selectionView;
@property (nonatomic) UIView *containerView;
@property (nonatomic) UIImageView *trackImageView;
@property (nonatomic) RSBLoaderView *loaderView;
@property (nonatomic) UIView *titlesContainerView;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *subtitleLabel;
@property (nonatomic) UIView *separatorView;

@property (nonatomic, readonly) UIEdgeInsets contentInsets;

@end

@implementation CPITunesSearchTrackTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = self.backgroundColor;

        self.selectionView = [[UIView alloc] init];
        [self.contentView addSubview:self.selectionView];
        
        self.containerView = [[UIView alloc] init];
        [self.contentView addSubview:self.containerView];

        self.trackImageView = [[UIImageView alloc] init];
        self.trackImageView.clipsToBounds = YES;
        self.trackImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.trackImageView.backgroundColor = [UIColor cpDarkGrayColor];
        [self.containerView addSubview:self.trackImageView];
        
        self.loaderView = [[RSBLoaderView alloc] init];
        self.loaderView.lineWidth = 2.0;
        self.loaderView.fillColor = [UIColor whiteColor];
        self.loaderView.circleColor = [UIColor clearColor];
        [self.contentView addSubview:self.loaderView];
        
        self.titlesContainerView = [[UIView alloc] init];
        [self.containerView addSubview:self.titlesContainerView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.font = [UIFont robotoRegularFontWithSize:16.0];
        self.titleLabel.textColor = [UIColor cpDarkGrayColor];
        self.titleLabel.numberOfLines = 1;
        [self.titlesContainerView addSubview:self.titleLabel];
        
        self.subtitleLabel = [[UILabel alloc] init];
        self.subtitleLabel.font = [UIFont robotoRegularFontWithSize:14.0];
        self.subtitleLabel.textColor = [[UIColor cpDarkGrayColor] colorWithAlphaComponent:0.5];
        self.subtitleLabel.numberOfLines = 1;
        [self.titlesContainerView addSubview:self.subtitleLabel];
        
        self.separatorView = [[UIView alloc] init];
        self.separatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
        [self.contentView addSubview:self.separatorView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    @weakify(self)
    [self.selectionView installFrames:^(NUIFramer * _Nonnull framer) {
        framer.edges(UIEdgeInsetsZero);
    }];
    [self.containerView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.edges(self.contentInsets);
    }];
    [self.trackImageView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(0.0);
        framer.top(0.0);
        framer.bottom(0.0);
        framer.width(self.containerView.frame.size.height);
    }];
    [self.loaderView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.centerX_to(self.trackImageView.nui_centerX, 0.0);
        framer.centerY_to(self.trackImageView.nui_centerY, 0.0);
        framer.width(24.0);
        framer.height(24.0);
    }];
    [self.titlesContainerView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left_to(self.trackImageView.nui_right, 16.0);
        framer.top(0.0);
        framer.bottom(0.0);
        framer.right(0.0);
    }];
    [self.titleLabel installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.top(0);
        framer.left(0);
        framer.sizeThatFits(self.titlesContainerView.frame.size);
    }];
    [self.subtitleLabel installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.top_to(self.titleLabel.nui_bottom, 2);
        framer.left(0);
        framer.sizeThatFits(self.titlesContainerView.frame.size);
    }];
    [self.titlesContainerView installFrames:^(NUIFramer * _Nonnull framer) {
        framer.container();
        framer.super_centerY(0.0);
    }];
    [self.separatorView installFrames:^(NUIFramer * _Nonnull framer) {
        @strongify(self)
        framer.left(self.contentInsets.left);
        framer.right(0.0);
        framer.bottom(0.0);
        framer.height(1.0);
    }];
}

#pragma mark - Setters/Getters

- (void)setCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel {
    _cellModel = cellModel;
    
    [self.trackImageView sd_setImageWithURL:_cellModel.imageURL placeholderImage:_cellModel.placeholderImage];
    self.titleLabel.text = _cellModel.title;
    self.subtitleLabel.text = _cellModel.subtitle;
    self.selectionStyle = _cellModel.selectionStyle;
    [self selectCell:_cellModel.selected];
    if (_cellModel.loaderHidden) {
        self.loaderView.hidden = YES;
        [self.loaderView stopAnimating];
    } else {
        self.loaderView.hidden = NO;
        [self.loaderView startAnimating];
    }
}

- (UIEdgeInsets)contentInsets {
    return UIEdgeInsetsMake(12.0, 16.0, 12.0, 16.0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self selectCell:selected];
}

#pragma mark - Helpers

- (void)selectCell:(BOOL)selected {
    
    if (selected) {
        self.selectionView.backgroundColor = [UIColor colorWithRed:(213.0/255.0f) green:(218.0/255.0f) blue:(220.0/255.0f) alpha:1.0];
    } else {
        self.selectionView.backgroundColor = [UIColor whiteColor];
    }
}

@end
