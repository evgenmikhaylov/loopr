//
//  CPITunesSearchViewTableViewCellItemsBuilder.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CPITunesSearchViewOutput;
@class CPITunesSearchTrackTableViewCellModel;
@class RSBTableViewCellItem;
@class CPITunesTrack;

NS_ASSUME_NONNULL_BEGIN

@interface CPITunesSearchViewTableViewCellItemsBuilder : NSObject

@property (nonatomic) id<CPITunesSearchViewOutput> output;

- (NSArray<RSBTableViewCellItem *> *)cellItemsWithCellModels:(nullable NSArray<CPITunesSearchTrackTableViewCellModel *> *)cellModels;
- (BOOL)cellItem:(RSBTableViewCellItem *)cellItem containsCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel;

@end

NS_ASSUME_NONNULL_END
