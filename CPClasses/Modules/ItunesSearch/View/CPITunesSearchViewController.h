//
//  CPiTunesSearchViewController.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPITunesSearchViewController;
@class CPITunesTrack;

@protocol CPITunesSearchViewControllerDelegate <NSObject>

@optional
- (void)iTunesSearchViewController:(CPITunesSearchViewController *)controller didPickTrack:(CPITunesTrack *)track;
- (void)iTunesSearchViewControllerDidCancel:(CPITunesSearchViewController *)controller;

@end

@interface CPITunesSearchViewController : UIViewController

@property (nonatomic) id<CPITunesSearchViewControllerDelegate> delegate;

@end
