//
//  CPITunesSearchPlayer.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchPlayer.h"
#import "CPITunesTrack.h"
#import <AVFoundation/AVFoundation.h>

@interface CPITunesSearchPlayer ()

@property (nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic) NSURL *currentFileURL;

@end

@implementation CPITunesSearchPlayer

- (void)startPlayingFileWithURL:(NSURL *)fileURL {
    if ([fileURL isEqual:self.currentFileURL]) {
        return;
    }
    self.currentFileURL = fileURL;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    self.audioPlayer.numberOfLoops = -1;
    [self.audioPlayer play];
}

- (void)stopPlayingFileWithURL:(NSURL *)fileURL {
    if (![fileURL isEqual:self.currentFileURL]) {
        return;
    }
    [self stopPlayingForce];
}

- (void)stopPlayingForce {
    
    self.currentFileURL = nil;
    [self.audioPlayer stop];
    self.audioPlayer = nil;
}

@end
