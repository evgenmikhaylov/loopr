//
//  CPITunesSearchPlayer.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CPITunesSearchPlayer : NSObject

- (void)startPlayingFileWithURL:(NSURL *)fileURL;
- (void)stopPlayingFileWithURL:(NSURL *)fileURL;
- (void)stopPlayingForce;

@end
