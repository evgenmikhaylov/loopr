    //
//  CPiTunesSearchService.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchService.h"
#import "CPITunesTrack.h"
#import <ReactiveCocoa.h>
#import <AFNetworking.h>

const NSUInteger CPITunesSearchPageLimit = 20;

@interface CPITunesSearchService ()

@property (nonatomic) AFHTTPSessionManager *sessionManager;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL lastPage;

@end

@implementation CPITunesSearchService

- (AFHTTPSessionManager *)sessionManager {
    if (!_sessionManager) {
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://itunes.apple.com"]];
    }
    return _sessionManager;
}

- (NSURLSessionDataTask *)fetchSongsWithTerm:(NSString *)term
                                       limit:(NSUInteger)limit
                                      offset:(NSUInteger)offset
                                  completion:(void(^)(NSArray<CPITunesTrack *> *tracks, BOOL lastPage, NSError *error))completion {
    NSString *path = [NSString stringWithFormat:@"/search?entity=song&term=%@&limit=%lu&offset=%lu", term, (unsigned long)limit, (unsigned long)offset];
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return [self.sessionManager POST:path parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject[@"results"] isKindOfClass:[NSArray class]]) {
            NSMutableArray<CPITunesTrack *> *tracks = [[NSMutableArray alloc] init];
            [responseObject[@"results"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CPITunesTrack *track = [CPITunesTrack objectWithProperties:obj];
                [tracks addObject:track];
            }];
            BOOL lastPage = [responseObject[@"resultCount"] intValue] < CPITunesSearchPageLimit;
            if (completion) {
                completion(tracks.copy, lastPage, nil);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (completion) {
            completion(nil, NO, error);
        }
    }];
    return nil;
}

- (NSURLSessionDownloadTask *)downloadFileWithURL:(NSURL *)fileURL
                                         localURL:(NSURL *)localURL
                                       completion:(void(^)(NSError *error))completion {
    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    NSURLSessionDownloadTask *downloadTask = [sessionManager downloadTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return localURL;
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (completion) {
            completion(error);
        }
    }];
    [downloadTask resume];
    return downloadTask;
}

- (RACSignal *)rac_fetchFirstSongsSignalWithTerm:(NSString *)term {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURLSessionDataTask *task = [self fetchSongsWithTerm:term limit:CPITunesSearchPageLimit offset:0 completion:^(NSArray<CPITunesTrack *> *tracks, BOOL lastPage, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                self.lastPage = lastPage;
                self.currentPage = 0;
                [subscriber sendNext:tracks];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
        return nil;
    }];
}

- (RACSignal *)rac_fetchNextSongsSignalWithTerm:(NSString *)term {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSUInteger offset = (self.currentPage + 1) * CPITunesSearchPageLimit;
        NSURLSessionDataTask *task = [self fetchSongsWithTerm:term limit:CPITunesSearchPageLimit offset:offset completion:^(NSArray<CPITunesTrack *> *tracks, BOOL lastPage, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                self.lastPage = lastPage;
                self.currentPage++;
                [subscriber sendNext:tracks];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }];
}

- (RACSignal *)rac_downloadPreviewForTrack:(CPITunesTrack *)track {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURL *localURL = [self previewFileURLForTrack:track];
        NSURLSessionDownloadTask *task = nil;
        if ([[NSFileManager defaultManager] fileExistsAtPath:localURL.path]) {
            track.localPreviewURL = localURL;
            [subscriber sendNext:localURL];
            [subscriber sendCompleted];
        }
        else {
            task = [self downloadFileWithURL:track.previewURL localURL:localURL completion:^(NSError *error) {
                if (error) {
                    [subscriber sendError:error];
                }
                else {
                    track.localPreviewURL = localURL;
                    [subscriber sendNext:localURL];
                    [subscriber sendCompleted];
                }
            }];
        }
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }];
}

- (NSURL *)previewFileURLForTrack:(CPITunesTrack *)track {
    NSString *fileName = [NSString stringWithFormat:@"%lu.%@", (unsigned long)track.objectID, track.previewURL.pathExtension];
    return [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:fileName]];
}

@end
