//
//  CPiTunesSearchService.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CPITunesTrack;
@class RACSignal;

@interface CPITunesSearchService : NSObject

@property (nonatomic, readonly) BOOL lastPage;

- (RACSignal *)rac_fetchFirstSongsSignalWithTerm:(NSString *)term;
- (RACSignal *)rac_fetchNextSongsSignalWithTerm:(NSString *)term;
- (RACSignal *)rac_downloadPreviewForTrack:(CPITunesTrack *)track;

- (NSURL *)previewFileURLForTrack:(CPITunesTrack *)track;

@end
