//
//  CPiTunesTrack.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPBaseObject.h"

@interface CPITunesTrack : CPBaseObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *album;
@property (nonatomic) NSString *artist;
@property (nonatomic) NSURL *previewURL;
@property (nonatomic) NSURL *imageURL;

@property (nonatomic) NSURL *localPreviewURL;

@end
