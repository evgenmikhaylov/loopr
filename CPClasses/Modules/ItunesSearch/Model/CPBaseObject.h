//
//  CPBaseObject.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping.h>

@interface CPBaseObject : NSObject <EKMappingProtocol, NSCoding>

@property (nonatomic) NSUInteger objectID;
@property (nonatomic) NSString *objectDescription;

+ (EKObjectMapping *)objectMapping;
+ (instancetype)objectWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;
- (void)fillWithProperties:(NSDictionary *)properties;
- (NSDictionary *)dictionaryRepresentation;

@end
