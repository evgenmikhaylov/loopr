//
//  CPBaseObject.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPBaseObject.h"

@implementation CPBaseObject

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [[EKObjectMapping alloc] initWithObjectClass:[self class]];
    mapping.ignoreMissingFields = YES;
    [mapping mapKeyPath:@"id" toProperty:@"objectID"];
    [mapping mapKeyPath:@"description" toProperty:@"objectDescription"];
    return mapping;
}

+ (instancetype)objectWithProperties:(NSDictionary *)properties{
    return [[self alloc] initWithProperties:properties];
}

- (instancetype)initWithProperties:(NSDictionary *)properties {
    if (self = [super init]) {
        [self fillWithProperties:properties];
    }
    return self;
}

- (void)fillWithProperties:(NSDictionary *)properties {
    EKObjectMapping *objectMapping = [[self class] objectMapping];
    [EKMapper fillObject:self fromExternalRepresentation:properties withMapping:objectMapping];
}

- (NSDictionary *)dictionaryRepresentation{
    return [EKSerializer serializeObject:self withMapping:[[self class] objectMapping]];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        NSDictionary *properties = [aDecoder decodeObjectForKey:@"dictionaryRepresentation"];
        [self fillWithProperties:properties];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self dictionaryRepresentation] forKey:@"dictionaryRepresentation"];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    CPBaseObject *copy = [[[self class] alloc] init];
    if (copy) {
        [copy fillWithProperties:[self dictionaryRepresentation]];
    }
    return copy;
}

@end
