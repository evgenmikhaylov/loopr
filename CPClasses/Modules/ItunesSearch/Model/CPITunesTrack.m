//
//  CPiTunesTrack.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesTrack.h"
#import "EKObjectMapping+Helpers.h"

@implementation CPITunesTrack

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapKeyPath:@"trackId" toProperty:@"objectID"];
    [mapping mapKeyPath:@"trackName" toProperty:@"name"];
    [mapping mapKeyPath:@"collectionName" toProperty:@"album"];
    [mapping mapKeyPath:@"artistName" toProperty:@"artist"];
    [mapping mapURLFromKeyPath:@"previewUrl" toProperty:@"previewURL"];
    [mapping mapURLFromKeyPath:@"artworkUrl100" toProperty:@"imageURL"];
    return mapping;
}

@end
