//
//  CPITunesSearchBarViewModelOutput.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@protocol CPITunesSearchBarViewModelOutput <NSObject>

@property (nonatomic) BOOL doneButtonEnabled;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *text;
@property (nonatomic, readonly) RACSignal *rac_closeButtonPressedSignal;
@property (nonatomic, readonly) RACSignal *rac_doneButtonPressedSignal;
@property (nonatomic, readonly) RACSignal *rac_searchButtonPressedSignal;

@end
