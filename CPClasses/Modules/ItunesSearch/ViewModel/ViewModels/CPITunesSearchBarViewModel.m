//
//  CPITunesSearchBarViewModel.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchBarViewModel.h"
#import <ReactiveCocoa.h>

static NSString * const CPITunesSearchBarViewModelTextKey = @"CPITunesSearchBarViewModelTextKey";

@interface CPITunesSearchBarViewModel ()

@property (nonatomic) NSString *text;
@property (nonatomic) RACSubject *rac_closeButtonPressedSignal;
@property (nonatomic) RACSubject *rac_doneButtonPressedSignal;
@property (nonatomic) RACSubject *rac_searchButtonPressedSignal;

@end

@implementation CPITunesSearchBarViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.text = [[NSUserDefaults standardUserDefaults] objectForKey:CPITunesSearchBarViewModelTextKey];
    }
    return self;
}

#pragma mark - CPITunesSearchBarViewModelOutput

- (NSString *)title {
    return NSLocalizedString(@"Search Music", nil);
}

- (RACSubject *)rac_closeButtonPressedSignal {
    if (!_rac_closeButtonPressedSignal) {
        _rac_closeButtonPressedSignal = [RACSubject subject];
    }
    return _rac_closeButtonPressedSignal;
}

- (RACSubject *)rac_doneButtonPressedSignal {
    if (!_rac_doneButtonPressedSignal) {
        _rac_doneButtonPressedSignal = [RACSubject subject];
    }
    return _rac_doneButtonPressedSignal;
}

- (RACSubject *)rac_searchButtonPressedSignal {
    if (!_rac_searchButtonPressedSignal) {
        _rac_searchButtonPressedSignal = [RACSubject subject];
    }
    return _rac_searchButtonPressedSignal;
}

#pragma mark - CPITunesSearchBarViewOutput

- (void)textDidChange:(NSString *)text {
    self.text = text;
    [[NSUserDefaults standardUserDefaults] setObject:text forKey:CPITunesSearchBarViewModelTextKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)closeButtonPressed {
    [self.rac_closeButtonPressedSignal sendNext:nil];
}

- (void)doneButtonPressed {
    [self.rac_doneButtonPressedSignal sendNext:nil];
}

- (void)searchButtonPressed {
    [self.rac_searchButtonPressedSignal sendNext:nil];
}

@end
