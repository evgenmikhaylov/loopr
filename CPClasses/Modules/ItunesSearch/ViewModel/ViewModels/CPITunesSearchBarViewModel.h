//
//  CPITunesSearchBarViewModel.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchBarViewModelOutput.h"
#import "CPITunesSearchBarViewOutput.h"

#import <Foundation/Foundation.h>

@interface CPITunesSearchBarViewModel : NSObject <CPITunesSearchBarViewOutput, CPITunesSearchBarViewModelOutput>

@property (nonatomic) BOOL doneButtonEnabled;
@property (nonatomic) BOOL closeButtonHidden;

@end
