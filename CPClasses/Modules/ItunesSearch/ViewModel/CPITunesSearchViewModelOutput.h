//
//  CPITunesSearchViewModelOutput.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPITunesSearchBarViewModel;
@class RACSignal;

@protocol CPITunesSearchViewModelOutput <NSObject>

@property (nonatomic, readonly) BOOL triggerPullToRefresh;
@property (nonatomic, readonly) BOOL enablePullToRefresh;
@property (nonatomic, readonly) BOOL endLoadingPullToRefresh;
@property (nonatomic, readonly) BOOL endLoadingInfiniteScrolling;
@property (nonatomic, readonly) BOOL enableInfiniteScrolling;
@property (nonatomic, readonly) BOOL hideKeyboard;
@property (nonatomic, readonly) BOOL hidePlaceholder;
@property (nonatomic, readonly) BOOL hideNoResultsTitle;
@property (nonatomic, readonly) BOOL hideNoInternetTitle;
@property (nonatomic, readonly) BOOL closeModule;

@property (nonatomic, readonly) UIImage *placeholderImage;
@property (nonatomic, readonly) NSString *placeholderTitle;
@property (nonatomic, readonly) NSString *noResultsTitle;
@property (nonatomic, readonly) NSString *noInternetTitle;

@property (nonatomic, readonly) RACSignal *rac_firstSongsSignal;
@property (nonatomic, readonly) RACSignal *rac_nextSongsSignal;
@property (nonatomic, readonly) RACSignal *rac_songUpdatedSignal;
@property (nonatomic, readonly) RACSignal *rac_selectSongSignal;
@property (nonatomic, readonly) RACSignal *rac_donePressedSignal;

@property (nonatomic, readonly) CPITunesSearchBarViewModel *searchBarViewModel;

@end
