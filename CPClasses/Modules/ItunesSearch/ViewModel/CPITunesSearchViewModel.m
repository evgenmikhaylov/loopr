//
//  CPItunesSearchViewModel.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchViewModel.h"

#import "CPITunesSearchTrackTableViewCellModel.h"
#import "CPITunesSearchBarViewModel.h"
#import "CPITunesSearchService.h"
#import "CPITunesSearchPlayer.h"

#import "CPITunesTrack.h"
#import <ReactiveCocoa.h>

@interface CPITunesSearchViewModel ()

@property (nonatomic) CPITunesSearchService *searchService;
@property (nonatomic) CPITunesSearchPlayer *searchPlayer;

@property (nonatomic) BOOL triggerPullToRefresh;
@property (nonatomic) BOOL enablePullToRefresh;
@property (nonatomic) BOOL endLoadingPullToRefresh;
@property (nonatomic) BOOL endLoadingInfiniteScrolling;
@property (nonatomic) BOOL enableInfiniteScrolling;
@property (nonatomic) BOOL hideKeyboard;
@property (nonatomic) BOOL hidePlaceholder;
@property (nonatomic) BOOL hideNoResultsTitle;
@property (nonatomic) BOOL hideNoInternetTitle;
@property (nonatomic) BOOL closeModule;

@property (nonatomic) RACSubject *rac_firstSongsSignal;
@property (nonatomic) RACSubject *rac_nextSongsSignal;
@property (nonatomic) RACSubject *rac_songUpdatedSignal;
@property (nonatomic) RACSubject *rac_selectSongSignal;
@property (nonatomic) RACSubject *rac_donePressedSignal;

@property (nonatomic) CPITunesSearchBarViewModel *searchBarViewModel;
@property (nonatomic) NSArray<CPITunesSearchTrackTableViewCellModel *> *cellModels;

@end

@implementation CPITunesSearchViewModel

#pragma mark - Setters/Getters

- (CPITunesSearchService *)searchService {
    if (!_searchService) {
        _searchService = [[CPITunesSearchService alloc] init];
    }
    return _searchService;
}

- (CPITunesSearchPlayer *)searchPlayer {
    if (!_searchPlayer) {
        _searchPlayer = [[CPITunesSearchPlayer alloc] init];
    }
    return _searchPlayer;
}

- (RACSubject *)rac_firstSongsSignal {
    if (!_rac_firstSongsSignal) {
        _rac_firstSongsSignal = [RACSubject subject];
    }
    return _rac_firstSongsSignal;
}

- (RACSubject *)rac_nextSongsSignal {
    if (!_rac_nextSongsSignal) {
        _rac_nextSongsSignal = [RACSubject subject];
    }
    return _rac_nextSongsSignal;
}

- (RACSubject *)rac_selectSongSignal {
    if (!_rac_selectSongSignal) {
        _rac_selectSongSignal = [RACSubject subject];
    }
    return _rac_selectSongSignal;
}

- (RACSubject *)rac_songUpdatedSignal {
    if (!_rac_songUpdatedSignal) {
        _rac_songUpdatedSignal = [RACSubject subject];
    }
    return _rac_songUpdatedSignal;
}

- (RACSubject *)rac_donePressedSignal {
    if (!_rac_donePressedSignal) {
        _rac_donePressedSignal = [RACSubject subject];
    }
    return _rac_donePressedSignal;
}

- (CPITunesSearchBarViewModel *)searchBarViewModel {
    if (!_searchBarViewModel) {
        _searchBarViewModel = [[CPITunesSearchBarViewModel alloc] init];
    }
    return _searchBarViewModel;
}

- (UIImage *)placeholderImage {    
    return [UIImage imageNamed:@"imgSearchPlaceholder"];
}

- (NSString *)placeholderTitle {
    return NSLocalizedString(@"Here you can find all kinds of music", nil);
}

- (NSString *)noResultsTitle {
    return NSLocalizedString(@"No results", nil);
}

- (NSString *)noInternetTitle {
    return NSLocalizedString(@"No internet connection", nil);
}

#pragma mark - CPITunesSearchViewOutput

- (void)viewDidLoad {
    
    self.hideKeyboard = NO;
    self.enablePullToRefresh = NO;
    self.hidePlaceholder = NO;
    self.hideNoResultsTitle = YES;
    self.hideNoInternetTitle = YES;
    self.searchBarViewModel.closeButtonHidden = NO;
    
    RAC(self,enableInfiniteScrolling) = [RACObserve(self.searchService, lastPage) not];
    @weakify(self)
    [self.searchBarViewModel.rac_closeButtonPressedSignal subscribeNext:^(id x) {
        @strongify(self)
        [self.searchPlayer stopPlayingForce];
        self.hideKeyboard = YES;
        self.closeModule = YES;
    }];
    [self.searchBarViewModel.rac_doneButtonPressedSignal subscribeNext:^(id x) {
        @strongify(self)
        [self.searchPlayer stopPlayingForce];
        [self.rac_donePressedSignal sendNext:self.readyCellModel];
    }];
    [self.searchBarViewModel.rac_searchButtonPressedSignal subscribeNext:^(id x) {
        @strongify(self)
        [self startSearch];
    }];
    if (self.searchBarViewModel.text.length > 0) {
        [self startSearch];
    }
}

- (void)viewWillAppear {
}

- (void)didTriggerPullToRefreshEvent {
    @weakify(self)
    [[[[self.searchService rac_fetchFirstSongsSignalWithTerm:self.searchBarViewModel.text]
      initially:^{
          @strongify(self)
          [self updateDoneButton];
          [self.searchPlayer stopPlayingForce];
          self.hidePlaceholder = YES;
      }] finally:^{
          @strongify(self)
          self.endLoadingPullToRefresh = YES;
      }] subscribeNext:^(NSArray<CPITunesTrack *> *tracks) {
          @strongify(self)
          NSArray<CPITunesSearchTrackTableViewCellModel *> *cellModels = [self cellModelsWithObjects:tracks];
          self.cellModels = cellModels;
          [self.rac_firstSongsSignal sendNext:cellModels];
          if (tracks.count == 0) {
              self.hideNoResultsTitle = NO;
              self.enablePullToRefresh = NO;
          } else {
              self.hideNoResultsTitle = YES;
              self.enablePullToRefresh = YES;
          }
      } error:^(NSError *error) {
          @strongify(self)
          if (error.code == NSURLErrorNotConnectedToInternet) {
              [self.rac_firstSongsSignal sendNext:nil];
              self.hideNoInternetTitle = NO;
          }
      }];
}

- (void)didTriggerInfiniteScrollEvent {
    @weakify(self)
    [[[self.searchService rac_fetchNextSongsSignalWithTerm:self.searchBarViewModel.text]
      finally:^{
          self.endLoadingInfiniteScrolling = YES;
      }] subscribeNext:^(NSArray<CPITunesTrack *> *tracks) {
          @strongify(self)
          NSArray<CPITunesSearchTrackTableViewCellModel *> *cellModels = [self cellModelsWithObjects:tracks];
          self.cellModels = [self.cellModels arrayByAddingObjectsFromArray:cellModels];
          [self.rac_nextSongsSignal sendNext:cellModels];
          [self.rac_selectSongSignal sendNext:self.selectedCellModel];
      }];
}

#pragma mark - Helpers

- (void)startSearch {
    self.hideNoResultsTitle = YES;
    self.hideNoInternetTitle = YES;
    self.hideKeyboard = YES;
    [self.rac_firstSongsSignal sendNext:nil];
    self.triggerPullToRefresh = YES;
}

- (CPITunesSearchTrackTableViewCellModel *)readyCellModel {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"selected == YES AND loaderHidden == YES"];
    NSArray *selectedCellModels = [self.cellModels filteredArrayUsingPredicate:predicate];
    return selectedCellModels.firstObject;
}

- (CPITunesSearchTrackTableViewCellModel *)selectedCellModel {
    NSArray *selectedCellModels = [self selectedCellModels];
    return selectedCellModels.firstObject;
}

- (NSArray<CPITunesSearchTrackTableViewCellModel *> *)selectedCellModels {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"selected == YES"];
    NSArray *selectedCellModels = [self.cellModels filteredArrayUsingPredicate:predicate];
    return selectedCellModels;
}

- (void)updateDoneButton {
    self.searchBarViewModel.doneButtonEnabled = (self.readyCellModel != nil);
}

#pragma mark - CellModels

- (NSArray<CPITunesSearchTrackTableViewCellModel *> *)cellModelsWithObjects:(NSArray<CPITunesTrack *> *)objects {
    NSMutableArray *cellModels = [[NSMutableArray alloc] init];
    [objects enumerateObjectsUsingBlock:^(CPITunesTrack * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CPITunesSearchTrackTableViewCellModel *cellModel = [[CPITunesSearchTrackTableViewCellModel alloc] init];
        cellModel.object = obj;
        cellModel.loaderHidden = YES;
        @weakify(cellModel)
        @weakify(self)
        [RACObserve(cellModel, selected) subscribeNext:^(id x) {
            @strongify(cellModel)
            @strongify(self)
            if ([x boolValue]) {
                [self.selectedCellModels enumerateObjectsUsingBlock:^(CPITunesSearchTrackTableViewCellModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (![obj isEqual:cellModel]) {
                        obj.selected = NO;
                    }
                }];
                cellModel.loaderHidden = NO;
                [self startPlayingInCellModel:cellModel];
            } else {
                cellModel.loaderHidden = YES;
                [self stopPlayingInCellModel:cellModel];
            }
            [self updateDoneButton];
            [self.rac_songUpdatedSignal sendNext:cellModel];
        }];
        [cellModels addObject:cellModel];
    }];
    return cellModels.copy;
}

#pragma mark - Audio

- (void)startPlayingInCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel {
    @weakify(self)
    [[[self.searchService rac_downloadPreviewForTrack:cellModel.object]
      finally:^{
          @strongify(self)
          cellModel.loaderHidden = YES;
          [self.rac_songUpdatedSignal sendNext:cellModel];
          [self updateDoneButton];
      }] subscribeNext:^(id x) {
          @strongify(self)
          if (cellModel.selected) {
              [self.searchPlayer startPlayingFileWithURL:[self.searchService previewFileURLForTrack:cellModel.object]];
          }
      }];
}

- (void)stopPlayingInCellModel:(CPITunesSearchTrackTableViewCellModel *)cellModel {
    [self.searchPlayer stopPlayingFileWithURL:[self.searchService previewFileURLForTrack:cellModel.object]];
}

@end
