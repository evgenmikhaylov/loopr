//
//  CPITunesSearchTrackTableViewCellModel.m
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchTrackTableViewCellModel.h"
#import "CPITunesTrack.h"
#import <ReactiveCocoa.h>

@implementation CPITunesSearchTrackTableViewCellModel

#pragma mark - CPITunesSearchTrackTableViewCellModelOutput

- (NSString *)title {
    return self.object.name;
}

- (NSString *)subtitle {
    NSMutableArray *details = [[NSMutableArray alloc] init];
    if (self.object.artist.length > 0) {
        [details addObject:self.object.artist];
    }
    if (self.object.album.length > 0) {
        [details addObject:self.object.album];
    }
    return [details componentsJoinedByString:@" - "];
}

- (NSURL *)imageURL {
    return self.object.imageURL;
}

- (UIImage *)placeholderImage {
    return [UIImage imageNamed:@"img_cover_album"];
}

- (CGFloat)height {
    return 72.0;
}

- (UITableViewCellSelectionStyle)selectionStyle {
    return UITableViewCellSelectionStyleNone;
}

#pragma mark - CPITunesSearchTrackTableViewCellOutput

@end
