//
//  CPITunesSearchTrackTableViewCellModelOutput.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RACSignal;

@protocol CPITunesSearchTrackTableViewCellModelOutput <NSObject>

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *subtitle;
@property (nonatomic, readonly) NSURL *imageURL;
@property (nonatomic, readonly) UIImage *placeholderImage;
@property (nonatomic, readonly) CGFloat height;
@property (nonatomic, readonly) UITableViewCellSelectionStyle selectionStyle;

@property (nonatomic) BOOL loaderHidden;


@end
