//
//  CPITunesSearchTrackTableViewCellModel.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 02/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchTrackTableViewCellModelOutput.h"
#import "CPITunesSearchTrackTableViewCellOutput.h"
#import <Foundation/Foundation.h>

@class CPITunesTrack;

@interface CPITunesSearchTrackTableViewCellModel : NSObject
<
CPITunesSearchTrackTableViewCellOutput,
CPITunesSearchTrackTableViewCellModelOutput
>

@property (nonatomic) BOOL selected;
@property (nonatomic) BOOL loaderHidden;
@property (nonatomic) CPITunesTrack *object;

@end
