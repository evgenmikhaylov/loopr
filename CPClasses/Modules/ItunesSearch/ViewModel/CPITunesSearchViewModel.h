//
//  CPItunesSearchViewModel.h
//  Cinepic
//
//  Created by Evgeny Mikhaylov on 01/08/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CPITunesSearchViewModelOutput.h"
#import "CPITunesSearchViewOutput.h"

#import <Foundation/Foundation.h>

@class RACSignal;

@interface CPITunesSearchViewModel : NSObject <CPITunesSearchViewModelOutput, CPITunesSearchViewOutput>

@end
