//
//  CPCameraViewController.h
//  Cinepic
//
//  Created by Artem on 4/8/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPImagePickerController.h"

@interface CPCameraViewController : UIViewController

@property (nonatomic, readonly) CPImagePickerControllerMediaType mediaType;

- (instancetype)initWithMediaType:(CPImagePickerControllerMediaType) mediaType;

@end
