//
//  CPCameraViewController.m
//  Cinepic
//
//  Created by Artem on 4/8/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPCameraViewController.h"
#import "CPCameraPreviewController.h"
#import "CPInfoView.h"
#import "CPCameraSettingsView.h"
#import "CPCameraButtonView.h"

#import "NSBundle+Additions.h"
#import "UIView+Nib.h"
#import "UIColor+CP.h"

#import <RACEXTScope.h>

@import AVFoundation;
@import AssetsLibrary;
@import MediaPlayer;


@interface CPCameraViewController ()
<
CameraSettingsViewDelegate,
AVCaptureFileOutputRecordingDelegate,
CPInfoViewDelegate
>

@property (nonatomic) CPImagePickerControllerMediaType mediaType;
@property (nonatomic) NSMutableDictionary *videoMediaInfo;

// UI
@property (nonatomic) UILabel *countdownLabel;
@property (nonatomic) CPCameraButtonView *cameraButtonView;
@property (nonatomic) CPCameraSettingsView *cameraSettingsView;

// Storyboard
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cameraViewHeightConstraint;

// Session managment
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDevice *captureDevice;
@property (nonatomic) AVCaptureDeviceInput *deviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;

// Video utilities
@property (nonatomic) NSTimer *videoTimer;
@property (nonatomic) int videoTime;
@property (nonatomic) BOOL videoRecording;

// Photo utilities
@property (nonatomic) NSTimer *countdownTimer;
@property (nonatomic) int timing;
@property (nonatomic) BOOL countdownLabelShowing;

@end

#define IS_IPHONE4 ([[UIScreen mainScreen] bounds].size.height == 480.0f)

const CGFloat kSettingPanelHeight = 44.0f;
const CGFloat kCameraSettingsViewHeightCoefficient = 0.25f;
const CGFloat kCameraAnimationDuration = 0.15;

@implementation CPCameraViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.session = [[AVCaptureSession alloc] init];

    [self addCameraSettingsView];
    switch (self.mediaType) {
        case CPImagePickerControllerMediaTypeImage:
            self.session.sessionPreset = AVCaptureSessionPresetPhoto;
            [self addCameraButtonViewWithButtonType:PhotoButton];
            [self.cameraButtonView videoTimingHidden:YES];
            [self.cameraSettingsView cancelPanelTransparent:NO];
            if (IS_IPHONE4) {
                [self replaceCameraViewConstraintsForSmallScreen];
            }
            else {
                [self updateCameraViewHeightConstraintWithCoefficient:kCameraSettingsViewHeightCoefficient
                                                             constant:0.0f];
            }
            break;
        case CPImagePickerControllerMediaTypeVideo:
            self.session.sessionPreset = AVCaptureSessionPresetHigh;
            [self addCameraButtonViewWithButtonType:VideoRecButton];
            self.cameraSettingsView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
            [self.cameraSettingsView timingHidden:YES];
            self.cameraSettingsView.torchNeeded = YES;
            break;
        default:
            break;
    }
    [self.cameraSettingsView.cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cameraButtonView.cameraButton addTarget:self action:@selector(cameraButtonStartAction:) forControlEvents:UIControlEventTouchUpInside];
    [self checkDeviceAuthorizationStatus];
    [self startSessionWithCameraPosition:self.cameraSettingsView.position];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.session startRunning];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeInterfaceWithOrientation:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopCapturingInBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showPreviewControllerIfNeeded)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [self rotateControlsWithOrientation:[UIDevice currentDevice].orientation
                      animationDuration:0.0];
    if (self.cameraSettingsView) {
        if (self.cameraSettingsView.torchNeeded) {
            [self enableTorchIfSupported:self.cameraSettingsView.torch];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.session stopRunning];
    if (self.countdownLabel) {
        [self deleteCountdownLabel:nil];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIDeviceOrientationDidChangeNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

- (void)viewDidLayoutSubviews
{
    [self rotateControlsWithOrientation:[UIDevice currentDevice].orientation
                      animationDuration:0.0];
    if (self.videoPreviewLayer) {
        self.videoPreviewLayer.frame = self.cameraView.bounds;
    }
}

- (void)checkDeviceAuthorizationStatus
{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo
                             completionHandler:^(BOOL granted) {
                                 if (!granted) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         self.cameraButtonView.userInteractionEnabled = NO;
                                         [self.cameraSettingsView freezeSettingsControls:YES];
                                         [self addInfoView];
                                     });
                                 }
                             }];
}

- (void)startSessionWithCameraPosition:(CameraPosition)position
{
    AVCaptureDevicePosition prefferingPosition;
    switch (position) {
        case CameraPositionBack:
            prefferingPosition = AVCaptureDevicePositionBack;
            break;
        case CameraPositionFront:
            prefferingPosition = AVCaptureDevicePositionFront;
            break;
        default:
            prefferingPosition = AVCaptureDevicePositionUnspecified;
            break;
    }
    self.captureDevice = [CPCameraViewController deviceWithMediaType:AVMediaTypeVideo
                                                  preferringPosition:prefferingPosition];
    [self.cameraSettingsView setFlashStateEnable:[self.captureDevice hasFlash]];
    NSError *deviceInputError;
    self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice
                                                             error:&deviceInputError];
    if (deviceInputError) {
        NSLog(@"%@", [deviceInputError localizedDescription]);
    }
    if ([self.session canAddInput:self.deviceInput]) {
        [self.session addInput:self.deviceInput];
    }
    self.movieFileOutput = [AVCaptureMovieFileOutput new];
    if ([self.session canAddOutput:self.movieFileOutput]) {
        [self.session addOutput:self.movieFileOutput];
    }
    self.stillImageOutput = [AVCaptureStillImageOutput new];
    if ([self.session canAddOutput:self.stillImageOutput]) {
        [self.stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
        [self.session addOutput:self.stillImageOutput];
    }
    NSError *audioInputError = nil;
    AVCaptureDevice *audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&audioInputError];
    if ([self.session canAddInput:audioInput]) {
        [self.session addInput:audioInput];
    }
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.cameraView.layer addSublayer:self.videoPreviewLayer];
    [self.session startRunning];
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            captureDevice = device;
            break;
        }
    }
    return captureDevice;
}

#pragma mark - Initialization

- (instancetype)initWithMediaType:(CPImagePickerControllerMediaType) mediaType;
{
    self = [super init];
    if (self) {
        self.mediaType = mediaType;
    }
    return self;
}

#pragma mark - Notifications
#pragma mark UIDeviceOrientationDidChangeNotification

- (void)changeInterfaceWithOrientation:(NSNotification *)notification
{
    [self rotateControlsWithOrientation:[[UIDevice currentDevice] orientation]
                      animationDuration:0.5];
}

- (void)rotateControlsWithOrientation:(UIDeviceOrientation)orientation animationDuration:(CGFloat)duration
{
    [UIView animateWithDuration:duration
                     animations:^{
                         [self.cameraSettingsView rotateControlsWithOrientation:orientation];
                         [self.cameraButtonView rotateControlsWithOrientation:orientation];
                         if (self.countdownLabel) {
                             CGFloat angle = 0.0;
                             switch (orientation) {
                                 case UIDeviceOrientationLandscapeLeft:
                                     angle = M_PI / 2;
                                     break;
                                 case UIDeviceOrientationLandscapeRight:
                                     angle = -(M_PI / 2);
                                     break;
                                 case UIDeviceOrientationPortraitUpsideDown:
                                     angle = M_PI;
                                 default:
                                     break;
                             }
                             [self.countdownLabel setTransform:CGAffineTransformMakeRotation(angle)];
                         }
                     }];
}

#pragma mark UIApplicationDidEnterBackgroundNotification

- (void)stopCapturingInBackground
{
    [self.session stopRunning];
    switch (self.mediaType) {
        case CPImagePickerControllerMediaTypeImage:
            [self deleteCountdownLabel:nil];
            break;
        default:
            break;
    }
}

#pragma mark UIApplicationDidBecomeActiveNotification

- (void)showPreviewControllerIfNeeded
{
    [self.session startRunning];
    if (self.mediaType == CPImagePickerControllerMediaTypeVideo) {
        [self.cameraSettingsView updateTorchState:CameraTorchOff];
        id sender = self.videoRecording ? self.cameraButtonView.cameraButton : nil;
        [self cameraButtonStopVideoAction:sender];
    }
}

#pragma mark - Camera Control Views

- (void)addInfoView
{
    CPInfoView *infoView = [CPInfoView new];
    infoView.backgroundColor = [UIColor blackColor];
    infoView.textColor = [UIColor whiteColor];
    infoView.font = [UIFont fontWithName:@"Roboto-Regular"
                                    size:18.0f];
    infoView.delegate = self;
    NSString *infoString = [NSString stringWithFormat:@"To allow access to your camera,\ngo to Settings > %@", [[NSBundle bundleName] capitalizedString]];
    infoView.text = NSLocalizedString(infoString, @"No photos");
    [self.cameraView addSubview:infoView];
    infoView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = NSDictionaryOfVariableBindings(infoView);
    NSArray *widthConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[infoView]|"
                                                                        options:0
                                                                        metrics:nil
                                                                          views:views];
    [NSLayoutConstraint activateConstraints:widthConstraints];
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:infoView
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self.view
                                                                        attribute:NSLayoutAttributeHeight
                                                                       multiplier:1.0 - kCameraSettingsViewHeightCoefficient
                                                                         constant:0.0f];
    heightConstraint.active = YES;
    NSLayoutConstraint *positionConstraint = [NSLayoutConstraint constraintWithItem:infoView
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.view
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:1.0f
                                                                           constant:0.0f];
    positionConstraint.active = YES;
}

- (void)addCameraSettingsView
{
    self.cameraSettingsView = [CPCameraSettingsView loadFromNib];
    self.cameraSettingsView.delegate = self;
    self.cameraSettingsView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.cameraSettingsView];
    NSDictionary *views = @{@"cameraSettingsView" : self.cameraSettingsView};
    NSArray *widthConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cameraSettingsView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
    [NSLayoutConstraint activateConstraints:widthConstraint];
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:self.cameraSettingsView
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self.view
                                                                        attribute:NSLayoutAttributeHeight
                                                                       multiplier:0.25f
                                                                         constant:0.0f];
    heightConstraint.active = YES;
    NSLayoutConstraint *positionConstraint = [NSLayoutConstraint constraintWithItem:self.cameraSettingsView
                                                                          attribute:NSLayoutAttributeBottom
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.view
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0f
                                                                           constant:0.0f];
    positionConstraint.active = YES;
}

- (void)addCameraButtonViewWithButtonType:(ButtonType)buttonType
{
    self.cameraButtonView = [CPCameraButtonView loadFromNib];
    [self.cameraButtonView showButtonWithState:buttonType];
    self.cameraButtonView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.cameraButtonView];
    NSLayoutConstraint *yPositionConstraint = [NSLayoutConstraint constraintWithItem:self.cameraButtonView
                                                                           attribute:NSLayoutAttributeCenterX
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.view
                                                                           attribute:NSLayoutAttributeCenterX
                                                                          multiplier:1.0f
                                                                            constant:0.0f];
    yPositionConstraint.active = YES;
    
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.cameraButtonView
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.view
                                                                       attribute:NSLayoutAttributeWidth
                                                                      multiplier:1.0f / 3
                                                                        constant:0.0f];
    widthConstraint.active = YES;
    
    NSLayoutConstraint *xPositionConstraint = [NSLayoutConstraint constraintWithItem:self.cameraButtonView
                                                                           attribute:NSLayoutAttributeBottom
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.view
                                                                           attribute:NSLayoutAttributeBottom
                                                                          multiplier:1.0f
                                                                            constant:0.0f];
    xPositionConstraint.active = YES;
    CGFloat constant = 0.0f;
    switch (self.mediaType) {
        case CPImagePickerControllerMediaTypeImage:
            constant = -kSettingPanelHeight;
            break;
        case CPImagePickerControllerMediaTypeVideo:
            break;
        default:
            break;
    }
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:self.cameraButtonView
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self.view
                                                                        attribute:NSLayoutAttributeHeight
                                                                       multiplier:kCameraSettingsViewHeightCoefficient
                                                                         constant:constant];
    heightConstraint.active = YES;
}

- (void)updateCameraViewHeightConstraintWithCoefficient:(CGFloat)coefficient constant:(CGFloat)constant
{
    self.cameraViewHeightConstraint.active = NO;
    CGFloat multiplier = 1.0f - coefficient;
    self.cameraViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.cameraView
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.view
                                                                   attribute:NSLayoutAttributeHeight
                                                                  multiplier:multiplier
                                                                    constant:constant];
    self.cameraViewHeightConstraint.active = YES;
}

- (void)replaceCameraViewConstraintsForSmallScreen
{
    [self.cameraView removeFromSuperview];
    [self.view insertSubview:self.cameraView
                belowSubview:self.cameraSettingsView];
    self.cameraView.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat constant = CGRectGetHeight(self.view.frame) * kCameraSettingsViewHeightCoefficient - kSettingPanelHeight;
    constant = constant / 2;
    NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint constraintWithItem:self.cameraView
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.view
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1.0
                                                                          constant:-constant];
    centerYConstraint.active = YES;
    
    NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint constraintWithItem:self.cameraView
                                                                         attribute:NSLayoutAttributeCenterX
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.view
                                                                         attribute:NSLayoutAttributeCenterX
                                                                        multiplier:1.0
                                                                          constant:0.0];
    centerXConstraint.active = YES;
    
    NSDictionary *views = @{@"cameraView" : self.cameraView};
    NSArray *widthConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cameraView]|"
                                                                        options:0
                                                                        metrics:nil
                                                                          views:views];
    [NSLayoutConstraint activateConstraints:widthConstraints];
    self.cameraViewHeightConstraint.active = NO;
    self.cameraViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.cameraView
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.view
                                                                   attribute:NSLayoutAttributeHeight
                                                                  multiplier:1.0f
                                                                    constant:0.0f];
    self.cameraViewHeightConstraint.active = YES;
}

#pragma mark - Countdown Label

- (void)showCountdownLabel
{
    [self.cameraSettingsView disableSettingsControls:YES];
    self.countdownLabel = [UILabel new];
    self.countdownLabel.textColor = [UIColor cpLightBlackColor];
    self.countdownLabel.font = [UIFont fontWithName:@"Roboto-Medium"
                                               size:12.0f];
    self.countdownLabel.backgroundColor = [UIColor clearColor];
    self.countdownLabel.textAlignment = NSTextAlignmentCenter;
    self.countdownLabel.text = [NSString stringWithFormat:@"%i", self.timing];
    [self.view addSubview:self.countdownLabel];
    self.countdownLabelShowing = YES;
    [self addConstraintsToCountdownLabel];
    CGFloat angle = 0.0;
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft:
            angle = M_PI / 2;
            break;
        case UIDeviceOrientationLandscapeRight:
            angle = -(M_PI / 2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            angle = M_PI;
        default:
            break;
    }
    [self.countdownLabel setTransform:CGAffineTransformMakeRotation(angle)];
    self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                           target:self
                                                         selector:@selector(updateCountdownLabel)
                                                         userInfo:nil
                                                          repeats:YES];
    [self.countdownTimer fire];
}

- (void)addConstraintsToCountdownLabel
{
    self.countdownLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.countdownLabel
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.countdownLabel
                                                                       attribute:NSLayoutAttributeHeight
                                                                      multiplier:1.0f
                                                                        constant:0.0f];
    widthConstraint.active = YES;
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:self.countdownLabel
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:nil
                                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                                       multiplier:0.0f
                                                                         constant:kSettingPanelHeight];
    heightConstraint.active = YES;
    NSLayoutConstraint *verticalPositionConstraint = [NSLayoutConstraint constraintWithItem:self.countdownLabel
                                                                                  attribute:NSLayoutAttributeBottom
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.view
                                                                                  attribute:NSLayoutAttributeBottom
                                                                                 multiplier:1.0f - kCameraSettingsViewHeightCoefficient
                                                                                   constant:kSettingPanelHeight];
    verticalPositionConstraint.active = YES;
    NSLayoutConstraint *horizontalPositionConstraint = [NSLayoutConstraint constraintWithItem:self.countdownLabel
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self.view
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0f
                                                                                     constant:0.0f];
    horizontalPositionConstraint.active = YES;
    [self.view setNeedsUpdateConstraints];
    [self.view layoutIfNeeded];
}

- (void)updateCountdownLabel
{
    self.countdownLabel.text = [NSString stringWithFormat:@"%i", self.timing];
    if (self.timing == 0) {
        [self deleteCountdownLabel:self.cameraButtonView.cameraButton];
        [self createAndShowPhoto];
    }
    else {
        self.timing -= 1;
    }
}

- (void)deleteCountdownLabel:(id)sender
{
    [self.countdownLabel removeFromSuperview];
    self.countdownLabelShowing = NO;
    [self.countdownTimer invalidate];
    [self.cameraSettingsView timingHidden:NO];
    [self.cameraSettingsView disableSettingsControls:NO];
    [self.cameraButtonView.cameraButton removeTarget:self
                                              action:@selector(deleteCountdownLabel:)
                                    forControlEvents:UIControlEventTouchUpInside];
    [self.cameraButtonView.cameraButton addTarget:self
                                           action:@selector(cameraButtonStartAction:)
                                 forControlEvents:UIControlEventTouchUpInside];
    [self cameraSettingsView:self.cameraSettingsView
                changeTiming:self.cameraSettingsView.timing];
    UIButton *senderButton = (UIButton *)sender;
    if (senderButton == self.cameraButtonView.cameraButton) {
        [self.cameraSettingsView setFlashStateEnable:[self.captureDevice hasFlash]];
    }
}

#pragma mark - Actions

- (void)cancelButtonAction:(id)sender
{
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.delegate respondsToSelector:@selector(cpImagePickerControllerDidCancel:)]) {
        [imagePickerController.delegate cpImagePickerControllerDidCancel:imagePickerController];
    }
}

- (void)cameraButtonStartAction:(id)sender
{
    switch (self.mediaType) {
        case CPImagePickerControllerMediaTypeImage: {
            if (self.timing > 0) {
                [self showCountdownLabel];
                [self.cameraButtonView.cameraButton removeTarget:self
                                                          action:@selector(cameraButtonStartAction:)
                                                forControlEvents:UIControlEventTouchUpInside];
                [self.cameraButtonView.cameraButton addTarget:self
                                                       action:@selector(deleteCountdownLabel:)
                                             forControlEvents:UIControlEventTouchUpInside];
            }
            else {
                [self createAndShowPhoto];
            }
            break;
        }
        case CPImagePickerControllerMediaTypeVideo: {
            self.cameraButtonView.videoTimeLabel.textColor = [UIColor whiteColor];
            [self.cameraButtonView videoTimeLabelShadowEnable:YES];
            self.cameraSettingsView.hidden = YES;
            [self.cameraButtonView showButtonWithState:VideoStopButton];
            [self.cameraButtonView.cameraButton removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
            [self.cameraButtonView.cameraButton addTarget:self action:@selector(cameraButtonStopVideoAction:) forControlEvents:UIControlEventTouchUpInside];
            self.videoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateVideoTime) userInfo:nil repeats:YES];
            self.videoRecording = YES;
            self.videoMediaInfo = [@{} mutableCopy];
            self.videoMediaInfo[CPImagePickerControllerMediaInfoOrientationKey] = @([UIDevice currentDevice].orientation);
            [self startRecordingVideoToOutputFile];
            [self.videoTimer fire];
        }
        default:
            break;
    }
}

- (void)cameraButtonStopVideoAction:(id)sender
{
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.datasource respondsToSelector:@selector(cpImagePickerControllerCameraVideoMinDuration:)]){
        int minVideoDuration = [imagePickerController.datasource cpImagePickerControllerCameraVideoMinDuration:imagePickerController];
        if (self.videoTime < minVideoDuration){
            return;
        }
    }
    
    [self.movieFileOutput stopRecording];
    self.videoRecording = NO;
    [self.videoTimer invalidate];
    self.videoTime = 0;
    
    self.cameraSettingsView.hidden = NO;
    self.cameraButtonView.videoTimeLabel.textColor = [UIColor cpLightRedColor];
    [self.cameraButtonView videoTimeLabelShadowEnable:NO];
    [self.cameraButtonView showButtonWithState:VideoRecButton];
    self.cameraButtonView.videoTimeLabel.text = [self stringForTime:self.videoTime];
    [self.cameraButtonView.cameraButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.cameraButtonView.cameraButton addTarget:self action:@selector(cameraButtonStartAction:) forControlEvents:UIControlEventTouchUpInside];
    if (sender == self.cameraButtonView.cameraButton) {
        NSString *outputPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[@"movie" stringByAppendingPathExtension:@"mov"]];
        self.videoMediaInfo[CPImagePickerControllerMediaInfoVideoPathKey] = outputPath;
        CPCameraPreviewController *cameraPreviewController = [[CPCameraPreviewController alloc] initWithMediaInfo:self.videoMediaInfo mediaType:self.mediaType];
        [self.navigationController pushViewController:cameraPreviewController animated:YES];
    }
}

#pragma mark - Protocols
#pragma mark CameraSettingsViewDelegate

- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeFlashMode:(CameraFlash)flash
{
    if (!self.videoRecording) {
        if ([self.captureDevice hasFlash]) {
            AVCaptureFlashMode flashMode;
            switch (flash) {
                case CameraFlashOn:
                    flashMode = AVCaptureFlashModeOn;
                    break;
                case CameraFlashOff:
                    flashMode = AVCaptureFlashModeOff;
                    break;
                case CameraFlashAuto:
                    flashMode = AVCaptureFlashModeAuto;
                    break;
                default:
                    flashMode = AVCaptureFlashModeOff;
            }
            NSError* flashError;
            [self.captureDevice lockForConfiguration:&flashError];
            if (flashError) {
                NSLog(@"%@", [flashError localizedDescription]);
            }
            else {
                [self.captureDevice setFlashMode:flashMode];
                [self.captureDevice unlockForConfiguration];
            }
        }
    }
}

#pragma mark CPInfoViewDelegate

- (void)cpInfoViewPressed:(CPInfoView*)infoView
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeTorchMode:(CameraTorch)torch
{
    if (!self.videoRecording) {
        [self enableTorchIfSupported:torch];
    }
}

- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeTiming:(CameraTiming)timing
{
    switch (timing) {
        case CameraTimingOff:
            self.timing = 0;
            break;
        case CameraTiming3Sec:
            self.timing = 3;
            break;
        case CameraTiming5Sec:
            self.timing = 5;
            break;
        case CameraTiming10Sec:
            self.timing = 10;
            break;
        default:
            self.timing = 0;
            break;
    }
}

- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeCameraPosition:(CameraPosition)position
{
    if (!self.videoRecording) {
        [self.cameraSettingsView cameraTypeChangingEnable:NO];
        AVCaptureDevicePosition preferredPosition = AVCaptureDevicePositionUnspecified;
        switch (position) {
            case CameraPositionBack:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
            case CameraPositionFront:
                preferredPosition = AVCaptureDevicePositionFront;
                break;
            default:
                preferredPosition = AVCaptureDevicePositionUnspecified;
                break;
        }
        self.captureDevice = [CPCameraViewController deviceWithMediaType:AVMediaTypeVideo
                                                      preferringPosition:preferredPosition];
        [self.cameraSettingsView setFlashStateEnable:[self.captureDevice hasFlash]];
        NSError *deviceInputError;
        AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice
                                                                                  error:&deviceInputError];
        if (deviceInputError) {
            NSLog(@"%@", [deviceInputError localizedDescription]);
        }
        [UIView animateWithDuration:kCameraAnimationDuration
                         animations:^{
                             self.cameraView.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             [[self session] beginConfiguration];
                             [self.session removeInput:self.deviceInput];
                             if ([self.session canAddInput:deviceInput]) {
                                 self.deviceInput = deviceInput;
                             }
                             [[self session] addInput:self.deviceInput];
                             [[self session] commitConfiguration];
                             if (!self.countdownLabelShowing) {
                                 [self.cameraSettingsView cameraTypeChangingEnable:YES];
                             }
                             if (self.cameraSettingsView.torchNeeded) {
                                 [self enableTorchIfSupported:self.cameraSettingsView.torch];
                             }
                             [UIView animateWithDuration:kCameraAnimationDuration
                                              animations:^{
                                                  self.cameraView.alpha = 1.0;
                                              }];
                         }];
    }
}

#pragma mark AVCaptureFileOutputRecordingDelegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    NSLog(@"Did finish recording, error %@ | path %@ | connections %@", error, [outputFileURL absoluteString], connections);
}

#pragma mark - Helpers

- (void)updateVideoTime
{
    self.cameraButtonView.videoTimeLabel.text = [self stringForTime:self.videoTime];
    self.videoTime += 1;
}

- (NSString *)stringForTime:(int)time
{
    NSString *finalString = @"";
    int seconds = time % 60;
    int minutes = (time / 60) % 60;
    int hours = time / 3600;
    if (hours > 0) {
        finalString = [NSString stringWithFormat:@"%01d:%02d:%02d",hours, minutes, seconds];
    }
    else {
        finalString = [NSString stringWithFormat:@"%01d:%02d", minutes, seconds];
    }
    return finalString;
}

- (void)createAndShowPhoto
{
    AVCaptureConnection *connection = [self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
    @weakify(self)
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
        else {
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                UIImage *image = [[UIImage alloc] initWithData:imageData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIDeviceOrientation currentDeviceOrientation = [UIDevice currentDevice].orientation;
                    if (self.cameraSettingsView.position == CameraPositionFront){
                        if (currentDeviceOrientation == UIDeviceOrientationLandscapeLeft){
                            currentDeviceOrientation = UIDeviceOrientationLandscapeRight;
                        } else if (currentDeviceOrientation == UIDeviceOrientationLandscapeRight){
                            currentDeviceOrientation = UIDeviceOrientationLandscapeLeft;
                        }
                    }
                    NSDictionary *mediaInfo = @{
                                                CPImagePickerControllerMediaInfoImageKey : image,
                                                CPImagePickerControllerMediaInfoOrientationKey : @(currentDeviceOrientation)
                                                };
                    CPCameraPreviewController *cameraPreviewController = [[CPCameraPreviewController alloc] initWithMediaInfo:mediaInfo mediaType:self.mediaType];
                    [self.navigationController pushViewController:cameraPreviewController animated:YES];
                });
            });
        }
    }];
}

- (void)startRecordingVideoToOutputFile
{
    NSString *outputPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[@"movie" stringByAppendingPathExtension:@"mov"]];
    NSFileManager *manager = [NSFileManager new];
    if ([manager fileExistsAtPath:outputPath]) {
        [manager removeItemAtPath:outputPath
                            error:nil];
    }
    AVCaptureConnection *connection = [self.movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
    if ([connection isVideoOrientationSupported]) {
        AVCaptureVideoOrientation videoOrientation;
        switch ([UIDevice currentDevice].orientation) {
            case UIDeviceOrientationLandscapeLeft:
                videoOrientation = AVCaptureVideoOrientationLandscapeRight;
                break;
            case UIDeviceOrientationLandscapeRight:
                videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
                break;
            case UIDeviceOrientationPortrait:
                videoOrientation = AVCaptureVideoOrientationPortrait;
                break;
            case UIDeviceOrientationPortraitUpsideDown:
                videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                break;
            default:
                videoOrientation = AVCaptureVideoOrientationPortrait;
                break;
        }
        [connection setVideoOrientation:videoOrientation];
    }
    [self.movieFileOutput startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputPath]
                                      recordingDelegate:self];
}

- (void)enableTorchIfSupported:(CameraTorch)torch
{
    if ([self.captureDevice hasTorch]) {
        AVCaptureTorchMode torchMode;
        switch (torch) {
            case CameraTorchOn:
                torchMode = AVCaptureTorchModeOn;
                break;
            case CameraTorchOff:
                torchMode = AVCaptureTorchModeOff;
                break;
            default:
                torchMode = AVCaptureTorchModeOff;
                break;
        }
        NSError* torchError;
        [self.captureDevice lockForConfiguration:&torchError];
        if (torchError) {
            NSLog(@"%@", [torchError localizedDescription]);
        }
        else {
            [self.captureDevice setTorchMode:torchMode];
            [self.captureDevice unlockForConfiguration];
        }
    }
}

@end
