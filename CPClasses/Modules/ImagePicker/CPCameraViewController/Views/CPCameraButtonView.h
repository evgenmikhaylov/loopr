//
//  CPCameraButtonView.h
//  Cinepic
//
//  Created by Artem on 4/9/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ButtonType) {
    PhotoButton,
    VideoRecButton,
    VideoStopButton,
};

@interface CPCameraButtonView : UIView

@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;

- (void)videoTimingHidden:(BOOL)hidden;
- (void)videoTimeLabelShadowEnable:(BOOL)enable;
- (void)showButtonWithState:(ButtonType)buttonType;
- (void)rotateControlsWithOrientation:(UIDeviceOrientation)orientation;

@end
