//
//  CPCameraSettingsView.h
//  Cinepic
//
//  Created by Artem on 4/8/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CameraFlash) {
    CameraFlashOff,
    CameraFlashOn,
    CameraFlashAuto,
    CameraFlashCount,
};

typedef NS_ENUM(NSUInteger, CameraTorch) {
    CameraTorchOff,
    CameraTorchOn,
    CameraTorchCount,
};

typedef NS_ENUM(NSUInteger, CameraTiming) {
    CameraTimingOff,
    CameraTiming3Sec,
    CameraTiming5Sec,
    CameraTiming10Sec,
    CameraTimingCount,
};

typedef NS_ENUM(NSUInteger, CameraPosition) {
    CameraPositionBack,
    CameraPositionFront,
    CameraPositionCount
};

@protocol CameraSettingsViewDelegate;

@interface CPCameraSettingsView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic) BOOL torchNeeded;
@property (nonatomic) CameraPosition position;
@property (nonatomic) CameraFlash flash;
@property (nonatomic) CameraTorch torch;
@property (nonatomic) CameraTiming timing;
@property (nonatomic, weak) id<CameraSettingsViewDelegate> delegate;

- (void)timingHidden:(BOOL)hidden;
- (void)setFlashStateEnable:(BOOL)enable;
- (void)cameraTypeChangingEnable:(BOOL)enable;
- (void)disableSettingsControls:(BOOL)disable;
- (void)freezeSettingsControls:(BOOL)freeze;
- (void)cancelPanelTransparent:(BOOL)transparent;
- (void)rotateControlsWithOrientation:(UIDeviceOrientation)orientation;
- (void)updateTorchState:(CameraTorch)torch;

@end

@protocol CameraSettingsViewDelegate <NSObject>

- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeFlashMode:(CameraFlash)flash;
- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeTorchMode:(CameraTorch)torch;
- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeTiming:(CameraTiming)timing;
- (void)cameraSettingsView:(CPCameraSettingsView *)cameraSettingsView changeCameraPosition:(CameraPosition)position;

@end
