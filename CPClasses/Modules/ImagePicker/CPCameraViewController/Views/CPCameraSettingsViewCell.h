//
//  CPCameraSettingsViewCell.h
//  Cinepic
//
//  Created by Artem on 4/8/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPCameraSettingsViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (void)hideTextLabel;

@end
