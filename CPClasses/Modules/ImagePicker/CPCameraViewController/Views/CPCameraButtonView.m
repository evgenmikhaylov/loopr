//
//  CPCameraButtonView.m
//  Cinepic
//
//  Created by Artem on 4/9/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPCameraButtonView.h"
#import "UIColor+CP.h"

@interface CPCameraButtonView()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation CPCameraButtonView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.videoTimeLabel.textColor = [UIColor cpRedColor];
    self.videoTimeLabel.font = [UIFont fontWithName:@"Roboto-Bold"
                                               size:12.0f];
}

- (void)videoTimeLabelShadowEnable:(BOOL)enable
{
    if (enable) {
        self.videoTimeLabel.layer.shadowColor = [UIColor blackColor].CGColor;
        self.videoTimeLabel.layer.shadowOffset = CGSizeZero;
        self.videoTimeLabel.layer.shadowOpacity = 0.75f;
        self.videoTimeLabel.layer.shadowRadius = 2.0f;
    } else {
        self.videoTimeLabel.layer.shadowColor = [UIColor clearColor].CGColor;
        self.videoTimeLabel.layer.shadowOffset = CGSizeZero;
        self.videoTimeLabel.layer.shadowOpacity = 0.0f;;
        self.videoTimeLabel.layer.shadowRadius = 0.0f;
    }
}

- (void)videoTimingHidden:(BOOL)hidden
{
    if (hidden) {
        self.heightConstraint.active = NO;
        self.heightConstraint = [NSLayoutConstraint constraintWithItem:self.videoTimeLabel
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:0.0f];
        self.heightConstraint.active = YES;
    }
}

- (void)showButtonWithState:(ButtonType)buttonType
{
    NSString *imageName;
    switch (buttonType) {
        case PhotoButton:
            imageName = @"btn_camera_photo.png";
            break;
        case VideoRecButton:
            imageName = @"btn_camera_video_rec.png";
            break;
        case VideoStopButton:
            imageName = @"btn_camera_video_stop.png";
            break;
    }
    
    [self.button setImage:[UIImage imageNamed:imageName]
                 forState:UIControlStateNormal];
}

- (void)rotateControlsWithOrientation:(UIDeviceOrientation)orientation
{
    CGFloat angle = 0.0;
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
            angle = M_PI / 2;
            break;
        case UIDeviceOrientationLandscapeRight:
            angle = -(M_PI / 2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            angle = M_PI;
        default:
            break;
    }
    [self.videoTimeLabel setTransform:CGAffineTransformMakeRotation(angle)];
}

@end
