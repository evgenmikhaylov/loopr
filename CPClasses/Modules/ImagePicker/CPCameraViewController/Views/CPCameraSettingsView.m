//
//  CPCameraSettingsView.m
//  Cinepic
//
//  Created by Artem on 4/8/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPCameraSettingsView.h"
#import "UIColor+CP.h"
#import "CPCameraSettingsViewCell.h"
#import "UIView+Nib.h"

typedef enum : NSUInteger {
    CollectionViewCellFlashType,
    CollectionViewCellTimingType,
    CollectionViewCellCameraSwitchType,
    CollectionViewCellCount,
} CollectionViewCellType;

@interface CPCameraSettingsView()
<
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout
>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *cancelPanelView;
@property (nonatomic) CGFloat angle;
@property (nonatomic) BOOL flashEnable;
@property (nonatomic) BOOL timingHidden;
@property (nonatomic) BOOL cameraSwitchEnable;

@end

@implementation CPCameraSettingsView

- (void)awakeFromNib
{
    [super awakeFromNib];
    NSString *cellIdentifier = NSStringFromClass([CPCameraSettingsViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil]
          forCellWithReuseIdentifier:cellIdentifier];
    self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.scrollEnabled = NO;
    self.cancelButton.titleLabel.textColor = [UIColor cpLightBlackColor];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Roboto-Medium"
                                                        size:14.0f];
    self.cameraSwitchEnable = YES;
}

- (void)rotateControlsWithOrientation:(UIDeviceOrientation)orientation
{
    self.angle = 0.0;
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
            self.angle = M_PI / 2;
            break;
        case UIDeviceOrientationLandscapeRight:
            self.angle = -(M_PI / 2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            self.angle = M_PI;
            break;
        default:
            break;
    }
    [self.cancelButton setTransform:CGAffineTransformMakeRotation(self.angle)];
    for (int i = 0; i < CollectionViewCellCount; i++) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:i
                                               inSection:0];
        CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[self.collectionView cellForItemAtIndexPath:path];
        [cell.label setTransform:CGAffineTransformMakeRotation(self.angle)];
        [cell.imageView setTransform:CGAffineTransformMakeRotation(self.angle)];
    }
}

#pragma mark - Control titles

- (NSString *)titleForTiming:(CameraTiming)timing
{
    NSString *title = @"";
    switch (timing) {
        case CameraTimingOff:
            title = NSLocalizedString(@"Off", nil);
            break;
        case CameraTiming3Sec:
            title = NSLocalizedString(@"3 sec", nil);
            break;
        case CameraTiming5Sec:
            title = NSLocalizedString(@"5 sec", nil);
            break;
        case CameraTiming10Sec:
            title = NSLocalizedString(@"10 sec", nil);
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)titleForFlashState:(CameraFlash)flash
{
    NSString *title = @"";
    switch (flash) {
        case CameraFlashAuto:
            title = NSLocalizedString(@"Auto", nil);
            break;
        case CameraFlashOff:
            title = NSLocalizedString(@"Off", nil);
            break;
        case CameraFlashOn:
            title = NSLocalizedString(@"On", nil);
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)titleForTorchState:(CameraTorch)torch
{
    NSString *title = @"";
    switch (torch) {
        case CameraTorchOff:
            title = NSLocalizedString(@"Off", nil);
            break;
        case CameraTorchOn:
            title = NSLocalizedString(@"On", nil);
            break;
        default:
            break;
    }
    return title;
}

#pragma mark - Settings Controls

- (void)timingHidden:(BOOL)hidden
{
    self.timingHidden = hidden;
    NSIndexPath *path = [NSIndexPath indexPathForRow:CollectionViewCellTimingType
                                           inSection:0];
    CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[self.collectionView cellForItemAtIndexPath:path];
    [cell setHidden:hidden];
}

- (void)setFlashStateEnable:(BOOL)enable
{
    self.flashEnable = enable;
    NSIndexPath *path = [NSIndexPath indexPathForRow:CollectionViewCellFlashType
                                           inSection:0];
    CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[self.collectionView cellForItemAtIndexPath:path];
    cell.imageView.alpha = enable ? 1.0 : 0.5;
    cell.label.textColor = enable ? [UIColor cpDarkGrayColor] : [UIColor cpLightGrayColor];
    cell.userInteractionEnabled = enable;
}

- (void)cameraTypeChangingEnable:(BOOL)enable
{
    self.cameraSwitchEnable = enable;
    NSIndexPath *path = [NSIndexPath indexPathForRow:CollectionViewCellCameraSwitchType
                                           inSection:0];
    CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[self.collectionView cellForItemAtIndexPath:path];
    cell.imageView.alpha = enable ? 1.0 : 0.5;
    cell.userInteractionEnabled = enable;
}

- (void)disableSettingsControls:(BOOL)disable
{
    [self timingHidden:disable];
    if (self.flashEnable) {
        [self setFlashStateEnable:!disable];
    }
    [self cameraTypeChangingEnable:!disable];
}

- (void)freezeSettingsControls:(BOOL)freeze
{
    self.collectionView.userInteractionEnabled = !freeze;
}

- (void)cancelPanelTransparent:(BOOL)transparent
{
    self.cancelPanelView.backgroundColor = transparent ? [UIColor clearColor] : [UIColor whiteColor];
}

- (void)updateTorchState:(CameraTorch)torch
{
    if (self.torchNeeded) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:CollectionViewCellFlashType
                                           inSection:0];
        CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[self.collectionView cellForItemAtIndexPath:path];
        self.torch = torch;
        cell.label.text = [self titleForTorchState:self.torch];
        if ([self.delegate respondsToSelector:@selector(cameraSettingsView:changeTorchMode:)]) {
            [self.delegate cameraSettingsView:self
                              changeTorchMode:self.torch];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return CollectionViewCellCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = NSStringFromClass([CPCameraSettingsViewCell class]);
    CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                                                                           forIndexPath:indexPath];
    [cell.label setTextColor:[UIColor cpLightBlackColor]];
    cell.label.font = [UIFont fontWithName:@"Roboto-Medium"
                                      size:12.0f];
    cell.label.backgroundColor = [UIColor clearColor];
    if (indexPath.row == CollectionViewCellFlashType) {
        cell.imageView.alpha = self.flashEnable ? 1.0 : 0.5;
        cell.label.textColor = self.flashEnable ? [UIColor cpDarkGrayColor] : [UIColor cpLightGrayColor];
        cell.userInteractionEnabled = self.flashEnable;
        if (self.torchNeeded) {
            cell.label.text = [self titleForTorchState:self.torch];
        } else {
            cell.label.text = [self titleForFlashState:self.flash];
        }
        cell.imageView.image = [UIImage imageNamed:@"icon_camera_flash.png"];
    }
    else if (indexPath.row == CollectionViewCellTimingType) {
        [cell setHidden:self.timingHidden];
        cell.label.text = [self titleForTiming:self.timing];
        cell.imageView.image = [UIImage imageNamed:@"icon_camera_timer.png"];
    }
    else if (indexPath.row == CollectionViewCellCameraSwitchType) {
        cell.imageView.image = [UIImage imageNamed:@"icon_camera_flip.png"];
        [cell hideTextLabel];
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CPCameraSettingsViewCell *cell = (CPCameraSettingsViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (indexPath.row == CollectionViewCellFlashType) {
        if (self.torchNeeded) {
            self.torch = (self.torch + 1) % CameraTorchCount;
            cell.label.text = [self titleForTorchState:self.torch];
            if ([self.delegate respondsToSelector:@selector(cameraSettingsView:changeTorchMode:)]) {
                [self.delegate cameraSettingsView:self
                                  changeTorchMode:self.torch];
            }
        } else {
            self.flash = (self.flash + 1) % CameraFlashCount;
            cell.label.text = [self titleForFlashState:self.flash];
            if ([self.delegate respondsToSelector:@selector(cameraSettingsView:changeFlashMode:)]) {
                [self.delegate cameraSettingsView:nil
                                  changeFlashMode:self.flash];
            }
        }
        
    }
    else if (indexPath.row == CollectionViewCellTimingType) {
        self.timing = (self.timing + 1) % CameraTimingCount;
        cell.label.text = [self titleForTiming:self.timing];
        if ([self.delegate respondsToSelector:@selector(cameraSettingsView:changeTiming:)]) {
            [self.delegate cameraSettingsView:self
                                 changeTiming:self.timing];
        }
    }
    else if (indexPath.row == CollectionViewCellCameraSwitchType) {
        self.position = (self.position + 1) % CameraPositionCount;
        if ([self.delegate respondsToSelector:@selector(cameraSettingsView:changeCameraPosition:)]) {
            [self.delegate cameraSettingsView:self
                         changeCameraPosition:self.position];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame) / 3, CGRectGetHeight(collectionView.frame));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

@end
