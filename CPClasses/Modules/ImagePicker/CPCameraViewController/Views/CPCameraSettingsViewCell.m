//
//  CPCameraSettingsViewCell.m
//  Cinepic
//
//  Created by Artem on 4/8/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPCameraSettingsViewCell.h"

@implementation CPCameraSettingsViewCell

- (void)hideTextLabel
{
    [self.label setHidden:YES];
    [self.imageView removeFromSuperview];
    [self addSubview:self.imageView];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *imageViewXPositionConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0f
                                                                                     constant:0.0f];
    imageViewXPositionConstraint.active = YES;
    NSLayoutConstraint *imageViewYPositionConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                                    attribute:NSLayoutAttributeCenterY
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self
                                                                                    attribute:NSLayoutAttributeCenterY
                                                                                   multiplier:1.0f
                                                                                     constant:0.0f];
    imageViewYPositionConstraint.active = YES;
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
}

@end
