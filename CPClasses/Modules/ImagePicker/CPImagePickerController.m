//
//  CPImagePickerController.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPImagePickerController.h"
#import "CPPhotoLibraryController.h"
#import "CPCameraViewController.h"
#import "CPChildControllersProtocol.h"

NSString * const CPImagePickerControllerMediaInfoAssetKey = @"CPImagePickerControllerMediaInfoAssetKey";
NSString * const CPImagePickerControllerMediaInfoImageKey  = @"CPImagePickerControllerMediaInfoImageKey";
NSString * const CPImagePickerControllerMediaInfoVideoPathKey  = @"CPImagePickerControllerMediaInfoVideoPathKey";
NSString * const CPImagePickerControllerMediaInfoOrientationKey  = @"CPImagePickerControllerMediaInfoOrientationKey";

@interface CPImagePickerController ()

@end

@implementation CPImagePickerController

@dynamic delegate;

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.sourceType = CPImagePickerControllerSourceTypePhotoLibrary;
    self.mediaType = CPImagePickerControllerMediaTypeImage;
    self.photoLibraryDoubleTapSelectionEnabled = NO;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationBarHidden = YES;
}

#pragma mark - Setters

- (void)setSourceType:(CPImagePickerControllerSourceType)sourceType {
    
    _sourceType = sourceType;
    
    switch (_sourceType) {
        case CPImagePickerControllerSourceTypePhotoLibrary: {
            CPPhotoLibraryController *photoLibraryController = [[CPPhotoLibraryController alloc] init];
            photoLibraryController.mediaType = _mediaType;
            self.viewControllers = @[photoLibraryController];
        }
            break;
        case CPImagePickerControllerSourceTypeCamera: {
            CPCameraViewController *cameraViewController = [[CPCameraViewController alloc] initWithMediaType:_mediaType];
            self.viewControllers = @[cameraViewController];
        }
        default:
            break;
    }
}

- (void)setMediaType:(CPImagePickerControllerMediaType)mediaType {
    
    _mediaType = mediaType;
    
    [self.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj respondsToSelector:@selector(setMediaType:)]){
            [obj setMediaType:mediaType];
        }
    }];
}

- (void)setSourceTypeIB:(NSUInteger)sourceTypeIB {
    
    self.sourceType = sourceTypeIB;
}

- (NSUInteger)sourceTypeIB {
    
    return self.sourceType;
}

- (void)setMediaTypeIB:(NSUInteger)mediaTypeIB {
    
    self.mediaType = mediaTypeIB;
}

- (NSUInteger)mediaTypeIB {
    
    return self.mediaTypeIB;
}

#pragma mark - Helpers

- (CPPhotoLibraryController *)photoLibraryController {
    
    if (self.sourceType==CPImagePickerControllerSourceTypeCamera)
        return nil;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class == %@", [CPPhotoLibraryController class]];
    CPPhotoLibraryController *photoLibraryController = [[self.viewControllers filteredArrayUsingPredicate:predicate] firstObject];
    return photoLibraryController;
}

- (void)selectPhotoLibraryAssetWithLocalIdentifier:(NSString *)localIdentifier {
    
    CPPhotoLibraryController *photoLibraryController = [self photoLibraryController];
    if (!photoLibraryController)
        return;
    
    [photoLibraryController selectAssetWithLocalIdentifier:localIdentifier];
}

- (UIImage *)photoLibraryThumbnailForLocation:(CGPoint)location {
    
    CPPhotoLibraryController *photoLibraryController = [self photoLibraryController];
    if (!photoLibraryController)
        return nil;
    
    return [photoLibraryController thumbnailForLocation:location];
}

- (CGRect)photoLibraryThumbnailRectForLocation:(CGPoint)location {
    
    CPPhotoLibraryController *photoLibraryController = [self photoLibraryController];
    if (!photoLibraryController)
        return CGRectNull;
    
    return [photoLibraryController thumbnailRectForLocation:location];
}

- (PHAsset *)photoLibraryAssetForLocation:(CGPoint)location {
    
    CPPhotoLibraryController *photoLibraryController = [self photoLibraryController];
    if (!photoLibraryController)
        return nil;
    
    return [photoLibraryController assetForLocation:location];
}

- (PHAuthorizationStatus)photoLibraryAuthorizationStatus {
    
    CPPhotoLibraryController *photoLibraryController = [self photoLibraryController];
    if (!photoLibraryController)
        return PHAuthorizationStatusNotDetermined;
    
    return photoLibraryController.authorizationStatus;
}

#pragma mark - CPChildControllersProtocol

- (UIScrollView *)cpScrollView {
    
    CPPhotoLibraryController *photoLibraryController = [self photoLibraryController];
    if (!photoLibraryController)
        return nil;
    
    return photoLibraryController.scrollView;
}

@end
