//
//  CPPreviewCollectionViewCell.h
//  Cinepic
//
//  Created by Artem on 4/9/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPPreviewCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
