//
//  CPPreviewCollectionViewCell.m
//  Cinepic
//
//  Created by Artem on 4/9/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPPreviewCollectionViewCell.h"

@implementation CPPreviewCollectionViewCell

- (void)awakeFromNib
{
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.75f;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 6.0f;
}

@end
