//
//  CPCameraPreviewController.h
//  Cinepic
//
//  Created by Artem on 4/17/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPImagePickerController.h"

@interface CPCameraPreviewController : UIViewController

@property (nonatomic) CPImagePickerControllerMediaType mediaType;

- (instancetype)initWithMediaInfo:(NSDictionary *)mediaInfo mediaType:(CPImagePickerControllerMediaType)mediaType;

@end
