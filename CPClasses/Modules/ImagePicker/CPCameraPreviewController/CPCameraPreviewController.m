//
//  CPCameraPreviewController.m
//  Cinepic
//
//  Created by Artem on 4/17/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPCameraPreviewController.h"
#import "CPPreviewCollectionViewCell.h"
#import "VideoPlayerView.h"
#import "CPImagePickerController.h"
#import "UIColor+CP.h"

typedef NS_ENUM(NSUInteger, CollectionViewCellType) {
    CollectionViewCellTypeCancel,
    CollectionViewCellTypeConfirm,
    CollectionViewCellTypeCount,
};

#define IS_IPHONE4 ([[UIScreen mainScreen] bounds].size.height == 480.0f)

@interface CPCameraPreviewController ()
<
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout
>

@property (nonatomic) VideoPlayerView *videoPlayerView;

// Storyboard
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *previewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *preview;

// Content
@property (nonatomic) NSMutableDictionary *mediaInfo;
@property (nonatomic) NSString *videoPath;
@property (nonatomic) UIImage *image;

@end

const CGFloat kCollectionViewInteritemSpacing = 0.5f;

@implementation CPCameraPreviewController

- (instancetype)initWithMediaInfo:(NSDictionary *)mediaInfo mediaType:(CPImagePickerControllerMediaType)mediaType
{
    self = [self initWithNibName:NSStringFromClass([self class]) bundle:nil];
    if (self) {
        self.mediaInfo = [NSMutableDictionary dictionaryWithDictionary:mediaInfo];
        self.mediaType = mediaType;
        switch (self.mediaType) {
            case CPImagePickerControllerMediaTypeImage:{
                self.image = [self imageRotatedUpFromMediaInfo:mediaInfo];
                self.mediaInfo[CPImagePickerControllerMediaInfoImageKey] = self.image;
            }
                break;
            case CPImagePickerControllerMediaTypeVideo:{
                self.videoPath = mediaInfo[CPImagePickerControllerMediaInfoVideoPathKey];
            }
                break;
            default:
                break;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    NSString *cellIdentifier = NSStringFromClass([CPPreviewCollectionViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.backgroundColor = [UIColor cpLightGrayDividerColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    if (self.videoPath) {
        [self updatePreviewHeightConstraintForVideo];
        self.collectionView.alpha = 0.7;
        self.videoPlayerView = [VideoPlayerView new];
        self.videoPlayerView.muted = NO;
        [self.preview addSubview:self.videoPlayerView];
        self.videoPlayerView.videoURL = [NSURL fileURLWithPath:self.videoPath];
    }
    if (self.image) {
        self.imageView.hidden = NO;
        self.imageView.image = self.image;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startPlayVideo)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [self startPlayVideo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect frame = self.preview.frame;
    if (IS_IPHONE4) {
        CGFloat videoAspectRatio = 16.0 / 9.0;
        CGFloat height = CGRectGetWidth(self.preview.frame) * videoAspectRatio;
        CGFloat yOffset = (height - CGRectGetHeight(self.preview.frame)) / 2;
        frame = CGRectMake(0, -yOffset, CGRectGetWidth(self.preview.frame), height);
    }
    self.videoPlayerView.frame = frame;
}

#pragma mark - Rotations

- (UIImage *)imageRotatedUpFromMediaInfo:(NSDictionary *)mediaInfo
{
    UIImage *image = mediaInfo[CPImagePickerControllerMediaInfoImageKey];
    UIDeviceOrientation deviceOrientation = [mediaInfo[CPImagePickerControllerMediaInfoOrientationKey] integerValue];
    UIImageOrientation rotationOrientation = [self rotationNeededForImageCapturedWithDeviceOrientation:deviceOrientation];
    return [[UIImage alloc] initWithCGImage:image.CGImage
                                      scale:1.0
                                orientation:rotationOrientation];
}

- (UIImageOrientation)rotationNeededForImageCapturedWithDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
    UIImageOrientation rotationOrientation;
    switch (deviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown: {
            rotationOrientation = UIImageOrientationLeft;
        } break;
            
        case UIDeviceOrientationLandscapeRight: {
            rotationOrientation = UIImageOrientationDown;
        } break;
            
        case UIDeviceOrientationLandscapeLeft: {
            rotationOrientation = UIImageOrientationUp;
        } break;
            
        case UIDeviceOrientationPortrait:
        default: {
            rotationOrientation = UIImageOrientationRight;
        } break;
    }
    return rotationOrientation;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return CollectionViewCellTypeCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = NSStringFromClass([CPPreviewCollectionViewCell class]);
    CPPreviewCollectionViewCell *cell = (CPPreviewCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                                                                                 forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    if (indexPath.row == CollectionViewCellTypeCancel) {
        cell.imageView.image = [UIImage imageNamed:@"icon_camera_cross.png"];
    }
    else if (indexPath.row == CollectionViewCellTypeConfirm) {
        cell.imageView.image = [UIImage imageNamed:@"icon_camera_check.png"];
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == CollectionViewCellTypeCancel) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (indexPath.row == CollectionViewCellTypeConfirm) {
        CPImagePickerController *imagePickerController = (id)self.navigationController;
        if ([imagePickerController.delegate respondsToSelector:@selector(cpImagePickerController:didSelectContentWithMediaInfo:)]) {
            [imagePickerController.delegate cpImagePickerController:imagePickerController didSelectContentWithMediaInfo:self.mediaInfo];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.bounds) / CollectionViewCellTypeCount - kCollectionViewInteritemSpacing, CGRectGetHeight(collectionView.bounds));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return kCollectionViewInteritemSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return kCollectionViewInteritemSpacing;
}

#pragma mark - Helpers

- (void)updatePreviewHeightConstraintForVideo
{
    self.previewHeightConstraint.active = NO;
    self.previewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.preview
                                                                attribute:NSLayoutAttributeHeight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.view
                                                                attribute:NSLayoutAttributeHeight
                                                               multiplier:1.0f
                                                                 constant:0.0f];
    self.previewHeightConstraint.active = YES;
}

- (void)startPlayVideo
{
    if (self.videoPath){
        [self.videoPlayerView prepareToPlay];
    }
}

@end
