//
//  CPImagePickerController.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPScrollDelegate.h"
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

typedef enum : NSUInteger {
    CPImagePickerControllerSourceTypePhotoLibrary,
    CPImagePickerControllerSourceTypeCamera,
} CPImagePickerControllerSourceType;

typedef enum : NSUInteger {
    CPImagePickerControllerMediaTypeImage,
    CPImagePickerControllerMediaTypeVideo,
    CPImagePickerControllerMediaTypeAll,
} CPImagePickerControllerMediaType;

extern NSString * const CPImagePickerControllerMediaInfoAssetKey;
extern NSString * const CPImagePickerControllerMediaInfoImageKey;
extern NSString * const CPImagePickerControllerMediaInfoVideoPathKey;
extern NSString * const CPImagePickerControllerMediaInfoOrientationKey;

@class CPImagePickerController;

@protocol CPImagePickerControllerDataSource <NSObject>

@optional
- (CGFloat)cpImagePickerControllerCameraVideoMinDuration:(CPImagePickerController*)imagePickerController;

@end

@protocol CPImagePickerControllerDelegate <UINavigationControllerDelegate>

@optional
- (void)cpImagePickerController:(CPImagePickerController*)imagePickerController didSelectContentWithMediaInfo:(NSDictionary *)mediaInfo;
- (void)cpImagePickerController:(CPImagePickerController*)imagePickerController photoLibraryAuthorizationStatusChanged:(PHAuthorizationStatus)status;
- (void)cpImagePickerControllerAddContentFromCameraButtonPressed:(CPImagePickerController*)imagePickerController;
- (void)cpImagePickerControllerDidCancel:(CPImagePickerController*)imagePickerController;

@end

@interface CPImagePickerController : UINavigationController

@property (weak, nonatomic) id<CPImagePickerControllerDataSource> datasource;
@property (weak, nonatomic) id<CPImagePickerControllerDelegate> delegate;
@property (weak, nonatomic) id<CPScrollDelegate> scrollDelegate;
@property (nonatomic) CPImagePickerControllerSourceType sourceType;
@property (nonatomic) CPImagePickerControllerMediaType mediaType;
@property (nonatomic) IBInspectable NSUInteger sourceTypeIB;
@property (nonatomic) IBInspectable NSUInteger mediaTypeIB;
@property (nonatomic) IBInspectable BOOL photoLibraryDoubleTapSelectionEnabled;

- (void)selectPhotoLibraryAssetWithLocalIdentifier:(NSString *)localIdentifier;
- (UIImage*)photoLibraryThumbnailForLocation:(CGPoint)location;
- (CGRect)photoLibraryThumbnailRectForLocation:(CGPoint)location;
- (PHAsset *)photoLibraryAssetForLocation:(CGPoint)location;
- (PHAuthorizationStatus)photoLibraryAuthorizationStatus;

@end

