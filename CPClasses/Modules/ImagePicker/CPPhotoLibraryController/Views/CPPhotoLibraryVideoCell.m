//
//  CPPhotoLibraryVideoCell.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPPhotoLibraryVideoCell.h"
#import "CPGradientView.h"
#import "UIColor+CP.h"
#import <Framer.h>

@interface CPPhotoLibraryVideoCell ()

@property (nonatomic) CPGradientView *shadowView;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *label;
@property (nonatomic) UIView *selectedView;

@end

@implementation CPPhotoLibraryVideoCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = self.backgroundColor;
        
        self.imageView = [[UIImageView alloc] init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        [self.contentView addSubview:self.imageView];
        
        self.shadowView = [[CPGradientView alloc] init];
        self.shadowView.gradientLayer.startPoint = CGPointMake(0.5, 1.0);
        self.shadowView.gradientLayer.endPoint = CGPointMake(0.5, 0.0);
        self.shadowView.gradientLayer.colors = [NSArray arrayWithObjects:
                                                (id)[[UIColor colorWithWhite:0.0 alpha:0.3f] CGColor],
                                                (id)[[UIColor clearColor] CGColor], nil];
        [self.contentView addSubview:self.shadowView];
        
        self.label = [[UILabel alloc] init];
        self.label.textColor = [UIColor whiteColor];
        self.label.font = [UIFont systemFontOfSize:17.0];
        self.label.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.label];
        
        self.selectedView = [[UIView alloc] init];
        [self.contentView addSubview:self.selectedView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.imageView installFrames:^(NUIFramer * _Nonnull framer) {
        framer.edges(UIEdgeInsetsZero);
    }];
    [self.label installFrames:^(NUIFramer * _Nonnull framer) {
        framer.sizeThatFits((CGSize){
            .width = CGRectGetWidth(self.contentView.frame) - 10.0,
            .height = CGRectGetHeight(self.contentView.frame) - 4.0,
        });
        framer.left(5.0);
        framer.right(5.0);
        framer.bottom(2.0);
    }];
    [self.shadowView installFrames:^(NUIFramer * _Nonnull framer) {
        framer.top_to(self.label.nui_top, -5.0);
        framer.left(0.0);
        framer.right(0.0);
        framer.bottom(0.0);
    }];
    [self.selectedView installFrames:^(NUIFramer * _Nonnull framer) {
        framer.edges(UIEdgeInsetsZero);
    }];
}

#pragma mark - Setters

- (void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    if (self.allowsSelection) {
        if (selected) {
            self.selectedView.layer.borderWidth = 3.0f;
            self.selectedView.layer.borderColor = [UIColor cpRedColor].CGColor;
        }
        else {
            self.selectedView.layer.borderWidth = 0.0f;
        }
    }
}

@end
