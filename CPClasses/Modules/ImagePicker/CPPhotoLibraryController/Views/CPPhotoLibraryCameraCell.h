//
//  CPPhotoLibraryCameraCell.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 12/10/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPPhotoLibraryCameraCell;

@protocol CPPhotoLibraryCameraCellDelegate <NSObject>

@optional
- (void)cpPhotoLibraryCameraCellSelected:(CPPhotoLibraryCameraCell *)cell;

@end

@interface CPPhotoLibraryCameraCell : UICollectionViewCell

@property (weak, nonatomic) id <CPPhotoLibraryCameraCellDelegate> delegate;
@property (nonatomic) IBOutlet UIImageView *imageView;

@end
