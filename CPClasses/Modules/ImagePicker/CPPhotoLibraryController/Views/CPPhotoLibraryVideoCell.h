//
//  CPPhotoLibraryVideoCell.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPPhotoLibraryVideoCell : UICollectionViewCell

@property (nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, readonly) UILabel *label;
@property (nonatomic, readonly) UIView *selectedView;
@property (nonatomic) BOOL allowsSelection;

@end
