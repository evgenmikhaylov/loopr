//
//  CPPhotoLibraryPhotoCell.m
//  Faded
//
//  Created by EvgenyMikhaylov on 4/17/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "CPPhotoLibraryPhotoCell.h"
#import "UIColor+CP.h"

@interface CPPhotoLibraryPhotoCell ()

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIView *selectedView;

@end

@implementation CPPhotoLibraryPhotoCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = self.backgroundColor;
        
        self.imageView = [[UIImageView alloc] init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        [self.contentView addSubview:self.imageView];
        
        self.selectedView = [[UIView alloc] init];
        [self.contentView addSubview:self.selectedView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.selectedView.frame = self.bounds;
    self.imageView.frame = self.bounds;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (self.allowsSelection) {
        if (selected) {
            self.selectedView.layer.borderWidth = 3.0f;
            self.selectedView.layer.borderColor = [UIColor cpRedColor].CGColor;
        }
        else {
            self.selectedView.layer.borderWidth = 0.0f;
        }
    }
}

@end
