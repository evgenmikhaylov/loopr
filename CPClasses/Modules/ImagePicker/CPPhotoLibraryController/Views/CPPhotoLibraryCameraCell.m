//
//  CPPhotoLibraryCameraCell.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 12/10/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "CPPhotoLibraryCameraCell.h"

@implementation CPPhotoLibraryCameraCell

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

#pragma mark - Setters

- (void)setHighlighted:(BOOL)highlighted {
    
    [super setHighlighted:highlighted];
    self.alpha = highlighted ? 0.5f : 1.0f;
}

#pragma mark - Actions

- (void)tapAction:(UIGestureRecognizer *)sender {
    
    if ([self.delegate respondsToSelector:@selector(cpPhotoLibraryCameraCellSelected:)]) {
        [self.delegate cpPhotoLibraryCameraCellSelected:self];
    }
}

@end
