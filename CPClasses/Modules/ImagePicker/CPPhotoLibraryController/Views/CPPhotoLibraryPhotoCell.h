//
//  CPPhotoLibraryPhotoCell.h
//  Faded
//
//  Created by EvgenyMikhaylov on 4/17/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPPhotoLibraryPhotoCell : UICollectionViewCell

@property (nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, readonly) UIView *selectedView;
@property (nonatomic) BOOL allowsSelection;

@end
