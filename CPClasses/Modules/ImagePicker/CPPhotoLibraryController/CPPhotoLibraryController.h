//
//  CPPhotoLibraryController.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPImagePickerController.h"

@interface CPPhotoLibraryController : UIViewController

@property (nonatomic) CPImagePickerControllerMediaType mediaType;

- (void)selectAssetWithLocalIdentifier:(NSString *)localIdentifier;
- (UIImage *)thumbnailForLocation:(CGPoint)location;
- (CGRect)thumbnailRectForLocation:(CGPoint)location;
- (PHAsset *)assetForLocation:(CGPoint)location;
- (UIScrollView*)scrollView;
- (PHAuthorizationStatus)authorizationStatus;

@end
