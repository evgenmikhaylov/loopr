//
//  CPPhotoLibraryController.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 3/2/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "CPPhotoLibraryController.h"
#import "CPPhotoLibraryCameraCell.h"
#import "CPPhotoLibraryPhotoCell.h"
#import "CPPhotoLibraryVideoCell.h"

#import "UIViewController+Additions.h"
#import "PHAsset+Additions.h"
#import "NSString+Time.h"
#import "UIView+Frame.h"

#import <Photos/Photos.h>

@interface CPPhotoLibraryController ()
<
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
PHPhotoLibraryChangeObserver,
CPPhotoLibraryCameraCellDelegate
>

@property (nonatomic) PHFetchResult *assetsFetchResults;
@property (nonatomic) PHAssetMediaType assetMediaType;
@property (nonatomic) PHAuthorizationStatus authorizationStatus;

@property (nonatomic) NSString *selectedAssetLocalIdentifier;
@property (nonatomic) BOOL performingCollectionViewUpdates;

@property (nonatomic) UIView *navigationBarView;
@property (nonatomic) UIButton *closeButton;
@property (nonatomic) UICollectionView *collectionView;

@end

static NSUInteger const CPNumberOfCellsInRow = 4;
static CGFloat const CPInteritemSpacing = 4.0f;
static CGFloat const CPEdgeInset = 10.0f;
static CGFloat const CPCellShadowRadius = 2.0f;

@implementation CPPhotoLibraryController

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.authorizationStatus = [PHPhotoLibrary authorizationStatus];
    self.mediaType = CPImagePickerControllerMediaTypeImage;
    self.performingCollectionViewUpdates = NO;
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
}

- (void)dealloc {
    
    [[PHPhotoLibrary sharedPhotoLibrary] unregisterChangeObserver:self];
}

#pragma mark - Appearance

- (void)viewDidLoad {
    
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationBarView = [[UIView alloc] init];
    self.navigationBarView.backgroundColor = [UIColor colorWithRed:(250.0/255.0) green:(250.0/255.0) blue:(250.0/255.0) alpha:0.9];
    [self.view addSubview:self.navigationBarView];
    
    self.closeButton = [[UIButton alloc] init];
    [self.closeButton setImage:[UIImage imageNamed:@"iconSearchCancel"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBarView addSubview:self.closeButton];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerClass:[CPPhotoLibraryPhotoCell class]
            forCellWithReuseIdentifier:NSStringFromClass([CPPhotoLibraryPhotoCell class])];
    [self.collectionView registerClass:[CPPhotoLibraryVideoCell class]
            forCellWithReuseIdentifier:NSStringFromClass([CPPhotoLibraryVideoCell class])];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CPPhotoLibraryCameraCell class]) bundle:nil]
          forCellWithReuseIdentifier:NSStringFromClass([CPPhotoLibraryCameraCell class])];
    [self.collectionView.panGestureRecognizer addTarget:self action:@selector(panGestureAction:)];
    [self.view addSubview:self.collectionView];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGestureAction:)];
    tapGestureRecognizer.numberOfTapsRequired = 2;
    [self.collectionView addGestureRecognizer:tapGestureRecognizer];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.navigationBarView.left = 0.0;
    self.navigationBarView.rightForFixedLeft = self.view.width;
    self.navigationBarView.top = 0.0;
    self.navigationBarView.height = 44.0;

    self.closeButton.left = 1.0;
    self.closeButton.width = 45.0;
    self.closeButton.centerY = self.navigationBarView.height / 2.0;
    self.closeButton.height = 45.0;

    self.collectionView.left = 0.0;
    self.collectionView.rightForFixedLeft = self.view.width;
    self.collectionView.top = self.navigationBarView.bottom;
    self.collectionView.bottomForFixedTop = self.view.height;
}

#pragma mark - Setters/Getters

- (void)setMediaType:(CPImagePickerControllerMediaType)mediaType {
    
    _mediaType = mediaType;
    
    self.assetsFetchResults = nil;
    
    [self.collectionView reloadData];
    [self.collectionView setContentOffset:CGPointZero animated:NO];
    self.collectionView.scrollEnabled = NO;
    self.collectionView.scrollEnabled = YES;
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    self.assetsFetchResults = [PHAsset fetchAssetsWithMediaType:self.assetMediaType options:fetchOptions];
    
    [self.collectionView reloadData];
}

- (PHAssetMediaType)assetMediaType {
    
    switch (_mediaType) {
        case CPImagePickerControllerMediaTypeImage:     return PHAssetMediaTypeImage;        break;
        case CPImagePickerControllerMediaTypeVideo:     return PHAssetMediaTypeVideo;        break;
        case CPImagePickerControllerMediaTypeAll:       return PHAssetMediaTypeUnknown;      break;
    }
}

- (UIScrollView*)scrollView {
    
    return self.collectionView;
}

#pragma mark - Actions

- (void)panGestureAction:(UIGestureRecognizer *)sender {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.scrollDelegate respondsToSelector:@selector(cpScroll:panGesture:)]){
        [imagePickerController.scrollDelegate cpScroll:self.collectionView panGesture:sender];
    }
}

- (void)doubleTapGestureAction:(UIGestureRecognizer *)sender {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if (imagePickerController.photoLibraryDoubleTapSelectionEnabled) {
        CGPoint location = [sender locationInView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
        if (indexPath) {
            [self selectAssetAtIndexPath:indexPath];
        }        
    }
}

- (void)closeButtonPressed:(id)sender {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.delegate respondsToSelector:@selector(cpImagePickerControllerDidCancel:)]) {
        [imagePickerController.delegate cpImagePickerControllerDidCancel:imagePickerController];
    }
}

#pragma mark - Items

- (NSUInteger)itemsCount {
    
    return self.assetsFetchResults.count + 1;
}

- (PHAsset *)assetAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item == 0) {
        return nil;
    }
    return self.assetsFetchResults[indexPath.item - 1];
}

- (NSArray *)itemsIndexPathsFromAssetsIndexes:(NSIndexSet*)indexes {
    
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:indexes.count];
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:idx+1 inSection:0]];
    }];
    return indexPaths;
}

#pragma mark - Helpers

- (void)selectAssetWithLocalIdentifier:(NSString *)localIdentifier {
    
    self.selectedAssetLocalIdentifier = localIdentifier;
    [self.collectionView reloadData];
}

- (NSIndexPath *)indexPathForLocation:(CGPoint)location {
    
    CGPoint newLocation = (CGPoint){
        .x = location.x+self.collectionView.contentOffset.x,
        .y = location.y+self.collectionView.contentOffset.y,
    };
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:newLocation];
    return indexPath;
}

- (UIImage *)thumbnailForLocation:(CGPoint)location {
    
    NSIndexPath *indexPath = [self indexPathForLocation:location];
    if (!indexPath){
        return nil;
    }
    PHAsset *asset = [self assetAtIndexPath:indexPath];
    if (asset == nil) {
        return nil;
    }
    id cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    if ([cell respondsToSelector:@selector(imageView)]){
        UIImageView *imageView = [cell imageView];
        return imageView.image;
    }
    return nil;
}

- (CGRect)thumbnailRectForLocation:(CGPoint)location {
    
    NSIndexPath *indexPath = [self indexPathForLocation:location];
    if (!indexPath) {
        return CGRectNull;
    }
    PHAsset *asset = [self assetAtIndexPath:indexPath];
    if (asset == nil) {
        return CGRectNull;
    }
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect rect = (CGRect){
        .origin.x = CGRectGetMinX(attributes.frame)-self.collectionView.contentOffset.x,
        .origin.y = CGRectGetMinY(attributes.frame)-self.collectionView.contentOffset.y,
        .size = attributes.frame.size,
    };
    return rect;
}

- (PHAsset *)assetForLocation:(CGPoint)location {
    
    NSIndexPath *indexPath = [self indexPathForLocation:location];
    if (!indexPath)
        return nil;
    
    PHAsset *asset = [self assetAtIndexPath:indexPath];
    return asset;
}

- (void)selectAssetAtIndexPath:(NSIndexPath *)indexPath {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.delegate respondsToSelector:@selector(cpImagePickerController:didSelectContentWithMediaInfo:)]){
        PHAsset *asset = [self assetAtIndexPath:indexPath];
        NSDictionary *mediaInfo = @{CPImagePickerControllerMediaInfoAssetKey:asset};
        [imagePickerController.delegate cpImagePickerController:imagePickerController didSelectContentWithMediaInfo:mediaInfo];
    }
}

- (CGSize)cellSize {
    
    CGFloat width = (CGRectGetWidth(self.collectionView.frame)-2*CPEdgeInset-(CPNumberOfCellsInRow-1)*CPInteritemSpacing)/CPNumberOfCellsInRow;
    return CGSizeMake(width, width);
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [self itemsCount];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    PHAsset *asset = [self assetAtIndexPath:indexPath];
    if (asset == nil) {
        CPPhotoLibraryCameraCell *cell = [cv dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CPPhotoLibraryCameraCell class])
                                                                       forIndexPath:indexPath];
        cell.delegate = self;
        cell.layer.shadowOpacity = 0.3f;
        cell.layer.shadowRadius = CPCellShadowRadius;
        cell.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        cell.layer.shadowColor = [UIColor blackColor].CGColor;
        cell.clipsToBounds = NO;
        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.layer.bounds].CGPath;
        
        if (self.mediaType == CPImagePickerControllerMediaTypeImage) {
            cell.imageView.image = [UIImage imageNamed:@"icon_add_photo"];
        }
        else if (self.mediaType == CPImagePickerControllerMediaTypeVideo) {
            cell.imageView.image = [UIImage imageNamed:@"icon_add_video"];
        }
        else {
            cell.imageView.image = nil;
        }
        
        return cell;
    }
    else if (asset.mediaType == PHAssetMediaTypeVideo) {
        CPPhotoLibraryVideoCell *cell = [cv dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CPPhotoLibraryVideoCell class])
                                                                      forIndexPath:indexPath];
        cell.allowsSelection = !imagePickerController.photoLibraryDoubleTapSelectionEnabled;
        cell.label.font = [UIFont fontWithName:@"Roboto-Regular" size:11.0f];
        cell.label.text = [NSString stringWithTimeInterval:asset.duration];
        if ([asset.localIdentifier isEqualToString:self.selectedAssetLocalIdentifier]) {
            cell.selected = YES;
            [cv selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        }
        cell.tag = indexPath.row;
        cell.imageView.image = nil;
        CGFloat scale = [UIScreen mainScreen].scale;
        CGSize size = CGSizeApplyAffineTransform(CGSizeMake(60, 60), CGAffineTransformMakeScale(scale, scale));
        [asset requestImageWithSize:size contentMode:PHImageContentModeAspectFill resultHandler:^(UIImage *result) {
            if (cell.tag==indexPath.row){
                cell.imageView.image = result;
            }
        }];
        return cell;
    }
    else if (asset.mediaType == PHAssetMediaTypeImage){
        CPPhotoLibraryPhotoCell *cell = [cv dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CPPhotoLibraryPhotoCell class])
                                                                      forIndexPath:indexPath];
        cell.allowsSelection = !imagePickerController.photoLibraryDoubleTapSelectionEnabled;
        if ([asset.localIdentifier isEqualToString:self.selectedAssetLocalIdentifier]) {
            cell.selected = YES;
            [cv selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        }
        cell.tag = indexPath.row;
        cell.imageView.image = nil;
        CGFloat scale = [UIScreen mainScreen].scale;
        CGSize size = CGSizeApplyAffineTransform(CGSizeMake(60, 60), CGAffineTransformMakeScale(scale, scale));
        [asset requestImageWithSize:size contentMode:PHImageContentModeAspectFill resultHandler:^(UIImage *result) {
            if (cell.tag==indexPath.row){
                cell.imageView.image = result;
            }
        }];        
        return cell;
    }
    else {
        return nil;
    }
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PHAsset *asset = [self assetAtIndexPath:indexPath];
    if (asset == nil) {
        return NO;
    }
    
    NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
    return ![indexPath isEqual:selectedIndexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PHAsset *asset = [self assetAtIndexPath:indexPath];
    self.selectedAssetLocalIdentifier = asset.localIdentifier;
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if (!imagePickerController.photoLibraryDoubleTapSelectionEnabled) {
        [self selectAssetAtIndexPath:indexPath];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self cellSize];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(CPCellShadowRadius, CPEdgeInset, CPEdgeInset, CPEdgeInset);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return CPInteritemSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return CPInteritemSpacing;
}

#pragma mark - CPPhotoLibraryCameraCellDelegate

- (void)cpPhotoLibraryCameraCellSelected:(CPPhotoLibraryCameraCell *)cell {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.delegate respondsToSelector:@selector(cpImagePickerControllerAddContentFromCameraButtonPressed:)]) {
        [imagePickerController.delegate cpImagePickerControllerAddContentFromCameraButtonPressed:imagePickerController];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.scrollDelegate respondsToSelector:@selector(cpWillBeginDragging:)]){
        [imagePickerController.scrollDelegate cpWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.scrollDelegate respondsToSelector:@selector(cpDidScroll:)]){
        [imagePickerController.scrollDelegate cpDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.scrollDelegate respondsToSelector:@selector(cpDidEndDragging:willDecelerate:)]){
        [imagePickerController.scrollDelegate cpDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CPImagePickerController *imagePickerController = (id)self.navigationController;
    if ([imagePickerController.scrollDelegate respondsToSelector:@selector(cpDidEndDecelerating:)]){
        [imagePickerController.scrollDelegate cpDidEndDecelerating:scrollView];
    }
}

#pragma mark - PHPhotoLibraryChangeObserver

- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        PHAuthorizationStatus authorizationStatus = [PHPhotoLibrary authorizationStatus];
        if (authorizationStatus != self.authorizationStatus){
            self.authorizationStatus = authorizationStatus;
            CPImagePickerController *imagePickerController = (id)self.navigationController;
            if ([imagePickerController.delegate respondsToSelector:@selector(cpImagePickerController:photoLibraryAuthorizationStatusChanged:)]){
                [imagePickerController.delegate cpImagePickerController:imagePickerController photoLibraryAuthorizationStatusChanged:self.authorizationStatus];
            }
        }
        PHFetchResultChangeDetails *collectionChanges = [changeInstance changeDetailsForFetchResult:self.assetsFetchResults];
        if (collectionChanges) {
            self.assetsFetchResults = [collectionChanges fetchResultAfterChanges];
            if (![collectionChanges hasIncrementalChanges] || [collectionChanges hasMoves] || ![self isVisible]){
                [self.collectionView reloadData];
            }
            else{
                if (!self.performingCollectionViewUpdates){
                    self.performingCollectionViewUpdates = YES;
                    [self.collectionView performBatchUpdates:^{
                        NSIndexSet *removedIndexes = [collectionChanges removedIndexes];
                        if ([removedIndexes count]) {
                            [self.collectionView deleteItemsAtIndexPaths:[self itemsIndexPathsFromAssetsIndexes:removedIndexes]];
                        }
                        NSIndexSet *insertedIndexes = [collectionChanges insertedIndexes];
                        if ([insertedIndexes count]) {
                            [self.collectionView insertItemsAtIndexPaths:[self itemsIndexPathsFromAssetsIndexes:insertedIndexes]];
                        }
                        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
                    } completion:^(BOOL finished) {
                        self.performingCollectionViewUpdates = NO;
                    }];
                }
            }
        }
    });
}

@end
