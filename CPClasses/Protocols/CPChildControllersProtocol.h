//
//  CPChildControllersProtocol.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 5/5/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CPChildControllersProtocol <NSObject>

@optional
- (void)cpClean;
- (UIScrollView*)cpScrollView;

@end
