//
//  CPScrollDelegate.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 7/30/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CPScrollDelegate <NSObject>

@optional
- (void)cpWillBeginDragging:(UIScrollView *)scrollView;
- (void)cpDidScroll:(UIScrollView *)scrollView;
- (void)cpDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)cpDidEndDecelerating:(UIScrollView *)scrollView;
- (void)cpScroll:(UIScrollView *)scrollView panGesture:(UIGestureRecognizer*)gestureRecognizer;

@end
